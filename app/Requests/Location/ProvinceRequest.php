<?php

namespace Canela\CanelaTools\Requests\Location;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class ProvinceRequest
 * @package Canela\CanelaTools\Requests\Location
 */
class ProvinceRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'country_id' => 'required|integer|exists:country,id',
            'name'       => 'required|string|min:1|unique:province,name',
            'code'       => 'required|max:2|unique:province,code',
        );

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id = $this->route('province');

            $rules['name'] .= ",$id";
            $rules['code'] .= ",$id";
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'country_id' => trans('canelatools::canelatools.entity.province.field.country_id'),
            'name'       => trans('canelatools::canelatools.entity.province.field.name'),
            'code'       => trans('canelatools::canelatools.entity.province.field.code'),
        ];
    }
}
