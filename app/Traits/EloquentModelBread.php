<?php

namespace Canela\CanelaTools\Traits;


use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\HistoryLog;

trait EloquentModelBread
{
    /*________________________________
    |
    |       BREAD functions
    |__________________________________*/

    /**
     * @see Model::save()
     *
     * @param integer $actionId
     * @param integer $moduleId
     * @param integer $userId
     * @param null    $registroPre
     * @param bool    $saveValues
     * @param string  $comment
     * @param array   $options
     * @return bool
     * @throws \Exception
     */
    public function commit($actionId, $moduleId, $userId, $registroPre = null, $saveValues = true, $comment = null, array $options = [])
    {
        try {
            \DB::beginTransaction();
            // Historico
            HistoryLog::log(self::getTableName(), $moduleId, $actionId, $userId, $this, $registroPre, $comment, $saveValues);
            $result = parent::save($options);
            \DB::commit();

            return $result;
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
            throw $e;
        }

    }

    /**
     * @see Model::delete()
     *
     * @param integer $moduleId
     * @param integer $userId
     * @return bool|null
     *
     * @throws \Exception
     */
    public function erase($moduleId, $userId)
    {
        try {
            \DB::beginTransaction();
            // Historico
            HistoryLog::log(self::getTableName(), $moduleId, Action::DELETE, $userId, null, $this);
            $result = parent::delete();
            \DB::commit();

            return $result;

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * @see Model::forceDelete()
     *
     * @param integer $moduleId
     * @param integer $userId
     * @return bool|null
     *
     * @throws \Exception
     */
    public function forceErase($moduleId, $userId)
    {
        try {
            \DB::beginTransaction();
            // Historico
            HistoryLog::log(self::getTableName(), $moduleId, Action::DELETE, $userId, null, $this);
            $result = parent::forceDelete();
            \DB::commit();

            return $result;

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
            throw $e;
        }
    }
}