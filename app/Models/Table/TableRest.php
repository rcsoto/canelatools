<?php

namespace Canela\CanelaTools\Models\Table;


use Canela\CanelaTools\Enums\TableColumnTypes;
use App\Models\General\ArchiveType;
use Canela\CanelaTools\Manager\UtilsBasicManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\Pure;

class TableRest
{
    /** @var bool $showBtnNew Indica si se muestra el boton de nuevo */
    public $showBtnNew  = true;

    /** @var bool $showBtnEdit Indica si se puede entiar el formulario */
    public $showBtnEdit  = true;

    /** @var string $routeEntity Indica la ruta de la entidad */
    public $routeEntity;

    /** @var string $formTitle Título del formulario */
    public $formTitle;

    /** @var string $tableName ID de la tabla
     */
    public $tableNameId = "advanced-table";

    /** @var string $title Titulo de la página */
    public $title;

    /** @var string $urlAdd Indica el campo que se usa para la ordenación */
    public $urlAdd;

    /** @var string $urlIndex Indica unr Index */
    public $urlIndex;

    /** @var string $urlStore Indica url Store */
    public $urlStore;

    /** @var string $urlUpdate Indica url update */
    public $urlUpdate;

    /** @var string $urlListAjax Indica url para cargar informacion en la lista */
    public $urlListAjax;

    /** @var array $tableForm Informacion de formulario */
    public $tableForm;

    /** @var array $tableColumn Informacion de la lista */
    public $tableColumn;

    /** @var string $jsDynamicTable Indica el archivo JS que hay que usar para la tabla dinamica */
    public $jsDynamicTable = "canelatools::rest.js-dynamic-table";

    /** @var string $layoutsMaster Indica el archivo Master del proyecto */
    public $layoutsMaster;


    /** @var string $layoutsDateState Indica el archivo que muestra fechas y estado */
    public $layoutsDateState = 'canelatools::layouts.datestate';


    /** @var array $parametersHidden Parametros (nombre => valor) de parametros ocultos
                                     que se envian al formulario */
    private $parametersHidden = [];


    /** @var array $parameterFilters Parametros que se envian desde el controlador para filtrar
                                     la tabla */
    public $parameterFilters = [];


    /**
     * TableRest constructor.
     * @param string|null $layoutsMaster
     */
    function __construct($layoutsMaster = null) {
        $this->layoutsMaster = $layoutsMaster ?? config('canelatools.blade.layouts.master', 'layouts.master');
    }


    /**
     * @param string $entity
     * @return TableRest
     */
    function init(string $entity): TableRest
    {
        return $this->setTitle(trans($entity::getResourceTrans('list')))
                    ->setFormTitle(trans($entity::getResourceTrans('report')))
                    ->setRouteEntity($entity::getRoutePath())
                    ->setUrlListAjax(route($entity::getRoutePath('.listAjax')));
    }


    /**
     *
     * @param array $value
     * @return TableRest
     */
    public function setTableForm(array $value)
    {
        $this->tableForm = $value;
        return $this;
    }


    /**
     *
     * @return array
     */
    public function getTableForm()
    {
        return $this->tableForm;
    }


    /**
     * @param string $value
     * @return mixed
     */
    public function setTableName(string $value) {
        $this->tableNameId = $value;
        return $this;
    }

    public function getTableName() {
        return $this->tableNameId;
    }


    /**
     *
     * @param array $value
     * @return TableRest
     */
    public function setTableColumn(array $value)
    {
        $this->tableColumn = $value;
        return $this;
    }


    /**
     *
     * @return array
     */
    public function getTableColumn()
    {
        return $this->tableColumn;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setRouteEntity(string $value)
    {
        $this->routeEntity  = $value;
        $this->urlAdd       = $this->urlAdd         ?? (\Route::has($value.'.create') ? route($value.'.create') : null);
        $this->urlIndex     = $this->urlIndex       ?? (\Route::has($value.'.index') ? route($value.'.index') : null);
        $this->urlStore     = $this->urlStore       ?? $value.'.store';
        $this->urlUpdate    = $this->urlUpdate      ?? $value.'.update';
        $this->urlListAjax  = $this->urlListAjax    ?? $value.'.list.ajax';
        return $this;
    }


    /**
     *
     * @param bool $value
     * @return TableRest
     */
    public function setShowBtnNew(bool $value)
    {
        $this->showBtnNew = $value;
        return $this;
    }


    /**
     *
     * @param bool $value
     * @return TableRest
     */
    public function setShowBtnEdit(bool $value)
    {
        $this->showBtnEdit = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setFormTitle(string $value): TableRest
    {
        $this->formTitle = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setTitle(string $value)
    {
        $this->title = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setUrlAdd(string $value)
    {
        $this->urlAdd = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setUrlIndex(string $value)
    {
        $this->urlIndex = $value;
        return $this;
    }


    /**
     *
     * @param string $label
     * @return TableRest
     */
    public function setUrlStore(string $value)
    {
        $this->urlStore = $value;
        return $this;
    }

    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setUrlUpdate(string $value)
    {
        $this->type = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setUrlListAjax(string $value)
    {
        $this->urlListAjax = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setJsDynamicTable(string $value)
    {
        $this->jsDynamicTable = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setLayoutsMaster(string $value)
    {
        $this->layoutsMaster = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableRest
     */
    public function setLayoutsDateState(string $value)
    {
        $this->layoutsDateState = $value;
        return $this;
    }


    /**
     * @return array
     */
    public function getParametersHidden(): array
    {
        return $this->parametersHidden;
    }


    /**
     *
     * @param array $value
     * @return TableRest
     */
    public function setParametersHidden(array $value)
    {
        $this->parametersHidden = $value;
        return $this;
    }


    /**
     * @param array $values
     */
    public function addParametersHidden(array $values)
    {
        foreach ($values as $key => $value) {
            $this->parametersHidden[$key] = $value;
        }
    }


    /**
     *
     * @param array $value
     * @return TableRest
     */
    public function setParametersFilters(array $value)
    {
        $this->parameterFilters = $value;
        return $this;
    }


    /**
     * @param $key
     * @return TableForm|null
     */
    public function getTableFormItem($key): ?TableForm
    {
        foreach ($this->tableForm as $tableFormItem)
        {
            if ($tableFormItem->id == $key) {
                return $tableFormItem;
            }
        }
        return null;
    }


    /**
     * getTableColumnTypesFields
     *
     * @param array $types
     * @return array
     */
    public function getTableColumnTypesFields(array $types): array
    {
        $result = [];
        /** @var TableForm $tableFormItem */
        foreach ($this->tableForm as $tableFormItem)
        {
            if (in_array($tableFormItem->type, $types)) {
                $result[] = $tableFormItem;
            }
        }
        return $result;
    }


    /**
     * hasTableColumnTypesFields
     *
     * @param array $types
     * @return boolean
     */
    public function hasTableColumnTypesFields(array $types): bool
    {
        return !empty($this->getTableColumnTypesFields($types));
    }


    /**
     * Returns ArchiveType corresponding to given TableColumnType.
     *
     * @param string|null $tableColumnType
     * @return integer|null
     */
    public static function tableColumnTypeToArchiveType(?string $tableColumnType): ?int
    {
        switch ($tableColumnType) {
            case TableColumnTypes::IMAGE:
            case TableColumnTypes::IMAGE_CROPPER:
                return ArchiveType::IMAGE;
            case TableColumnTypes::FILE:
                return ArchiveType::OTHER;
            default:
                return null;
        }
    }


    /**
     * Returns TableColumnType corresponding to given ArchiveType.
     *
     * @param integer|null $archiveType
     * @return string|null
     */
    public static function archiveTypeToTableColumnType(?int $archiveType): ?string
    {
        switch ($archiveType) {
            case (ArchiveType::VIDEO):
            case (ArchiveType::PDF):
            case (ArchiveType::OTHER):
                return TableColumnTypes::FILE;
            case (ArchiveType::IMAGE):
                return TableColumnTypes::IMAGE_CROPPER;
            case (ArchiveType::PODCASTS):
            case (ArchiveType::YOUTUBE):
                return TableColumnTypes::TEXT;
            default:
                return null;
        }
    }


    /**
     * Set Initial values with request parameters.
     *
     * @param Request $request
     * @return void
     */
    public function setInitialValues(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            foreach ($this->tableForm as $tableForm) {
                if ($tableForm->id == $key) {
                    $tableForm->value = $request->$key;
                    break;
                }
            }
        }
    }


    /**
     * Returns the local storage key name used for
     * getting/storing current entity attributes values.
     *
     * @return string
     */
    public function getLocalStorageEntityKeyName(): string {
        $routeItems = explode('.', $this->routeEntity);
        $routeNumItems = count($routeItems);
        if ($routeNumItems > 0) {
            $entityName = $routeItems[$routeNumItems-1];
        } else {
            $entityName = '';
        }
        return UtilsBasicManager::sanearString(strtolower(config('app.name'))).'-entity-'.$entityName;
    }

}
