@php
use Canela\CanelaTools\Enums\MessageType;

/* Indicar que se han de mostrar los paneles con los mensajes */
$showPanel = $showPanel ?? true;
/* Indicar que se han de mostrar los notify */
$showNoty = $showNoty ?? false;

@endphp

<div class="container message-container">
    <!-- Messages -->
    @if(Session::has(MessageType::SUCCESS) || !empty($msgSuccess))
        @include('canelatools::layouts.messages.message', ['type' => MessageType::SUCCESS])
    @endif

    @if (Session::has(MessageType::INFO) || !empty($msgInfo))
        @include('canelatools::layouts.messages.message', ['type' => MessageType::INFO])
    @endif

    @if (Session::has(MessageType::WARNING) || !empty($msgWarning))
        @include('canelatools::layouts.messages.message', ['type' => MessageType::WARNING])
    @endif

    @if (Session::has(MessageType::ERROR) || !empty($msgError) || count($errors) > 0)
        @include('canelatools::layouts.messages.message', ['type' => MessageType::ERROR])
    @endif

    <messages ref="messages"></messages>
    <!-- /Messages -->
</div>



