<?php
/** @var \Canela\CanelaTools\Models\Table\TableRest $tableRest */
?>
@section('body-modals')
    @parent
    <div class="modal fade" id="modal-input-translation" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('canelatools::canelatools.buttons.cancel')</button>
                    <button type="button" class="btn btn-primary save-btn">@lang('canelatools::canelatools.buttons.save')</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('body-scripts')
    @parent
    <script type="text/javascript">
        jQuery(document).ready(function () {
            const modalInputTranslation = jQuery('#modal-input-translation');
            const modalInputTranslationTitle = modalInputTranslation.find('.modal-title');
            const modalInputTranslationBody = modalInputTranslation.find('.modal-body');
            const modalInputTranslationTranslateBtn = modalInputTranslation.find('button.save-btn');

            jQuery('a.translate-link').on('click', function (event) {
                event.preventDefault();

                const $translateLink = jQuery(event.currentTarget);
                const $canelaInputFileContainer = $translateLink.parent().parent().parent();
                if ($canelaInputFileContainer.hasClass('canela-input-file-container')) {
                    if (selectedArchiveTypeId !== undefined) {
                        $canelaInputFileContainer.find('a.translate-link').attr('data-archive-type-id', selectedArchiveTypeId);
                    }
                }
                jQuery.getTranslation(event);
            });

            modalInputTranslationTranslateBtn.on('click', function () {
                jQuery.saveTranslation($(this));
            });

            jQuery.getTranslation = function (event) {
                const link = jQuery(event.currentTarget);
                const attribute = link.data('attribute');
                const language = link.data('language');

                // parametros
                const formData = new FormData();

                // recargar
                formData.append("_token", '{{ csrf_token() }}');
                formData.append('entity_id', '{{$entityId}}');
                formData.append('register_id', '{{$register->id}}');
                formData.append('attribute', attribute);
                formData.append('language', language);

                jQuery.ajax({
                    url: '{{route('admin.translation.ajax.get')}}',
                    dataType: "json",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            jQuery.openTranslationModal(event, data.data.translation);
                        } else {
                            jQuery.messageWarning(data.msg, 3000);
                        }

                        jQuery.loadEnd();
                    },
                });
            };

            jQuery.loadFieldData = function (sourceInput, clonedInputId, parent, disabled=false, changeValue=false, value=null) {
                const clonedInput = sourceInput.clone();
                clonedInput.prop('id', clonedInputId).prop('disabled', disabled).addClass('m-b-20');

                if (sourceInput.hasClass('classic-editor')) {
                    clonedInput.prop('style', '');
                }

                if (changeValue) {
                    clonedInput.val(value);
                }

                parent.append(clonedInput);

                if (sourceInput.hasClass('classic-editor')) {     // WYSIWYG
                    classicEditors[clonedInputId] = CKEDITOR.replace(clonedInputId);
                    // ClassicEditor.create(clonedInput.get()[0])
                    //     .then(editor => {
                    //         classicEditors[clonedInputId] = editor;
                    //         editor.isReadOnly = disabled;
                    //     })
                    //     .catch(error => {
                    //         console.log(error);
                    //     });
                } else if (sourceInput.hasClass('tagsinput')) {   // TAGS
                    // Implement in onShown event.
                }
            }

            jQuery.openTranslationModal = function (event, translation) {
                const link = jQuery(event.currentTarget);
                const id = link.data('id');
                const type = link.data('type');
                const attribute = link.data('attribute');
                const label = link.data('label');
                const language = link.data('language');
                const archiveTypeId = link.data('archive-type-id');

                modalInputTranslationTitle.html('{{trans('canelatools::canelatools.translations.actions.translate')}} '+label);
                modalInputTranslationBody.empty()

                const input = jQuery('#'+id);
                const originalInputId = 'modal-original-'+id;
                const translationInputId = 'modal-translation-'+id;

                // Original
                const formGroup = jQuery('<div class="form-group"></div>');
                formGroup.append(jQuery('<label for="'+originalInputId+'" class="canela-input-text">' + '{{trans('canelatools::canelatools.translations.labels.original')}} ' + '</label>'));
                modalInputTranslationBody.append(formGroup);

                if (archiveTypeId === undefined) {
                    jQuery.loadFieldData(input, originalInputId, formGroup, true);
                } else {
                    if (archiveTypeId === {{\Canela\CanelaTools\Models\General\ArchiveType::IMAGE}}) {
                        const previewImage = input.parent().find('.preview-container img');
                        const previewImageContainer = jQuery('<div class="text-center"></div>');
                        previewImageContainer.append(previewImage.clone().css('max-width', '200px'));
                        formGroup.append(previewImageContainer);
                    } else {
                        const archiveType = archiveTypes.find(function (item) { return item.id === archiveTypeId; });
                        if (archiveType !== undefined) {
                            if (!archiveType.is_text_input) {
                                const fileLink = input.parent().find('.preview-container a.file-link');
                                const fileLinkContainer = jQuery('<div></div>');
                                fileLinkContainer.append(fileLink.clone());
                                formGroup.append(fileLinkContainer);
                            } else {
                                jQuery.loadFieldData(input, originalInputId, formGroup, true);
                            }
                        }
                    }
                }

                // Translation
                const translationLabel = jQuery('<label for="'+translationInputId+'" class="canela-input-text">' + '{{trans('canelatools::canelatools.translations.labels.translation')}} ' + '</label>');
                translationLabel.append(jQuery(link).find('span').clone().addClass('m-l-10'));
                modalInputTranslationBody.append(translationLabel);

                if (archiveTypeId === undefined) {
                    jQuery.loadFieldData(input, translationInputId, modalInputTranslationBody, false, true, translation);
                } else {
                    if (archiveTypeId === {{\Canela\CanelaTools\Models\General\ArchiveType::IMAGE}}) {
                        const imageContainer = jQuery('<div id="canela-input-file-container-'+translationInputId+'" class="canela-input-file-container"></div>');
                        const inputParent = input.parent();
                        const previewContainer = inputParent.find('.preview-container').clone().addClass('m-auto');
                        previewContainer.find('a.clean-file-link').attr("href", "javascript:jQuery.cleanFile('" + translationInputId + "', '" + type + "')");

                        const imageUrl = translation !== null ? '{{asset(\App\Models\General\Archive::DIR_ARCHIVES)}}/' + language + '/' + translation : '';
                        previewContainer.find('img').attr('src', imageUrl).css('max-width', '250px');
                        const removeFileLink = previewContainer.find('a.remove-file-link');
                        if (translation !== null) {
                            initialImages[translationInputId] = imageUrl;
                            removeFileLink.attr("href", "javascript:jQuery.confirmDeleteTranslationFile()");
                        } else {
                            removeFileLink.remove();
                        }

                        imageContainer.append(previewContainer);
                        imageContainer.append(inputParent.find('input').clone().prop('id', translationInputId).addClass('m-b-20'));
                        modalInputTranslationBody.append(imageContainer);
                        jQuery.initCropperListener();
                    } else {
                        const archiveType = archiveTypes.find(function (item) { return item.id === archiveTypeId; });
                        if (archiveType !== undefined) {
                            if (!archiveType.is_text_input) {
                                const fileContainer = jQuery('<div id="canela-input-file-container-'+translationInputId+'" class="canela-input-file-container"></div>');
                                const inputParent = input.parent();
                                const previewContainer = inputParent.find('.preview-container').clone();

                                const fileUrl = translation !== null ? '{{asset(\App\Models\General\Archive::DIR_ARCHIVES)}}/' + language + '/' + translation : '';
                                previewContainer.find('a.file-link').attr("href", fileUrl).html(translation);
                                const removeFileLink = previewContainer.find('a.remove-file-link');
                                if (translation !== null) {
                                    removeFileLink.attr("href", "javascript:jQuery.confirmDeleteTranslationFile()");
                                } else {
                                    removeFileLink.remove();
                                }

                                fileContainer.append(previewContainer);
                                fileContainer.append(inputParent.find('input').clone().prop('id', translationInputId).addClass('m-b-20'));
                                modalInputTranslationBody.append(fileContainer);
                            } else {
                                jQuery.loadFieldData(input, translationInputId, modalInputTranslationBody, false, true, translation);
                            }
                        }
                    }
                }

                modalInputTranslationTranslateBtn.data('id', id);
                modalInputTranslationTranslateBtn.data('attribute', attribute);
                modalInputTranslationTranslateBtn.data('language', language);
                modalInputTranslationTranslateBtn.data('archive-type-id', archiveTypeId);

                modalInputTranslation.modal('show');
            };

            jQuery.updateTranslationStateIcon = function (iconSelector, ok) {
                jQuery(iconSelector).find('i').removeClass().addClass(ok ? 'fa fa-check text-success' : 'fa fa-times text-danger');
            };

            jQuery.saveTranslation = function (button) {
                const id = button.data('id');
                const attribute = button.data('attribute');
                const language = button.data('language');
                const archiveTypeId = button.data('archive-type-id');
                const translationInputId = 'modal-translation-'+id;
                const translationInput = jQuery('#'+translationInputId);

                // parametros
                const formData = new FormData();

                // recargar
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('_method', 'PUT');
                formData.append('entity_id', '{{$entityId}}');
                formData.append('register_id', '{{$register->id}}');
                formData.append('attribute', attribute);
                formData.append('language', language);
                if (archiveTypeId !== undefined) {
                    formData.append('archive_type_id', archiveTypeId);
                }

                var translation = undefined;
                if (translationInput.hasClass('classic-editor')) {  // WYSIWYG
                    const classicEditor = classicEditors[translationInputId];
                    if (classicEditor !== undefined) {
                        translation = classicEditor.getData();
                    }
                } else {
                    if (translationInput.attr('type') === 'file') {
                        if (archiveTypeId === {{\Canela\CanelaTools\Models\General\ArchiveType::IMAGE}}) {
                            const cropperImage = cropperImages[translationInputId];
                            if (cropperImage !== undefined) {
                                translation = cropperImage.data;
                            }
                        } else {
                            const file = translationInput[0].files[0];
                            if (file !== undefined) {
                                translation = file;
                            }
                        }
                    } else {
                        translation = translationInput.val();
                    }
                }

                if (translation !== undefined) {
                    if (typeof cropperImage !== 'undefined') {
                        formData.append('translation', translation, cropperImage.fileName);
                    } else {
                        formData.append('translation', translation);
                    }
                }

                jQuery.ajax({
                    url: '{{route('admin.translation.ajax.set')}}',
                    dataType: "json",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            modalInputTranslation.modal('hide');

                            var checked = false;
                            if (translation !== undefined) {
                                if (typeof translation === 'string') {
                                    checked = translation.length > 0;
                                } else {
                                    checked = true;
                                }
                            }

                            jQuery.updateTranslationStateIcon('#'+id+'-'+language, checked);
                            jQuery.messageSuccess(data.msg, 3000);
                        } else {
                            jQuery.messageWarning(data.msg, 3000);
                        }

                        jQuery.loadEnd();
                    },
                });
            };

            modalInputTranslation.on('shown.bs.modal', function () {
                jQuery(this).find('.tagsinput').each(function () {
                    jQuery(this).tagsinput('destroy');
                    jQuery(this).tagsinput('items');
                });
            })

            modalInputTranslation.on('hidden.bs.modal', function () {
                const input = modalInputTranslation.find('input.canela-input-image-cropper')
                if (input.length == 1) {
                    const inputId = input.prop('id');
                    delete initialImages[inputId];
                    delete cropperImages[inputId];
                }
            });

            jQuery.confirmDeleteTranslationFile = function () {
                Swal.fire({
                    text: '{{trans('canelatools::canelatools.canela-rest.label.question_delete_translation_file')}}',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('canelatools::canelatools.buttons.continue')}}',
                    cancelButtonText: '{{trans('canelatools::canelatools.buttons.cancel')}}',
                }).then(function (result) {
                    if (result.isConfirmed) {
                        jQuery.deleteTranslationFile();
                    }
                });
            }

            jQuery.deleteTranslationFile = function () {
                const id = modalInputTranslationTranslateBtn.data('id');
                const attribute = modalInputTranslationTranslateBtn.data('attribute');
                const language = modalInputTranslationTranslateBtn.data('language');

                // parametros
                const formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('_method', 'DELETE');
                formData.append('entity_id', '{{$entityId}}');
                formData.append('register_id', '{{$register->id}}');
                formData.append('attribute', attribute);
                formData.append('language', language);

                jQuery.ajax({
                    url: '{{route('admin.translation.ajax.delete')}}',
                    dataType: "json",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            modalInputTranslation.modal('hide');
                            jQuery.updateTranslationStateIcon('#'+id+'-'+language, false);
                            jQuery.messageSuccess(data.msg, 3000);
                        } else {
                            jQuery.messageWarning(data.msg, 3000);
                        }

                        jQuery.loadEnd();
                    },
                });

            };

        });
    </script>
@endsection
