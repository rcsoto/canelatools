<?php namespace Canela\CanelaTools\Controller\Database;

use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\MessageType;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SqlController extends BasicController {

    public function __construct() {

        $this->menu       = AdminMenu::SQL;
        $this->menu_group = AdminMenu::GROUP_CONFIG;

        $this->vistaLista = 'canelatools::database.sentenceSql';

        parent::__construct();
    }


    /**
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $resultado = [];
        try {
            $sentencias = $request->get('sentences');
            DB::beginTransaction();
            $arregloSql = array_filter(explode(";", $sentencias));
            foreach ($arregloSql as $consulta)
            {
                $resultado[] = trans('canelatools::canelatools.database.sql.registers', ['value' => DB::statement($consulta)]);
            }

            DB::commit();
        }
        catch (\Exception $exception)
        {
            DB::rollBack();
            Log::error($exception);
            if ($request->ajax()) {
                return \Response::ajax(false, 0, $exception->getMessage());
            }
            else {
                return redirect()->back()->withInput()->with(MessageType::WARNING, $exception->getMessage());
            }
        }

        if ($request->ajax()) {
            return \Response::ajax(true, 1, implode('<br/>', $resultado));
        }
        else {
            return redirect()->back()->withInput()->with(MessageType::INFO, implode('<br/>', $resultado));
        }
    }


}
?>
