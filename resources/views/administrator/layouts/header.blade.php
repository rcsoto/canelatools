<!-- [ Header ] start -->
<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="{!! route('home') !!}" target="_blank" class="b-brand">
            <span class="logo-default"><img src="{{asset(\App\Models\Client\Customer::DIR_LOGO.$customer->image_logo_dark)}}" alt="{{$customer->business_name}}"></span>
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>

    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown drp-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="feather icon-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <span>{{\Auth::user()->name}}</span>
                            {!! Form::open(['id' => 'logout-form', 'url' => url('logout'), 'method' => 'POST']) !!}
                            {!! Form::close() !!}
                            <a href="javascript:document.getElementById('logout-form').submit();" class="dud-logout" title="@lang('canelatools::canelatools.general.actions.logout')">
                                <i class="feather icon-log-out"></i>
                            </a>
                        </div>
                        <ul class="pro-body">
                            <li><a href="{{route('admin.configuration.user.edit', auth()->user()->id)}}" class="dropdown-item"><i class="feather icon-user"></i> @lang('canelatools::webbuilder.common.labels.profile')</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>

    </div>

</header>
<!-- [ Header ] end -->
