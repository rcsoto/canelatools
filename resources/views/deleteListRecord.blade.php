﻿@php
    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
@endphp
@if(isset($modalSectionName))
    @section('body-modals')
        @parent
        @include('canelatools::layouts.modalDeleteListRecord')
    @endsection
@else
    @include('canelatools::layouts.modalDeleteListRecord')
@endif

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">
        var deleteButton = null;

        $(document).ready(function () {

            loadButtons();

            $('#modalDeleteRecord').find('.btn-delete-js').click(function () {
                var modal = $('#modalDeleteRecord');
                var modelId = modal.data('item-id');
                var url = modal.data('url');
                var stopOnRelations = modal.data('stop-on-relations');

                delRecord(modelId, 0, deleteButton, url, stopOnRelations);
            });

            $('#modalModelHasAssoc').find('.btn-delete-js').click(function () {
                var modal = $('#modalDeleteRecord');
                var modelId = modal.data('item-id');
                var url = modal.data('url');
                var stopOnRelations = modal.data('stop-on-relations');

                delRecord(modelId, 1, deleteButton, url, stopOnRelations);
            });


        });

        function loadButtons() {
            $("table").find("a[data-action='delete']").click(function () {
                var modal = $('#modalDeleteRecord');
                deleteButton = $(this);

                modal.data('item-id', $(this).data('item-id'));
                modal.data('stop-on-relations', $(this).data('stop-on-relations'));
                modal.data('url', $(this).data('url'));
                modal.modal('show');
            })
        }


        function delRecord(id, forceDelete, deleteButton, url, stopOnRelations) {

            var delete_url;
            if (url !== undefined) {
                delete_url = url + '?force=' + forceDelete;
            } else {
                delete_url = '{{$delete_url ?? null}}' + '/' + id + '?force=' + forceDelete;
            }

            jQuery.ajax({
                url: delete_url,
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: null,
                processData: false,
                contentType: false,
                dataType: "json",
                type: "DELETE",
                beforeSend: function () {
                    jQuery('.loader-overlay').removeClass('loaded');
                },
                error: function (object1, descr_error, object2) {
                    var msg = jQuery.procesarRespuestaAjax(object1, descr_error, object2);
                    jQuery('.loader-overlay').addClass('loaded');
                    jQuery.messageError(msg);
                },
                success: function (data) {
                    jQuery('.loader-overlay').addClass('loaded');
                    if (data.result === true) {
                        var modalWarn = $('#modalModelHasAssoc');
                        $('#modalDeleteRecord').modal('hide');

                        if (data.delete === true) {
                            jQuery.messageInfo(data.msg, 2000);
                            modalWarn.modal('hide');
                            //DELETE TABLE ROW
                            deleteButton.parents("tr:first").remove();
                        } else {
                            modalWarn.data('item-id', id);
                            modalWarn.data('url', url);
                            modalWarn.data('stop-on-relations', stopOnRelations);
                            modalWarn.find('.warn-tables-js').html(data.tables);

                            if (stopOnRelations) {
                                modalWarn.find('.force-delete-js').hide();
                            } else {
                                modalWarn.find('.force-delete-js').show();
                            }
                            modalWarn.modal('show');
                        }
                    } else {
                        jQuery.messageError(data.msg, 2000);
                    }
                }
            });
        }
    </script>
@endsection
