@section('body-modals')
@parent
    <div class="modal fade" id="modal-image-cropper" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">@lang('canelatools::canelatools.cropper.view.select_image')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img src="" alt="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('canelatools::canelatools.buttons.cancel')</button>
                    <button type="button" class="btn btn-primary" id="crop">@lang('canelatools::canelatools.buttons.save')</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('body-scripts')
@parent
    <script type="text/javascript">
        var cropperImages = [];
        var imageCropperModal;
        const $imageCropperModal = jQuery('#modal-image-cropper');
        const imageCropperModalImage = $imageCropperModal.find('.img-container img').get(0);
        var previewImage;
        var canelaInputFileChangedContainer;
        var fileName;
        var fileMimeType;

        jQuery.initCropperListener = function () {
            jQuery('.canela-input-image-cropper').on('change', function (e) {
                const input = e.target;
                const files = input.files;
                const canelaInputFileContainer = document.getElementById('canela-input-file-container-' + input.id);
                canelaInputFileChangedContainer = jQuery(canelaInputFileContainer);
                previewImage = jQuery(canelaInputFileContainer).find('.preview-container img').get(0);

                var reader;
                var file;
                if (files && files.length > 0) {
                    file = files[0];
                    fileName = file.name;
                    fileMimeType = file.type;

                    if (fileMimeType !== 'image/svg+xml') {
                        const done = function (url) {
                            input.value = '';
                            imageCropperModalImage.src = url;
                            $imageCropperModal.modal('show');
                        };

                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });
        }

        jQuery(document).ready(function () {
            /***** Image Cropper *****/
            jQuery.initCropperListener();

            $imageCropperModal.on('shown.bs.modal', function () {
                imageCropperModal = new Cropper(imageCropperModalImage, {
                    autoCropArea: 1     // Crop box with full image width and height.
                });
            }).on('hidden.bs.modal', function () {
                imageCropperModal.destroy();
                imageCropperModal = null;
            });

            jQuery('#crop').on('click', function () {
                if (imageCropperModal) {
                    const canvas = imageCropperModal.getCroppedCanvas({
                    });
                    previewImage.src = canvas.toDataURL(fileMimeType);
                    canvas.toBlob(function (blob) {
                        cropperImages[canelaInputFileChangedContainer.find('input.canela-input-image-cropper').prop('id')] = {
                            fileName: fileName,
                            data: blob
                        };
                        canelaInputFileChangedContainer.find('a.remove-file-link').hide();
                        canelaInputFileChangedContainer.find('a.clean-file-link').show();
                        $imageCropperModal.modal('hide');
                    }, fileMimeType);
                }
            });
            /***** /Image Cropper ****/
        });
    </script>
@endsection
