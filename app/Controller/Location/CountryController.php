<?php

namespace Canela\CanelaTools\Controller\Location;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Models\Location\Country;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Location\CountryRequest;
use Canela\CanelaTools\HtmlLists;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class CountryController
 * @package Canela\CanelaTools\Controller\Location
 */
class CountryController extends BasicController
{
    use HtmlLists;

    /**
     * CountryController constructor.
     */
    public function __construct()
    {
        $this->menu_group = AdminMenu::GROUP_LOCATION;
        $this->menu       = AdminMenu::COUNTRY;

        $this->entidad = Country::class;
        $this->routeEntity = 'location.country';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::canelarest.addEdit';
        $this->vistaAdd    = 'canelatools::canelarest.addEdit';

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.country.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(true)
            ->setTableColumn([
                (new TableColumn())->setTable('country')->setFieldOrder('id'),
                (new TableColumn())->setTable('country')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.country.field.name')),
                (new TableColumn())->setTable('country')->setFieldOrder('code2')->setLabel(trans('canelatools::canelatools.entity.country.field.code2')),
                (new TableColumn())->setTable('country')->setFieldOrder('code3')->setLabel(trans('canelatools::canelatools.entity.country.field.code3')),
                (new TableColumn())->setTable('country')->setFieldOrder('code_number')->setLabel(trans('canelatools::canelatools.entity.country.field.code_number')),
                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([
                (new TableForm())->setName('name')->setLabel(trans('canelatools::canelatools.entity.country.field.name'))->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('code2')->setLabel(trans('canelatools::canelatools.entity.country.field.code2'))->setContainerClass('col-6 mb-2')
                    ->setMin(2)->setMax(3),
                (new TableForm())->setName('code3')->setLabel(trans('canelatools::canelatools.entity.country.field.code3'))->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('code_number')->setLabel(trans('canelatools::canelatools.entity.country.field.code_number'))
                    ->setContainerClass('col-6 mb-2')->setType(TableColumnTypes::INT)->setMin(1)->setMax(999),
            ]);

        parent::__construct();
    }


    /**
     *
     * Enter description here ...
     */
    public function getConsultaPersonalizada($request)
    {
        return Country::select(['country.id',
                                'country.name',
                                'country.code2',
                                'country.code3',
                                'country.code_number',
                                'country.id as action_country_id'
                               ]);
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_country_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, false, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CountryRequest $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(CountryRequest $request)
    {
        return $this->storeBasic($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CountryRequest $request
     * @param int $country_id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(CountryRequest $request, int $country_id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $country_id);
    }
}
