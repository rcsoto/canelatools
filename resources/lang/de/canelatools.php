<?php

return [

    /*
     |--------------------------------------------------------------------------
     |Allgemeine Etiketten
     |--------------------------------------------------------------------------
     |
     | canela
     |
     */

    // GENERAL
    'general' => [

        'message'  => [
            'send_correct' => 'Nachricht erfolgreich gesendet',
            'send_error'   => 'Fehler beim Senden der Nachricht',
        ],

        'actions'  => [
            'exit'          => 'Beenden',
            'logout'        => 'Sitzung schließen',
            'edit'          => 'Bearbeiten',
            'select'        => 'Abfragen',
            'save'          => 'Speichern',
            'disabled'      => 'Logische Abmeldung',
            'enabled'       => 'Wiederherstellen',
            'send'          => 'Senden',
        ],
        'label'    => [
            'attention'     => 'Achtung!',
            'information'   => 'Informationen',
            'success'       => 'Anfrage erfolgreich verarbeitet',
            'trace_new'     => 'Anmeldung von :model_name',
            'trace_update'  => 'Bearbeitung von :model_name',
            'date'          => 'Datum Anmeldung',
            'last_date'     => 'Datum letzte Änderung',
            'loading'       => 'Wird geladen ...',
            'select'        => 'Auswählen',
            'select_items_and_click_continue' => 'Elemente auswählen und auf Fortfahren klicken.',
            'cookies-policy' => 'Cookie-Richtlinie',
            'legal-warning' => 'Disclaimer',
            'data-protection' => 'Datenschutz',
            'privacy-policy' => 'Datenschutzrichtlinie',
            'read_more' => ' ...mehr lesen',
            'accept'        => 'Akzeptieren',
            'accept-privacy-policy' => 'Datenschutzrichtlinie annehmen',
            'have-read-and-accept-privacy-policy' => 'Ich habe die <a href=":route" target="_blank">Datenschutzrichtlinie gelesen und verstanden</a>',
            'must-accept-privacy-policy' => 'Sie sollten die Datenschutzrichtlinie annehmen.',
            'query' => 'Anfrage',
        ],
    ],


    // Authentifizierung
    'auth'    => [
        'email' => [
            'change_password' => 'Passwort ändern',
            'back_login'      => 'Zum Login zurückkehren',
            'send_reminder'   => 'Erinnerung senden',
        ],
        'login' => [
            'init'               => 'Beginn der Sitzung',
            'remember'           => 'Erinnern',
            'forgotten_password' => 'Hast du das Passwort vergessen?',
            'enter'              => 'EINGEBEN',
        ],
    ],


    /*
     |--------------------------------------------------------------------------
     | Allgemeine Etiketten
     |--------------------------------------------------------------------------
     |
     | Allgemein
     |
     */

    'bool'       => [
        'yes' => 'Ja',
        'no'  => 'Nein',
    ],
    'ajax'       => [
        'unsupported' => 'Nicht unterstützter Vorgang',
    ],
    'model'      => [
        'list'               => ['correct' => 'Korrekt aufgelistete Datensätze.',
                                 'error'   => 'Es ist ein Fehler bei der Auflistung der Datensätze aufgetreten.',],
        'update'             => ['correct' => ':Wert erfolgreich aktualisiert.',
                                 'error'   => 'Es ist ein Fehler bei der Aktualisierung des Datensatzes aufgetreten',],
        'store'              => ['correct' => ':Wert erfolgreich erstellt.',
                                 'error'   => 'Es ist ein Fehler bei der Erstellung des Datensatzes aufgetreten',],
        'find'               => ['error' => 'Datensatz nicht gefunden',],
        'delete'             => ['correct' => ':Wert erfolgreich gelöscht.',
                                 'error'   => 'Es ist ein Fehler beim Löschen des Datensatzes aufgetreten',],
        'copy'               => ['correct' => ':Wert erfolgreich verdoppelt.',
                                 'error'   => 'Es ist ein Fehler beim Duplizieren des Datensatzes aufgetreten',],
        'alignment'          => ['center' => 'Mittig',
                                 'left'   => 'Links',
                                 'right'  => 'Rechts',],
        'vertical_alignment' => ['medium' => 'Mitte',
                                 'bottom' => 'Unterhalb/normal',
                                 'top'    => 'Hochgestellt',],
        'reorder'            => ['reorder_ok'    => 'Wurde erfolgreich neu geordnet.',
                                 'reorder_error' => 'Es ist ein Fehler bei der erneuten Anordnung aufgetreten.',],
        'update_status'      => ['correct' => 'Der Status wurde erfolgreich neu geordnet.',
                                 'error'   => 'Es ist ein Fehler bei der Aktualisierung des Status aufgetreten.',],
        'fusion'             => ['correct' => 'Die Datensätze wurden erfolgreich zusammengelegt.',
                                 'error'   => 'Es ist ein Problem beim Zusammenlegen der Datensätze aufgetreten.',],
        'generic_error'      => 'Es ist ein Fehler aufgetreten. Führen Sie den Vorgang erneut aus. Sollte der Fehler fortbestehen, setzen Sie sich bitte mit dem Kundensupport in Verbindung.',
    ],
    'file'       => [
        'upload_ok'    => 'Datei erfolgreich hochgeladen.',
        'upload_error' => 'Beim Hochladen der Datei ist ein Fehler aufgetreten.',
    ],
    'buttons'    => [
        'accept'        => 'Akzeptieren',
        'approve'       => 'Genehmigen',
        'back'          => 'Zurück',
        'cancel'        => 'Abbrechen',
        'continue'      => 'Fortsetzen',
        'close'         => 'Schließen',
        'copy'          => 'Kopieren',
        'delete'        => 'Löschen',
        'edit'          => 'Bearbeiten',
        'new'           => 'Neu',
        'preview'       => 'Vorschau',
        'refuse'        => 'Ablehnen',
        'save'          => 'Speichern',
        'send'          => 'Senden',
        'show'          => 'Abfrage',
        'update'        => 'Aktualisieren',
        'activate'      => 'Aktivieren',
        'deactivate'    => 'Abmeldung',
        'disable'       => 'Deaktivieren',
        'execute'       => 'Ausführen',
        'upload'        => 'Hochladen',
        'download'      => 'Herunterladen',
        'share'         => 'Teilen',
        'prev'          => 'Zurück',
        'next'          => 'Vor',
    ],

    'select'     => [
        'placeholder' => 'Wählen Sie einen Wert aus ...',
    ],


    /*
     |--------------------------------------------------------------------------
     | Seite FEHLER
     |--------------------------------------------------------------------------
     */
    '401' => [
        'title' => 'Nicht berechtigt',
        'message' => 'Nicht zum Zugriff auf diesen Datensatz berechtigt.',
    ],
    '403' => [
        'title' => 'Ohne Berechtigungen',
        'message' => 'Sie haben keine Berechtigung für den Zugriff auf diese Datensätze.',
    ],
    '404' => [
        'title' => 'Seite nicht gefunden',
        'message' => 'Diese Seite scheint nicht zu existieren oder wurde gelöscht.',
    ],
    '419' => [
        'title' => 'Seite abgelaufen',
        'message' => 'Seite abgelaufen. Ihre Berechtigugnsnachweise sind abgelaufen.',
    ],
    '429' => [
        'title' => 'Zu viele Anfragen.',
        'message' => 'Zu viele Anfragen.',
    ],
    '500' => [
        'title' => 'Fehler/Vorfall',
        'message' => 'Es ist ein Serverfehler aufgetreten. Versuchen Sie es erneut. Wenn der Fehler fortbesteht, setzen Sie sich mit dem Supportteam in Verbindung.',
    ],
    '503' => [
        'title' => 'Wartung',
        'message' => 'Das System wird einer Wartung unterzogen. Wir sind in einigen Minuten zurück. Danke.',
    ],
    '550' => [
        'title' => 'Fehler/Vorfall',
        'message' => 'es ist ein Serverfehler aufgetreten. Versuchen Sie es erneut. Wenn der Fehler fortbesteht, setzen Sie sich mit dem Supportteam in Verbindung.',
    ],

    /*
     |--------------------------------------------------------------------------
     | Update Browser
     |--------------------------------------------------------------------------
     */

    'updateBrowser' => [
        'title'                  => ':project Browserversion veraltet',
        'obsolete_browser'       => 'Wenn diese Seite angezeigt wird, ist die Version Ihres Browsers veraltet',
        'update_version_browser' => 'Um deine Sicherheit und die sichere Anzeige moderner Websites mit all ihren Funktionen zu gewährleisten, empfehlen wir, die Version des von dir bevorzugten Browsers zu aktualisieren.',
        'important'              => 'Wichtig',
        'browser_funcionality'   => 'Prüfen Sie, ob die Funktionalität in Ihrem Browser aktiviert',
        'compability_view'       => 'Kompatibilitätsansicht',
        'disable'                => 'In diesem Fall solltest du diese zur korrekten Anzeige der Seite deaktivieren.',
        'more_information'       => 'Weitere Informationen hier',
        'google_chrome'          => 'Google Chrome',
        'firefox'                => 'Firefox',
        'explorer'               => 'Microsoft Edge',
        'update_visit'           => 'Vergessen Sie nach dem Aktualisieren nicht, uns erneut zu besuchen!',
        'rights_reserved'        => 'ALLE RECHTE VORBEHALTEN',
        'show_image'             => 'Bild anzeigen',
        'url_chrome'             => 'https://www.google.com/intl/es/chrome/browser/?hl=es',
        'url_firefox'            => 'http://www.mozilla.org/es-ES/firefox/fx/',
        'url_edge'               => 'https://support.microsoft.com/es-es/help/4501095/download-the-new-microsoft-edge-based-on-chromium',
    ],


];
