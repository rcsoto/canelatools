﻿@php

// valores por defecto
$title = $tableRest->title ?? "??";
$selectable = request()->has('selectable') && request()->has('url_select') && request()->has('url_back');


    /** @var String $nameSectionBreadcrumbs nombre de la seccion de las migas de pan */
    $nameSectionBreadcrumbs = $nameSectionBreadcrumbs ?? config('canelatools.blade.section.breadcrumbs');

    /** @var String $nameSectionContent nombre de la seccion principal */
    $nameSectionContent = $nameSectionContent ?? config('canelatools.blade.section.content');

    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');

@endphp

@extends($tableRest->layoutsMaster)


@section('breadcrumbs')
    @include('canelatools::rest.layouts.list-breadcrumbs')
@endsection

@section('content')

	<div class="row">
    	<div class="col-sm-12">
        	<div class="card">
				<div class="card-block">

                    @if($selectable)
                        <div class="selectable-header m-b-20 text-center">
                            <div class="card">
                                <p>@lang('canelatools::canelatools.general.label.select_items_and_click_continue')</p>
                                <div>
                                    <a class="btn btn-secondary waves-effect waves-light" href="{{request()->get('url_back')}}" role="button"><i class="fa fa-angle-left"></i> @lang('canelatools::canelatools.buttons.back')</a>
                                    <a class="btn btn-primary waves-effect waves-light" href="javascript:jQuery.selectItems();" role="button">@lang('canelatools::canelatools.buttons.continue') <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    @else
                        @if(($tableRest->showBtnNew ?? false))
                            <div class="m-b-20">
                                <div class="btn-group">
                                    <a class="btn btn-success btn-tumblr waves-effect waves-light"
                                       href="{{ $tableRest->urlAdd }}">
                                        <i class="fa fa-plus"></i> {{ trans('canelatools::canelatools.canela-rest.list.btn.new', ['model' => $tableRest->formTitle ?? $title]) }}</a>
                                </div>
                            </div>
                        @endif
                    @endif

					@include('canelatools::rest.layouts.table', array('tableRest' => $tableRest))

        	  	</div>
        	</div>
      	</div>
	</div>

@endsection

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">

        jQuery(document).ready(function () {

            @if($selectable)
                jQuery.selectItems = function () {
                    const ids = jQuery('.selectable:checked').map(function(){ return jQuery(this).val();}).toArray();

                    jQuery.ajax({
                        url: '{{request()->get('url_select')}}',
                        dataType: "json",
                        data: {
                            '_token': '{{ csrf_token() }}',
                            '_method': 'PATCH',
                            'ids': ids
                        },
                        type: "POST",
                        processData: true,
                        beforeSend: function () {
                            jQuery.loadIni();
                        },
                        error: function (jqXHR, exception, errorThrown) {
                            const msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                            jQuery.messageWarning(msg);
                            jQuery.loadEnd();
                        },
                        success: function (data) {
                            if (data.result) {
                                location.href = '{{request()->get('url_back')}}';
                            } else {
                                jQuery.messageWarning(data.message);
                            }
                            jQuery.loadEnd();
                        },
                    });

                };
            @endif

        });
    </script>
@endsection
