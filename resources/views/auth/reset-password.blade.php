@extends('canelatools::auth.layouts.main')

@php
    use App\Models\Project\WebBuilder\Web;
    $web = Web::firstOrFail();
    $customer = $web->customer;
@endphp

@section('body-class', 'page-login')

@section('content')
    <!-- [ auth-signup ] start -->
    <div class="auth-wrapper">
        <div class="auth-content">
            <div class="card">
                <div class="row align-items-center text-center">
                    <div class="col-md-12">
                        <div class="card-body">
                            @include('canelatools::auth.layouts.logo-container')

                            {!! Form::open(['method' => 'POST', 'url' => route('password.update')]) !!}
                                {!! Form::hidden('token', $request->route('token')) !!}
                                <div class="form-group mb-3">
                                    <label class="floating-label" for="email">@lang('canelatools::webbuilder.common.fields.email')</label>
                                    <input type="text" class="form-control" name="email" placeholder="" value="{{old('email', $request->email)}}">
                                    @if($errors->has('email'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <div class="form-group mb-4">
                                    <label class="floating-label" for="password">@lang('canelatools::webbuilder.common.fields.password')</label>
                                    <input type="password" class="form-control" name="password" placeholder="">
                                    @if($errors->has('password'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>
                                <div class="form-group mb-4">
                                    <label class="floating-label" for="password_confirmation">@lang('canelatools::webbuilder.pages.auth.reset-password.forms.reset-password.fields.password_confirmation')</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="">
                                    @if($errors->has('password_confirmation'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('password_confirmation') }}</div>
                                    @endif
                                </div>
                                <button class="btn btn-primary btn-block">@lang('canelatools::webbuilder.pages.auth.reset-password.forms.reset-password.buttons.reset-password')</button>
                                <a href="{{route('login')}}" class="btn btn-dark btn-block text-white mb-4">@lang('canelatools::webbuilder.common.buttons.back')</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ auth-signup ] end -->
@endsection

