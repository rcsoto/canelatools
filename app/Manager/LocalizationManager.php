<?php

namespace Canela\CanelaTools\Manager;

use Canela\CanelaTools\Models\Language;
use Exception;
use Illuminate\Http\Request;

class LocalizationManager
{
    const SESSION_LANG = 'language';
    const SESSION_LANG_IS_MAIN = 'locale_is_main';

    /**
     * Returns first accepted language locale from browser or config default if not exists.
     *
     * @param Request $request
     * @return string
     */
    public static function getBrowserLocaleOrDefault(Request $request) {
        $resultLocale = null;
        $acceptedLocales = collect(explode(',',$request->server('HTTP_ACCEPT_LANGUAGE')))->map(function ($item) {
            return explode(';', $item)[0];
        });
        $availableLocales = Language::actives()->pluck('code')->toArray();

        foreach ($acceptedLocales as $locale) {
            if (in_array($locale, $availableLocales)) {
                $resultLocale = $locale;
                break;
            }
        }

        return $resultLocale ?? config('app.locale');
    }


    /**
     * @param string $locale
     * @param bool $mustExists
     * @throws Exception
     */
    public static function storeLocaleInSession(string $locale, bool $mustExists=true)
    {
        $language = Language::where('code', $locale)->active();
        $language = $mustExists ? $language->firstOrFail() : $language->first();

        if (!is_null($language)) {
            $webLanguage = $language->webLanguage;
            if (!is_null($webLanguage)) {
                session()->put(self::SESSION_LANG, $language->code);
                session()->put(self::SESSION_LANG_IS_MAIN, $webLanguage->principal);
            } else {
                throw new Exception('Language '.$language->code.' must have one WebLanguage associated.');
            }
        } else {
            session()->remove(self::SESSION_LANG);
            session()->remove(self::SESSION_LANG_IS_MAIN);
        }
    }

}
