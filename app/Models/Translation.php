<?php

namespace Canela\CanelaTools\Models;


/**
 * App\Models\Translation
 *
 * @property int                                      $id
 * @property int                                      $language_id
 * @property int                                      $entity_id
 * @property int                                      $register_id
 * @property string                                   $entity
 * @property string                                   $code
 * @property string|null                              $value
 * @property \Carbon\Carbon|null                      $created_at
 * @property \Carbon\Carbon|null                      $updated_at
 * @property-read string                              $created_at_format
 * @property-read string                              $table
 * @property-read string                              $updated_at_format
 * @property-read string                              $combo_identificator
 * @property-read string                              $value_identificator
 * @property-read string                              $created_at_short
 * @property-read string                              $updated_at_short
 * @property-read \Canela\CanelaTools\Models\Language $language
 * @property-read \Canela\CanelaTools\Models\Entity   $entityRelation
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereEntity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereRegisterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Translation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboAreaList($areaId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @mixin \Eloquent
 */
class Translation extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'translation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_id',
        'entity_id',
        'register_id',
        'entity',
        'code',
        'value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'language_id' => 'int',
        'entity_id'   => 'int',
        'register_id' => 'int',
        'entity'      => 'string',
        'code'        => 'string',
        'value'       => 'string',
    ];

    /**
     * Related language.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * Related entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelation()
    {
        return $this->belongsTo(Entity::class, 'entity_id');
    }

}