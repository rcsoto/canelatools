@section('body-scripts')
    @parent
    <script type="text/javascript">
        @php
            $initialImages = empty($register) ? '{}' : collect($tableRest->tableForm)->filter(function ($tableForm) use ($register) {
                                            $field = $tableForm->name;
                                            $value = $register->$field;
                                            return \Canela\CanelaTools\Enums\TableColumnTypes::isImageType($tableForm->type) && !is_null($value);
                                        })->mapWithKeys(function ($tableForm) use ($register) {
                                            $id = $tableForm->id;
                                            $field = $tableForm->name;
                                            $value = asset($tableForm->getFileDir().$register->$field);
                                            return [$id => $value];
                                        })->toJson();
        @endphp

        var initialImages = JSON.parse('{!!$initialImages!!}');

        jQuery(document).ready(function () {

            jQuery('.canela-input-image').on('change', function (e) {
                const input = e.target;
                const files = input.files;
                const [file] = files;
                if (file) {
                    const canelaInputFileContainer = document.getElementById('canela-input-file-container-' + input.id);
                    canelaInputFileChangedContainer = jQuery(canelaInputFileContainer);
                    previewImage = jQuery(canelaInputFileContainer).find('.preview-container img').get(0);
                    previewImage.src = URL.createObjectURL(file);

                    canelaInputFileChangedContainer.find('a.remove-file-link').hide();
                    canelaInputFileChangedContainer.find('a.clean-file-link').show();
                }
            });

            jQuery.cleanFile = function (fileId, type) {
                const containerId = 'canela-input-file-container-' + fileId;
                const container = jQuery('#' + containerId);
                const imagePreview = container.find('.preview-container img');

                if (initialImages[fileId] !== undefined) {
                    imagePreview.prop('src', initialImages[fileId]);
                    container.find('a.remove-file-link').show();
                } else {
                    imagePreview.prop('src', '');
                }

                container.find('a.clean-file-link').hide();

                if (type === '{{\Canela\CanelaTools\Enums\TableColumnTypes::IMAGE_CROPPER}}') {
                    delete cropperImages[fileId];
                } else {
                    container.find('input').val(null);
                }
            };

        });
    </script>
@endsection
