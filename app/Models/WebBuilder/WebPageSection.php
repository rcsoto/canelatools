<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\State;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class WebPageSection
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property WebPage page
 */
class WebPageSection extends BasicModel
{

    public const DIR_IMAGE_HEADER = 'project/section/header/';
    public const DIR_IMAGE_THUMB  = 'project/section/thumb/';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_section';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'state_id',
        'position',
        'slug',
        'title',
        'subject',
        'body',
        'seo_title',
        'seo_description',
        'external_link',
        'open_in_new_window',
        'file_header_id',
        'file_thumb_id',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id'           => 'int',
        'state_id'          => 'int',
        'position'          => 'int',
        'slug'            => 'string',
        'title'             => 'string',
        'subject'           => 'string',
        'body'              => 'string',
        'seo_title'         => 'string',
        'seo_description'   => 'string',
        'external_link'     => 'string',
        'open_in_new_window'=> 'bool',
        'file_header_id'    => 'int',
        'file_thumb_id'     => 'int',
    ];


    /**
     * @return BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * @return BelongsTo
     */
    public function page(): BelongsTo
    {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(WebPageCategory::class, 'web_page_section_category', 'section_id', 'category_id')
                    ->orderBy('position');
    }
    public function getCategoryIdsAttribute() {
        return $this->categories->pluck('id')->toArray();
    }


    /**
     * @return HasMany
     */
    public function archives() {
        return $this->hasMany(WebPageSectionArchive::class, 'section_id')
                    ->orderBy('position');
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages($this->page->web_id);
    }


    /**
     * @return BelongsTo
     */
    public function fileHeader(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'file_header_id');
    }


    /**
     * @return BelongsTo
     */
    public function fileThumb(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'file_thumb_id');
    }

}
