<?php

namespace Canela\CanelaTools\Contracts;


use Canela\CanelaTools\Models\BasicModel;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

interface ApplicationControllerContract
{

    function __construct();

    /**
     * Index view. Show list of items.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    //function index();

    /**
     * Index Query build
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder|BasicModel
     */
    function getConsultaPersonalizada(Request $request);

    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    function personalizaLista($jsonItem);

    /**
     * Create item view.
     *
     * @param mixed $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    //function create($request);

    /**
     * Store a newly created resource in storage.
     *
     * @param mixed $request
     * @return JsonResponse
     * @throws Exception
     */
    //function store($request);

    /**
     * Show the form for editing the specified resource.
     *
     * @param mixed   $request
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    //function edit($request, $id);

    /**
     * Update the specified resource in storage.
     *
     * @param mixed   $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    //function update($request, int $id);
}
