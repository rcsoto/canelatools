@php
use Canela\CanelaTools\Enums\MessageType;

/** @var string $type */
/** @var string $msgSuccess */
/** @var string $msgInfo */
/** @var string $msgWarning */
switch($type) {
    case MessageType::SUCCESS:
        $class      = 'success';
        $icon       = 'fa-check-circle';
        $title	    = trans('canelatools::canelatools.general.label.success');
        $message    = Session::has(MessageType::SUCCESS)
                                ? Session::get(MessageType::SUCCESS)
                                : $msgSuccess;
        break;
    case MessageType::INFO:
        $class      = 'info';
        $icon       = 'fa-info-circle';
        $title      = trans('canelatools::canelatools.general.label.information');
        $message    = Session::has(MessageType::INFO)
                                ? Session::get(MessageType::INFO)
                                : $msgInfo;
        break;
    case MessageType::WARNING:
        $class      = 'warning';
        $icon       = 'fa-exclamation-circle';
        $title      = trans('canelatools::canelatools.general.label.attention');
        $message    = Session::has(MessageType::WARNING)
                                ? Session::get(MessageType::WARNING)
                                : $msgWarning;
        break;
    case MessageType::ERROR:
        $class      = 'danger';
        $icon       = 'fa-exclamation-triangle';
        $title      = trans('canelatools::canelatools.general.label.attention');
        $message    = Session::has(MessageType::ERROR)
                                ? Session::get(MessageType::ERROR)
                                : (!empty($msgError) ? $msgError : $errors->all());
        break;
}
@endphp

@section('body-scripts')
@parent

@if($showPanel)
<script type="text/javascript">
	jQuery(window).on("load", function() {
		@php($msg = is_array($message) ? implode("<br/>", $message) : $message)
		APP.showMessage('{{$type}}', '{!! $msg !!}');
	});
</script>

{{--<div class="row">--}}
{{--	<div id="panel-message" class="hidden col-lg-12 animated visible" data-animate="">--}}
{{--		<div role="alert" class="alert alert-{{$class}} alert-dismissible">--}}
{{--			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button>--}}
{{--			<h4><i class="fa {{$icon}}"></i> {{$title}}</h4>--}}
{{--			@if(is_array($message))--}}
{{--				@foreach ($message as $msg)--}}
{{--				<div class="m-r-10">{!! $msg !!}</div>--}}
{{--				@endforeach--}}
{{--			@else--}}
{{--				<div class="m-r-10">{!! $message !!}</div>--}}
{{--			@endif--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

@endif

@if($showNoty)
<script type="text/javascript">

	jQuery(window).on("load", function() {
		const animations = jQuery.getAnimations();
		let animation = animations[Math.floor(Math.random() * animations.length)];
		jQuery("#panel-message").addClass(animation).data('animate', animation);

		@if($type == MessageType::SUCCESS)
			jQuery.messageSuccess("{!! $message !!}", 3000);
		@elseif($type == MessageType::INFO)
			jQuery.messageInfo("{!! $message !!}", 3000);
		@elseif($type == MessageType::WARNING)
			jQuery.messageWarning("{!! $message !!}", 3000);
		@elseif($type == MessageType::ERROR)
			@php($msg = is_array($message) ? implode("<br/>", $message) : $message)
			jQuery.messageError("{!! $msg !!}");
		@endif

	});
</script>
@endif

@endsection
