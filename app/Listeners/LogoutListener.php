<?php

namespace Canela\CanelaTools\Listeners;

use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\HistoryLog;
use Canela\CanelaTools\Models\Module;
use Canela\CanelaTools\Models\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogoutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        HistoryLog::log(entityName: User::getTableName(), moduleId: Module::WEB, actionId: Action::LOGOUT, userId: $event->user->id, saveValues: false);
    }
}
