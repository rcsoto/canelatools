<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class GenericMail
 * @package App\Mails
 */
class GenericMail  extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

}