<?php

namespace Canela\CanelaTools\Manager\SecurityManager;


/**
 * Interface SecurityManagerInterface
 * @package Canela\CanelaTools\Manager\SecurityManager
 */
interface SecurityManagerInterface
{
    /**
     * Establece la seguridad de una consulta para datos de una lista
     * @param $query
     * @return mixed
     */
    public static function setSecurityList($query);

}
