<?php namespace Canela\CanelaTools\Models\AttributeFree;


use Canela\CanelaTools\Models\ModelCatalog;


/**
 * Canela\CanelaTools\Models\AttributeFree\AttributeFreeType
 *
 * @property int                 $id
 * @property string              $name
 * @property string|null         $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string         $created_at_format
 * @property-read string         $updated_at_format
 * @property-read string         $table
 * @property-read string         $combo_identificator
 * @property-read string         $value_identificator
 * @property-read string         $created_at_short
 * @property-read string         $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Illuminate\Support\Collection comboListAttribute($area_id = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @mixin \Eloquent
 */
class AttributeFreeType extends ModelCatalog
{
    // Constantes
    const TEXTO_CORTO = 1; // 'Alfanumerico. Caracteres'
    const ENTERO      = 2; // 'Numero sin decimales'
    const DECIMAL     = 3; // 'Numero con decimales'
    const FECHA       = 4; // 'Fecha'
    const FECHA_HORA  = 5; // 'Fecha y Hora'
    const HORA        = 6; // 'Hora'
    const BOOLEANO    = 7; // 'Valor Si/No'
    const VALORACION  = 8; // 'Determina una valoracion'
    const LISTA       = 9; // 'Lista de valores'
    const MULTIMEDIA  = 10; // 'Archivo'
    const TEXTO_LARGO = 11; // 'Archivo'


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_free_type';


}
