<?php

namespace Canela\CanelaTools\Controller;


use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use ReflectionClass;

trait DeletableController
{
    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $model_id
     * @return JsonResponse|void
     * @throws Exception
     */
    public function destroy(Request $request, int $model_id)
    {
        if ($request->ajax()) {
            try {
                /** @var Model $model */
                $model = ($this->entidad)::findOrFail($model_id);

                $reflectionClass = new ReflectionClass($model);
                if ($reflectionClass->implementsInterface('Canela\CanelaTools\Contracts\DeletableContract')) {
                    $relationtables = [];
                    if ($model->canDelete($relationtables) || $request->get('force', false)) {
                        DB::beginTransaction();

                        if (method_exists($this, 'destroyPre')) {
                            $this->destroyPre($model);
                        }
                        $result = $model->delete();

                        DB::commit();
                        return response()->json([
                                                    'result' => $result,
                                                    'delete' => true,
                                                    'msg'    => trans('canelatools::canelatools.model.delete.correct', ['value' => $model->value_identificator]),
                                                ]);
                    } else {
                        return response()->json([
                                                    'result' => true,
                                                    'delete' => false,
                                                    'tables' => implode(', ', $relationtables),
                                                    'msg'    => trans('canelatools::canelatools.model.delete.error'),
                                                ]);
                    }
                } else {
                    return response()->json([
                                                'result' => false,
                                                'msg'    => 'Model does not use Deletable trait',
                                            ]);
                }
            } catch (Exception $exception) {
                DB::rollBack();
                return response()->json([
                                            'result' => false,
                                            'msg'    => config('app.debug')
                                                ? $exception->getMessage()
                                                : trans('canelatools::canelatools.model.delete.error'),
                                        ]);
            }
        } else {
            abort(550, trans('canelatools::canelatools.ajax.unsupported'));
        }
    }
}
