<?php

namespace Canela\CanelaTools\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

abstract class GenericRequest extends FormRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [];


    /**
     * @var string|null
     * Stores request entity when adding/editing model throw form. Null otherwise.
     */
    protected ?string $entity = null;


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        if (!is_null($this->entity)) {
            $this->transPath = ($this->entity)::getResourceTransPath('field.');
        }
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // se comprueba que este logado
        return Auth::check();
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }


    /**
     * Get rules.
     *
     * @return array
     */
    public static function getRules()
    {
        $args = func_num_args();
        if ($args == 0) {
            return (new static)->rules();
        } elseif ($args == 1) {
            return (new static)->rules(func_get_args()[0]);
        } else {
            return [];
        }
    }


    /**
     * Get attributes.
     *
     * @return array
     */
    public static function getAttributes()
    {
        return (new static)->attributes();
    }


    /**
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidatorInstance()
    {
        return parent::getValidatorInstance();
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = [];
        foreach($this->fields as $field) {
            $attributes[$field] = trans($this->transPath.$field);
        }
        return $attributes;
    }

}
