<?php

namespace Canela\CanelaTools\Console;

use Illuminate\Console\Command;

class DocsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate docs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commands = collect([
                                'ide-helper:eloquent',
                                'ide-helper:meta',
                                'ide-helper:generate',
                                'ide-helper:models -W',


                            ]);

        $this->info('Generando documentacion interna');
        $bar = $this->output->createProgressBar($commands->count());
        $bar->start();

        $commands->each(function ($command) use (&$bar) {
            \Artisan::call($command);
            $bar->advance();
        });

        $bar->finish();

        /*
        if ($this->confirm('', false)) {
           //
        }
        */

        $this->info(' ');
        $this->info('Finish Docs');

        return true;
    }

}
