@if(!empty($registro))
    <div class="card">
        <div class="card-block">
            <div class="row">
                @if(!empty($registro->state))
                    <div class="col-sm-4 form-group">
                        <p>Estado</p>
                        <p class="form-control input-no-edit pl-2 pr-2">{{ $registro->state->name }}</p>
                    </div>
                @endif
                <div class="col-sm-4 form-group">
                    <p>Fecha de alta</p>
                    <p class="form-control input-no-edit text-right pl-2 pr-2">{{ $registro->createdAtFormat }}</p>
                </div>
                <div class="col-sm-4 form-group">
                    <p>Fecha última modificacion</p>
                    <p class="form-control input-no-edit text-right pl-2 pr-2">{{ $registro->updatedAtFormat }}</p>
                </div>
            </div>
        </div>
    </div>
@endif
