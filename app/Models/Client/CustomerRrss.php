<?php namespace Canela\CanelaTools\Models\Client;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\Client\Customer;


class CustomerRrss extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_rrss';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'rrss_id',
        'name',
        'url',
        'position',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'customer_id'   => 'int',
        'rrss_id'       => 'int',
        'name'          => 'string',
        'url'           => 'string',
        'position'      => 'int',
    ];

    protected $with = ['rrss'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rrss() {
        return $this->belongsTo(Rrss::class, 'rrss_id');
    }
}
