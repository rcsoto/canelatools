<?php

namespace Canela\CanelaTools\Requests\General;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class ListOfGenericIdsRequest
 * @package Canela\CanelaTools\Requests\General
 */
class ListOfGenericIdsRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ids'           => 'required|array|min:1',
            'ids.*'         => 'required|integer|min:1',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'ids.*'   => 'Seleccione al menos un elemento.',
            'ids.*.*'   => 'Ids con formato incorrecto.',
        ];
    }


}
