<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageNoticeRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageNoticeArchiveRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'notice_id',
        'archive_id',
        'position',
        'title',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_notice_archive.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return array(
            'notice_id'         => 'required|integer|min:1|exists:web_page_notice,id',
            'archive_id'        => 'nullable|integer|min:1|exists:archive,id',
            'position'          => 'nullable|integer|min:0|max:999999',
            'title'             => 'nullable|max:255',
        );
    }

}
