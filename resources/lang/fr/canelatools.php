<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Étiquettes générales
     |--------------------------------------------------------------------------
     |
     | canela
     |
     */

    // GÉNÉRAL
    'general' => [

        'message'  => [
            'send_correct' => 'Message envoyé correctement',
            'send_error'   => "Erreur d'envoi de message",
        ],

        'actions'  => [
            'exit'          => 'Sortir',
            'logout'        => 'Se déconnecter',
            'edit'          => 'Éditer',
            'select'        => 'Consulter',
            'save'          => 'Sauvegarder',
            'disabled'      => 'Désactiver',
            'enabled'       => 'Restaurer',
            'send'          => 'Envoyer',
        ],
        'label'    => [
            'attention'     => 'Attention!',
            'information'   => 'Information',
            'success'       => 'Demande traitée correctement',
            'trace_new'     => 'Inscription de :model_name',
            'trace_update'  => 'Édition de :model_name',
            'date'          => "Date d'inscription",
            'last_date'     => 'Date dernière modification',
            'loading'       => 'Chargement ...',
            'select'        => 'Sélectionner',
            'select_items_and_click_continue' => 'Sélectionnez les éléments et appuyez sur continuer.',
            'cookies-policy' => 'Politique de cookies',
            'legal-warning' => 'Avis légal',
            'data-protection' => 'Protection des données',
            'privacy-policy' => 'Politique de confidentialité',
            'read_more' => ' ...en savoir plus',
            'accept'        => 'Accepter',
            'accept-privacy-policy' => 'Accepter Politique de confidentialité',
            'have-read-and-accept-privacy-policy' => "J'ai lu et j'accepte la <a href=':route' target='_blank'>Politique de confidentialitéd</a>",
            'must-accept-privacy-policy' => 'Vous devez accepter la politique de confidentialité.',
            'query' => 'Consultation',
        ],
    ],


    // Authentification
    'auth'    => [
        'email' => [
            'change_password' => 'Changer le mot de passe',
            'back_login'      => 'Retour au login',
            'send_reminder'   => 'Envoyer un rappel',
        ],
        'login' => [
            'init'               => 'Connexion ',
            'remember'           => 'Mémoriser',
            'forgotten_password' => 'Vous avez oublié votre mot de passe ?',
            'enter'              => 'ENTRER',
        ],
    ],


    /*
     |--------------------------------------------------------------------------
     | Étiquettes générales
     |--------------------------------------------------------------------------
     |
     | général
     |
     */

    'bool'       => [
        'yes' => 'Oui',
        'no'  => 'Non',
    ],
    'ajax'       => [
        'unsupported' => 'Opération non prise en charge',
    ],
    'model'      => [
        'list'               => ['correct' => 'Registres répertoriés correctement.',
                                 'error'   => "Une erreur s'est produite lors du répertoire des registres.",],
        'update'             => ['correct' => ':valeur mise à jour correctement.',
                                 'error'   => "Une erreur s'est produite lors de la mise à jour du registre",],
        'store'              => ['correct' => ':valeur créée correctement.',
                                 'error'   => "Une erreur s'est produite lors de la création du registre",],
        'find'               => ['error' => 'Registre non trouvé',],
        'delete'             => ['correct' => ':valeur éliminé correctement.',
                                 'error'   => "Une erreur s'est produite lors de la suppression du registre",],
        'copy'               => ['correct' => ':valeur dupliquée correctement.',
                                 'error'   => "Une erreur s'est produite lors de la duplication du registre",],
        'alignment'          => ['center' => 'Centré',
                                 'left'   => 'Gauche',
                                 'right'  => 'Droit',],
        'vertical_alignment' => ['medium' => 'Moyen',
                                 'bottom' => 'Inférieur/normal',
                                 'top'    => 'Exposant',],
        'reorder'            => ['reorder_ok'    => 'Il a été réordonné correctement.',
                                 'reorder_error' => 'Il y a eu un problème de réorganisation.',],
        'update_status'      => ['correct' => "L'état a été modifié correctement.",
                                 'error'   => "Un problème a été rencontré lors de la mise à jour de l'état.",],
        'fusion'             => ['correct' => 'Les registres ont été fusionnés correctement.',
                                 'error'   => 'Un problème a été rencontré lors de la fusion des registres.',],
        'generic_error'      => "Une erreur s'est produite. Veuillez recommencer l'opération. Si l'erreur persiste, veuillez contacter l'équipe d'assistance.",
    ],
    'file'       => [
        'upload_ok'    => 'Fichier chargé correctement.',
        'upload_error' => "Une erreur s'est produite lors du chargement du fichier.",
    ],
    'buttons'    => [
        'accept'        => 'Accepter',
        'approve'       => 'Approuver',
        'back'          => 'Retourner',
        'cancel'        => 'Annuler',
        'continue'      => 'Continuer',
        'close'         => 'Fermer',
        'copy'          => 'Copier',
        'delete'        => 'Éliminer',
        'edit'          => 'Editer',
        'new'           => 'Nouveau',
        'preview'       => 'Prévisualiser',
        'refuse'        => 'Refuser',
        'save'          => 'Sauvegarder',
        'send'          => 'Envoyer',
        'show'          => 'Consultation',
        'update'        => 'Mettre à jour',
        'activate'      => 'Activer',
        'deactivate'    => 'Désactivation',
        'disable'       => 'Désactiver',
        'execute'       => 'Exécuter',
        'upload'        => 'Charger',
        'download'      => 'Télécharger',
        'share'         => 'Partager',
        'prev'          => 'Précédent',
        'next'          => 'Suivant',
    ],

    'select'     => [
        'placeholder' => 'Sélectionnez une valeur ...',
    ],


    /*
     |--------------------------------------------------------------------------
     | Page ERREURS
     |--------------------------------------------------------------------------
     */
    '401' => [
        'title' => 'Non autorisé',
        'message' => "Vous n'êtes pas autorisé à accéder à ce registre.",
    ],
    '403' => [
        'title' => 'Sans permission',
        'message' => "Vous n'avez pas la permission d'accéder à ce registre.",
    ],
    '404' => [
        'title' => 'Page non trouvée',
        'message' => "Il semble que cette page n'existe pas ou a été supprimée.",
    ],
    '419' => [
        'title' => 'Page a expiré',
        'message' => "La page a expiré. Vos informations d'identification ont expiré.",
    ],
    '429' => [
        'title' => 'Trop de demandes.',
        'message' => 'Trop de demandes.',
    ],
    '500' => [
        'title' => 'Erreur/Incident',
        'message' => "Une erreur s'est produite sur le serveur. Veuillez réessayer. Si l'erreur persiste, veuillez contacter notre équipe d'assistance.",
    ],
    '503' => [
        'title' => 'Entretien',
        'message' => 'Entretien en cours. Nous serons de retour dans quelques minutes. Merci.',
    ],
    '550' => [
        'title' => 'Erreur/Incident',
        'message' => "Une erreur s'est produite sur le serveur. Veuillez réessayer. Si l'erreur persiste, veuillez contacter notre équipe d'assistance.",
    ],

    /*
     |--------------------------------------------------------------------------
     |Mise à jour du navigateur
     |--------------------------------------------------------------------------
     */

    'updateBrowser' => [
        'title'                  => ':project Navigateur obsolète',
        'obsolete_browser'       => 'Si vous visualisez cette page, cela veut dire que votre navigateur est obsolète',
        'update_version_browser' => 'Pour votre sécurité et pour visualiser au mieux les pages web modernes, nous vous recommandons de passer à la dernière version de votre navigateur préféré.',
        'important'              => 'Important',
        'browser_funcionality'   => 'Vérifiez si la fonctionnalité est activée sur votre navigateur',
        'compability_view'       => 'Vue de compatibilité',
        'disable'                => "Si c'est le cas, vous devez le désactiver afin de pouvoir visualiser la page.",
        'more_information'       => "Plus d'informations ici",
        'google_chrome'          => 'Google Chrome',
        'firefox'                => 'Firefox',
        'explorer'               => 'Microsoft Edge',
        'update_visit'           => "Après la mise à jour, n'oubliez pas de revenir nous voir!",
        'rights_reserved'        => 'TOUS DROITS RÉSERVÉS',
        'show_image'             => "Montrer l'image",
        'url_chrome'             => 'https://www.google.com/intl/es/chrome/browser/?hl=es',
        'url_firefox'            => 'http://www.mozilla.org/es-ES/firefox/fx/',
        'url_edge'               => 'https://support.microsoft.com/es-es/help/4501095/download-the-new-microsoft-edge-based-on-chromium',
    ],


];
