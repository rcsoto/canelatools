@extends('canelatools::administrator.layouts.main')

@section('breadcrumbs')
    <div class="row align-items-center">
        <div class="col-md-12">
            <div class="page-header-title">
                <h5 class="m-b-10">Dashboard Analytics</h5>
            </div>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Dashboard Analytics</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="card">
        <div class="card-block">

            Dashboard

        </div>
    </div>

@endsection

@section('body-scripts')
    @parent
    <!-- custom-chart js -->
{{--    <script src="{{asset('js/template-pages/dashboard-main.js')}}"></script>--}}
@endsection
