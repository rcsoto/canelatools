<?php

namespace Canela\CanelaTools\Console;

use Artisan;
use Illuminate\Console\Command;

/**
 * Class ClearAllCommand
 * @package App\Console\Commands
 */
class ClearAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clear all caches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commands = collect([
                                'clear-compiled',
                                'cache:clear',
                                'route:clear',
                                'config:clear',
                                'view:clear',
//                                'optimize:clear',
                            ]);

        $bar = $this->output->createProgressBar($commands->count());

        $this->info('Server in maintenance');
        Artisan::call('down');

        $this->line('Deleting all caches');
        $bar->start();

        $commands->each(function ($command) use (&$bar) {
            Artisan::call($command);
            $bar->advance();
        });

        $bar->finish();

        /*
        if ($this->confirm('', false)) {
           //
        }
        */

        $this->info(' ');
        $this->info('Server up');
        Artisan::call('up');

        $this->info('Finish Deleting');

        return true;
    }
}
