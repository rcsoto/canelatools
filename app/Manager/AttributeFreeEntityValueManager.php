<?php namespace Canela\CanelaTools\Manager;

use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity;
use Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntityValue;
use Canela\CanelaTools\Models\AttributeFree\AttributeFreeType;
use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\HistoryLog;

class AttributeFreeEntityValueManager  {

	
	const PREFIJO_AFEV 			= 'prefafev';
	const ENTIDAD_DESTINO		= 'entidaddestino';
	const ENTIDAD_DESTINO_ID 	= 'entidaddestinoid';
	
	
	/**
	 * Funcion para guardar el valor de los atributos libres asociados a una Entidad
	 * Como parametros se han de pasar todos los atributos mas:
	 * - La entidad Destino en un campo denominado "entidaddestino"
	 * @param Request $request
	 */
	public static function storeValues($request, $moduloId)
	{
		$msg 		= null;
		$error 	 	= false;
		$errores 	= [];
		$registro 	= null;
		// usario logado ?
		$usuario = (Auth::check() ? Auth::user() : null);
		
		// inicuar guardado de datos
		try {
			// Start transaction!
			DB::beginTransaction ();
		
			// entidad destino
			$entidadName 	= null;
			if ($request->has(self::ENTIDAD_DESTINO) && $request->has(self::ENTIDAD_DESTINO_ID))
			{	
				$entidadName 	= $request->get(self::ENTIDAD_DESTINO);
				$entidadId 		= $request->get(self::ENTIDAD_DESTINO_ID);
				$registro		= DB::table($entidadName)->where('id', $entidadId)->first();
			}
			if (empty($registro))
			{
				$msg 	= "No se ha proporcionado la entidad. ".trans('tango.error.contacto');
				$error 	= true;
			}
			
			// CONTINUAR
		
			foreach ($request->all() as $key => $value)
			{
				// es un atributo
				if (substr($key, 0, strlen(self::PREFIJO_AFEV)) == self::PREFIJO_AFEV)
				{
					/*
					$atributoId = substr($key, strlen(self::PREFIJO_AFEV));
					$atributoFree 		= AttributeFree::find($atributoId);
					$atributoEntidad 	= AttributeFreeEntity::get();
					*/ 
					$atributoEntidadId  = substr($key, strlen(self::PREFIJO_AFEV));
					$atributoEntidad 	= AttributeFreeEntity::find($atributoEntidadId);
					$atributoFree 		= $atributoEntidad->attribute;
					$atributoId			= $atributoFree->id;
					
					// Validaciones
					$errores = self::validateValor($atributoId, $atributoEntidad, $atributoFree, $value, $errores);
					
					// Guardar valores
					$entityDestino = Entity::getEntityByName($entidadName);
					
					$atributoEntidadValor = AttributeFreeEntityValue::updateOrCreate([
							'attribute_free_id'			=> $atributoId,
							'entity_id'					=> $entityDestino->id,
							'register_id'				=> $registro->id,
					],[
							//'attribute_free_id'			=> $atributoId,
							//'entity_id'					=> $entityDestino->id,
							//'register_id'				=> 
							'attribute_free_list_id'	=> (empty($value) && $value == "" ? null : (AttributeFreeType::LISTA == $atributoFree->type_id ? $value : null)),
							'value'						=> (empty($value) && $value == "" ? null : (AttributeFreeType::LISTA == $atributoFree->type_id ? null : $value)),
					]);
					$atributoEntidadValor->save();
				}
				
				// Guardar en el Log la operacion de registro de atributos
				HistoryLog::log($entidadName, $moduloId, Action::UPDATE, (empty($usuario) ? null : $usuario->id), null, null, 'Actualización de atributos', false);
				
			}
			
		}
		/* ********************************************************************
		* * CATCH
		* ********************************************************************/
		catch ( \Exception $e ) {
			$msg 	= $e->getMessage ();
			$error 	= true;
			Log::error($msg);
			DB::rollback ();
		}
	
		// RETURN 
		if ($error || !empty($errores))
		{
			$msg = empty($errores) ? $msg : "Revise los campos indicados";
			
			return array ('result' => 0, 'msg' => $msg, 'errores' => $errores, 'registro' => $registro );
		}
		else
		{
			// finalizar correcto
			DB::commit ();
			return array ('result' => 1, 'msg' => $msg, 'errores' => $errores, 'registro' => $registro );
		}
	}
	
	
	/**
	 * Validar el valor proporcionado para el atributo
	 * @param int $atributoEntidad
	 * @param AttributeFreeEntity $atributoFree
	 * @param array $value
	 */
	private static function validateValor($atributoId, $atributoEntidad, $atributoFree, $value, $errores)
	{
		// ogligatorio ?
		if ($atributoEntidad->required == 1 && strlen($value) == 0)
		{
			$errores[$atributoId] = str_replace(':attribute', $atributoFree->name, trans('validation.required')); 
		}
		// si esta informado
		else if (!empty($value))
		{
			// segun tipo de dato
			if ($atributoFree->type_id == AttributeFreeType::ENTERO || $atributoFree->type_id == AttributeFreeType::VALORACION)
			{
				if (!is_numeric($value) || ($value - (int) $value != 0))
				{
					$errores[$atributoId] = str_replace(':attribute', $atributoFree->name, trans('validation.integer'));
				}
			}
			else if ($atributoFree->type_id == AttributeFreeType::DECIMAL)
			{
				if (!is_numeric($value))
				{
					$errores[$atributoId] = str_replace(':attribute', $atributoFree->name, trans('validation.numeric'));
				}	
			}
			else if ($atributoFree->type_id == AttributeFreeType::BOOLEANO)
			{
				if ($value != 'true' && $value != 'false')
				{	
					$errores[$atributoId] = str_replace(':attribute', $atributoFree->name, trans('validation.boolean'));
				}
			}	
			
			// segun valor
			if ($atributoFree->type_id == AttributeFreeType::ENTERO || 
					$atributoFree->type_id == AttributeFreeType::VALORACION ||
					$atributoFree->type_id == AttributeFreeType::DECIMAL ||
					$atributoFree->type_id == AttributeFreeType::TEXTO_CORTO ||
					$atributoFree->type_id == AttributeFreeType::TEXTO_LARGO)
			{
				$validationType = 'numeric';
				$valueValidation = $value;
				if ($atributoFree->type_id == AttributeFreeType::TEXTO_CORTO ||
						$atributoFree->type_id == AttributeFreeType::TEXTO_LARGO)
				{	
					$validationType 	= 'string';
					$valueValidation	= strlen($value);
				}	
				
				if (!empty($atributoFree->min_value) && !empty($atributoFree->max_value))
				{
					if ($valueValidation< $atributoFree->min_value || $valueValidation> $atributoFree->max_value)
					{	
						$errores[$atributoId] = str_replace(':max', str_replace(':min', str_replace(':attribute', $atributoFree->name, trans('validation.between.'.$validationType)), 
													$atributoFree->min_value), $atributoFree->max_value);
					}
				}
				else if (!empty($atributoFree->min_value))
				{	
					if ($valueValidation< $atributoFree->min_value)
					{
						$errores[$atributoId] = str_replace(':min', str_replace(':attribute', $atributoFree->name, trans('validation.min.'.$validationType)),
								$atributoFree->min_value);
					}
				}
				else if (!empty($atributoFree->max_value))
				{
					if ($valueValidation> $atributoFree->max_value)
					{
						$errores[$atributoId] = str_replace(':max', str_replace(':attribute', $atributoFree->name, trans('validation.max.'.$validationType)),
								$atributoFree->max_value);
					}
				}
			}

		}

		return $errores;
	}
	
	
	
	
	/**
	 * 
	 * @param boolean $request
	 */
	public static function storeImage($request)
	{
/*	    
		// entidad destino
		$entidadName 	= null;
		if ($request->has(self::ENTIDAD_DESTINO) && $request->has(self::ENTIDAD_DESTINO_ID))
		{
			$entidadName 	= $request->get(self::ENTIDAD_DESTINO);
			$entidadId 		= $request->get(self::ENTIDAD_DESTINO_ID);
			$registro		= DB::table($entidadName)->where('id', $entidadId)->first();
		}
		if (empty($registro))
		{
			$msg 	= "No se ha proporcionado la entidad. ".trans('tango.error.contacto');
			$error 	= true;
		}

		// CONTINUAR
		
		$nameFile = UtiltsFile::saveArchive($request, $entidadName, $entidadId, 'file', Fecha::fechaHora().Utilidades::generarCodigoAleatorio(5), AttributeFreeEntityValue::DIR_MULTIMEDIA, false);
		// generar THUMBNAIL
		UtiltsFile::createThumbCenterCrop(public_path(AttributeFreeEntityValue::DIR_MULTIMEDIA.$nameFile),
											public_path(AttributeFreeEntityValue::DIR_MULTIMEDIA_THUMBNAIL.$nameFile),
										  	AttributeFreeEntityValue::THUMBNAIL_WIDTH, 
										  	AttributeFreeEntityValue::THUMBNAIL_HEIGHT);
		
		
		// es un atributo
		$atributoFree 			= AttributeFree::find($request->get('data-attribute-free-id'));
		$atributoId				= $atributoFree->id;
		$attributeFreeEntity 	= AttributeFreeEntity::find($request->get('data-attribute_free_entity_id'));
			
		// Guardar valores
		$entityDestino = Entity::getEntityByName($entidadName);
				
		// Multiple ?
		if ($attributeFreeEntity->multiple == 1)
		{
			// nuevo valor
			$atributoEntidadValor = new AttributeFreeEntityValue([
					'attribute_free_id'			=> $atributoId,
					'entity_id'					=> $entityDestino->id,
					'register_id'				=> $registro->id,
					'attribute_free_list_id'	=> null,
					'value'						=> $nameFile,
			]);
		}	
		else
		{	
			// un unico valor
			$atributoEntidadValor = AttributeFreeEntityValue::updateOrCreate([
					'attribute_free_id'			=> $atributoId,
					'entity_id'					=> $entityDestino->id,
					'register_id'				=> $registro->id,
			],[
					'attribute_free_list_id'	=> null,
					'value'						=> $nameFile,
			]);
		}
		$atributoEntidadValor->save();

		return true;
*/		
	}

	
	
	/**
	 * 
	 * @param integer $attributeId
	 * @param string $entityDestino
	 * @param integer $registreId
	 * @return string
	 */
	public static function getSqlToValue($attributeId, $entityDestino, $registreId, $etiqueta)
	{
		
		$consulta =
			" (select case attribute_free.type_id
				  	 	when ". AttributeFreeType::LISTA ." then (select attribute_free_list.name from attribute_free_list where attribute_free_list.id = attribute_free_entity_value.attribute_free_list_id)
						else attribute_free_entity_value.value
					  end as valor
			   from attribute_free_entity_value
					inner join attribute_free on attribute_free_entity_value.attribute_free_id = attribute_free.id
					inner join entity on attribute_free_entity_value.entity_id = entity.id
					left join attribute_free_classification on attribute_free.classification_id = attribute_free_classification.id
				where attribute_free.id = ". $attributeId."
			      and entity.name = '". $entityDestino ."'
			      and attribute_free_entity_value.register_id = ". $registreId ."
			 ) as '". $etiqueta ."' ";
		
		
		return $consulta;
	}
	
	
	
	/**
	 * 
	 * @param unknown $enyityName
	 * @param unknown $registerId
	 * @return unknown
	 */
	public static function getEntityValues($enyityName, $registerId)
	{
		$entity = Entity::getEntityByName($enyityName);
		
		return AttributeFreeEntityValue::where('entity_id', $entity->id)
										->where('register_id', $registerId)
										->get();
	}
}