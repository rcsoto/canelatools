<?php

namespace Canela\CanelaTools\Controller\Notification;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Models\General\NotificationWeb;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

/**
 * Class NotificationWebController
 * @package Canela\CanelaTools\Controller\Location
 */
class NotificationWebController extends BasicController
{

    /**
     * CityController constructor.
     */
    public function __construct()
    {
        $this->entidad = NotificationWeb::class;

        parent::__construct();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function addEndPoint(Request $request)
    {
        NotificationWeb::add($request->get('endpoint'));
        return Response::ajax(true, 1,'ok');
    }

}
