<?php

namespace Canela\CanelaTools\Providers;

use Canela\CanelaTools\Routing\CanelaToolsRouter;
use Canela\CanelaTools\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ToolsRouteServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRouter();
    }

    /**
     * Register the router instance.
     *
     * @return void
     */
    protected function registerRouter()
    {
        $this->app->singleton(CanelaToolsRouter::class, function ($app) {
            return new CanelaToolsRouter();
            // return new Router($app['events'], $app);
        });
    }
}
