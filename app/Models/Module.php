<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\Module
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string         $created_at_format
 * @property-read string         $updated_at_format
 * @property-read string         $table
 * @property-read mixed          $combo_identificator
 * @property-read string         $value_identificator
 * @property-read string         $created_at_short
 * @property-read string         $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Module query()
 * @mixin \Eloquent
 */
class Module extends BasicModel
{
    // constantes
    const WEB    = 1;    // Web
    const APP    = 2;    // Aplicacion movil
    const SERVER = 3;    // Servidor de Laravel


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'module';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
