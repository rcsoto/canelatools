<?php

namespace Canela\CanelaTools\Models\Table;

use Canela\CanelaTools\Enums\TableFilterTypes;
use Illuminate\Database\Eloquent\Scope;

class TableColumn
{
    /** @var string $orderByDefault Indica si la columna se ordena por defecto. Valores "ASC" o "DESC" */
    public $orderByDefault = '';

    /** @var bool $withOrderBy Indica si se ha de mostrar orden en la columna */
    public $withOrderBy = true;

    /** @var bool $withFilter Indica si se ha de mostrar diltro en la columna */
    public $withFilter = true;

    /** @var string $table Indica la tabla asociada a la columna */
    public $table;

    /** @var string $fieldOrder Indica el campo que se usa para la ordenacion */
    public $fieldOrder;

    /** @var string $fieldFilter Indica el campo que se usa para el filtrado. Si no se informa, se usa el mismo que la ordenacion */
    public $fieldFilter;

    /** @var string $label Etiqueta del campo */
    public $label = '#';

    /** @var Scope $comboList Informacion para los Select */
    public $comboList;

    /** @var string $type Tipo de la columna */
    public $type = TableFilterTypes::FILTRO_TIPO_TEXT;

    /** @var string $filterSql Especifica un filtro especial a ejecutar */
    public $filterSql = "";

    /**
     * TableColumn constructor.
     * @param string|null $table
     * @param string|null $fieldOrder
     * @param string|null $label
     * @param string|null $type
     * @param string|null $fieldFilter
     * @param mixed|null $comboList
     * @param bool|null $withOrderBy
     * @param bool|null $withFilter
     */
    public function __construct(
        ?string $table = null,
        ?string $fieldOrder = null,
        ?string $label = null,
        ?string $type = null,
        ?string $fieldFilter = null,
        mixed $comboList = null,
        ?bool $withOrderBy = null,
        ?bool $withFilter = null,
    )
    {
        if(!is_null($table))            $this->setTable($table);
        if(!is_null($fieldOrder))       $this->setFieldOrder($fieldOrder);
        if(!is_null($label))            $this->setLabel($label);
        if(!is_null($type))             $this->setType($type);
        if(!is_null($fieldFilter))      $this->setFieldFilter($fieldFilter);
        if(!is_null($comboList))        $this->setComboList($comboList);
        if(!is_null($withOrderBy))      $this->setWithOrderBy($withOrderBy);
        if(!is_null($withFilter))       $this->setWithFilter($withFilter);
    }


    /**
     * @param string $value
     * @return $this
     */
    public function setOrderByDefault(string $value): TableColumn
    {
        $this->orderByDefault = $value;
        return $this;
    }


    /**
     *
     * @param bool $value
     * @return TableColumn
     */
    public function setWithOrderBy(bool $value): TableColumn
    {
        $this->withOrderBy = $value;
        return $this;
    }


    /**
     *
     * @param bool $value
     * @return TableColumn
     */
    public function setWithFilter(bool $value): TableColumn
    {
        $this->withFilter = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setTable(string $value): TableColumn
    {
        $this->table = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setFieldOrder(string $value): TableColumn
    {
        $this->fieldOrder   = $value;
        $this->fieldFilter  = $this->fieldFilter ?? $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setFieldFilter(string $value): TableColumn
    {
        $this->fieldFilter = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setLabel(string $value): TableColumn
    {
        $this->label = $value;
        return $this;
    }


    /**
     *
     * @param $value
     * @return TableColumn
     */
    public function setComboList($value): TableColumn
    {
        $this->comboList = $value;
        $this->type = TableFilterTypes::FILTRO_TIPO_SELECT;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setType(string $value): TableColumn
    {
        $this->type = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableColumn
     */
    public function setFilterSql(string $value): TableColumn
    {
        $this->filterSql = $value;
        return $this;
    }

}
