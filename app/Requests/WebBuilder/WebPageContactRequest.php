<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageContactRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageContactRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'type_id',
        'position',
        'concept',
        'value',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_contact.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'type_id'           => 'required|integer|min:1|exists:web_page_contact_type,id',
            'position'          => 'required|integer|min:1|max:99999',
            'concept'           => 'required|max:45',
            'value'             => 'required|max:4000',
        );
        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');

        return $attributes;
    }

}
