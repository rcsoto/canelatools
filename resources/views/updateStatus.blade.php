﻿@php
    /** @var String $nameSectionModal nombre de la seccion donde se añaden los modales */
    $nameSectionModal = $nameSectionModal ?? (config('canelatools.blade.section.bodyModal') ?? config('canelatools::canelatools.blade.section.bodyModal'));

    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
@endphp
@section('body-modals')
    @parent

    {{-- Status Modal --}}
    <div class="modal fade" id="modalChangeStatusGeneral" tabindex="-1" role="dialog"
         aria-labelledby="modalChangeStatusGeneralLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalChangeStatusGeneralLabel">
                        {{trans("canelatools::canelatools.update_status")}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">
                    @if(isset($commentRequired) && $commentRequired)
                        <label class="label">
                            {{trans("canelatools.update_status_question")}}
                            <textarea class="form-control" rows="3" id="modal-cambio-estado-motivo-textarea"
                                      placeholder="Motivo"></textarea>
                        </label>
                    @else
                        {!! trans("canelatools::canelatools.update_status_dynamic") !!}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        @lang('canelatools::canelatools.buttons.cancel')</button>
                    <a type="button" class="btn btn-primary" data-action="submit">@lang('canelatools::canelatools.buttons.accept')</a>
                </div>
            </div>
        </div>
    </div>
    {{-- End status Modal --}}
@endsection

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">
        let commentRequired = '{{$commentRequired ?? false}}';

        $(document).ready(function () {
            loadStatusButtons();

            $("#modalChangeStatusGeneral").find('a[data-action="submit"]').click(function () {
                updateStatus();
            });

            $(document).on('beginChangeStatus', function (event, button) {
                //
            });

            $(document).on('endChangeStatus', function (event, data, button) {
                //
            });

            $(document).on('changeStatusError', function (event, data, button) {
                //
            });
        });

        /** load listeners **/
        function loadStatusButtons() {
            $("table").find("a[data-action='activate'], a[data-action='deactivate'], a[data-action='disable']")
                .each(function () {
                    configureButton($(this));
                });

            $("table a[data-action='activate']").click(function () {
                let modal = $('#modalChangeStatusGeneral');

                modal.find('.status-js').text('activar');
                modal.data('item-id', $(this).data('item-id'));
                modal.data('url', $(this).data('url'));
                modal.data('button', $(this));
                modal.modal('show');
            });

            $("table a[data-action='deactivate']").click(function () {
                let modal = $('#modalChangeStatusGeneral');

                modal.find('.status-js').text('desactivar');
                modal.data('item-id', $(this).data('item-id'));
                modal.data('url', $(this).data('url'));
                modal.data('button', $(this));
                modal.modal('show');
            });

            $("table a[data-action='disable']").click(function () {
                let modal = $('#modalChangeStatusGeneral');

                modal.find('.status-js').text('deshabilitar');
                modal.data('item-id', $(this).data('item-id'));
                modal.data('url', $(this).data('url'));
                modal.data('button', $(this));
                modal.modal('show');
            });
        }

        /** Configure buttons visibility **/
        function configureButton(button) {
            if (button.data('type') === parseInt('{{\Canela\CanelaTools\Models\BasicModel::TYPE_CHANGE_STATE}}')) {
                switch (button.data('status')) {
                    case parseInt('{{\Canela\CanelaTools\Models\State::ACTIVO}}'):
                        button.data('action') === 'activate'
                            ? button.hide()
                            : button.siblings("a[data-action='activate']:first").hide();

                        button.siblings("a[data-action='deactivate']:first").show();
                        break;
                    case parseInt('{{\Canela\CanelaTools\Models\State::BAJA}}'):
                        button.data('action') === 'deactivate'
                            ? button.hide()
                            : button.siblings("a[data-action='deactivate']:first").hide();

                        button.siblings("a[data-action='activate']:first").show();
                        break;
                }
            } else {
                switch (button.data('status')) {
                    case parseInt('{{\Canela\CanelaTools\Models\User\UserState::ACTIVO}}'):
                        button.data('action') === 'activate'
                            ? button.hide()
                            : button.siblings("a[data-action='activate']:first").hide();

                        button.siblings("a[data-action='deactivate']:first").show();
                        button.siblings("a[data-action='disable']:first").show();
                        break;
                    case parseInt('{{\Canela\CanelaTools\Models\User\UserState::BORRADO}}'):
                        button.data('action') === 'deactivate'
                            ? button.hide()
                            : button.siblings("a[data-action='deactivate']:first").hide();

                        button.siblings("a[data-action='activate']:first").show();
                        button.siblings("a[data-action='disable']:first").show();
                        break;
                    case parseInt('{{\Canela\CanelaTools\Models\User\UserState::DESHABILITADO}}'):
                        button.data('action') === 'disable'
                            ? button.hide()
                            : button.siblings("a[data-action='disable']:first").hide();

                        button.siblings("a[data-action='activate']:first").show();
                        button.siblings("a[data-action='deactivate']:first").show();
                        break;
                }
            }
        }

        /** update registry status **/
        function updateStatus() {
            let modal = $('#modalChangeStatusGeneral');
            let comment = modal.find('textarea');

            if (commentRequired && comment.val().length === 0) {
                jQuery.messageWarning("{{trans('canelatools::canelatools.comment_not_empty')}}", 3000);
                return false;
            } else {
                $.loadIni();
                let button = modal.data('button');
                $(document).trigger('beginChangeStatus', [button]);

                let formData = new FormData();
                formData.append('_token', window.Laravel.csrfToken);
                formData.append('_method', 'PUT');
                formData.append('comment', comment.val());

                $.ajax({
                    url: modal.data('url'),
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    dataType: "json",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    error: function (objeto1, descr_error, objeto2) {
                        $.loadEnd();

                        @if(config('app.debug'))
                        console.log(objeto1, descr_error, objeto2);
                        @endif

                        $.messageError("{{trans('canelatools::canelatools.model.generic_error')}}");
                    },
                    success: function (data) {
                        $.loadEnd();
                        modal.modal("hide");

                        if (data.result) {
                            jQuery("#column-state-name-" + button.data('item-id')).html(data.stateName);
                            button.data('status', data.stateId);
                            configureButton(button);

                            $.messageInfo(data.message, 3000);
                            $(document).trigger('endChangeStatus', [data, button]);

                            if (commentRequired) {
                                comment.val('');
                            }
                        } else {
                            $(document).trigger('changeStatusError', [data, button]);
                            $.messageWarning(data.message, 3000);
                        }
                    },
                });
            }
        }
    </script>
@stop
