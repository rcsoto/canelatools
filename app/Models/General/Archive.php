<?php namespace Canela\CanelaTools\Models\General;

use Canela\CanelaTools\Manager\LocalizationManager;
use Canela\CanelaTools\Models\BasicModel;
use App\Models\Project\WebBuilder\Web;
use App\Models\Project\WebBuilder\WebLanguage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use App\Models\General\ArchiveType;

class Archive extends BasicModel
{
    const DIR_ARCHIVES = 'project/archives/';
    const ENTITY_TABLE = 'archive';
    const ARCHIVE_FIELD_NAME = 'archive';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;

    public $comboIdentifierField = 'archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'available_in_main_language',
        'duration',
        self::ARCHIVE_FIELD_NAME,
        'title',
        'name_download',
        'description',
        'reference',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'type_id'                   => 'int',
        'available_in_main_language'=> 'bool',
        'duration'                  => 'int',
        self::ARCHIVE_FIELD_NAME    => 'string',
        'title'                     => 'string',
        'name_download'             => 'string',
        'description'               => 'string',
        'reference'                 => 'string',
    ];

    /**
     * type
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo {
        return $this->belongsTo(ArchiveType::class, 'type_id');
    }

    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages(Web::firstOrFail()->id);
    }


    /**
     * @param string $reference
     * @return Archive|null
     */
    public static function getByReference(string $reference): ?Archive
    {
        return static::where('reference', $reference)->first();
    }


    /**
     * @return string
     */
    public static function getCurrentLocaleDirArchives(): string
    {
        $currentLocale = session()->has(LocalizationManager::SESSION_LANG) ? session()->get(LocalizationManager::SESSION_LANG) : '';
        $currentLocaleIsMain = session()->has(LocalizationManager::SESSION_LANG_IS_MAIN) ? session()->get(LocalizationManager::SESSION_LANG_IS_MAIN) : true;
        return self::DIR_ARCHIVES.(!$currentLocaleIsMain ? $currentLocale.'/' : '');
    }


    /**
     * getIdtTranslateFieldAttribute
     *
     * @return string
     */
    public function getIdtTranslateFieldAttribute()
    {
        $result = '';
        if (!empty( $this->title)) {
            $result =  $this->title.' - ';
        }
        return $result.$this->archive;
    }
}
