<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\Calendar
 *
 * @property int                 $id
 * @property bool                is_festive
 * @property string              $name
 * @property \Carbon\Carbon      $date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string         $created_at_format
 * @property-read string         $table
 * @property-read string         $updated_at_format
 * @property-read mixed          $combo_identificator
 * @property-read string         $value_identificator
 * @property-read string         $created_at_short
 * @property-read string         $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereIsFestive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Calendar query()
 * @mixin \Eloquent
 */
class Calendar extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_festive',
        'date',
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'         => 'int',
        'is_festive' => 'boolean',
        'date'       => 'datetime',
        'name'       => 'string',
    ];
}