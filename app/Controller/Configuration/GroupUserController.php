<?php

namespace Canela\CanelaTools\Controller\Configuration;

use Canela\CanelaTools\Contracts\ApplicationControllerContract;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\User\GroupUser;
use Canela\CanelaTools\Models\User\User;
use Canela\CanelaTools\Requests\User\GroupUserRequest;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class GroupUserController
 * @package App\Http\Controllers\Configuration
 */
class GroupUserController extends BasicController implements ApplicationControllerContract
{
    use HtmlLists;

    /**
     * GroupUserController constructor.
     */
    public function __construct()
    {
        $this->menu       = AdminMenu::GROUP_USER;
        $this->menu_group = AdminMenu::GROUP_CONFIGURATION;

        $this->entidad    = GroupUser::class;
        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->routeEntity = 'admin.configuration.groupuser';

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.groupuser.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                (new TableColumn())->setTable('group_user')->setFieldOrder('id'),
                (new TableColumn())->setTable('group_user')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.groupuser.field.name')),
                (new TableColumn())->setTable('group_user')->setFieldOrder('description')->setLabel(trans('canelatools::canelatools.entity.groupuser.field.description')),
                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([
                (new TableForm())->setName('name')->setLabel(trans('canelatools::canelatools.entity.groupuser.field.name')),
                (new TableForm())->setName('description')->setLabel(trans('canelatools::canelatools.entity.groupuser.field.description')),
                (new TableForm())->setName('userids')->setLabel(trans('canelatools::canelatools.entity.groupuser.field.users'))
                    ->setComboListJson(User::ComboList(), true, 'selectedUsersIds')->setType(TableColumnTypes::SELECT2),
            ]);

        parent::__construct();
    }

    /**
     * Index Query build
     * @param Request $request
     * @return BasicModel|GroupUser|Builder
     */
    function getConsultaPersonalizada(Request $request)
    {
        return GroupUser::select([
            'group_user.id',
            'group_user.name',
            'group_user.description',
            'group_user.id as action_group_user_id',
        ]);
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            // Acciones
            switch ($key) {
                case 'action_group_user_id':
                    $txt = $this->addActionBtn($value, $this->routeEntity, null, true, true, true, true);
                    break;
                default:
                    $txt = $value;
                    break;
            }
            $fila[] = $txt;
        }
        return $fila;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GroupUserRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(GroupUserRequest $request)
    {
        return $this->storeBasic($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GroupUserRequest $request
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(GroupUserRequest $request, int $id)
    {
        return $this->updateBasic($request, $id);
    }


    /**
     * @param GroupUser $model
     * @param Request $request
     * @return GroupUser
     */
    public function updatePost($model, $request)
    {
        return $this->syncRelations($model, $request);
    }


    /**
     * @param GroupUser $model
     * @param Request $request
     * @return GroupUser
     */
    public function storePost($model, $request)
    {
        return $this->syncRelations($model, $request);
    }


    /**
     * @param GroupUser $groupUser
     * @param Request $request
     * @return GroupUser
     */
    private function syncRelations(GroupUser &$groupUser, Request $request): GroupUser
    {
        $userids = $request->filled('userids') ? array_map('intval', explode(',', $request->userids[0])) : [];
        $groupUser->users()->sync($userids);

        return $groupUser;
    }
}
