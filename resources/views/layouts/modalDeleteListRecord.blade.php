<!-- Modal DELETE-->
<div class="modal fade" id="modalDeleteRecord" tabindex="-1" role="dialog" aria-labelledby="modalDeleteRecord">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <h4 class="modal-title">{{trans("canelatools::canelatools.delete_record")}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {{trans("canelatools::canelatools.delete_record_question")}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    @lang('canelatools::canelatools.buttons.cancel')</button>
                <a type="button" class="btn btn-danger btn-delete-js">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;@lang('canelatools::canelatools.buttons.delete')
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Modal DELETE-->

<div class="modal fade" id="modalModelHasAssoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <h4 class="modal-title">{{trans("canelatools::canelatools.recorded_dependencies")}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {{trans("canelatools::canelatools.recorded_dependencies_tables")}}:
                <p class="c-red text-center warn-tables-js"></p>
                <span class="force-delete-js">{{trans("canelatools::canelatools.delete_recorded_dependencies")}}</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    @lang('canelatools::canelatools.buttons.cancel')</button>
                <a type="button" class="btn btn-danger btn-delete-js force-delete-js">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;{{trans("canelatools::canelatools.delete_anyway")}}
                </a>
            </div>
        </div>
    </div>
</div>
