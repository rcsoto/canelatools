<?php

namespace Canela\CanelaTools\Requests\Configuration;

use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class LanguageRequest
 * @package Canela\CanelaTools\Requests\Configuration
 */
class LanguageRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'name',
        'code',
        'code2',
        'reference',
        'position',
        'state_id',
    ];


    /**
     * @var string|null
     * Stores request entity when adding/editing model throw form. Null otherwise.
     */
    protected ?string $entity = Language::class;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
                'name'      => 'required|string|max:255|unique:language,name',
                'code'      => 'required|string|max:45|unique:language,code',
                'code2'     => 'required|string|max:45|unique:language,code2',
                'reference' => 'required|string|max:45|unique:language,reference',
                'position'  => 'required|int|min:1|max:999',
                'state_id'  => 'required|integer|min:1|exists:state,id',
        ];

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id                 = $this->route('language');
            $rules['name']      = $rules['name'] . ',' . $id;
            $rules['code']      = $rules['code'] . ',' . $id;
            $rules['code2']     = $rules['code2'] . ',' . $id;
            $rules['reference'] = $rules['reference'] . ',' . $id;
        }

        return $rules;
    }

}
