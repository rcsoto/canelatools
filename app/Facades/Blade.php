<?php

namespace Canela\CanelaTools\Facades;


use Canela\CanelaTools\BladeResolver;
use Canela\CanelaTools\HtmlLists;
use Illuminate\Support\Facades\Facade;

class Blade extends Facade
{
    use HtmlLists;

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return BladeResolver::class;
    }
}