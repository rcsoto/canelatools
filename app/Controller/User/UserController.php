<?php

namespace Canela\CanelaTools\Controller\User;

use App\Models\Client\Client;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\User\Profile;
use Canela\CanelaTools\Models\User\User;
use Canela\CanelaTools\Models\User\UserState;
use Canela\CanelaTools\Requests\User\UserRequest;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use stdClass;

/**
 * Class UserController
 * @package Canela\CanelaTools\Controller\User
 */
class UserController extends BasicController
{
    use HtmlLists;

    protected Client $client;


    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->entidad     = User::class;
        $this->routeEntity = 'admin.configuration.user';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::USERS;
        $this->menu_group = AdminMenu::GROUP_CONFIGURATION;

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.user.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.user.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: User::ENTITY_TABLE,
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: User::ENTITY_TABLE,
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.user.field.name'),
                ),
                new TableColumn(
                    table: User::ENTITY_TABLE,
                    fieldOrder: 'email',
                    label: trans('canelatools::canelatools.entity.user.field.email'),
                ),
                new TableColumn(
                    table: 'profile',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.user.field.profile_id'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: Profile::comboTranslate(),
                ),
                new TableColumn(
                    table: 'user_state',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.general.field.state'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: UserState::comboTranslate(),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'client_id',
                    required: true,
                    type: TableColumnTypes::HIDDEN,
                    value: $this->client->id,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.user.field.name'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'email',
                    label: trans('canelatools::canelatools.entity.user.field.email'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'profile_id',
                    label: trans('canelatools::canelatools.entity.user.field.profile_id'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: Profile::comboTranslate(),
                ),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: UserState::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = User::select([
            'user.id',
            'user.name',
            'user.email',
            'profile.name as profile_name',
            'user_state.name as state_name',
            'user.id as action_user_id',
            'user_state.id as state_id',
        ])
            ->join('profile', 'user.profile_id', 'profile.id')
            ->join('user_state', 'user.state_id', 'user_state.id')
            ->groupBy('user.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'action_user_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, false, false, true, false, true,
                        BasicModel::TYPE_CHANGE_USER_STATE, $jsonItem->state_id,
                        true, true);
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
                    break;
                default:
                    $txt = $value;
            }

            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create(\Illuminate\Http\Request $request): \Illuminate\Contracts\View\Factory|View
    {
        $this->tableRest->getTableFormItem('state_id')->setValue(UserState::DESHABILITADO);
        return parent::create($request);
    }


    /**
     * @param UserRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(UserRequest $request) {
        return $this->storeBasic($request);
    }


    /**
     * @param UserRequest $request
     * @param $id
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function update(UserRequest $request, $id) {
        return $this->updateBasic($request, $id);
    }

}
