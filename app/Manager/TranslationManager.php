<?php

namespace Canela\CanelaTools\Manager;


use Canela\CanelaTools\Models\Entity;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\General\ArchiveType;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Translation;
use Canela\CanelaTools\Requests\Configuration\GetSetDeleteTranslationRequest;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Log;
use Session;
use Storage;

/**
 * Class TranslationManager
 * @package App\Http\Manager
 */
class TranslationManager
{
    /**
     * Translate selected field.
     *
     * @param $model
     * @param string $tableName
     * @param string $attribute
     * @param string $default
     * @param int $rowId
     * @param string|null $lang
     * @param bool $returnNullWhenNotFound
     * @return string|null
     */
    public static function translate($model, string $tableName, string $attribute, string $default, int $rowId, ?string $lang = null, bool $returnNullWhenNotFound = false): ?string
    {
        $lang = $lang ?? Session::get(LocalizationManager::SESSION_LANG);
        $language = Language::where('code', $lang)->first();
        if ($language) {
            $translation = Translation::whereRegisterId($rowId)
                ->whereEntity($tableName)
                ->whereCode($attribute)
                ->whereLanguageId($language->id)
                ->first();

            if (is_null($translation)) {
                return $returnNullWhenNotFound ? null : $default;
            } else {
                if (is_a($model, Archive::class)) {
                    $archive = Archive::findOrFail($rowId);
                    if (!ArchiveType::isTextInputArchiveTypeById($archive->type_id) && $attribute == Archive::ARCHIVE_FIELD_NAME) {
                        $webLanguage = $language->webLanguage;
                        return ($webLanguage?->principal == true ? '' : $language->code.'/').$translation->value;
                    } else {
                        return $translation->value;
                    }
                } else {
                    return $translation->value;
                }
            }
        } else {
            return $returnNullWhenNotFound ? null : $default;
        }
    }


    /**
     * @param int $entity_id
     * @param int $register_id
     * @param string $attribute
     * @param string|null $lang
     * @return string|null
     */
    public static function getTranslation(int $entity_id, int $register_id, string $attribute, ?string $lang = null): ?string
    {
        $lang = $lang ?? Session::get(LocalizationManager::SESSION_LANG);
        $language = Language::where('code', $lang)->first();
        if ($language) {
            $translation = Translation::select(['translation.*'])
                                        ->join('language', 'translation.language_id', 'language.id')
                                        ->where('language.code', $lang)
                                        ->where('translation.entity_id', $entity_id)
                                        ->where('translation.register_id', $register_id)
                                        ->where('translation.code', $attribute)
                                        ->first();

            return $translation?->value;
        } else {
            return null;
        }
    }


    /**
     * setTranslation
     *
     * @param GetSetDeleteTranslationRequest $request
     * @return boolean
     */
    public static function setTranslation(GetSetDeleteTranslationRequest $request): bool
    {
        $entity_id = $request->entity_id;
        $entityReference = Entity::findOrFail($entity_id)->reference;
        $register_id = $request->register_id;
        $attribute = $request->attribute;
        $lang = $request->language;
        $archiveTypeId = $request->archive_type_id;
        $translation = $request->translation;

        $language = Language::where('code', $lang)->first();
        if ($language) {
            $model = Translation::where('language_id', $language->id)
                                ->where('entity_id', $entity_id)
                                ->where('register_id', $register_id)
                                ->where('code', $attribute)
                                ->first();

            $archivesLocaleDir = Archive::DIR_ARCHIVES.'/'.$lang;
            if (is_null($translation)) {
                if (!is_null($model)) {
                    if (!is_null($archiveTypeId)) {
                        // Delete translation archive if exists.
                        $path = $archivesLocaleDir.'/'.$model->value;
                        if (Storage::exists($path)) {
                            Storage::delete($path);
                        }
                    }

                    $model->delete();
                }
            } else {
                if (!is_null($archiveTypeId)) {
                    $archiveType = ArchiveType::findOrFail($archiveTypeId);

                    if ($model)
                    { // Delete previous archive.
                        $path = $archivesLocaleDir . '/' . $model->value;
                        if (Storage::exists($path)) {
                            Storage::delete($path);
                        }
                    }

                    if (!$archiveType->isTextInputArchiveType()) {
                        // Save translation archive.
                        $filename = $register_id.'-'.UtilsBasicManager::sanitizeFileNameWithExtension($translation->getClientOriginalName());
                        $translation->storeAs($archivesLocaleDir, $filename);

                        // Update value with filename.
                        $translation = $filename;
                    }
                }

                if (is_null($model)) {
                    Translation::create([
                        'language_id'   => $language->id,
                        'entity_id'     => $entity_id,
                        'register_id'   => $register_id,
                        'code'          => $attribute,
                        'entity'        => $entityReference,
                        'value'         => $translation,
                    ]);
                } else {
                    $model->update(['value' => $translation]);
                }
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     * @param GetSetDeleteTranslationRequest $request
     * @return bool
     */
    public static function deleteTranslation(GetSetDeleteTranslationRequest $request): bool {
        try {
            $entity_id = $request->entity_id;
            $archiveEntity = Entity::where('reference', Archive::ENTITY_TABLE)->firstOrFail();
            $register_id = $request->register_id;
            $attribute = $request->attribute;
            $lang = $request->language;

            $language = Language::where('code', $lang)->firstOrFail();
            $model = Translation::where('language_id', $language->id)
                                        ->where('entity_id', $entity_id)
                                        ->where('register_id', $register_id)
                                        ->where('code', $attribute)
                                        ->firstOrFail();
            if ($entity_id == $archiveEntity->id && $attribute == Archive::ARCHIVE_FIELD_NAME) {
                // Delete translation archive if exists.
                $archivesLocaleDir = Archive::DIR_ARCHIVES.'/'.$lang;
                $path = $archivesLocaleDir.'/'.$model->value;
                if (Storage::exists($path)) {
                    Storage::delete($path);
                }
            }

            $model->delete();

            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * @param int $entity_id
     * @param int $register_id
     * @return Translation[]|Collection
     */
    public static function getTranslatedLanguages(int $entity_id, int $register_id): Collection|array
    {
        return Translation::select(['language.id', 'language.code', DB::raw('translation.code as attribute')])
                            ->join('language', 'translation.language_id', 'language.id')
                            ->where('translation.entity_id', $entity_id)
                            ->where('translation.register_id', $register_id)
                            ->get();
    }

}
