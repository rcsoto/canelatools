<?php

namespace Canela\CanelaTools\Manager;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class UtilsManager
 * @package App\Manager
 */
class UtilsBasicManager
{

    /**
     * Construye un array de numeros aleatorios entre dos numeros
     *
     * @param int     $ini     Numero inicial
     * @param int     $end     Numero final
     * @param int     $lenList Cantidad de numero aleatorios a generar
     * @param boolean $repeat  Indica si se pueden repetir los numero
     * @return array
     */
    public static function listIndexRand($ini, $end, $lenList, $repeat = false)
    {
        $listNum = array();
        do {
            $numAle = rand($ini, $end);
            if ($repeat || !in_array($numAle, $listNum)) {
                $listNum[] = $numAle;
            }
        } while (count($listNum) < $lenList);

        return $listNum;
    }

    /**
     * Reemplaza todos los acentos por sus equivalentes sin ellos
     *
     * @param $string string la cadena a sanear
     *
     * @return mixed|string $string string saneada
     */
    public static function sanearString($string, $charReplace='-')
    {

        $string = trim($string);

        //Esta parte se encarga cambiar espacios por guiones
        $string = str_replace(
            array(" "),
            $charReplace,
            $string
        );

        // reemplazar acentos y letras especiales
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "~",
                  "#", "@", "|", "!", "\"",
                  "·", "$", "%", "&", "/",
                  "(", ")", "?", "'", "¡",
                  "¿", "[", "^", "`", "]",
                  "+", "}", "{", "¨", "´",
                  ">", "< ", ";", ",", ":"),
            '',
            $string
        );

        // saltos de linea
        $string = preg_replace('[\n|\r|\n\r]', $charReplace, $string);


        return $string;
    }


    /**
     * @param string $fileNameWithExtension
     * @return string
     */
    public static function sanitizeFileNameWithExtension(string $fileNameWithExtension)
    {
        $fileName = Str::beforeLast($fileNameWithExtension, '.');
        $extension = Str::afterLast($fileNameWithExtension, '.');
        return self::sanitizeFileName($fileName, $extension);
    }


    /**
     * Sanear el nombre de un archivo
     * @param string $fileName
     * @param string $extension
     * @return string
     */
    public static function sanitizeFileName(string $fileName, string $extension)
    {
        // quitamos la extension
        $fileName = str_replace(".".$extension, '', $fileName);
        // limpiar de caracteres especiales
        $fileName = self::sanearString($fileName);
        //Esta parte se encarga de eliminar cualquier caracter extraño
        $fileName = str_replace(".", '_', $fileName);
        // nombre mas extension
        return $fileName.".".$extension;
    }



    /**
     * Separa los elementos de un array por un separador, prefijo y sufijo dado
     *
     * @param array  $array
     * @param string $separador
     * @param string $prefijo
     * @param string $sufijo
     * @return string
     */
    public static function canelaSplit($array, $separador = null, $prefijo = null, $sufijo = null)
    {
        $cadena = "";
        $sep    = "";
        foreach ($array as $item) {
            $cadena .= $sep . $prefijo . $item . $sufijo;
            $sep    = $separador;
        }
        return $cadena;
    }

    /**
     * Convert to Bytes.
     *
     * @param string $value
     * @return bool|int|string
     */
    public static function convertBytes($value)
    {
        if (is_numeric($value))
            return $value;
        else {
            $value_length = strlen($value);
            $qty          = substr($value, 0, $value_length - 1);
            $unit         = strtolower(substr($value, $value_length - 1));
            switch ($unit) {
                case 'k':
                    $qty *= 1024;
                    break;
                case 'm':
                    $qty *= 1048576;
                    break;
                case 'g':
                    $qty *= 1073741824;
                    break;
            }
            return $qty;
        }
    }


    /**
     * Generar un string con caracteres aleatorios
     *
     * @param integer $tam
     * @return string
     */
    public static function generarCodigoAleatorio($tam = 10)
    {
        $valores = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $codigo  = "";
        for ($i = 0; $i < $tam; $i++)
            $codigo .= $valores[rand(0, strlen($valores) - 1)];

            return $codigo;
    }


    /**
     *
     * Enter description here ...
     * @param unknown_type $validator
     */
    public static function validatorMsgToString($validator)
    {
        $msgs = array();
        $messages = $validator->messages();
        foreach($messages->all() as $msg)
        {
            $msgs[] = $msg ;
        }
        $msg = implode("\n", array_unique($msgs));

        return $msg;
    }


    /**
     * Conviente un objeto stdObject a array, pero unicamente los valores
     * @param unknown $arr
     * @return unknown
     */
    public static function stdObject_to_array_values($arr)
    {
        $resultado = [];
        foreach ($arr as $key => $value)
        {
            $resultado[] = array_values(get_object_vars($value));
        }

        return $resultado;
    }


    /**
     *
     * Obtener la URL actual de la pagina
     */
    public static function curPageURL()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80")
        {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }


    /**
     * appendParametersToUrlQueryString
     *
     * @param Request $request
     * @param array $newParams
     *
     * @return string
     */
    public static function appendParametersToUrlQueryString(Request $request, array $newParams): string
    {
        $currentQuery = $request->query();
        $newQuery = array_merge($currentQuery, $newParams);
        return $request->fullUrlWithQuery($newQuery);
    }


    /**
     *
     * Convierte un array mutidimensional en un array de una dimension
     * @param unknown_type $array
     */
    public static function array_flatten($array)
    {
        if (!is_array($array))
        {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result = array_merge($result, array_flatten($value));
            }
            else
            {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Retorna el texto recortado a una longitud
     * @param $text
     * @param $textShowMore
     * @param $lengthTxt
     * @return false|mixed|string
     */
    public static function truncateTextShowMore($text, $textShowMore, $lengthTxt) {
        if (mb_strlen($text) > $lengthTxt) {
            return mb_strcut($text, 0, $lengthTxt- mb_strlen($textShowMore));
        }
        else {
            return $text;
        }
    }

}
