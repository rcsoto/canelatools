<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\State;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;


class WebLanguage extends BasicModel
{
    /**
     * Database table name.
     * @var string
     */
    const ENTITY_TABLE = 'web_language';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'web_id',
        'language_id',
        'principal',
        'position',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'web_id'                => 'int',
        'language_id'           => 'int',
        'principal'             => 'bool',
        'position'              => 'int',
    ];


    /** @var string $routePath Model route path */
    protected static string $routePath = 'admin.webbuilder.weblanguage';


    /** @var string $resourceTransPath Model resource translations path */
    protected static string $resourceTransPath = 'canelatools::canelatools.entity.web_language.';


    /**
     * @return BelongsTo
     */
    public function web(): BelongsTo
    {
        return $this->belongsTo(Web::class, 'web_id');
    }


    /**
     * @return BelongsTo
     */
    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'language_id');
    }


    /**
     * @param int $webId
     * @param bool $withPrincipal
     * @return Collection
     */
    public static function webLanguages(int $webId, bool $withPrincipal = false): Collection {
        $query = WebLanguage::select([
                'language.id',
                'language.name',
                'language.code',
                'web_language.principal',
            ])->join('language', function ($query) {
                $query->on('web_language.language_id', 'language.id')
                    ->where('language.state_id', State::ACTIVO);
            })
            ->where('web_language.web_id', $webId);

        if (!$withPrincipal) {
            $query->where('web_language.principal', false);
        }

        $query->orderBy('web_language.position');
        return $query->get();
    }

}
