<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * Class WebPageFaq
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property WebPage page
 *
 */
class WebPageCategory extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'position',
        'name',
        'description',
        'abbreviation',
        'color_text',
        'color_border',
        'color_bg',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id'           => 'int',
        'position'          => 'int',
        'name'              => 'string',
        'description'   => 'string',
        'abbreviation'  => 'string',
        'color_text'    => 'string',
        'color_border'  => 'string',
        'color_bg'      => 'string',
    ];


    /**
     * @return BelongsTo
     */
    public function page(): BelongsTo
    {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * @return BelongsToMany
     */
    public function sections(): BelongsToMany
    {
        return $this->belongsToMany(WebPageSection::class, 'web_page_section_category', 'category_id', 'section_id');
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages($this->page->web_id);
    }

}
