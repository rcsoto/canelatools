<?php

namespace Canela\CanelaTools\Requests\Client;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class CustomerRequest
 * @package Canela\CanelaTools\Requests\Client
 */
class CustomerRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'client_id',
        'web_id',
        'name',
        'business_name',
        'image_logo',
        'image_logo_dark',
        'map_latitude',
        'map_longitude',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.customer.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'         => 'required|integer|exists:client,id',
            'web_id'            => 'nullable|integer|exists:web,id',
            'name'              => 'required|string|min:1|max:255',
            'business_name'     => 'required|string|min:1|max:255',
            'image_logo'        => 'image|max:'.config('canelatools.default_upload_image_max_size'),
            'image_logo_dark'   => 'image|max:'.config('canelatools.default_upload_image_max_size'),
            'map_latitude'      => 'nullable|numeric',
            'map_longitude'     => 'nullable|numeric',
        ];
    }

}
