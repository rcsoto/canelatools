<?php namespace Canela\CanelaTools\Controller;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ArchivePrivateController extends BasicController
{
    /**
     * @param $folder
     * @param $filename
     * @return BinaryFileResponse
     */
    public function getArchive(Request $request)
    {
        $folder = $request->folder;
        $filename = $request->filename;
        $contentType = $request->contentType;
        try {
            $filePath = Storage::disk('local')->path($folder . DIRECTORY_SEPARATOR . $filename);
            $response = new BinaryFileResponse($filePath, 200, [
                'Content-Type' => $contentType,
            ]);
            $name = basename($filePath);
            $response->setContentDisposition('attachment', $name, str_replace('%', '', Str::ascii($name)));

            return $response;
        } catch (Exception $e) {
            Log::error($e);
            abort(404);
        }
    }
}
