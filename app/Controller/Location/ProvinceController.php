<?php

namespace Canela\CanelaTools\Controller\Location;


use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\Location\Country;
use Canela\CanelaTools\Models\Location\Province;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Requests\Location\ProvinceRequest;
use Canela\CanelaTools\Requests\Location\ProvinceRefreshRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class ProvinceController
 * @package Canela\CanelaTools\Controller\Location
 */
class ProvinceController extends BasicController
{
    use HtmlLists;

    /**
     * ProvinceController constructor.
     */
    public function __construct()
    {
        $this->menu_group = AdminMenu::GROUP_LOCATION;
        $this->menu       = AdminMenu::PROVINCE;

        $this->routeEntity = 'location.province';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::canelarest.addEdit';
        $this->vistaAdd    = 'canelatools::canelarest.addEdit';

        $this->entidad = Province::class;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.province.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(true)
            ->setTableColumn([
                (new TableColumn())->setTable('province')->setFieldOrder('id'),
                (new TableColumn())->setTable('country')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.province.field.country_id'))
                    ->setFieldFilter('id')->setComboList(Country::comboList())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                (new TableColumn())->setTable('province')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.province.field.name')),
                (new TableColumn())->setTable('province')->setFieldOrder('code')->setLabel(trans('canelatools::canelatools.entity.province.field.code')),
                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([
                (new TableForm())->setName('country_id')->setLabel(trans('canelatools::canelatools.entity.province.field.country_id'))
                    ->setComboListJson(Country::comboList())->setType(TableColumnTypes::SELECT2)->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('name')->setLabel(trans('canelatools::canelatools.entity.province.field.name'))->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('code')->setLabel(trans('canelatools::canelatools.entity.province.field.code'))->setContainerClass('col-6 mb-2'),
            ]);

        parent::__construct();
    }


    /**
     *
     * Enter description here ...
     */
    public function getConsultaPersonalizada($request)
    {
        return Province::select(['province.id',
                                 'country.name as country_name',
                                 'province.name',
                                 'province.code',
                                 'province.id as action_province_id'
                               ])
                        ->join('country', 'province.country_id', 'country.id');
    }

    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_province_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, false, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ProvinceRequest $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(ProvinceRequest $request)
    {
        return $this->storeBasic($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProvinceRequest $request
     * @param int $province_id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(ProvinceRequest $request, int $province_id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $province_id);
    }


    /**
     * refreshAjax
     *
     * @param Canela\CanelaTools\Requests\Location\ProvinceRefreshRequest $request
     * @return \Illuminate\Http\Response
     */
    public function refreshAjax(ProvinceRefreshRequest $request)
    {
        $countryIds = '';
        if ($request->has('country_id')) {
            $countryIds = explode(',', $request->country_id);
        } else {
            $countryIds = [];
        }
        return response()->json(['result' => 1,
                                 'data'   => json_decode(Province::getComboListJson(Province::whereIn('country_id', $countryIds)->comboList()))]);
    }

}
