<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\WebPage;
use Canela\CanelaTools\Models\WebBuilder\WebPageContact;
use Canela\CanelaTools\Models\WebBuilder\WebPageContactType;
use Canela\CanelaTools\Models\WebBuilder\WebPageType;
use Canela\CanelaTools\Requests\WebBuilder\WebPageContactRequest;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageContactController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageContactController extends BasicController
{
    use HtmlLists;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageContact::class;
        $this->routeEntity = 'admin.webbuilder.webpagecontact';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_contact.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_contact.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_contact',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'customer_name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page.field.title'),
                ),
                new TableColumn(
                    table: 'web_page_contact_type',
                    fieldOrder: 'web_page_contact_type_contact',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.type_id'),
                    fieldFilter: 'id',
                    comboList: WebPageContactType::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page_contact',
                    fieldOrder: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                ),
                new TableColumn(
                    table: 'web_page_contact',
                    fieldOrder: 'concept',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.concept'),
                ),
                new TableColumn(
                    table: 'web_page_contact',
                    fieldOrder: 'value',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.value'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.page_id'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    containerClass: 'col-6 mb-2',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'type_id',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.type_id'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                    comboList: WebPageContactType::comboTranslate(),
                ),
                new TableForm(
                    name: 'concept',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.concept'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'value',
                    label: trans('canelatools::canelatools.entity.web_page_contact.field.value'),
                    required: true,
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang = App::getLocale();
        $query = WebPageContact::select([
            'web_page_contact.id',
            'customer.name as customer_name',
            'web_page.title as web_page_title',
            DB::Raw((Language::DEFAULT_CODE == $lang ? "web_page_contact_type.contact" : "trans('" . $lang . "', 'web_page_contact_type', web_page_contact_type.id, 'contact', web_page_contact_type.contact)") . " as web_page_contact_type_contact"),
            'web_page_contact.position',
            'web_page_contact.concept',
            'web_page_contact.value',
            'web_page_contact.id as action_web_page_contact_id',
        ])
        ->join('web_page_contact_type', 'web_page_contact.type_id', 'web_page_contact_type.id')
        ->join('web_page', 'web_page_contact.page_id', 'web_page.id')
        ->join('web', 'web_page.web_id', 'web.id')
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_web_page_contact_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, true, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }



    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_DATA_CONTACT));
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, int $id): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_DATA_CONTACT));
        return parent::edit($request, $id);
    }


    /**
     * @param WebPageContactRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageContactRequest $request): Model|JsonResponse|RedirectResponse|null
    {
        return $this->storeBasic($request);
    }


    /**
     * @param WebPageContactRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageContactRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
