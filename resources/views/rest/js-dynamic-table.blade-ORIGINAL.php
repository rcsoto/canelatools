<?php
use Canela\CanelaTools\Manager\UtilsBasicManager;
use Canela\CanelaTools\Enums\TableFilterTypes;
?>

@section('content-footer')
    @parent
    @php
        $exportUrlQueryString = "?action=export&". http_build_query(request()->toArray());
    @endphp
    {!! Form::open(array('url' => $urlentidad.$exportUrlQueryString, 'id' => 'excel-report-table')) !!}
    {!! Form::hidden('filters', null, array('id' => 'report-filters')) !!}
    {!! Form::hidden('orderbys', null, array('id' => 'report-orderbys')) !!}
    {!! Form::hidden('titles', null, array('id' => 'report-titles')) !!}
    {!! Form::close() !!}

    @include('canelatools::deleteListRecord')
    @include('canelatools::updateStatus')
@stop

@push('style-table')
    <!-- Data Table Css -->
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/back-office.min.css')) }}">--}}
@endpush

@section('body-scripts-table')

@stop

<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function () {

        // ----------------------------------------------------------------------
        // Variables globales
        // ----------------------------------------------------------------------
        let PAGINATION;
        let PAGE = 1;
        let COLUMNAS = 0;
        var _INICIAR_BUSQUEDA = false;

        var _TABLE_ID 	= '#advanced-table';

        var _ACTIVE_TABLE 		 		= '{{UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeTable';
        var _ACTIVE_FILTERS_DATA 		= '{{UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeFiltersData';
        var _ACTIVE_FILTERS_CONDITION   = '{{UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeFiltersCondition';

        var _URL_ENTIDAD = "{{ url($urlentidad) }}";

        var _PARAMETER_FILTERS = [];
        // parametros desde el controlador
        @if(!empty($parameterFilters))
        @foreach($parameterFilters as $key => $value)
            item = {};
            item['{{$key}}'] = '{{$value}}';
            _PARAMETER_FILTERS.push(item);
        @endforeach
        @endif


        // ----------------------------------------------------------------------
        // Change urlentidad
        // ----------------------------------------------------------------------
        jQuery.changeUrlEntidad = function (url, params) {
            _URL_ENTIDAD = url;
            jQuery('#excel-report-table').attr('action', _URL_ENTIDAD + "{{ $exportUrlQueryString }}");
            jQuery.setParameterFilters(params);
        }


        // ----------------------------------------------------------------------
        // Set parameter filters
        // ----------------------------------------------------------------------
        jQuery.setParameterFilters = function (params) {
            _PARAMETER_FILTERS = params;
        }


        // ----------------------------------------------------------------------
        // Crear tabla
        // ----------------------------------------------------------------------
        let langUrl = '//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json';
        @if(App::getLocale() == 'es')
            langUrl = '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json';
        @endif
        window.dtable = jQuery(_TABLE_ID).DataTable({
            // set the initial value
            "bPaginate": false,
            "searching": false,
            "info": false,
            "bSort": false,
            //"aoColumns": 	{{ $columns }},
            language: {
                url: langUrl
            },
            initComplete: function (settings, json) {
                jQuery.hideAdvancedFilters();
            }
        });

        // ----------------------------------------------------------------------
        // Buscar al hacer en intro en un campo de busqueda
        // ----------------------------------------------------------------------
        jQuery('input.search_init').keypress(function (e) {
            if (e.which === 13) {
                jQuery.buscar();
            }
        });


        // ----------------------------------------------------------------------
        // Cambio en select de elementos por pagina
        // ----------------------------------------------------------------------
        jQuery("select.search_init").change(function () {
        	jQuery.buscar();
        });


        /**
         * Recoge valor de paginacion
         */
        jQuery.getPaginacion = function() {
            let valor = jQuery('#select-pagination').val();
            if (!valor) {
                valor = 10;
            }
            const pageTmp = PAGE;
            PAGE = 1;

            return "&page=" + pageTmp + "&pagination=" + valor;
        }

        /**
         * recogemos filtros
         */
        jQuery.getFiltros = function() {
            let jsonObj = [];

            jQuery("#tr-filters").children().each(function () {
                let item = {};
                let existe = false;
                jQuery(this).children().each(function () {
                    item['type'] = null;
                    if (jQuery(this).hasClass('search_init')) {
                        existe = true;
                        item['id'] 			= jQuery(this).attr('name');
                        item['table'] 		= jQuery(this).data('table');
                        item['field'] 		= jQuery(this).data('field');
                        item['filter_sql'] 	= jQuery(this).data('filtersql');
                        item['value'] 		= null;
                        item['data_type'] 	= (typeof jQuery(this).attr('data-type') !== 'undefined' ? jQuery(this).attr('data-type') : null);
                        let variable = jQuery(this).val();
                        if (!("undefined" === typeof variable || variable === '' || variable == null)) {
                            item['value'] = variable;
                        }
                    } else if (jQuery(this).hasClass('select-filter-opt')) {
                        item['type'] = jQuery(this).val();
                    }
                });
                if (existe) {
                    jsonObj.push(item);
                }
            });
            return JSON.stringify(jsonObj);
        }


        /**
         * limpiar filtros
         */
        jQuery.limpiarFiltros = function () {
            jQuery("#tr-filters").children().each(function () {
                jQuery(this).children().each(function () {
                    if (jQuery(this).hasClass('search_init'))
                    {
                        if (jQuery(this).hasClass('select2'))
                        {
                            jQuery(this).val('').trigger('change');
                        }
                        else
                        {
                            jQuery(this).val('');
                        }
                    }
                    else if (jQuery(this).hasClass('select-filter-opt')) {
                        jQuery(this)[0].selectedIndex = 0;
                    }
                });
                jQuery.saveCookieFilters();
            });
        };

        /**
         * Recoge orderBy
         */
        jQuery.getOrderBy = function () {
            let jsonObj = [];
            jQuery("#tr-orders").children().each(function () {
                const variable = jQuery(this).attr('data-orderby');
                if (!("undefined" === typeof variable || variable === '')) {
                    let item = {};
                    item['id'] = jQuery(this).attr('id');
                    item['table'] = jQuery(this).data('table');
                    item['field'] = jQuery(this).data('field');
                    item['value'] = variable;
                    jsonObj.push(item);
                }
            });
            return JSON.stringify(jsonObj);
        };

        /**
         * BUSQUEDA
         */
         jQuery.buscar = function() {
        	if (_INICIAR_BUSQUEDA)
        	{
                // guardar filtros
                jQuery.saveCookieFilters();
                // Elementos a mostrar
                let pagination = jQuery.getPaginacion();
                // Filtros
                let filters = jQuery.getFiltros();
                // order bys
                let orderbys = jQuery.getOrderBy();

                // parametros
                let formData = new FormData();

                // recargar
                formData.append("_token", '{{ csrf_token() }}');
                formData.append("filters", filters);
                formData.append("orderbys", orderbys);
                formData.append("parameterFilters", JSON.stringify(_PARAMETER_FILTERS));

                const urlCP = _URL_ENTIDAD + "?" + pagination + "&" + $.param({!! json_encode(request()->toArray()) !!});
                jQuery.ajax({
                    url: urlCP,
                    dataType: "json",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    beforeSend: function () {
                        // Handle the beforeSend event
                        jQuery.loadIni();
                        PAGINATION = {};
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        jQuery.loadEnd();
                        console.log(exception, errorThrown);
                        jQuery.messageError("@lang('canelatools::canelatools.canela-rest.table.errors.generic')");
                    },
                    success: function (datos) {
                        // ocultar espera
                        if (datos.result === 1) {
                            dtable.clear();
                            dtable.rows.add(datos.data);
                            dtable.draw();

                            PAGINATION = datos.pagination;
                            COLUMNAS = datos.columnas;

                            // personalizar lista
                            if (jQuery.isFunction(jQuery.personalizarLista)) {
                                jQuery.personalizarLista(dtable);
                            }

                            // Refrescar los tooltips de la tabla
                            jQuery('.popover-dynamic').popover({
                                trigger: 'focus'    // Dismiss on next click
                            });
                        } else {
                            // Mostrar mensaje de error
                            jQuery.messageWarning(datos.msg, 3000);
                        }

                        // actualizar informacion de la tabla
                        jQuery.createBotonesNavegacion();

                        jQuery.updateElementosPorPagina();

                        @if (isset($group_btn) &&  $group_btn)
                        jQuery.groupActionBtn();
                        @endif

                        @if (isset($grid_btn) &&  $grid_btn)
                        jQuery.gridActionBtn();
                        @endif

                        if (typeof loadButtons === 'function') {
                            loadButtons();
                        }

                        if (typeof loadStatusButtons === 'function') {
                            loadStatusButtons();
                        }
                        $(document).trigger('endDatatableSearch', []);
                        jQuery.loadEnd();
                    },
                });
        	}
        }


        jQuery("#tr-orders").children().each(function () {
            // ----------------------------------------------------------------------
            // Pulsar en cabeceras de orden
            // ----------------------------------------------------------------------
            jQuery(this).click(function () {
                if (jQuery(this).attr('data-orderby') === 'ASC') {
                    // vaciar todos los campos de orden
                    jQuery("#tr-orders").children().each(function () {
                        jQuery(this).attr('data-orderby', '');
                        jQuery(this).children().each(function () {
                            jQuery(this).removeClass('fa-sort-asc, fa-sort-desc').addClass('fa-sort');
                        });
                    });

                    jQuery(this).children().each(function () {
                        jQuery(this).addClass('fa-sort-asc');
                        jQuery(this).removeClass('fa-sort-desc');
                    });

                    jQuery(this).attr('data-orderby', 'DESC');
                } else {
                    // vaciar todos los campos de orden
                    jQuery("#tr-orders").children().each(function () {
                        jQuery(this).attr('data-orderby', '');
                        jQuery(this).children().each(function () {
                            jQuery(this).removeClass('fa-sort-asc, fa-sort-desc').addClass('fa-sort');
                        });
                    });

                    jQuery(this).children().each(function () {
                        jQuery(this).addClass('fa-sort-desc');
                        jQuery(this).removeClass('fa-sort-asc');
                    });

                    jQuery(this).attr('data-orderby', 'ASC');
                }

                jQuery.buscar();
            });


            // ----------------------------------------------------------------------
            // Configuar cabeceras de orden
            // ----------------------------------------------------------------------
            if (jQuery(this).attr('id') != null) {
                jQuery(this).css('cursor', 'pointer');

                if (jQuery(this).attr('data-orderby') === 'ASC') {
                    jQuery(this).children(function () {
                        jQuery(this).removeClass('fa-sort-asc');
                        jQuery(this).addClass('fa-sort-desc');
                    });

                } else {
                    jQuery(this).children(function () {
                        jQuery(this).removeClass('fa-sort-desc');
                        jQuery(this).addClass('fa-sort-asc');
                    });
                }
            }

        });

        // ----------------------------------------------------------------------
        // Imagen a mostrar
        // ----------------------------------------------------------------------
        jQuery.configureBtnOrder = function(tipoOrden) {
            if (tipoOrder === 'ASC') {
                return 'fa-sort-asc';
            } else if (tipoOrder === 'DESC') {
                return 'fa-sort-desc';
            }
            return 'fa-order';
        }


        // ----------------------------------------------------------------------
        // Crea la barra de navegacion por paginas
        // ----------------------------------------------------------------------
        jQuery.createBotonesNavegacion = function() {
            const tablita = jQuery(_TABLE_ID);
            jQuery(_TABLE_ID+" tfoot").remove();

            const txtDisabledPagePrev = (PAGINATION.current_page === 1 ? 'disabled' : '');
            const txtDisabledPageNext = (PAGINATION.current_page === PAGINATION.last_page ? 'disabled' : '');

            let txt = '<div class="dataTables_paginate paging_simple_numbers" id="tabla_paginate">'+
                        '<ul class="pagination">'+
                        '<li class="paginate_button page-item previous '+ txtDisabledPagePrev +'" aria-controls="tabla" tabindex="0">'+
                            '<a class="page-link" href="javascript:jQuery.goPage('+ (PAGINATION.current_page - 1) +')">'+
                            '<i class="fa fa-angle-left"></i>'+
                            '</a>'+
                        '</li>';

            // siempre boton primera pagina
            txt += '<li class="paginate_button page-item '+ (PAGINATION.current_page === 1 ? 'active' : '') +'" '+
                        ' aria-controls="tabla" tabindex="0">'+
                        '<a class="page-link" href="javascript:jQuery.goPage(1);">1</a>'+
                    '</li>';

            // boton "..." por no se correlativo
            if (PAGINATION.current_page >= 4 && PAGINATION.last_page > 5) {
                txt += '<li class="paginate_button page-item disabled" aria-controls="tabla" tabindex="0">'+
                            '<a class="page-link" href="#">…</a>'+
                       '</li>';

            }

            // botones intermedios
            let ii = Math.max(2, PAGINATION.current_page - 1);
            let i = Math.min(ii, Math.max(5, (PAGINATION.last_page - 3)));
            let paso = 0;
            while (i < PAGINATION.last_page && paso < 3) {
                txt += '<li class="paginate_button page-item '+ (PAGINATION.current_page === i ? 'active' : '') +'" '+
                    		'aria-controls="tabla" tabindex="0">'+
                    		'<a class="page-link" href="javascript:jQuery.goPage('+ i +');"> '+ i +'</a>'+
                		'</li>';

                paso++;
                i++;
            }

            // boton "..." por no se correlativo
            if ((PAGINATION.last_page - 3) >= PAGINATION.current_page && PAGINATION.last_page > 5) {
                txt += '<li class="paginate_button page-item disabled" aria-controls="tabla" tabindex="0">'+
                            '<a class="page-link" href="#">…</a>'+
                        '</li>';

            }

            // siempre boton ultima pagina (menos si ya lo hemos pintado)
            if (PAGINATION.last_page != paso && PAGINATION.last_page != 1) {
                txt += '<li class="paginate_button page-item '+ (PAGINATION.current_page === PAGINATION.last_page ? 'active' : '') +'" '+
                            'aria-controls="tabla" tabindex="0">'+
                            '<a class="page-link" href="javascript:jQuery.goPage('+ PAGINATION.last_page +');">'+
                                PAGINATION.last_page +'</a>'+
                            '</li>';

            }


            txt += '<li class="paginate_button page-item next '+ txtDisabledPageNext +'" aria-controls="tabla" tabindex="0">'+
            			'<a class="page-link" href="javascript:jQuery.goPage('+ (PAGINATION.current_page + 1) +');">'+
	                		'<i class="fa fa-angle-right"></i>'+
    	                '</a>'+
        	        '</li>'+
            		'</ul>'+
            		'</div>';

            tablita.append('<tfoot class="t-right"><tr><td colspan="'+ jQuery('table thead tr:first').children().length +'"> '+ txt +'</td></tr></tfoot>');
        }


        // ----------------------------------------------------------------------
        // Va a la pagina seleccionada
        // ----------------------------------------------------------------------
        jQuery.goPage = function (page) {
            PAGE = page;
            jQuery.buscar();
        };

        // ----------------------------------------------------------------------
        // Crea el combo box de elementos por pagina
        // ----------------------------------------------------------------------
        jQuery.createElementosPorPagina = function(porDefecto, hideCombo) {
            const tablita = jQuery(_TABLE_ID);

            jQuery("#combo-items").remove();

            const txt = '<div class="row fluid">'+
                            '<div class="col-xs-6">'+
                                '<span class="hide select-pagination-span">&nbsp;</span>'+
                                '<select id="select-pagination" class="form-control hide" data-style="white">'+
                                '<option value="20">20</option>'+
                                '<option value="100">100</option>'+
                                '<option value="500">500</option>'+
                                '<option value="1000">1000</option>'+
                                '<option value="10000">10000</option>'+
                                '</select>'+
                            '</div>'+
                            '<div class="col-xs-6">'+
                                '<span class="hide select-pagination-span">&nbsp;</span>'+
                                '<span id="combo-items-select-pagination-info"></span>'+
                            '</div>'+
                         '</div>';

            tablita.before('<thead id="combo-items"><tr><th colspan="'+ COLUMNAS +'"> '+ txt +' </th></tr></thead>');

            const paginationSelect = jQuery("#select-pagination");
            paginationSelect.val(porDefecto).trigger('change');

            if (hideCombo === 1) {
                paginationSelect.addClass('hide');
                jQuery(".select-pagination-span").removeClass('hide');
            } else {
                paginationSelect.removeClass('hide');
                jQuery(".select-pagination-span").addClass('hide');
            }

            // Cambio en paginador
            paginationSelect.change(function () {
            	jQuery.buscar();
            });
        }

        // ----------------------------------------------------------------------
        // Crea un combo debajo de cada filtro con las opciones de busqueda
        // ----------------------------------------------------------------------
        jQuery.busquedasAvanzadas = function(hideCombo) {
            // ocultar
            const classHide = hideCombo === 1 ? " hide " : "";

            const txtCombo = "<select class='select-filter-opt form-control mt-1 "+ classHide +" ' onchange='javascript:jQuery.buscar();'>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_CONTIENE }}'>@lang('canelatools::canelatools.canela-rest.table.filter.contains')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.equal')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_MAYOR }}'>@lang('canelatools::canelatools.canela-rest.table.filter.higher')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_MAYOR_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.higher_equal')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_MENOR }}'>@lang('canelatools::canelatools.canela-rest.table.filter.less')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_MENOR_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.less_equal')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_DISTINTO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.distinct')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_NO_CONTIENE }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_contains')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.empty')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_NO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_empty')</option>"+
                "</select>";

            const txtCombo2 = "<select class='select-filter-opt form-control mt-1 "+ classHide +"' onchange='javascript:jQuery.buscar();'>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.equal')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_DISTINTO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.distinct')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.empty')</option>"+
                "<option value='{{ TableFilterTypes::TIPO_FILTRO_NO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_empty')</option>"+
                "</select>";

            jQuery("#tr-filters").children().each(function () {
                let isSelect = false;
                let existeControl = false;
                jQuery(this).children().each(function () {
                    isSelect = isSelect || jQuery(this).is('select');
                    existeControl = existeControl || jQuery(this).is('select') || jQuery(this).is('input');
                });
                if (existeControl) {
                    if (isSelect) {
                        jQuery(this).append(txtCombo2);
                    } else {
                        jQuery(this).append(txtCombo);
                    }
                }
            });

        }

        /**
         * Must hide advanced filters?
         */
        jQuery.hideAdvancedFilters = function() {
            if (!!JSON.parse('{{ empty($hideFilter) ? 0 : ($hideFilter ? 1 : 0) }}')) {
                $('#tr-filters').hide();
            }
        }

        /**
         * Actualiza elementos visualizados
         */
        jQuery.updateElementosPorPagina = function() {
            jQuery("#combo-items-select-pagination-info").html(
                    "@lang('canelatools::canelatools.canela-rest.table.register.registers')" +
                    PAGINATION.total +
                    "@lang('canelatools::canelatools.canela-rest.table.register.page')" +
                    PAGINATION.current_page +
                    "@lang('canelatools::canelatools.canela-rest.table.register.of')" + PAGINATION.last_page);
        }

        // ----------------------------------------------------------------------
        // EXPORTAR EXCEL
        // ----------------------------------------------------------------------
        jQuery.setExportBtn = function(container) {
            const excelbtn = '<a role="button" class="btn btn-sm btn-success dt-action" style="float:right;" '+
                                'title="@lang('canelatools::canelatools.canela-rest.table.excel')" onclick="jQuery.exportToExcel()">'+
                                    '<i class="fa fa-file-excel"></i>'+
                              '</a>';
            container.prepend(excelbtn);
        }

        jQuery.exportToExcel = function () {
            // order bys
            jQuery('#report-filters').val(jQuery.getFiltros());
            // Filtros
            jQuery('#report-orderbys').val(jQuery.getOrderBy());
            //column titles
            let titles = [];
            jQuery('.dataTables_wrapper').find('thead:eq(0)').find('th').each(function () {
                titles.push($(this).text().trim());
            });
            jQuery('#report-titles').val(JSON.stringify(titles));

            let visibilities = dtable.columns().visible().toArray();
            jQuery('#report-visibilities').val(JSON.stringify(visibilities));

            jQuery('#excel-report-table').submit();
        };

        // ----------------------------------------------------------------------
        // AÑADIR BOTÓN LIMPIAR FILTROS
        // ----------------------------------------------------------------------
        jQuery.setClearFiltersBtn = function(container) {
            const clearFiltersBtn = '<a role="button" class="btn btn-sm btn-warning dt-action" '+
                                        ' style="float: right;margin-right: 10px;" title="@lang('canelatools::canelatools.canela-rest.table.filter.clear')" '+
                                        ' onclick="jQuery.limpiarFiltros();">'+
                                        '<i class="fa fa-filter"></i>'+
                                     '</a>';
            container.prepend(clearFiltersBtn);
        }

        jQuery.setTableTopRightBtnContainer = function() {
            const container = $("<div id='table-top-right-btn-container' class='col-xs-6'></div>");
            jQuery('.table-responsive').prepend(container);

            jQuery.setClearFiltersBtn(container);
            jQuery.setExportBtn(container);
        }

        // ----------------------------------------------------------------------
        // Reload table on status change
        // ----------------------------------------------------------------------
        jQuery(document).on('endChangeStatus', function (event, data, button) {
            jQuery.buscar();
        });


     	// ----------------------------------------------------------------------
        // GUARDAR FILTROS EN COOKIE
        // ----------------------------------------------------------------------


        /**
         * Get filters from storage
         */
        jQuery.cookieFilters = function() {
            const table = jQuery(_TABLE_ID);
            const idTable = jQuery.getCodeTable();
            let activeTable = localStorage.getItem(_ACTIVE_TABLE);
            if (activeTable && activeTable === idTable) {
                let filtersData 		= JSON.parse(localStorage.getItem(_ACTIVE_FILTERS_DATA));
                let filtersCondition	= JSON.parse(localStorage.getItem(_ACTIVE_FILTERS_CONDITION));
                var contadorCondition = 0;
                jQuery("#tr-filters").children().each(function () {
                    jQuery(this).children().each(function () {
                        if (jQuery(this).hasClass('search_init'))
                        {
                            const input = jQuery(this);
                            var nameInput = input.attr('name');
                            filtersData.forEach(function(data, index)
        					{
        						if (data.name == nameInput)
        						{
        							if (input.hasClass('select2'))
        							{
        	                            input.val(data.value).trigger('change');
        	                        } else {
        	                        	input.val(data.value);
        	                        }
        						}
        					});
                        }
                        else if (jQuery(this).hasClass('select-filter-opt'))
                        {
                            if (filtersCondition != null)
                            {
                                var valueCondition = filtersCondition[contadorCondition];
    							if (!("undefined" === typeof valueCondition || valueCondition == '' || valueCondition == null))
    							{
                            		jQuery(this).val(valueCondition).trigger('change');
    							}
                            }
                        	contadorCondition++;
                        }
                    });
                });
            } else {
                localStorage.setItem(_ACTIVE_TABLE, idTable);
                jQuery.saveCookieFilters();
            }
        }


        /**
         * Save active filters to cookie
         */
         jQuery.saveCookieFilters = function() {
            let filtersData = [];
            let filtersCondition = [];
            jQuery("#tr-filters").children().each(function () {
                jQuery(this).children().each(function () {
                    if (jQuery(this).hasClass('search_init'))
                    {
                        var name = jQuery(this).attr('name');
                        if (!("undefined" === typeof name || name == '' || name == null))
                        {
                        	var variable = jQuery(this).val();
                        	if ("undefined" === typeof variable || variable == '' || variable == null)
                        	{
                            	variable = "";
                        	}
                    		filtersData.push({name: name, value: variable});
                        }
                    }
                    else if (jQuery(this).hasClass('select-filter-opt'))
                    {
                    	filtersCondition.push(jQuery(this).val());
                    }
                });
            });

			// save values
            localStorage.setItem(_ACTIVE_TABLE, jQuery.getCodeTable());
            localStorage.setItem(_ACTIVE_FILTERS_DATA, JSON.stringify(filtersData));
            localStorage.setItem(_ACTIVE_FILTERS_CONDITION, JSON.stringify(filtersCondition));
        }


        /**
         * Codigo hash que identifica a una tabla
         */
        jQuery.getCodeTable = function()
        {
        	const table = jQuery(_TABLE_ID);
        	var tt = table.find("thead:first tr th");
    		var codigoTabla = "";
    		tt.each(function(index, e) {
    			var id = jQuery(e).attr('id');
    			if (id != undefined && id != "")
    			{
        			codigoTabla += jQuery(e).attr('id');
        		}
        	});
    		//return hash(JSON.stringify(codigoTabla));
    		return (JSON.stringify(codigoTabla));
        }


        /**
         * Group action buttons
         */
        jQuery.groupActionBtn = function() {
            $('tbody tr').each(function () {
                let actions = $(this).find('td:last-child');
                let html = actions.html();
                actions.html('<div class="btn-group">'+ html +'</div>');
            });
        }

        /**
         * Group action buttons
         */
        jQuery.gridActionBtn = function() {
            $('tbody tr').each(function () {
                let actions = $(this).find('td:last-child');
                actions.css('display', 'grid');
            });
        }




        // ----------------------------------------------------------------------
        // CARGA INICIAL
        // ----------------------------------------------------------------------
        jQuery.busquedasAvanzadas('{{ empty($hideFilter) ? 0 : ($hideFilter ? 1 : 0) }}');
        jQuery.createElementosPorPagina('{{ empty($itemsByDefaultSelect) ? 20 : $itemsByDefaultSelect}}', '{{ empty($hideFilter) ? 0 : ($hideFilter ? 1 : 0) }}');
        jQuery.cookieFilters();
        _INICIAR_BUSQUEDA = true;
        jQuery.buscar();

        jQuery.setTableTopRightBtnContainer();
    });







    /*
     * Event listener on search ends.
        $(document).on('endDatatableSearch', function (event, data, button) {
        //some stuff here
        });
    */
</script>
