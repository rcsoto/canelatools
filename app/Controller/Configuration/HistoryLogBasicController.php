<?php

namespace Canela\CanelaTools\Controller\Configuration;

use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\HistoryLog;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Module;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

/**
 * Class HistoryLogController
 * @package App\Http\Controllers\Admin\Configuration
 */
class HistoryLogBasicController extends BasicController
{
    use HtmlLists;

    /**
     * HistoryLogController constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->entidad      = HistoryLog::class;
        $this->routeEntity  = 'configuration.historylog';

        $this->menu_group = AdminMenu::GROUP_CONFIG;
        $this->menu       = AdminMenu::HISTORY_LOG;

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';


        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.history_log.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(false)
            ->setShowBtnEdit(false)
            ->setTableColumn([  (new TableColumn())->setTable('history_log')->setFieldOrder('id'),
                                (new TableColumn())->setTable('module')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.history_log.field.module_id'))
                                    ->setFieldFilter('id')->setComboList(Module::comboTranslate())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                                (new TableColumn())->setTable('entity')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.history_log.field.entity_id'))
                                    ->setFieldFilter('id')->setComboList(Entity::comboTranslate())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                                (new TableColumn())->setTable('history_log')->setFieldOrder('register')->setLabel(trans('canelatools::canelatools.entity.history_log.field.register')),
                                (new TableColumn())->setTable('action')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.history_log.field.action_id'))
                                    ->setFieldFilter('id')->setComboList(Action::comboTranslate())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                                (new TableColumn())->setTable('user')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.history_log.field.user_id')),
                                (new TableColumn())->setTable('history_log')->setFieldOrder('created_at')->setLabel(trans('model.common.created_at')),
                                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([(new TableForm())->setName('module_id')->setLabel(trans('canelatools::canelatools.entity.history_log.field.module_id'))
                                ->setReadOnly(true)->setComboListJson(Module::comboTranslate())->setType(TableColumnTypes::SELECT)->setCol(6),
                            (new TableForm())->setName('action_id')->setLabel(trans('canelatools::canelatools.entity.history_log.field.action_id'))
                                ->setReadOnly(true)->setComboListJson(Action::comboTranslate())->setType(TableColumnTypes::SELECT)->setCol(6),
                            (new TableForm())->setName('entity_id')->setLabel(trans('canelatools::canelatools.entity.history_log.field.entity_id'))
                                ->setReadOnly(true)->setComboListJson(Entity::comboTranslate())->setType(TableColumnTypes::SELECT)->setCol(6),
                            (new TableForm())->setName('user_id')->setLabel(trans('canelatools::canelatools.entity.history_log.field.user_id'))
                                ->setReadOnly(true)->setComboListJson(User::comboList())->setType(TableColumnTypes::SELECT)->setCol(6),

                            (new TableForm())->setName('register_id')->setLabel(trans('canelatools::canelatools.entity.history_log.field.register_id'))->setReadOnly(true)->setCol(6),
                            (new TableForm())->setName('register')->setLabel(trans('canelatools::canelatools.entity.history_log.field.register'))->setReadOnly(true)->setCol(6),

                            (new TableForm())->setName('comment')->setLabel(trans('canelatools::canelatools.entity.history_log.field.comment'))->setReadOnly(true),
                            (new TableForm())->setName('value_post')->setLabel(trans('canelatools::canelatools.entity.history_log.field.value_post'))->setReadOnly(true),
                            (new TableForm())->setName('value_pre')->setLabel(trans('canelatools::canelatools.entity.history_log.field.value_pre'))->setReadOnly(true),
            ]);

        parent::__construct();
    }

    /**
     * Index Query build
     * @param Request $request
     * @return Builder
     */
    public function getConsultaPersonalizada(Request $request)
    {
        $lang     = App::getLocale();

        $consulta = HistoryLog::select(['history_log.id',
                                        DB::Raw((Language::DEFAULT_CODE == $lang ? "module.name" : "trans('" . $lang . "', 'module', module.id, 'name', module.name)") . " as module_name"),
                                        DB::Raw((Language::DEFAULT_CODE == $lang ? "entity.name" : "trans('" . $lang . "', 'entity', entity.id, 'name', entity.name)") . " as entity_name"),
                                        'history_log.register as register',
                                        DB::Raw((Language::DEFAULT_CODE == $lang ? "action.name" : "trans('" . $lang . "', 'action', action.id, 'name', action.name)") . " as action_name"),
                                        'user.name as user_name',
                                        'history_log.created_at as date',
                                        'history_log.id as action_history_id',
                                       ])
                              ->leftJoin('module', 'history_log.module_id', '=', 'module.id')
                              ->leftJoin('entity', 'history_log.entity_id', '=', 'entity.id')
                              ->leftJoin('action', 'history_log.action_id', '=', 'action.id')
                              ->leftJoin('user', 'history_log.user_id', '=', 'user.id');

        return $consulta;
    }


    /**
     * Transform buttons.
     *
     * @param $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'action_history_id':
                    $fila[] = $this->getShowButton(route('configuration.historylog.edit', $value), $value, true);
                    break;
                default:
                    $fila[] = $value;
                    break;
            }
        }
        return $fila;
    }
}
