<?php

namespace Canela\CanelaTools\Requests\Location;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class ProvinceRefreshRequest
 * @package Canela\CanelaTools\Requests\Location
 */
class ProvinceRefreshRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            // 'country_id' => 'integer|exists:country,id',
        );

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'country_id' => trans('canelatools::canelatools.entity.province.field.country_id'),
        ];
    }
}
