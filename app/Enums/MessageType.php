<?php

namespace Canela\CanelaTools\Enums;

/**
 * Class MessageType
 * @package App\Http\Enums
 */
abstract class MessageType extends Enum
{
    const SUCCESS   = 'message';
    const INFO      = 'msgInfo';
    const WARNING   = 'msgWarning';
    const ERROR     = 'msgError';
}
