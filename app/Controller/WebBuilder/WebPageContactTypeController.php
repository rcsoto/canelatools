<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Parameter;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\WebPageContactType;
use Canela\CanelaTools\Requests\WebBuilder\WebPageContactTypeRequest;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * Class WebPageContactTypeController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageContactTypeController extends BasicController
{
    use HtmlLists;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageContactType::class;
        $this->routeEntity = 'admin.webbuilder.webpagecontacttype';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::WEB_PAGE_CONTACT_TYPE;
        $this->menu_group = AdminMenu::GROUP_CATALOGUE;

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->init($this->entidad)
            ->setTableColumn([
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'contact',
                    label: trans(($this->entidad)::getResourceFieldTrans('contact')),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'icon',
                    label: trans('canelatools::canelatools.general.field.icon'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    comboList: State::comboTranslate(),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'contact',
                    label: trans(($this->entidad)::getResourceFieldTrans('contact')),
                    required: true,
                    containerClass: 'col-12 col-sm-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'icon',
                    label: trans('canelatools::canelatools.general.field.icon') . ' ('.Parameter::getValue('label-icon-info').')',
                    containerClass: 'col-12 col-sm-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    containerClass: 'col-12 col-sm-6 mb-2',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6 mb-2',
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang = App::getLocale();
        $query = WebPageContactType::select([
            'web_page_contact_type.id',
            'web_page_contact_type.contact',
            'web_page_contact_type.icon',
            'web_page_contact_type.position',
            DB::Raw((Language::DEFAULT_CODE == $lang ? "state.name" : "trans('" . $lang . "', 'state', state.id, 'name', state.name)") . " as state_name"),
            'web_page_contact_type.id as action_web_page_contact_type_id',
            'state.id as state_id',
        ])
        ->join('state', 'web_page_contact_type.state_id', 'state.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            switch ($key) {
                case 'action_web_page_contact_type_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, true, true, true, true, false,
                    BasicModel::TYPE_CHANGE_STATE, $jsonItem->state_id,
                    true, true);
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param WebPageContactTypeRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageContactTypeRequest $request): Model|JsonResponse|RedirectResponse|null
    {
        return $this->storeBasic($request);
    }


    /**
     * @param WebPageContactTypeRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageContactTypeRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
