<?php

namespace Canela\CanelaTools\Requests\Client;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class CityRequest
 * @package Canela\CanelaTools\Requests\Location
 */
class RrssRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'state_id'          => 'required|integer|exists:state,id',
            'name'              => 'required|string|min:1|max:45',
            'icon'              => 'nullable|string|min:1|max:45',
        );

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'state_id'  => trans('canelatools::canelatools.general.field.state'),
            'name'      => trans('canelatools::canelatools.entity.rrss.field.name'),
            'icon'      => trans('canelatools::canelatools.entity.rrss.field.icon'),
        ];
    }
}
