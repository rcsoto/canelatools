<?php

namespace Canela\CanelaTools\Controller\Configuration;


use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Configuration\LanguageRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Request;
use stdClass;

/**
 * Class ConfigurationController
 * @package App\Http\Controllers\Admin\Configuration
 */
class LanguageBasicController extends BasicController
{
    use HtmlLists;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Language::class;
        $this->menu       = AdminMenu::LANGUAGE;
        $this->menu_group = AdminMenu::GROUP_CONFIG;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->init($this->entidad)
            ->setTableColumn([
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'id'
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'position',
                    label: ($this->entidad)::getResourceFieldTrans('position'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'name',
                    label: ($this->entidad)::getResourceFieldTrans('name'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'code',
                    label: ($this->entidad)::getResourceFieldTrans('code'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'code2',
                    label: ($this->entidad)::getResourceFieldTrans('code2'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'reference',
                    label: ($this->entidad)::getResourceFieldTrans('reference'),
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'name',
                    label: ($this->entidad)::getResourceFieldTrans('state_id'),
                    fieldFilter: 'id',
                    comboList: State::comboTranslate(),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                ),
                new TableColumn(
                    withOrderBy: false,
                    withFilter: false,
                    label: trans('canelatools::canelatools.general.acciones'),
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'name',
                    label: ($this->entidad)::getResourceFieldTrans('name'),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'code',
                    label: ($this->entidad)::getResourceFieldTrans('code'),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'code2',
                    label: ($this->entidad)::getResourceFieldTrans('code2'),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'reference',
                    label: ($this->entidad)::getResourceFieldTrans('reference'),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'position',
                    label: ($this->entidad)::getResourceFieldTrans('position'),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'state_id',
                    label: ($this->entidad)::getResourceFieldTrans('state_id'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang     = App::getLocale();
        $consulta = Language::select(['language.id',
                                         'language.position',
                                         'language.name',
                                         'language.code',
                                         'language.code2',
                                         'language.reference',
                                         DB::Raw((Language::DEFAULT_CODE == $lang ? "state.name" : "trans('" . $lang . "', 'state', state.id, 'name', state.name)") . " as state_name"),
                                         'language.id as action_language_id',
                                         'state.id as state_id'
                                        ])
                                   ->join('state', 'language.state_id', 'state.id');

        return $this->setSecurityList($consulta);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            switch ($key) {
                case 'action_language_id':
                    $txt = $this->addActionButton($value, ($this->entidad)::getRoutePath(), null,
                                                  true, false, false, true, true, false,
                                                  BasicModel::TYPE_CHANGE_STATE, $jsonItem->state_id,
                                                  true, true);
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param LanguageRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(LanguageRequest $request)
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param LanguageRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(LanguageRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }


}
