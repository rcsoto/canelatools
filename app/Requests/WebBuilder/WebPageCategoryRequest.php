<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageSectionRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageCategoryRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'name',
        'position',
        'description',
        'abbreviation',
        'color_text',
        'color_border',
        'color_bg',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_category.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'position'          => 'required|integer|min:1|max:99999',
            'name'             => 'required|max:255|uniqueXFields:web_page_category,name,page_id,'.$this->get('page_id'),
            'description'       => 'nullable|max:255',
            'abbreviation'      => 'nullable|max:45',
            'color_text'        => 'nullable|max:45',
            'color_border'      => 'nullable|max:45',
            'color_bg'          => 'nullable|max:45',
        );
        if ($this->method() == 'PUT' || $this->method() == 'PATCH')
        {
            $id = $this->route('webpagecategory');
            $rules['name'] = $rules['name'].','.$id;
        }

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');

        return $attributes;
    }

}
