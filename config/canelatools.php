<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Models location to include
    |--------------------------------------------------------------------------
    |
    | Define in which directory are models allocated
    |
    */
    'model_locations'  => 'app/Models',

    /*
    |--------------------------------------------------------------------------
    | Database Schema used in app
    |--------------------------------------------------------------------------
    |
    | Only MySQL database schemas
    |
    */
    'database'         => env('DB_DATABASE'),

    /*
    |--------------------------------------------------------------------------
    | Translation file for the models
    |--------------------------------------------------------------------------
    |
    | Indicate translation files o path to use in translation functions
    | @deprecated
    */
    'lang_file'        => 'model',

    /*
    |--------------------------------------------------------------------------
    | Table with entities
    |--------------------------------------------------------------------------
    |
    | Indicate translation table for entities if exists. Set null if not.
    |
    */
    'entity_table'     => 'entity',
    'entity_reference' => 'reference',
    'entity_name'      => 'name',

    /*
    |--------------------------------------------------------------------------
    | Set tmp Excel to public folder
    |--------------------------------------------------------------------------
    |
    | Set the document folder (or whatever folder) to configure where base Excel
    | should be located.
    |
    */
    'document_root' => 'document/tmp',

    /*
    |--------------------------------------------------------------------------
    | Default upload image max size
    |--------------------------------------------------------------------------
    |
    | Set default upload image max size.
    |
    */
    'default_upload_image_max_size' => 10240,

    /*
    |--------------------------------------------------------------------------
    | BLADE
    |--------------------------------------------------------------------------
    |
    |
    */
    'blade' => [
        'section' => [
            'styles' => 'styles',
            'headScripts' => 'head-scripts',
            'headerPage' => 'header-page',
            'breadcrumbs' => 'breadcrumbs',
            'content' => 'content',
            'contentFooter' => 'content-footer',
            'contentTable' => 'content-table',
            'bodyScripts' => 'body-scripts',
            'bodyScriptsExtra' => 'body-scripts-extra',
            'bodyModal' => 'body-modals',
            'scriptsPopup' => 'scripts-popup',
        ],
        'layouts' => [
            'master'    => 'administrator.layouts.main',
            'message'   => 'layouts.message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | FOLDER
    |--------------------------------------------------------------------------
    |
    |
    */
    'folder' => [
        'storage' => env('FOLDER_STORAGE', 'storage/'),
    ],
];
