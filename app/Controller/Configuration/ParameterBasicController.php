<?php

namespace Canela\CanelaTools\Controller\Configuration;


use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Models\Parameter;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Configuration\ParameterRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class ConfigurationController
 * @package App\Http\Controllers\Admin\Configuration
 */
class ParameterBasicController extends BasicController
{
    use HtmlLists;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Parameter::class;
        $this->routeEntity = 'admin.configuration.parameter';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::PARAMETER;
        $this->menu_group = AdminMenu::GROUP_CONFIG;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.parameter.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.parameter.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: Parameter::ENTITY_TABLE,
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: Parameter::ENTITY_TABLE,
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.parameter.field.name'),
                ),
                new TableColumn(
                    table: Parameter::ENTITY_TABLE,
                    fieldOrder: 'reference',
                    label: trans('canelatools::canelatools.entity.parameter.field.reference'),
                ),
                new TableColumn(
                    table: Parameter::ENTITY_TABLE,
                    fieldOrder: 'value',
                    label: trans('canelatools::canelatools.entity.parameter.field.value'),
                ),
                new TableColumn(
                    withOrderBy: false,
                    withFilter: false,
                    label: trans('canelatools::canelatools.general.acciones'),
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.parameter.field.name'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'reference',
                    label: trans('canelatools::canelatools.entity.parameter.field.reference'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'value',
                    label: trans('canelatools::canelatools.entity.parameter.field.value'),
                ),
            ]);

        parent::__construct();
    }


    /**
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request)
    {
        $query = Parameter::select([
            'parameter.id',
            'parameter.name',
            'parameter.reference',
            'parameter.value',
            'parameter.id as action_parameter_id',
            ]);
        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_parameter_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                                              true, true, false, false,
                                              false, false, 0, 0, true,
                                              true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ParameterRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(ParameterRequest $request)
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ParameterRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(ParameterRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
