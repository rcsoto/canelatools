@php
    $disabled = false;  // FOR TESTING!!!
    $selectInputName = 'selectPrueba';  // FOR TESTING!!!

    /**
     * @param string $selectInputName required
     * @param string $selectInputId
     * @param bool $disabled
     * @param int $value
     **/

    if (!isset($disabled)) { $disabled = false; }
    if (!isset($value)) { $value = null; }
    if (!isset($selectInputId)) { $selectInputId = $selectInputName; }
@endphp
<div class="select-input-container select-input-container-{{ $selectInputName }}">
    <div class="input-group">
        <input type="text" class="form-control form-white select-input-input" placeholder="Selecciona un@" readonly @if ($disabled) disabled @endif>
        {!! Form::hidden($selectInputName, $value, ['id' => $selectInputId]) !!}
        @if (!$disabled)
            <div class="btn-group">
                <a class="btn btn-primary btn-sm btn-select-input btn-select" title="Seleccionar" data-type="select" data-name="{{ $selectInputName }}"><i class="fa fa-mouse-pointer"></i></a>
                <a class="btn btn-info btn-sm btn-select-input btn-clean" title="Limpiar" data-type="clean" data-name="{{ $selectInputName }}"><i class="fa fa-times-circle"></i></a>
                <a class="btn btn-success btn-sm btn-select-input btn-add" title="Añadir nuev@" data-type="add" data-name="{{ $selectInputName }}"><i class="fa fa-plus"></i></a>
                <a class="btn btn-warning btn-sm btn-select-input btn-edit" title="Editar" data-type="edit" data-name="{{ $selectInputName }}"><i class="fa fa-edit"></i></a>
            </div>
        @endif
      </div>
</div>
