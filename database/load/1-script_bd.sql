
-- -----------------------------------------------------
-- Table `sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS .`sessions` ;

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` VARCHAR(191) NOT NULL,
  `user_id` BIGINT(20) UNSIGNED NULL,
  `ip_address` VARCHAR(45) NULL,
  `user_agent` TEXT NULL,
  `payload` TEXT NOT NULL,
  `last_activity` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `sessions_user_id_index` USING BTREE (`user_id`),
  INDEX `essions_last_activity_index` USING BTREE (`last_activity`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `user_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_state` ;

CREATE TABLE IF NOT EXISTS `user_state` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `profile` ;

CREATE TABLE IF NOT EXISTS `profile` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id` INT UNSIGNED NOT NULL,
  `profile_id` INT UNSIGNED NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `token` VARCHAR(255) NULL,
  `remember_token` VARCHAR(255) NULL,
  `email_verified_at` TIMESTAMP NULL,
  `password` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_user_user_state_idx` (`state_id` ASC),
  INDEX `fk_user_profile1_idx` (`profile_id` ASC),
  CONSTRAINT `fk_user_user_state`
    FOREIGN KEY (`state_id`)
    REFERENCES `user_state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_profile1`
    FOREIGN KEY (`profile_id`)
    REFERENCES `profile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `state` ;

CREATE TABLE IF NOT EXISTS `state` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `entity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `entity` ;

CREATE TABLE IF NOT EXISTS `entity` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `reference` VARCHAR(45) NOT NULL,
  `model` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `reference_UNIQUE` (`reference` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `action`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `action` ;

CREATE TABLE IF NOT EXISTS `action` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `reference` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `reference_UNIQUE` (`reference` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `module` ;

CREATE TABLE IF NOT EXISTS `module` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `history_log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `history_log` ;

CREATE TABLE IF NOT EXISTS `history_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` INT UNSIGNED NOT NULL,
  `action_id` INT UNSIGNED NOT NULL,
  `entity_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NULL,
  `register` VARCHAR(4000) NULL,
  `register_id` INT(10) UNSIGNED NULL,
  `comment` VARCHAR(4000) NULL,
  `value_pre` TEXT NULL,
  `value_post` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_history_log_module1_idx` (`module_id` ASC),
  INDEX `fk_history_log_action1_idx` (`action_id` ASC),
  INDEX `fk_history_log_entity1_idx` (`entity_id` ASC),
  INDEX `fk_history_log_user1_idx` (`user_id` ASC),
  INDEX `idx_history_log_register_id` (`register_id` ASC),
  CONSTRAINT `fk_history_log_module1`
    FOREIGN KEY (`module_id`)
    REFERENCES `module` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_log_action1`
    FOREIGN KEY (`action_id`)
    REFERENCES `action` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_log_entity1`
    FOREIGN KEY (`entity_id`)
    REFERENCES `entity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_log_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `password_resets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `password_resets` ;

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` VARCHAR(255) NULL,
  `token` MEDIUMTEXT NULL,
  `created_at` TIMESTAMP NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `calendar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calendar` ;

CREATE TABLE IF NOT EXISTS `calendar` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_festive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `date` DATE NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `date_UNIQUE` (`date` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `configuration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `configuration` ;

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `reference` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL,
  `value` VARCHAR(4000) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `reference_UNIQUE` (`reference` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `language` ;

CREATE TABLE IF NOT EXISTS `language` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `code2` VARCHAR(45) NOT NULL,
  `reference` VARCHAR(45) NOT NULL,
  `position` SMALLINT(3) UNSIGNED NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  UNIQUE INDEX `code2_UNIQUE` (`code2` ASC),
  UNIQUE INDEX `reference_UNIQUE` (`reference` ASC),
  INDEX `fk_language_state1_idx` (`state_id` ASC),
  CONSTRAINT `fk_language_state1`
    FOREIGN KEY (`state_id`)
    REFERENCES `state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parameter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `parameter` ;

CREATE TABLE IF NOT EXISTS `parameter` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `reference` VARCHAR(45) NOT NULL,
  `value` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `translation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `translation` ;

CREATE TABLE IF NOT EXISTS `translation` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `language_id` INT UNSIGNED NOT NULL,
  `entity_id` INT UNSIGNED NOT NULL,
  `register_id` INT(10) NULL,
  `entity` VARCHAR(45) NULL,
  `code` VARCHAR(45) NULL,
  `value` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_translation_language1_idx` (`language_id` ASC),
  INDEX `fk_translation_entity1_idx` (`entity_id` ASC),
  UNIQUE INDEX `translation_unique` (`language_id` ASC, `entity_id` ASC, `register_id` ASC, `code` ASC),
  CONSTRAINT `fk_translation_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_translation_entity1`
    FOREIGN KEY (`entity_id`)
    REFERENCES `entity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `attribute_free_classification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_classification` ;

CREATE TABLE IF NOT EXISTS `attribute_free_classification` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `position` SMALLINT(5) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_type` ;

CREATE TABLE IF NOT EXISTS `attribute_free_type` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free` ;

CREATE TABLE IF NOT EXISTS `attribute_free` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id` INT(10) UNSIGNED NOT NULL,
  `type_id` INT(10) UNSIGNED NOT NULL,
  `classification_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `required` TINYINT(1) NOT NULL DEFAULT '0',
  `multiple` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Indica si como respuesta se puede proporcionar mas de un valor',
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `text_help` VARCHAR(4000) NULL DEFAULT NULL,
  `min_value` VARCHAR(45) NULL DEFAULT NULL,
  `max_value` VARCHAR(45) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_attribute_free_state1_idx` (`state_id` ASC),
  INDEX `fk_attribute_free_free_classification_idx` (`classification_id` ASC),
  INDEX `fk_attribute_free_free_type_idx` (`type_id` ASC),
  CONSTRAINT `fk_attribute_free_state1`
    FOREIGN KEY (`state_id`)
    REFERENCES `state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_free_classification`
    FOREIGN KEY (`classification_id`)
    REFERENCES `attribute_free_classification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_free_type`
    FOREIGN KEY (`type_id`)
    REFERENCES `attribute_free_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free_entity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_entity` ;

CREATE TABLE IF NOT EXISTS `attribute_free_entity` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_free_id` INT(10) UNSIGNED NOT NULL,
  `entity_destination_id` INT(10) UNSIGNED NOT NULL COMMENT 'Entidad sobre la que se muestra el atributo',
  `entity_origin_id` INT(10) UNSIGNED NOT NULL COMMENT 'Entidad sobre la que se aplica condicion para mostrar atributo',
  `register_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `is_apply` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Indica si la condicion es para mostrar (1) o para ocultar (0)',
  `required` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Indica si es obligatorio',
  `multiple` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Indica si se pueden seleccionar multiples valores. Unicamente si el tipo de dato es Lista',
  `position` SMALLINT(5) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_attribute_free_entity_attribute_free1_idx` (`attribute_free_id` ASC),
  INDEX `fk_attribute_free_entity_destination1_idx` (`entity_destination_id` ASC),
  INDEX `fk_attribute_free_entity_origin1_idx` (`entity_origin_id` ASC),
  CONSTRAINT `fk_attribute_free_entity_destination1`
    FOREIGN KEY (`entity_destination_id`)
    REFERENCES `entity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_entity_origin1`
    FOREIGN KEY (`entity_origin_id`)
    REFERENCES `entity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_entity_attribute_free`
    FOREIGN KEY (`attribute_free_id`)
    REFERENCES `attribute_free` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_list` ;

CREATE TABLE IF NOT EXISTS `attribute_free_list` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_free_id` INT(10) UNSIGNED NOT NULL,
  `position` SMALLINT(5) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_attribute_free_list_attribute_idx` (`attribute_free_id` ASC),
  CONSTRAINT `fk_attribute_free_list_attribute_free`
    FOREIGN KEY (`attribute_free_id`)
    REFERENCES `attribute_free` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free_entity_value`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_entity_value` ;

CREATE TABLE IF NOT EXISTS `attribute_free_entity_value` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_free_id` INT(10) UNSIGNED NOT NULL,
  `entity_id` INT(10) UNSIGNED NOT NULL,
  `register_id` INT(10) UNSIGNED NOT NULL,
  `attribute_free_list_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `value` TEXT NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_attribute_free_entity_attributte_free_list_idx` (`attribute_free_list_id` ASC),
  INDEX `fk_attribute_free_entity_entity_idx` (`entity_id` ASC),
  INDEX `fk_attribute_free_entity_attributte_free_idx` (`attribute_free_id` ASC),
  INDEX `idx_attribute_free_entity_value_register_id` (`register_id` ASC),
  CONSTRAINT `fk_attribute_free_entity_attributte_free_list`
    FOREIGN KEY (`attribute_free_list_id`)
    REFERENCES `attribute_free_list` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_entity_entity`
    FOREIGN KEY (`entity_id`)
    REFERENCES `entity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_entity_attributte_free`
    FOREIGN KEY (`attribute_free_id`)
    REFERENCES `attribute_free` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `attribute_free_profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attribute_free_profile` ;

CREATE TABLE IF NOT EXISTS `attribute_free_profile` (
  `attribute_free_id` INT(10) UNSIGNED NOT NULL,
  `profile_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  UNIQUE INDEX `unique` (`attribute_free_id` ASC, `profile_id` ASC),
  INDEX `fk_attribute_free_profile_attribute_free1_idx` (`attribute_free_id` ASC),
  INDEX `fk_attribute_free_profile_profile1_idx` (`profile_id` ASC),
  CONSTRAINT `fk_attribute_free_profile_profile1`
    FOREIGN KEY (`profile_id`)
    REFERENCES `profile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_free_profile_attribute_free`
    FOREIGN KEY (`attribute_free_id`)
    REFERENCES `attribute_free` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `country` ;

CREATE TABLE IF NOT EXISTS `country` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `code2` VARCHAR(2) NOT NULL,
  `code3` VARCHAR(3) NOT NULL,
  `code_number` VARCHAR(3) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `code2_UNIQUE` (`code2` ASC),
  UNIQUE INDEX `code3_UNIQUE` (`code3` ASC),
  UNIQUE INDEX `code_number_UNIQUE` (`code_number` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `province`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `province` ;

CREATE TABLE IF NOT EXISTS `province` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `code` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_province_country1_idx` (`country_id` ASC),
  UNIQUE INDEX `province_unique` (`country_id` ASC, `name` ASC),
  CONSTRAINT `fk_province_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `city`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `city` ;

CREATE TABLE IF NOT EXISTS `city` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `province_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `code` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_city_province1_idx` (`province_id` ASC),
  UNIQUE INDEX `city_unique` (`province_id` ASC, `name` ASC),
  CONSTRAINT `fk_city_province1`
    FOREIGN KEY (`province_id`)
    REFERENCES `province` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notifications`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `notifications` ;

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` CHAR(36) NOT NULL,
  `type` VARCHAR(191) NOT NULL,
  `notifiable_type` VARCHAR(191) NOT NULL,
  `notifiable_id` BIGINT(20) UNSIGNED NOT NULL,
  `data` TEXT NOT NULL,
  `read_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `group_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group_user` ;

CREATE TABLE IF NOT EXISTS `group_user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(4000) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idgroup_user_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_group_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_group_user` ;

CREATE TABLE IF NOT EXISTS `user_group_user` (
  `user_id` INT UNSIGNED NOT NULL,
  `group_id` INT UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`user_id`, `group_id`),
  INDEX `fk_user_group_user_group_user1_idx` (`group_id` ASC),
  CONSTRAINT `fk_table1_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_group_user_group_user1`
    FOREIGN KEY (`group_id`)
    REFERENCES `group_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
