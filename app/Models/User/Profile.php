<?php

namespace Canela\CanelaTools\Models\User;


use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Configuration\Profile
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property \Illuminate\Support\Carbon             $created_at
 * @property \Illuminate\Support\Carbon             $updated_at
 * @property-read string                            $created_at_format
 * @property-read string                            $updated_at_format
 * @property-read string                            $value_identificator
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_short
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class Profile extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profile';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    /**
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'profile_id');
    }


    /**
     * @param array $profiles
     * @return bool
     */
    public static function hasProfile(array $profiles): bool
    {
        if (auth()->check()) {
            $user = auth()->user();
            return empty($user) ? false : ($user->state_id == UserState::ACTIVO && in_array($user->profile_id, $profiles));
        } else {
            return false;
        }
    }

}
