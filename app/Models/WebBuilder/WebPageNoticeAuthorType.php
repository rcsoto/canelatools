<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;

class WebPageNoticeAuthorType extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_notice_author_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'name'          => 'string',
        'description'   => 'string',
    ];

}
