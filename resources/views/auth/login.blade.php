@extends('canelatools::auth.layouts.main')

@php
    use App\Models\Project\WebBuilder\Web;
    $web = Web::firstOrFail();
    $customer = $web->customer;
@endphp

@section('body-class', 'page-login')

@section('content')
    <!-- [ auth-signup ] start -->
    <div class="auth-wrapper">
        <div class="auth-content">
            <div class="card">
                <div class="row align-items-center text-center">
                    <div class="col-md-12">
                        <div class="card-body">
                            @include('canelatools::auth.layouts.logo-container')

                            @if (session('status'))
                                <div class="mt-4 mb-3 text-success font-weight-bold">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <h4 class="mb-3 f-w-400">@lang('canelatools::webbuilder.pages.auth.login.labels.login')</h4>
                            {!! Form::open(['method' => 'POST', 'url' => url('login')]) !!}
                                <div class="form-group mb-3">
                                    <label class="floating-label" for="email">@lang('canelatools::webbuilder.common.fields.email')</label>
                                    <input type="text" class="form-control" name="email" placeholder="" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <div class="form-group mb-4">
                                    <label class="floating-label" for="password">@lang('canelatools::webbuilder.common.fields.password')</label>
                                    <input type="password" class="form-control" name="password" placeholder="">
                                    @if($errors->has('password'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>
                                <div class="custom-control custom-checkbox  text-left mb-4 mt-2">
                                    <input type="checkbox" class="custom-control-input" name="remember">
                                    <label class="custom-control-label" for="remember">@lang('canelatools::webbuilder.pages.auth.login.forms.login.fields.remember-me')</label>
                                </div>
                                <button class="btn btn-primary btn-block mb-4">@lang('canelatools::webbuilder.pages.auth.login.forms.login.buttons.login')</button>

                                @if (Route::has('password.request'))
                                    <a class="" href="{{ route('password.request') }}">
                                        @lang('canelatools::webbuilder.pages.auth.login.forms.login.links.forgot-password')
                                    </a>
                                @endif
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ auth-signup ] end -->
@endsection

