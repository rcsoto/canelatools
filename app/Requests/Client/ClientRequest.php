<?php

namespace Canela\CanelaTools\Requests\Client;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class ClientRequest
 * @package Canela\CanelaTools\Requests\Client
 */
class ClientRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'name',
        'contact_name',
        'cif',
        'full_address',
        'contact_telephone',
        'image_logo',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.client.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'name'              => 'required|string|min:1|max:255',
            'contact_name'      => 'nullable|string|min:1|max:255',
            'cif'               => 'nullable|string|min:1|max:255',
            'full_address'      => 'nullable|string|min:1|max:2000',
            'contact_telephone' => 'nullable|string|min:1|max:45',
            'image_logo'        => 'image|max:'.config('canelatools.default_upload_image_max_size'),
        );

        return $rules;
    }

}
