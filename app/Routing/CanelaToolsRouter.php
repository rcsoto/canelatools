<?php

namespace Canela\CanelaTools\Routing;

use App\Http\Controllers\Admin\Client\ClientController;
use App\Http\Controllers\Admin\Client\CustomerController;
use App\Http\Controllers\Admin\Configuration\LanguageBasicController;
use App\Http\Controllers\Admin\Configuration\ParameterBasicController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\General\ArchiveController;
use App\Http\Controllers\Admin\User\UserController;
use App\Http\Controllers\Admin\WebBuilder\WebController;
use App\Http\Controllers\Admin\WebBuilder\WebLanguageController;
use App\Http\Controllers\Admin\WebBuilder\WebPageCategoryController;
use App\Http\Controllers\Admin\WebBuilder\WebPageContactController;
use App\Http\Controllers\Admin\WebBuilder\WebPageContactTypeController;
use App\Http\Controllers\Admin\WebBuilder\WebPageController;
use App\Http\Controllers\Admin\WebBuilder\WebPageDataController;
use App\Http\Controllers\Admin\WebBuilder\WebPageFaqController;
use App\Http\Controllers\Admin\WebBuilder\WebPageNoticeController;
use App\Http\Controllers\Admin\WebBuilder\WebPageNoticeHoraryController;
use App\Http\Controllers\Admin\WebBuilder\WebPageSectionController;
use App\Http\Controllers\Admin\WebBuilder\WebTemplateController;
use App\Models\User\Profile;
use Canela\CanelaTools\Controller\Configuration\TranslationBasicController;
use Canela\CanelaTools\Controller\LocalizationController;
use Canela\CanelaTools\Controller\System\SystemController;
use Illuminate\Support\Facades\Route;

/**
 * Class CanelaToolsRouter
 * @package Canela\CanelaTools\Routing
 */
class CanelaToolsRouter
{
    /**
     * Configuration Routes.
     *
     */
    public function configuration()
    {
        Route::group(array('namespace' => '\Canela\CanelaTools\Controller\Configuration',
                           'prefix'    => 'configuration',
                           'as'        => 'configuration.'), function () {

            // Historico de Cambios
            Route::resource('historylog', 'HistoryLogBasicController')->only(['index', 'edit']);
            Route::group(array('prefix' => 'historylog', 'as' => 'historylog.'), function () {
                Route::post('list/ajax',        'HistoryLogBasicController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'HistoryLogBasicController@exportAjax')->name('exportAjax');
            });

            // Configuration
            Route::resource('configuration',       'ConfigurationBasicController')->except(['show']);
            Route::group(array('prefix' => 'configuration', 'as' => 'configuration.'), function () {
                Route::post('list/ajax',        'ConfigurationBasicController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'ConfigurationBasicController@exportAjax')->name('exportAjax');
            });

            // Translation
            Route::resource('translation',       'TranslationBasicController')->except(['show']);
            Route::group(array('prefix' => 'translation', 'as' => 'translation.'), function () {
                Route::post('list/ajax',        'TranslationBasicController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'TranslationBasicController@exportAjax')->name('exportAjax');
            });

            // Grupos de usuarios
            Route::resource('groupuser', 'GroupUserController')->except(['show']);
            Route::group(['prefix' => 'groupuser', 'as' => 'groupuser.'], function () {
                Route::post('list/ajax', 'GroupUserController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'GroupUserController@exportAjax')->name('exportAjax');
            });

        });

        Route::group([
            'prefix'    => 'configuration',
            'as'        => 'configuration.',
            ], function () {

            // Parameter
            Route::resource('parameter',       ParameterBasicController::class)->except(['show']);
            Route::group(array('prefix' => 'parameter', 'as' => 'parameter.'), function () {
                Route::post('list/ajax',        [ParameterBasicController::class, 'listAjax'])->name('listAjax');
                Route::post('list/ajax/export', [ParameterBasicController::class, 'exportAjax'])->name('exportAjax');
            });

            // Idiomas
            Route::resource('language',       LanguageBasicController::class)->except(['show']);
            Route::group(array('prefix' => 'language', 'as' => 'language.'), function () {
                Route::post('list/ajax', [LanguageBasicController::class, 'listAjax'])->name('listAjax');
                Route::post('list/ajax/export', [LanguageBasicController::class, 'exportAjax'])->name('exportAjax');
                Route::put('deactivate/{id}', [LanguageBasicController::class, 'deactivate'])->name('deactivate');
                Route::put('activate/{id}', [LanguageBasicController::class, 'activate'])->name('activate');
            });

            // Usuarios
            Route::resource('user', UserController::class)->except(['show', 'destroy']);
            Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
                Route::put('activate/{id}', [UserController::class, 'activate'])->name('activate');
                Route::put('disable/{id}', [UserController::class, 'disable'])->name('disable');
                Route::post('list/ajax', [UserController::class, 'listAjax'])->name('listAjax');
                Route::post('list/ajax/export', [UserController::class, 'exportAjax'])->name('exportAjax');
            });

        });


        Route::group(array('namespace' => '\Canela\CanelaTools\Controller\Database',
            'prefix'    => 'database',
            'as'        => 'database.'), function () {
            // Ejecucion de Sentencias
            Route::get('sql/index', 			'SqlController@index')->name('sql.index');
            Route::post('sql/execute', 			'SqlController@execute')->name('sql.execute');
        });


        // System
        Route::group(array('prefix' => 'system', 'as' => 'system.',
            'namespace' => 'System'), function() {
            Route::get('index', 				[SystemController::class, 'index'])->name('index');
            Route::get('systemData', 			[SystemController::class, 'getSystemData'])->name('systemData');
            Route::post('clearCache', 		    [SystemController::class, 'clearCache'])->name('clearCache');
        });

    }


    /**
     * Location routes.
     *
     */
    public function location()
    {
        Route::group(array('namespace' => '\Canela\CanelaTools\Controller\Location',
                           'prefix'    => 'location',
                           'as'        => 'location.'), function () {

            Route::resource('country', 'CountryController')->except(['show']);
            Route::group(['prefix' => 'country', 'as' => 'country.'], function () {
                Route::post('list/ajax', 'CountryController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'CountryController@exportAjax')->name('exportAjax');
            });

            Route::resource('province', 'ProvinceController')->except(['show']);
            Route::group(['prefix' => 'province', 'as' => 'province.'], function () {
                Route::post('list/ajax', 'ProvinceController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'ProvinceController@exportAjax')->name('exportAjax');
                Route::post('refresh/ajax', 'ProvinceController@refreshAjax')->name('refreshAjax');
            });

            Route::resource('city', 'CityController')->except(['show']);
            Route::group(['prefix' => 'city', 'as' => 'city.'], function () {
                Route::post('list/ajax', 'CityController@listAjax')->name('listAjax');
                Route::post('list/ajax/export', 'CityController@exportAjax')->name('exportAjax');
            });
        });
    }


    /**
     * Translation routes.
     *
     */
    public function translation()
    {
        Route::group(['prefix' => 'translation',
                        'as' => 'translation.'], function () {

            Route::group(['prefix' => 'ajax',
                            'as' => 'ajax.'], function () {
                Route::post('get', [TranslationBasicController::class, 'getTranslation'])->name('get');
                Route::put('set', [TranslationBasicController::class, 'setTranslation'])->name('set');
                Route::delete('delete', [TranslationBasicController::class, 'deleteTranslation'])->name('delete');
            });
        });

    }


    /**
     * Localization routes.
     *
     */
    public function localization()
    {
        Route::group(['prefix' => 'localization',
                        'as' => 'localization.'], function () {

            Route::get('changelocale/{locale}', [LocalizationController::class, 'changeLocale'])->name('change-locale');
        });
    }


    /**
     * WebBuilder admin panel routes.
     */
    public function webBuilderAdminPanelRoutes()
    {
        Route::group([], function () {
           $this->translation();

            Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

            // Client
            Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
                // Client
//                Route::middleware(['profile:'. Profile::ADMINISTRADOR])->group(function () {
                    Route::resource('client', ClientController::class)->except(['show', 'destroy']);
                    Route::group(array('prefix' => 'client', 'as' => 'client.'), function () {
                        Route::post('list/ajax', [ClientController::class, 'listAjax'])->name('listAjax');
                        Route::post('list/ajax/export', [ClientController::class, 'exportAjax'])->name('exportAjax');
                        Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                            Route::post('deletefile', [ClientController::class, 'ajaxDeleteFile'])->name('deletefile');
                        });
                    });
//                });

                // Customer
                Route::resource('customer', CustomerController::class)->except(['show', 'destroy']);
                Route::group(array('prefix' => 'customer', 'as' => 'customer.'), function () {
                    Route::post('list/ajax', [CustomerController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [CustomerController::class, 'exportAjax'])->name('exportAjax');
                    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                        Route::post('deletefile', [CustomerController::class, 'ajaxDeleteFile'])->name('deletefile');
                    });
                });
            });

            // General
            Route::group(['prefix' => 'general', 'as' => 'general.'], function () {
                // Archive
                Route::resource('archive', ArchiveController::class)->except(['show']);
                Route::group(['prefix' => 'archive', 'as' => 'archive.'], function () {
                    Route::post('list/ajax', [ArchiveController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [ArchiveController::class, 'exportAjax'])->name('exportAjax');
                    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                        Route::post('deletefile', [ArchiveController::class, 'ajaxDeleteFile'])->name('deletefile');
                    });
                });
            });

            // WebBuilder
            Route::group(['prefix' => 'webbuilder', 'as' => 'webbuilder.'], function () {
                // Web
                Route::resource('web', WebController::class)->except(['show', 'destroy']);
                Route::group(['prefix' => 'web', 'as' => 'web.'], function () {
                    Route::post('list/ajax', [WebController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebController::class, 'exportAjax'])->name('exportAjax');
                });

                // Web Page
                Route::resource('webpage', WebPageController::class)->except(['show', 'destroy']);
                Route::group(['prefix' => 'webpage', 'as' => 'webpage.'], function () {
                    Route::post('list/ajax', [WebPageController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageController::class, 'exportAjax'])->name('exportAjax');
                    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                        Route::post('deletefile', [WebPageController::class, 'ajaxDeleteFile'])->name('deletefile');
                        Route::patch('sortassociatedSections/{id}', [WebPageController::class, 'sortAssociatedSections'])->name('sortassociatedsections');
                        Route::patch('associatearchives/{id}', [WebPageController::class, 'associateArchives'])->name('associatearchives');
                        Route::patch('disassociatearchive/{id?}', [WebPageController::class, 'disassociateArchive'])->name('disassociatearchive');
                        Route::patch('sortassociatedArchives', [WebPageController::class, 'sortAssociatedArchives'])->name('sortassociatedarchives');
                    });
                });

                // Web Page Faq
                Route::resource('webpagefaq', WebPageFaqController::class)->except(['show']);
                Route::group(['prefix' => 'webpagefaq', 'as' => 'webpagefaq.'], function () {
                    Route::put('activate/{id}', [WebPageFaqController::class, 'activate'])->name('activate');
                    Route::put('deactivate/{id}', [WebPageFaqController::class, 'deactivate'])->name('deactivate');
                    Route::post('list/ajax', [WebPageFaqController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageFaqController::class, 'exportAjax'])->name('exportAjax');
                });


                // Web Page Contact
                Route::resource('webpagecontact', WebPageContactController::class)->except(['show']);
                Route::group(['prefix' => 'webpagecontact', 'as' => 'webpagecontact.'], function () {
                    Route::post('list/ajax', [WebPageContactController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageContactController::class, 'exportAjax'])->name('exportAjax');
                });


                // Web Page Contact Type
                Route::resource('webpagecontacttype', WebPageContactTypeController::class)->except(['show']);
                Route::group(['prefix' => 'webpagecontacttype', 'as' => 'webpagecontacttype.'], function () {
                    Route::put('activate/{id}', [WebPageContactTypeController::class, 'activate'])->name('activate');
                    Route::put('deactivate/{id}', [WebPageContactTypeController::class, 'deactivate'])->name('deactivate');
                    Route::post('list/ajax', [WebPageContactTypeController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageContactTypeController::class, 'exportAjax'])->name('exportAjax');
                });

                // Web Page Section
                Route::resource('webpagesection', WebPageSectionController::class)->except(['show']);
                Route::group(['prefix' => 'webpagesection', 'as' => 'webpagesection.'], function () {
                    Route::put('activate/{id}', [WebPageSectionController::class, 'activate'])->name('activate');
                    Route::put('deactivate/{id}', [WebPageSectionController::class, 'deactivate'])->name('deactivate');
                    Route::post('list/ajax', [WebPageSectionController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageSectionController::class, 'exportAjax'])->name('exportAjax');
                    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                        Route::post('deletefile', [WebPageSectionController::class, 'ajaxDeleteFile'])->name('deletefile');
                        Route::patch('associatearchives/{id}', [WebPageSectionController::class, 'associateArchives'])->name('associatearchives');
                        Route::patch('disassociatearchive/{id?}', [WebPageSectionController::class, 'disassociateArchive'])->name('disassociatearchive');
                        Route::patch('sortassociatedArchives', [WebPageSectionController::class, 'sortAssociatedArchives'])->name('sortassociatedarchives');
                    });
                });

                // Web Page Notice
                Route::resource('webpagenotice', WebPageNoticeController::class)->except(['show']);
                Route::group(['prefix' => 'webpagenotice', 'as' => 'webpagenotice.'], function () {
                    Route::put('activate/{id}', [WebPageNoticeController::class, 'activate'])->name('activate');
                    Route::put('deactivate/{id}', [WebPageNoticeController::class, 'deactivate'])->name('deactivate');
                    Route::post('list/ajax', [WebPageNoticeController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageNoticeController::class, 'exportAjax'])->name('exportAjax');
                    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
                        Route::post('deletefile', [WebPageNoticeController::class, 'ajaxDeleteFile'])->name('deletefile');
                        Route::patch('associatearchives/{id}', [WebPageNoticeController::class, 'associateArchives'])->name('associatearchives');
                        Route::patch('disassociatearchive/{id?}', [WebPageNoticeController::class, 'disassociateArchive'])->name('disassociatearchive');
                        Route::patch('sortassociatedArchives', [WebPageNoticeController::class, 'sortAssociatedArchives'])->name('sortassociatedarchives');
                    });
                });

                // Web Page Notice Horary
                Route::resource('webpagenoticehorary', WebPageNoticeHoraryController::class)->except(['show']);
                Route::group(['prefix' => 'webpagenoticehorary', 'as' => 'webpagenoticehorary.'], function () {
                    Route::post('list/ajax', [WebPageNoticeHoraryController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageNoticeHoraryController::class, 'exportAjax'])->name('exportAjax');
                });

                // Web Template
                Route::resource('webtemplate', WebTemplateController::class)->except(['show', 'destroy']);
                Route::group(array('prefix' => 'webtemplate', 'as' => 'webtemplate.'), function () {
                    Route::post('list/ajax', [WebTemplateController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebTemplateController::class, 'exportAjax'])->name('exportAjax');
                });


                // Web Page Category
                Route::resource('webpagecategory', WebPageCategoryController::class)->except(['show']);
                Route::group(array('prefix' => 'webpagecategory', 'as' => 'webpagecategory.'), function () {
                    Route::post('list/ajax', [WebPageCategoryController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageCategoryController::class, 'exportAjax'])->name('exportAjax');
                });

                // Web Page Data
                Route::resource('webpagedata', WebPageDataController::class)->except(['show']);
                Route::group(array('prefix' => 'webpagedata', 'as' => 'webpagedata.'), function () {
                    Route::post('list/ajax', [WebPageDataController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebPageDataController::class, 'exportAjax'])->name('exportAjax');
                });


                // Web Language
                Route::resource('weblanguage', WebLanguageController::class)->except(['show']);
                Route::group(array('prefix' => 'weblanguage', 'as' => 'weblanguage.'), function () {
                    Route::post('list/ajax', [WebLanguageController::class, 'listAjax'])->name('listAjax');
                    Route::post('list/ajax/export', [WebLanguageController::class, 'exportAjax'])->name('exportAjax');
                });


            });

        });
    }
}
