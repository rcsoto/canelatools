<?php

namespace Canela\CanelaTools\Manager\SecurityManager;

use Exception;

trait SecurityManagerTrait
{
    /**
     * Establece la seguridad de una consulta para datos de una lista
     * @param $query
     * @return mixed
     * @throws Exception
     */
    public function setSecurityList($query)
    {
        return $query;
    }
}
