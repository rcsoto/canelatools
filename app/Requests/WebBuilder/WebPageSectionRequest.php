<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageSectionRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageSectionRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'title',
        'subject',
        'body',
        'slug',
        'seo_title',
        'seo_description',
        'file_header_id',
        'file_thumb_id',
        'external_link',
        'open_in_new_window',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_section.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'state_id'          => 'required|integer|min:1|exists:state,id',
            'position'          => 'required|integer|min:1|max:99999',
            'slug'              => 'nullable|max:255',
            'title'             => 'required|max:255',
            'subject'           => 'nullable|max:4000',
            'body'              => 'nullable|max:'.TableColumnMaxSize::LONGTEXT_MAX,
            'seo_title'         => 'nullable|max:255',
            'seo_description'   => 'nullable|max:4000',
            'file_header_id'    => 'nullable|integer|min:1|exists:archive,id',
            'file_thumb_id'     => 'nullable|integer|min:1|exists:archive,id',
            'external_link'     => 'max:4000',
            'open_in_new_window'=> 'required|integer|between:0,1',
        );
        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');

        return $attributes;
    }

}
