<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;

class WebState extends BasicModel
{
    const ELABORACION   = 1;
    const PUBLICADO     = 2;
    const REVISION      = 3;
    const BORRADO       = 4;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'reference',
        'value',
    ];

}
