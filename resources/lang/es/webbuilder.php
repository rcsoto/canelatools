<?php

return [
    'common' => [
        'fields' => [
            'email' => 'Email',
            'password' => 'Contraseña',
        ],
        'buttons' => [
            'send' => 'Enviar',
            'back' => 'Volver',
        ],
        'labels' => [
            'legal-conditions' => 'Condiciones legales',
            'monday-to-friday' => 'Lunes - Viernes',
            'saturday-and-sunday' => 'Sábado - Domingo',
            'phone' => 'Teléfono',
            'see-more' => 'Ver más',
            'home' => 'Inicio',
            'about-me' => 'Acerca de',
            'query' => 'Consulta',
            'download' => 'Descargar',
            'profile' => 'Perfil',
        ],
    ],
    'pages' => [
        'auth' => [
            'login' => [
                'labels' => [
                    'login' => 'Iniciar sesión',
                ],
                'forms' => [
                    'login' => [
                        'fields' => [
                            'remember-me' => 'Mantener la sesión abierta',
                        ],
                        'links' => [
                            'forgot-password' => '¿Olvidaste la contraseña?',
                        ],
                        'buttons' => [
                            'login' => 'Entrar',
                        ],
                    ]
                ]
            ],
            'forgot-password' => [
                'labels' => [
                    'forgot-your-password-no-problem' => '¿Olvidaste la contraseña? No hay problema. Sólo indícanos tu email y te enviaremos un enlace para que la puedas cambiar.',
                ],
                'forms' => [
                    'forgot-password' => [
                        'fields' => [
                        ],
                    ],
                ]
            ],
            'reset-password' => [
                'forms' => [
                    'reset-password' => [
                        'fields' => [
                            'password_confirmation' => 'Confirmación de contraseña',
                        ],
                        'buttons' => [
                            'reset-password' => 'Cambiar contraseña',
                        ]
                    ]
                ]
            ],
        ],
        'admin' => [
            'common' => [
                'main-menu' => [
                    'configuration' => [
                        'label' => 'Configuración',
                    ],
                    'catalogue' => [
                        'label' => 'Catálogo',
                    ],
                    'web' => [
                        'label' => 'Web',
                    ],
                    'utilities' => [
                        'label' => 'Utilidades',
                        'items' => [
                            'sql' => 'Sentencias SQL',
                        ]
                    ],
                ],
            ],
            'dashboard' => [
                'labels' => [
                ],
                'forms' => [
                ],
                'menus' => [
                ],
            ],
        ],
    ]
];
