<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\WebTemplate;
use Canela\CanelaTools\Requests\WebBuilder\WebTemplateRequest;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use stdClass;

/**
 * Class WebTemplateController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebTemplateController extends BasicController
{
    use HtmlLists;

    /**
     * WebTemplateController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebTemplate::class;
        $this->routeEntity = 'admin.webbuilder.webtemplate';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_template.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_template.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'id',

                ),
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'code',
                    label: trans('canelatools::canelatools.entity.web_template.field.code'),
                ),
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_template.field.name'),
                ),
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'onepage',
                    label: trans('canelatools::canelatools.entity.web_template.field.onepage'),
                ),
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'description',
                    label: trans('canelatools::canelatools.entity.web_template.field.description'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'code',
                    label: trans('canelatools::canelatools.entity.web_template.field.code'),
                    required: true,
                    max: 45,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.web_template.field.name'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'onepage',
                    label: trans('canelatools::canelatools.entity.web_template.field.onepage'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebTemplate::comboBoolean(),
                ),
                new TableForm(
                    name: 'description',
                    label: trans('canelatools::canelatools.entity.web_template.field.description'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = WebTemplate::select([  'web_template.id',
            'web_template.code',
            'web_template.name',
            'web_template.onepage',
            'web_template.description',
            'web_template.id as action_web_template_id',
        ]);

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = match ($key) {
                'onepage' => WebTemplate::booleanToTextStatic($value),
                'action_web_template_id' => $this->addActionButton($value, $this->routeEntity, null,
                    true, false, false, false,
                    false, false, 0, 0, true,
                    true),
                default => $value
            };
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WebTemplateRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(WebTemplateRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request, route('admin.webbuilder.webtemplate.index'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param WebTemplateRequest $request
     * @param integer              $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception|ModelNotFoundException
     */
    public function update(WebTemplateRequest $request, $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
