<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebTemplateRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebTemplateRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'code',
        'name',
        'onepage',
        'description',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_template.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'          => 'required|max:45',
            'name'          => 'required|max:255',
            'onepage'       => 'required|integer|between:0,1',
            'description'   => 'max:4000',
        ];
    }

}
