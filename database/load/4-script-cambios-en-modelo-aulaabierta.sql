-- -----------------------------------------------------
-- External Link
-- -----------------------------------------------------

ALTER TABLE `web_page`
ADD COLUMN `external_link` VARCHAR(4000) NULL AFTER `tags`;

ALTER TABLE `web_page_faq`
ADD COLUMN `external_link` VARCHAR(4000) NULL AFTER `description`;

ALTER TABLE `web_page_gallery`
ADD COLUMN `external_link` VARCHAR(4000) NULL AFTER `tags`;

ALTER TABLE `web_page_notice`
ADD COLUMN `external_link` VARCHAR(4000) NULL AFTER `seo_description`;

ALTER TABLE `web_page_section`
ADD COLUMN `external_link` VARCHAR(4000) NULL AFTER `seo_description`;

-- -----------------------------------------------------
-- Table `archive_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `archive_type` ;

CREATE TABLE IF NOT EXISTS `archive_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`created_at`,`updated_at`) values
('Tipo de archivo', 'archive_type', now(), now());

ALTER TABLE `web_page_notice`
ADD COLUMN `archive_type_id` INT(10) UNSIGNED NULL AFTER `state_id`,
ADD INDEX `fk_web_page_notice_archive_type1_idx` (`archive_type_id` ASC) ;
ALTER TABLE `web_page_notice`
ADD CONSTRAINT `fk_web_page_notice_archive_type1`
  FOREIGN KEY (`archive_type_id`)
  REFERENCES `archive_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `archive_type` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Video', 'Video', now(), now()),
(2, 'Pdf', 'Pdf', now(), now()),
(3, 'Imagen', 'Imagen', now(), now()),
(4, 'Podcasts', 'Podcasts', now(), now()),
(5, 'YouTube', 'Política de Privacidad', now(), now());

-- -----------------------------------------------------
-- Table `web_page_notice`
-- -----------------------------------------------------
ALTER TABLE `web_page_notice`
ADD COLUMN `coming_soon` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `archive_type_id`,
ADD COLUMN `important` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `coming_soon`;

ALTER TABLE `web_page_notice`
ADD COLUMN `is_live` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Es en directo' AFTER `important`;

ALTER TABLE `web_page_notice`
ADD COLUMN `image_thumbnail` VARCHAR(255) NULL AFTER `image_thumb`,
ADD COLUMN `archive` VARCHAR(255) NULL AFTER `image_thumbnail`,
ADD COLUMN `archive_duration` INT(6) UNSIGNED NULL AFTER `archive`;

-- -----------------------------------------------------
-- Table `web_page` -> unicamente enlace
-- -----------------------------------------------------
ALTER TABLE `web_page`
ADD COLUMN `is_external_link` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Indica que es un enlace a otra pagina externa, no tiene contenido' AFTER `show_header`;

-- -----------------------------------------------------
--
-- VERSION 2
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `web` -> `open_in_new_window`
-- -----------------------------------------------------
ALTER TABLE `web_page`
ADD COLUMN `open_in_new_window` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_external_link`;

ALTER TABLE `web_page_gallery`
ADD COLUMN `open_in_new_window` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `page_id`;

ALTER TABLE `web_page_section`
ADD COLUMN `open_in_new_window` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `state_id`;

ALTER TABLE `web_page_notice`
ADD COLUMN `open_in_new_window` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_live`;

ALTER TABLE `web_page_faq`
ADD COLUMN `open_in_new_window` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `state_id`;


-- -----------------------------------------------------
-- Table `web_page_notice_author_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_notice_author_type` ;

CREATE TABLE IF NOT EXISTS `web_page_notice_author_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web_page_notice_author`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_notice_author` ;

CREATE TABLE IF NOT EXISTS `web_page_notice_author` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_id` INT UNSIGNED NOT NULL,
  `type_id` INT UNSIGNED NULL,
  `name` VARCHAR(255) NULL,
  `employee` VARCHAR(255) NULL,
  `company` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_web_page_author_web_page_notice1_idx` (`notice_id` ASC),
  INDEX `fk_web_page_author_web_page_notice_author_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_web_page_author_web_page_notice1`
    FOREIGN KEY (`notice_id`)
    REFERENCES `web_page_notice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_author_web_page_notice_author_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `web_page_notice_author_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`created_at`,`updated_at`) values
('Noticias - Tipo de autor', 'web_page_notice_author_type', now(), now()),
('Noticias - Autores', 'web_page_notice_author', now(), now());


-- -----------------------------------------------------
-- Table `web_notice_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_notice_category` ;

CREATE TABLE IF NOT EXISTS `web_notice_category` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL,
  `color_text` VARCHAR(45) NULL,
  `color_border` VARCHAR(45) NULL,
  `color_bg` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web_page_notice_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_notice_category` ;

CREATE TABLE IF NOT EXISTS `web_page_notice_category` (
  `notice_id` INT UNSIGNED NOT NULL,
  `category_id` INT UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  INDEX `fk_web_page_notice_category_web_page_notice1_idx` (`notice_id` ASC),
  INDEX `fk_web_page_notice_category_web_notice_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_web_page_notice_category_web_page_notice1`
    FOREIGN KEY (`notice_id`)
    REFERENCES `web_page_notice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_notice_category_web_notice_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `web_notice_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`created_at`,`updated_at`) values
('Noticias cetegoria', 'web_notice_category', now(), now()),
('Noticias vs Categorías', 'web_page_notice_category', now(), now());

-- -----------------------------------------------------
-- Table `web_page_notice_category`
-- -----------------------------------------------------
ALTER TABLE `web_page_gallery`
ADD COLUMN `text_external_link` VARCHAR(255) NULL AFTER `tags`;



-- -----------------------------------------------------
--
-- VERSION 3
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `customer_rrss` -> revision de indice unico
-- -----------------------------------------------------
ALTER TABLE `customer_rrss`
DROP INDEX `unique_idx` ;
ALTER TABLE `customer_rrss`
ADD UNIQUE INDEX `unique_idx` (`customer_id` ASC, `rrss_id` ASC);


-- -----------------------------------------------------
-- Table `web_page_gallery` -> subtitle
-- -----------------------------------------------------
ALTER TABLE `web_page_gallery`
ADD COLUMN `subtitle` VARCHAR(4000) NULL AFTER `name`;


-- -----------------------------------------------------
-- Table `web_notice_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_notice_type` ;

CREATE TABLE IF NOT EXISTS `web_notice_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`created_at`,`updated_at`) values
('Tipo de noticia', 'web_notice_type', now(), now());

ALTER TABLE `web_page_notice`
ADD COLUMN `type_id` INT(10) UNSIGNED NULL AFTER `state_id`,
ADD INDEX `fk_web_page_notice_web_notice_type1_idx` (`type_id` ASC) ;

ALTER TABLE `web_page_notice`
ADD CONSTRAINT `fk_web_page_notice_web_notice_type1`
  FOREIGN KEY (`type_id`)
  REFERENCES `web_notice_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


-- -----------------------------------------------------
--
-- VERSION 3
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `web_notice_category` campo abreviatura
-- -----------------------------------------------------
ALTER TABLE `web_notice_category`
ADD COLUMN `abbreviation` VARCHAR(45) NULL AFTER `name`,
CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ;


-- -----------------------------------------------------
--
-- VERSION 4
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `web_page_notice_archive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_notice_archive` ;

CREATE TABLE IF NOT EXISTS `web_page_notice_archive` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_id` INT UNSIGNED NOT NULL,
  `type_id` INT UNSIGNED NOT NULL,
  `position` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1,
  `duration` SMALLINT(5) UNSIGNED NULL,
  `archive` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_web_page_notice_archive_web_page_notice1_idx` (`notice_id` ASC),
  INDEX `fk_web_page_notice_archive_archive_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_web_page_notice_archive_web_page_notice1`
    FOREIGN KEY (`notice_id`)
    REFERENCES `web_page_notice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_notice_archive_archive_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `archive_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `web_page_notice` -> elimnar campos de tipo archivo
-- -----------------------------------------------------
ALTER TABLE `web_page_notice`
DROP FOREIGN KEY `fk_web_page_notice_archive_type1`;

ALTER TABLE `web_page_notice`
DROP COLUMN `archive_duration`,
DROP COLUMN `archive`,
DROP COLUMN `archive_type_id`,
DROP INDEX `fk_web_page_notice_archive_type1_idx` ;

-- -----------------------------------------------------
--
-- VERSION 5
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `notification_web`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `notification_web` ;

CREATE TABLE IF NOT EXISTS `notification_web` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `endpoint` VARCHAR(4000) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) -- ,
  -- UNIQUE INDEX `endpoint_UNIQUE` (`endpoint` ASC)
  )
ENGINE = InnoDB;
