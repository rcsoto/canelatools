<?php namespace Canela\CanelaTools\Models\General;

use Canela\CanelaTools\Models\BasicModel;

class NotificationWeb extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification_web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'endpoint',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'endpoint'          => 'string',
    ];


    /**
     * Add new value
     * @param $endpoint
     */
    public static function add($endpoint) {
        NotificationWeb::updateOrCreate(['endpoint' => $endpoint],
            ['endpoint' => $endpoint]);
    }
}
