<?php

namespace Canela\CanelaTools\Models\User;

use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

/**
 * Canela\CanelaTools\Models\User
 *
 * @property int                                                                                                            $id
 * @property string                                                                                                         $login
 * @property string|null                                                                                                    $password
 * @property string                                                                                                         $name
 * @property string                                                                                                         $email
 * @property int                                                                                                            $state_id
 * @property int                                                                                                            $profile_id
 * @property string                                                                                                         $token
 * @property string|null                                                                                                    $remember_token
 * @property \Illuminate\Support\Carbon                                                                                     $created_at
 * @property \Illuminate\Support\Carbon                                                                                     $updated_at
 * @property string|null                                                                                                    $email_verified_at
 * @property-read string                                                                                                    $created_at_format
 * @property-read string                                                                                                    $created_at_short
 * @property-read string                                                                                                    $updated_at_format
 * @property-read string                                                                                                    $updated_at_short
 * @property-read string                                                                                                    $value_identificator
 * @property-read string                                                                                                    $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity                                                                         $table
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Canela\CanelaTools\Models\User\Profile                                                                   $profile
 * @property-read \Canela\CanelaTools\Models\User\UserState                                                                 $state
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\User\User whereToken($value)
 * @mixin \Eloquent
 */
class User extends BasicModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;

    const ENTITY_TABLE = 'user';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'state_id',
        'profile_id',
        'client_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'name'              => 'string',
        'email'             => 'string',
        'state_id'          => 'integer',
        'profile_id'        => 'integer',
        'client_id'         => 'integer',
        'token'             => 'string',
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'created_at', 'updated_at', 'token', 'remember_token');


    /**
     * Related status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(UserState::class);
    }

    /**
     * Related profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }


    /**
     * Related groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(GroupUser::class, 'user_group_user', 'user_id', 'group_id');
    }


    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
