<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\State;

class WebPageType extends BasicModel
{
    const IS_HOME           = 'is_home';
    const WITH_SECTION      = 'with_section';
    const WITH_SLIDER       = 'with_slider';
    const WITH_GALLERY      = 'with_gallery';
    const WITH_MAP          = 'with_map';
    const WITH_DATA_CONTACT = 'with_data_contact';
    const WITH_PLAN         = 'with_plan';
    const WITH_NOTICE       = 'with_notice';
    const WITH_FAQ          = 'with_faq';
    const WITH_DATA         = 'with_data';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'is_home',
        'with_section',
        'with_slider',
        'with_gallery',
        'with_map',
        'with_data_contact',
        'with_plan',
        'with_notice',
        'with_faq',
        'with_horary',
        'with_data',
        'multiple',
        'name',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'state_id'          => 'int',
        'is_home'           => 'bool',
        'with_section'      => 'bool',
        'with_slider'       => 'bool',
        'with_gallery'      => 'bool',
        'with_map'          => 'bool',
        'with_data_contact' => 'bool',
        'with_plan'         => 'bool',
        'with_notice'       => 'bool',
        'with_faq'          => 'bool',
        'multiple'          => 'bool',
        'with_horary'       => 'bool',
        'with_data'         => 'bool',
        'name'              => 'string',
        'description'       => 'string',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state() {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images() {
        return $this->hasMany(WebPageTypeImage::class, 'type_id')
                    ->orderBy('position', 'asc');
    }

    /**
     * Imagen principal
     * @return mixed
     */
    public function image() {
        return $this->images()->first();
    }

}
