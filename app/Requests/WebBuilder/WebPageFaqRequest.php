<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageFaqRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageFaqRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'name',
        'position',
        'description',
        'state_id',
        'external_link',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_faq.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'name'              => 'required|max:4000',
            'position'          => 'required|integer|min:1|max:99999',
            'description'       => 'nullable|max:'.TableColumnMaxSize::TEXT_MAX,
            'state_id'          => 'required|integer|min:1|exists:state,id',
            'external_link'     => 'max:4000',
        );

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');

        return $attributes;
    }

}
