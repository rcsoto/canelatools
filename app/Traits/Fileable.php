<?php


namespace Canela\CanelaTools\Traits;

use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Manager\UtilsBasicManager;
use Canela\CanelaTools\Models\Table\TableForm;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Log;
use Storage;

trait Fileable
{
    /**
     * @param $request
     * @param $fieldName
     * @param $path
     * @param $model
     * @param string $disk
     * @param null $filename
     * @param true $deletePrevios
     */
    public function saveFile($request, $fieldName, $path, $model, $disk = 'public', $filename = null, $deletePrevios = true)
    {
        if ($request->has($fieldName)) {
            if ($deletePrevios) {
                $oldFilename = $model->$fieldName;
                $filePath = $path.$oldFilename;
                if (Storage::disk($disk)->exists($filePath)) {
                    Storage::disk($disk)->delete($filePath);
                }
            }

            $request->file($fieldName)->storeAs($path, $filename, $disk);
            $model->$fieldName = $filename;
            $model->save();
        }
    }


    /**
     * @param $model
     * @param $request
     * @throws Exception
     */
    private function saveFiles($model, $request)
    {
        foreach ($this->tableRest->getTableColumnTypesFields(TableColumnTypes::fileTypes) as $field) {
            /** @var TableForm $field */
            if ($request->has($field->name)) {
                $this->saveFile(
                    request: $request,
                    fieldName: $field->name,
                    path: $field->getFileDir(),
                    model: $model,
                    filename: $this->getFileNameWithExtension($model, $request, $field)
                );
            }
        }
    }


    /**
     * @param $model
     * @param $request
     * @param $field
     * @return string
     */
    private function getFileNameWithExtension($model, $request, $field): string
    {
        $extension = $request->file($field->name)->getClientOriginalExtension();
        if (empty($extension)) {
            $extension = $request->file($field->name)->extension();
        }

        if (!is_null($field->getFileName())) {
            $filename = $model->id.'-'.$field->getFileName().'.'.$extension;
        } else {
            $filename = $model->id.'-'.UtilsBasicManager::sanitizeFileNameWithExtension($request->file($field->name)->getClientOriginalName());
        }

        return $filename;
    }


    /**
     * @param $model
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function storeFilesPost($model, $request): mixed
    {
        $this->saveFiles($model, $request);
        return $model;
    }


    /**
     * @param $model
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function updateFilesPost($model, $request): mixed
    {
        $this->saveFiles($model, $request);
        return $model;
    }


    /**
     * @param $fieldName
     * @param $path
     * @param $model
     * @param string $disk
     * @return bool
     */
    public function deleteFile($fieldName, $path, $model, $disk = 'public'): bool
    {
        try {
            if (!is_null($model->$fieldName)) {
                $filePath = $path.$model->$fieldName;
                if (Storage::disk($disk)->exists($filePath)) {
                    Storage::disk($disk)->delete($filePath);
                }
                $model->$fieldName = null;
                $model->save();
            }

            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxDeleteFile(Request $request): JsonResponse
    {
        $request->validate([
            'id'            => 'required|integer|min:1',
            'field_name'    => 'required|filled',
            'dir'           => 'required|filled',
        ]);

        try {
            $model = ($this->entidad)::findOrFail($request->id);

            return response()->json(['result' => $this->deleteFile(fieldName: $request->field_name, path: $request->dir, model: $model) ? 1 : 0 ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['result' => 0]);
        }
    }

}
