<?php

namespace Canela\CanelaTools\Models\AttributeFree;


use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\User\Profile;
use Illuminate\Support\Collection;
use Canela\CanelaTools\Traits\Deletable;
use Canela\CanelaTools\Contracts\DeletableContract;


/**
 * Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification
 *
 * @property int                                                                                                               $id
 * @property int                                                                                                               $state_id
 * @property int                                                                                                               $type_id
 * @property int|null                                                                                                          $classification_id
 * @property int                                                                                                               $required
 * @property int                                                                                                               $multiple Indica si como respuesta se puede proporcionar mas de un valor
 * @property string                                                                                                            $name
 * @property string|null                                                                                                       $description
 * @property string|null                                                                                                       $text_help
 * @property string|null                                                                                                       $min_value
 * @property string|null                                                                                                       $max_value
 * @property \Carbon\Carbon|null                                                                                               $created_at
 * @property \Carbon\Carbon|null                                                                                               $updated_at
 * @property-read string                                                                                                       $created_at_format
 * @property-read string                                                                                                       $updated_at_format
 * @property-read string                                                                                                       $table
 * @property-read string                                                                                                       $combo_identificator
 * @property-read string                                                                                                       $value_identificator
 * @property-read string                                                                                                       $created_at_short
 * @property-read string                                                                                                       $updated_at_short
 * @property-read \Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification|null                                    $classification
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity[]      $entities
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList[]        $list
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntityValue[] $values
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\User\Profile[]                           $profiles
 * @property-read \Canela\CanelaTools\Models\AttributeFree\AttributeFreeType                                                   $type
 * @property-read \Canela\CanelaTools\Models\State                                                                             $state
 * @property-read string                                                                                                       $name_by_classification
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereClassificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereMaxValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereMinValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereMultiple($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereTextHelp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListOrd()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @mixin \Eloquent
 */
class AttributeFree extends BasicModel implements DeletableContract
{
    use Deletable;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_free';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'type_id',
        'classification_id',
        'required',
        'multiple',
        'name',
        'description',
        'text_help',
        'min_value',
        'max_value',
    ];

    protected $with = ['classification'];
    

    /**
     * Related state
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * Related attribute type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AttributeFreeType::class, 'type_id');
    }


    /**
     * Related attribute classification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classification()
    {
        return $this->belongsTo(AttributeFreeClassification::class, 'classification_id');
    }


    /**
     * Related attribute free list.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function list()
    {
        return $this->hasMany(AttributeFreeList::class, 'attribute_free_id');
    }


    /**
     * Related entities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entities()
    {
        return $this->hasMany(AttributeFreeEntity::class, 'attribute_free_id');
    }


    /**
     * Related associated values.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(AttributeFreeEntityValue::class, 'attribute_free_id');
    }


    /**
     * Related profiles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }


    /**
     *
     * @return string
     */
    public function getNameByClassificationAttribute()
    {
        return empty($this->classification_id)
            ? $this->name
            : "{$this->name} ({$this->classification->name})";
    }



    //TODO: NO COMPROBADO ⬇🔽🔽

    /**
     *
     * @param AttributeFree $registroDestino Registro en el que se van a mostrar los atributos
     * @param bool          $withExclude
     * @param array         $typesExcludes
     * @return array|null
     */
    public static function getAttributesFree($registroDestino, $withExclude = true, $typesExcludes = [])
    {
        $consulta = null;
        $txtUnion = "";

        $whereExcludeAttrFree = '';
        if ($withExclude) {
            $atributesExclude     = self::getAttributeFreeNotApply($registroDestino);
            $whereExcludeAttrFree = empty($atributesExclude) ? '' : '  and attribute_free.id not in (' . Utilidades::canelaSplit($atributesExclude, ',', '', '') . ') ';
        }

        foreach ($registroDestino->getEntidadesCatalogo() as $entidadOrigen) {
            // Si es una collection nos quedamos con el primero
            if ($entidadOrigen instanceof Collection) {
                $entidadOrigen = $entidadOrigen[0];
            }

            // consulta
            $consulta .= $txtUnion .
                         "select attribute_free_classification.id as attribute_free_classification_id,
						attribute_free_classification.name as attribute_free_classification_name,
						attribute_free_classification.description as attribute_free_classification_description,
						attribute_free_classification.position as attribute_free_classification_position,
						attribute_free.id as attribute_free_id,
						attribute_free.name as attribute_free_name,
						attribute_free.description as attribute_free_description,
						attribute_free.text_help as attribute_free_text_help,
						attribute_free.min_value as attribute_free_min_value,
						attribute_free.max_value as attribute_free_max_value,
						attribute_free_type.id as attribute_free_type_id,
						attribute_free_type.name as attribute_free_type_name,
						attribute_free_type.description as attribute_free_type_description,
						attribute_free_entity.required as attribute_free_entity_required,
						attribute_free_entity.multiple as attribute_free_entity_multiple,
						attribute_free_entity.position as attribute_free_entity_position,
						attribute_free_entity.id as attribute_free_entity_id,
						GROUP_CONCAT((case
							when attribute_free_type.id = " . AttributeFreeType::LISTA . " then attribute_free_list.name
							else attribute_free_entity_value.value
						 end ), '') as attribute_free_entity_value_valor,
                        GROUP_CONCAT(attribute_free_list.id, ',') as  attribute_free_entity_value_list_id
				from attribute_free
					 inner join attribute_free_entity on attribute_free.id = attribute_free_entity.attribute_free_id
					 inner join entity destino on attribute_free_entity.entity_destination_id = destino.id
					 inner join entity origen on attribute_free_entity.entity_origin_id = origen.id
					 inner join attribute_free_type on attribute_free.type_id = attribute_free_type.id
					 left join attribute_free_classification on attribute_free.classification_id = attribute_free_classification.id
                     left join attribute_free_entity_value on attribute_free_entity_value.attribute_free_id = attribute_free.id
														  and attribute_free_entity_value.entity_id = destino.id
														  and attribute_free_entity_value.register_id = " . $registroDestino->id . "
				     left join attribute_free_list on attribute_free_entity_value.attribute_free_list_id = attribute_free_list.id
				where destino.name = '" . $registroDestino->getTableName() . "'
				  and origen.name = '" . $entidadOrigen->getTableName() . "'
				  and attribute_free_entity.register_id = " . $entidadOrigen->id . "
				  and attribute_free.state_id = " . State::ACTIVO . "
				  and attribute_free_entity.is_apply = 1 " .
                         (empty($typesExcludes) ? '' : "  and attribute_free_type.id not in (" . Utilidades::canelaSplit($typesExcludes, ',', null, null) . " ) ") .
                         $whereExcludeAttrFree . "
				group by attribute_free_classification.id,
						attribute_free_classification.name,
						attribute_free_classification.description,
						attribute_free_classification.position,
						attribute_free.id,
						attribute_free.name,
						attribute_free.description,
						attribute_free.text_help,
						attribute_free.min_value,
						attribute_free.max_value,
						attribute_free_type.id,
						attribute_free_type.name,
						attribute_free_type.description,
						attribute_free_entity.required,
						attribute_free_entity.multiple,
						attribute_free_entity.position,
						attribute_free_entity.id ";
            $txtUnion = " union all ";
        }
        // Vacia ?
        if (empty($consulta)) {
            return null;
        }
        // Continuar
        $consulta .= " order by attribute_free_classification_position, attribute_free_entity_position ";

        $atributos = \DB::select($consulta);

        // recorremos el array y lo agrupamos por Clasificacion
        $resultado       = [];
        $indice          = 0;
        $clasificacionId = -1;
        foreach ($atributos as $atributo) {
            if ($clasificacionId != $atributo->attribute_free_classification_id) {
                $indice++;
                $clasificacionId = $atributo->attribute_free_classification_id;
            }
            $resultado[$indice][] = $atributo;
        }

        return $resultado;
    }


    /**
     *
     * @param AttributeFree $registroDestino
     * @return array|NULL[]
     */
    private static function getAttributeFreeNotApply($registroDestino)
    {
        $consulta = null;
        $txtUnion = "";


        foreach ($registroDestino->getEntidadesCatalogo() as $entidadOrigen) {
            // Si es una collection nos quedamos con el primero
            if ($entidadOrigen instanceof Collection) {
                $entidadOrigen = $entidadOrigen[0];
            }

            // consulta
            $consulta .= $txtUnion .
                         "	select 	attribute_free.id
					from attribute_free
						 inner join attribute_free_entity on attribute_free.id = attribute_free_entity.attribute_free_id
						 inner join entity destino on attribute_free_entity.entity_destination_id = destino.id
						 inner join entity origen on attribute_free_entity.entity_origin_id = origen.id
					where destino.name = '" . $registroDestino->getTableName() . "'
					  and origen.name = '" . $entidadOrigen->getTableName() . "'
					  and attribute_free_entity.register_id = " . $entidadOrigen->id . "
					  and attribute_free.state_id = " . State::ACTIVO . "
					  and attribute_free_entity.is_apply = 0 ";
            $txtUnion = " union all ";
        }
        // Vacia ?
        if (empty($consulta)) {
            return [];
        }

        $atributos = \DB::select($consulta);

        $resultado = [];
        foreach ($atributos as $atributo) {
            $resultado[] = $atributo->id;
        }

        return $resultado;
    }


    /**
     * Devuelve los atributos que aplican a toda una Entidad
     * @param array $listEntidadesOrigen Array de array con los campos
     *                                   "entidad" : nombre de la entidad origen
     *                                   "id": id de la entidad origen
     *                                   string $entidadDestino Nombre de la entidad destino
     *                                   array $typesExcludes Array con los tipos de atributos que no queremos mostrar
     * @return array|NULL listado de atributos ordenados
     */
    public static function getAttributesFreeEntity($listEntidadesOrigen, $entidadDestino, $typesExcludes = [])
    {
        $consulta = null;
        $txtUnion = "";

        foreach ($listEntidadesOrigen as $item) {
            // consulta
            $consulta .= $txtUnion .
                         " select attribute_free.id,
					 attribute_free.name, 
					 attribute_free.description, 
					 attribute_free.text_help,
					 attribute_free_classification.name as classificacion_name,
					 concat(attribute_free.name, ' (',attribute_free_classification.name,')') as etiqueta
			  from attribute_free
				   inner join attribute_free_entity on attribute_free.id = attribute_free_entity.attribute_free_id
				   inner join entity origen on attribute_free_entity.entity_origin_id = origen.id
				   inner join entity destino on attribute_free_entity.entity_destination_id = destino.id
				   inner join attribute_free_classification on attribute_free.classification_id = attribute_free_classification.id
			  where destino.name = '" . $entidadDestino . "'
			    and attribute_free_entity.register_id = " . $item['id'] . "
			    and origen.name = '" . $item['entidad'] . "'
			    and attribute_free.state_id = " . State::ACTIVO .
                         (empty($typesExcludes) ? '' : "  and attribute_free.type_id not in (" . Utilidades::canelaSplit($typesExcludes, ',', null, null) . " ) ") . "
			  group by attribute_free.id,
					   attribute_free.name, 
					   attribute_free.description, 
					   attribute_free.text_help,
					   attribute_free_classification.name,
					   concat(attribute_free.name, ' (',attribute_free_classification.name,')') ";
            $txtUnion = " union all ";
        }

        // Vacia ?
        if (empty($consulta)) {
            return null;
        }

        // Continuar
        $consulta .= " order by attribute_free_classification.position, attribute_free_entity.position ";

        return \DB::select($consulta);
    }

}
