<?php

namespace Canela\CanelaTools\Facades;

use Canela\CanelaTools\Routing\CanelaToolsRouter;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Facade;

/**
 * Class RouteTools
 * @package App\Facades
 */
class RouteTools extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'routeTools';
    }

    /**
     * Configuration routes.
     *
     * @throws BindingResolutionException
     */
    public static function configuration()
    {
        static::$app->make(CanelaToolsRouter::class)->configuration();
    }

    /**
     * Location routes.
     *
     * @throws BindingResolutionException
     */
    public static function location()
    {
        static::$app->make(CanelaToolsRouter::class)->location();
    }

    /**
     * Translation routes.
     *
     * @throws BindingResolutionException
     */
    public static function translation()
    {
        static::$app->make(CanelaToolsRouter::class)->translation();
    }

    /**
     * Localization routes.
     *
     * @throws BindingResolutionException
     */
    public static function localization()
    {
        static::$app->make(CanelaToolsRouter::class)->localization();
    }

    /**
     * WebBuilder admin panel routes.
     *
     * @throws BindingResolutionException
     */
    public static function webBuilderAdminPanelRoutes()
    {
        static::$app->make(CanelaToolsRouter::class)->webBuilderAdminPanelRoutes();
    }
}
