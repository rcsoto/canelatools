@extends(config('canelatools.blade.layouts.master'))

@push('estilos')

@endpush

@section(config('canelatools.blade.section.breadcrumbs'))

    <h4>@lang('canelatools::canelatools.database.sql.title')</h4>
    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href="{{ url('home') }}"><i class="fa fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="#!">@lang('canelatools::canelatools.database.sql.title')</a></li>
    </ol>

@endsection

@section('content')
    <div class="card">
        <div class=card-title">
            <p class="c-red f-20"><strong>@lang('canelatools::canelatools.database.sql.avise')</strong></p>
        </div>
        <div class="card-body">

            {{ Form::open(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            <div class="control-group">
                {!! Form::label('sentences', trans('canelatools::canelatools.database.sql.sentences')) !!}
                {!! Form::textarea('sentences', null, array('class' => 'form-control', 'id' => 'sentences', 'rows' => '24')) !!}
            </div>

            <div class="control-group text-center m-t-20">
                <a class="btn btn-primary" id="btn-submit"> @lang('canelatools::canelatools.buttons.execute')</a>
                <a class="btn btn-default" type="button" href="{{ url('home') }}">@lang('canelatools::canelatools.buttons.cancel')</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('body-scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#btn-submit').click(function() {
                const route = "{{ route('admin.database.sql.execute') }}";
                const formdata = new FormData();
                formdata.append("_token", '{{ csrf_token() }}');
                formdata.append('sentences',jQuery("#sentences").val());

                jQuery.ajax({
                    url: route,
                    dataType: "json",
                    data: formdata,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            jQuery.messageInfo(data.message);
                        } else {
                            jQuery.messageWarning(data.message);
                        }
                        jQuery.loadEnd();
                    },
                });
            })

        });
    </script>
@stop
