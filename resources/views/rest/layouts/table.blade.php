﻿<?php
// valores por defecto
$numColumnVisible   = empty($tableRest->tableColumn) ? 0 : count($tableRest->tableColumn);

// columnas a mostrar
$columnsVisible = "[";
for($i=0;$i<$numColumnVisible;$i++)
{
    $columnsVisible .= "{bVisible: true},";
}
$columnsVisible .= "]";

    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
?>

<div class="dt-responsive table-responsive" data-pattern="priority-columns">
    <table id="{{$tableRest->tableNameId}}" class="table table-striped table-hover table-bordered nowrap">

		<thead>
           	<tr id="tr-orders">
           		@foreach($tableRest->tableColumn as $info)
           			@if($info->withOrderBy)

           				<th id="orderby_{{$info->table}}__{{$info->fieldOrder}}"
           					data-orderby="{{$info->orderByDefault ?? ''}}"
           					data-table="{{$info->table}}"
           					data-field="{{$info->fieldOrder}}">
           				<i class="fa fa-sort" aria-hidden="true"></i> {{$info->label ?? ""}}</th>
           			@else
               			<th>{{$info->label ?? ""}}</th>
               		@endif

				@endforeach
           	</tr>
        </thead>
		<thead>
			<tr id="tr-filters">
				@foreach($tableRest->tableColumn as $info)
					@if($info->withFilter)
						@if(!empty($info->comboList))
							<th>{{ Form::select('filter_'.$info->table.'__'.($info->fieldFilter ?? $info->fieldOrder),  $info->comboList, null,
									array('class' => 'search_init form-control select2', 'data-search' => 'true', 'multiple' => 'multiple',
										  'data-filtersql' => $info->filterSql,
										  'data-table' => $info->table, 'data-field' => ($info->fieldFilter ?? $info->fieldOrder) )) }}</th>
						@else
							<th>{{ Form::text('filter_'.$info->table.'__'.($info->fieldFilter ?? $info->fieldOrder), null,
									array('class' => 'search_init form-control',
                                            'data-type' => $info->type, 'data-filtersql' => $info->filterSql,
											'data-table' => $info->table, 'data-field' => ($info->fieldFilter ?? $info->fieldOrder) )) }}</th>
						@endif
					@else
						<th>&nbsp;</th>
					@endif
				@endforeach
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

@section('body-scripts-extra')
    @parent

    @include($tableRest->jsDynamicTable, array('urlentidad' => $tableRest->urlListAjax, 'parameterFilters' => $tableRest->parameterFilters,
        'table' => $tableRest->tableNameId, 'columns' => '{{$columnsVisible}}' ))

    <script type="text/javascript">
        jQuery(document).ready(function () {


        });

    </script>
@endsection
