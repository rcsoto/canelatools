<?php

namespace Canela\CanelaTools\Requests\User;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class UserRequest
 * @package Canela\CanelaTools\Requests\User
 */
class UserRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'client_id',
        'name',
        'email',
        'profile_id',
        'state_id',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.user.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'client_id'     => 'required|integer|min:1|exists:client,id',
            'profile_id'    => 'required|integer|min:1|exists:profile,id',
            'state_id'      => 'required|integer|min:1|exists:user_state,id',
            'name'          => 'required|max:255',
            'email'         => 'required|email|max:255|unique:user,email',
        );

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id                = $this->route('user');
            $rules['email']    = $rules['email'] . ',' . $id;

        }

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');

        return $attributes;
    }

}
