<?php namespace Canela\CanelaTools\Models\AttributeFree;


use Canela\CanelaTools\Models\BasicModel;

/**
 * Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification
 *
 * @property int                 $id
 * @property string              $name
 * @property string|null         $description
 * @property string|null         $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string         $created_at_format
 * @property-read string         $updated_at_format
 * @property-read string         $table
 * @property-read string         $combo_identificator
 * @property-read string         $value_identificator
 * @property-read string         $created_at_short
 * @property-read string         $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @mixin \Eloquent
 */
class AttributeFreeClassification extends BasicModel
{

    const SIN_CLASIFICACION = "Sin clasificación";


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_free_classification';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'name',
        'description',
    ];


}
