<?php

namespace Canela\CanelaTools\Controller\Location;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Models\Location\City;
use Canela\CanelaTools\Requests\Location\CityRequest;
use Canela\CanelaTools\Requests\Location\ProvinceRequest;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\Location\Country;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Location\Province;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class CityController
 * @package Canela\CanelaTools\Controller\Location
 */
class CityController extends BasicController
{
    use HtmlLists;

    /**
     * CityController constructor.
     */
    public function __construct()
    {
        $this->menu_group = AdminMenu::GROUP_LOCATION;
        $this->menu       = AdminMenu::CITY;

        $this->routeEntity = 'location.city';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::canelarest.addEdit';
        $this->vistaAdd    = 'canelatools::canelarest.addEdit';

        $this->entidad = City::class;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.city.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(true)
            ->setTableColumn([
                (new TableColumn())->setTable('city')->setFieldOrder('id'),
                (new TableColumn())->setTable('country')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.city.field.country_id'))
                    ->setFieldFilter('id')->setComboList(Country::comboList())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                (new TableColumn())->setTable('province')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.city.field.province_id'))
                    ->setFieldFilter('id')->setComboList(Province::comboList())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                (new TableColumn())->setTable('city')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.city.field.name')),
                (new TableColumn())->setTable('city')->setFieldOrder('code')->setLabel(trans('canelatools::canelatools.entity.city.field.code')),
                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([
                (new TableForm())->setName('country_id')->setLabel(trans('canelatools::canelatools.entity.city.field.country_id'))
                    ->setComboListJson(Country::comboList())->setType(TableColumnTypes::SELECT2)->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('province_id')->setLabel(trans('canelatools::canelatools.entity.city.field.province_id'))
                    ->setComboListJson(Province::comboList(), false, '', route('location.province.refreshAjax'), ['country_id'])->setType(TableColumnTypes::SELECT2)->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('name')->setLabel(trans('canelatools::canelatools.entity.city.field.name'))->setContainerClass('col-6 mb-2'),
                (new TableForm())->setName('code')->setLabel(trans('canelatools::canelatools.entity.city.field.code'))->setContainerClass('col-6 mb-2'),
            ]);

        parent::__construct();
    }


    /**
     *
     * Enter description here ...
     */
    public function getConsultaPersonalizada($request)
    {
        return City::select(['city.id',
                             'country.name as country_name',
                             'province.name as province_code',
                             'city.name',
                             'city.code',
                             'city.id as action_city_id'
                            ])
                   ->join('province', 'city.province_id', 'province.id')
                   ->join('country', 'province.country_id', 'country.id');
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_city_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, false, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CityRequest $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(CityRequest $request)
    {
        return $this->storeBasic($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CityRequest $request
     * @param int $city_id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(CityRequest $request, int $city_id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $city_id);
    }
}
