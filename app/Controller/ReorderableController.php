<?php

namespace Canela\CanelaTools\Controller;


use Canela\CanelaTools\Enums\HttpErrors;
use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Http\Request;

trait ReorderableController
{
    /**
     * Execute reorder via AJAX.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ajaxReorder(Request $request)
    {
        if ($request->ajax()) {
            try {
                $model = $this->entidad;
                \DB::beginTransaction();
                $itemList = json_decode($request->input('item_list'), true);
                foreach ($itemList as $listElement) {
                    /** @var BasicModel $item */
                    $item           = $model::findOrFail($listElement['item']);
                    $item->position = $listElement['position'];
                    $item->save();
                }

                \DB::commit();

                return response()->json(
                    array('error' => false,
                          'msg'   => trans('canelatools::canelatools.model.reorder.reorder_ok'),
                          'data'  => '')
                );
            } catch (\Exception $exception) {
                \DB::rollBack();
                \Log::error($exception);
                $msg = config('app.debug') ? $exception->getMessage() : trans('canelatools::canelatools.model.reorder.reorder_error');
                return response()->json(
                    array('error' => true,
                          'msg'   => $msg)
                );
            }
        } else {
            return abort(HttpErrors::HTTP_UNAUTHORIZED, trans('canelatools::canelatools.ajax.unsupported'));
        }
    }
}
