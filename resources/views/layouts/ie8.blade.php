<script type="text/javascript">
    jQuery(window).load(function(){

        /**
        * checkInternetExplorerBrowser
        *
        * Method that check it's beeing using Internet Explorer Browser. It returns it's version if so or null otherways.
        *
        * @return int|null
        */
        jQuery.checkInternetExplorerBrowser = function () {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
            {
                var ver = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
                if (isNaN(ver)) {
                    var pos = ua.indexOf('rv');
                    ver = ua.substring(pos + 3, pos + 5);
                }

                return ver;
            }
            else
            {
                return null;
            }

        }


        /******************************************************************
         * Detectar navegador
        ******************************************************************/
        if (jQuery.checkInternetExplorerBrowser() !== null) {
            location.href = "{{ url('/updatebrowser') }}";
        }
    });
</script>
