@section('associated-sections-nav-item')
    <li class="nav-item">
        <a class="nav-link text-uppercase has-ripple" id="associated-sections-tab" data-toggle="tab" href="#associated-sections" role="tab" aria-controls="associated-sections" aria-selected="false">@lang('canelatools::canelatools.canela-rest.form.label.associated-sections')<span class="ripple ripple-animate"></span></a>
    </li>
@endsection

@section('associated-sections-tab-content')
    <div class="tab-pane p-t-10 fade" id="associated-sections" role="tabpanel" aria-labelledby="associated-sections-tab">
        <div class="m-b-20">
            <div class="row associated-items-list">
                @foreach($register->sections()->with('fileThumb')->get() as $item)
                    <div class="col-sm-12 col-md-6 col-lg-3 col-xl-2 associated-item-container" data-id="{{$item->id}}">
                        <div>
                            <div class="sortable-move-icon-header"><i class="fa fa-arrows-alt"></i></div>
                            <?php
                            /** @var \Canela\CanelaTools\Models\WebBuilder\WebPageSection $item */
                            $archive = $item->fileThumb;
                            $archiveName = $archive?->translate('archive');
                            $archiveUrl = asset(\Canela\CanelaTools\Models\General\Archive::DIR_ARCHIVES.$archiveName);
                            $sectionUrl = route('admin.webbuilder.webpagesection.edit', $item->id);
                            ?>
                            @if (!is_null($archive) && $archive->type_id == \Canela\CanelaTools\Models\General\ArchiveType::IMAGE)
                                <a href="{{$sectionUrl}}">
                                    <img src="{{$archiveUrl}}" alt="{{$archive->translate('title')}}">
                                </a>
                            @endif
                            <div class="item-name-container"><a href="{{$sectionUrl}}">{!! $item->translate('title') !!}</a></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('body-scripts')
    @parent

    <script type="text/javascript">

        jQuery(document).ready(function () {

            const $associatedSectionsList = jQuery('#associated-sections .associated-items-list');

            jQuery.sortAssociatedSections = function () {
                const ids = $associatedSectionsList.find('.associated-item-container').map(function () {
                    return jQuery(this).data('id');
                }).toArray();

                jQuery.ajax({
                    url: '{{route($tableRest->routeEntity.'.ajax.sortassociatedsections', $register->id)}}',
                    dataType: "json",
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'PATCH',
                        'ids': ids
                    },
                    type: "POST",
                    processData: true,
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        const msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            jQuery.messageSuccess(data.message);
                        } else {
                            jQuery.messageWarning(data.message);
                        }
                        jQuery.loadEnd();
                    },
                });

            }

            $associatedSectionsList.sortable({
                update: function () {
                    jQuery.sortAssociatedSections();
                }
            });

        });
    </script>
@endsection

