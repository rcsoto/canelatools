<?php


namespace Canela\CanelaTools\Enums;


final class Environment extends \BenSampo\Enum\Enum
{
    const LOCAL = 'local';
    const STAGING = 'staging';
    const PRODUCTION = 'production';
}
