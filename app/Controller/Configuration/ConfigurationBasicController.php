<?php

namespace Canela\CanelaTools\Controller\Configuration;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Models\Configuration;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Configuration\ConfigurationRequest;
use Canela\CanelaTools\HtmlLists;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class ConfigurationController
 * @package App\Http\Controllers\Admin\Configuration
 */
class ConfigurationBasicController extends BasicController
{
    use HtmlLists;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Configuration::class;
        $this->routeEntity = 'configuration.configuration';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CONFIGURATION;
        $this->menu_group = AdminMenu::GROUP_CONFIG;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.configuration.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(false)
            ->setTableColumn([  (new TableColumn())->setTable('configuration')->setFieldOrder('id'),
                                (new TableColumn())->setTable('configuration')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.configuration.field.name')),
                                (new TableColumn())->setTable('configuration')->setFieldOrder('description')->setLabel(trans('canelatools::canelatools.entity.configuration.field.description')),
                                (new TableColumn())->setTable('configuration')->setFieldOrder('reference')->setLabel(trans('canelatools::canelatools.entity.configuration.field.reference')),
                                (new TableColumn())->setTable('configuration')->setFieldOrder('value')->setLabel(trans('canelatools::canelatools.entity.configuration.field.value')),
                                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([(new TableForm())->setName('name')->setLabel(trans('canelatools::canelatools.entity.configuration.field.name'))->setCol(6),
                            (new TableForm())->setName('reference')->setLabel(trans('canelatools::canelatools.entity.configuration.field.reference'))->setReadOnly(true)->setCol(6),
                            (new TableForm())->setName('description')->setLabel(trans('canelatools::canelatools.entity.configuration.field.description')),
                            (new TableForm())->setName('value')->setLabel(trans('canelatools::canelatools.entity.configuration.field.value')),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param Request $request
     * @return Builder|Configuration
     */
    public function getConsultaPersonalizada($request)
    {
        return Configuration::select(['configuration.id',
                                      'configuration.name',
                                      'configuration.description',
                                      'configuration.reference',
                                      'configuration.value',
                                      'configuration.id as action_configuration_id',
                                     ]);
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_configuration_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                                              true, false, false, false,
                                              false, false, 0, 0, true,
                                              true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ConfigurationRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(ConfigurationRequest $request)
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ConfigurationRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(ConfigurationRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
