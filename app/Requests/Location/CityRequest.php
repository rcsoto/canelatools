<?php

namespace Canela\CanelaTools\Requests\Location;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class CityRequest
 * @package Canela\CanelaTools\Requests\Location
 */
class CityRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'province_id' => 'required|integer|exists:province,id',
            'name'        => 'required|string|min:1|unique:city,name',
            'code'        => 'required|max:5|unique:city,code',
        );

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id = $this->route('city');

            $rules['name'] .= ",$id";
            $rules['code'] .= ",$id";
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'province_id' => trans('canelatools::canelatools.entity.city.field.province_id'),
            'name'        => trans('canelatools::canelatools.entity.city.field.name'),
            'code'        => trans('canelatools::canelatools.entity.city.field.code'),
        ];
    }
}
