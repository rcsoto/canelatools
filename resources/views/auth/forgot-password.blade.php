@extends('canelatools::auth.layouts.main')

@php
    use App\Models\Project\WebBuilder\Web;
    $web = Web::firstOrFail();
    $customer = $web->customer;
@endphp

@section('body-class', 'page-login')

@section('content')
    <!-- [ auth-signup ] start -->
    <div class="auth-wrapper">
        <div class="auth-content">
            <div class="card">
                <div class="row align-items-center text-center">
                    <div class="col-md-12">
                        <div class="card-body">
                            @include('canelatools::auth.layouts.logo-container')

                            <div class="mt-4 mb-3">
                                @lang('canelatools::webbuilder.pages.auth.forgot-password.labels.forgot-your-password-no-problem')
                            </div>

                            @if (session('status'))
                                <div class="mb-4 text-success font-weight-bold">
                                    {{ session('status') }}
                                </div>
                            @endif

                            {!! Form::open(['method' => 'POST', 'url' => route('password.email')]) !!}
                                <div class="form-group mb-3">
                                    <label class="floating-label" for="email">@lang('canelatools::webbuilder.common.fields.email')</label>
                                    <input type="text" class="form-control" name="email" placeholder="" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <div class="col-12 text-danger mt-2">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <button class="btn btn-primary btn-block">@lang('canelatools::webbuilder.common.buttons.send')</button>
                                <a href="{{route('login')}}" class="btn btn-dark btn-block text-white mb-4">@lang('canelatools::webbuilder.common.buttons.back')</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ auth-signup ] end -->
@endsection

