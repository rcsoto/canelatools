<?php

namespace Canela\CanelaTools\Models;


/**
 * Canela\CanelaTools\Models\Configuration
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property string                                 $description
 * @property string                                 $reference
 * @property string|null                            $value
 * @property \Illuminate\Support\Carbon             $created_at
 * @property \Illuminate\Support\Carbon             $updated_at
 * @property-read string                            $created_at_format
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_format
 * @property-read string                            $updated_at_short
 * @property-read string                            $value_identificator
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Configuration whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class Configuration extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configuration';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'reference',
        'value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'        => 'string',
        'description' => 'string',
        'reference'   => 'string',
        'value'       => 'string',
    ];

    /**
     * Get reference.
     *
     * @param string $reference
     * @return string|null
     */
    public static function getValue($reference)
    {
        $parameter = self::whereReference($reference)
                         ->first();

        return $parameter instanceof Configuration
            ? $parameter->value
            : null;
    }


    /**
     * Crea un nuevo parámetro o la actualiza si ya existe.
     *
     * @param string $reference
     * @param string $value
     * @return \Canela\CanelaTools\Models\Configuration
     */
    public static function setValue($reference, $value): Configuration
    {
        $parameter        = Configuration::whereReference($reference)
                                         ->firstOrCreate([
                                                             'name'      => $reference,
                                                             'reference' => $reference,
                                                             'value'     => $value,
                                                         ]);
        $parameter->value = $value;
        $parameter->save();

        return $parameter;
    }
}