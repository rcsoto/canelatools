<?php namespace Canela\CanelaTools\Models\Client;

use Canela\CanelaTools\Models\BasicModel;

class Client extends BasicModel
{
    public const DIR_LOGO = 'project/client/logo/';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'contact_name',
        'cif',
        'full_address',
        'contact_telephone',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'name'              => 'string',
        'contact_name'      => 'string',
        'cif'               => 'string',
        'full_address'      => 'string',
        'contact_telephone' => 'string',
        'image_logo'        => 'string',
    ];
}
