<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\Entity
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property string                                 $reference
 * @property \Illuminate\Support\Carbon|null        $created_at
 * @property \Illuminate\Support\Carbon|null        $updated_at
 * @property-read string                            $created_at_format
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_format
 * @property-read string                            $updated_at_short
 * @property-read string                            $value_identificator
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Entity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class Entity extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'entity';

    protected $fillable = [
        'name',
        'reference',
        'model',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'      => 'string',
        'reference' => 'string',
        'model'     => 'string',
    ];

    public $comboIdentifierField = 'name';

    /**
     * Retorna una entidad dado el nombre
     *
     * @param string $entidad
     *
     * @return \Canela\CanelaTools\Models\Entity
     */
    public static function getEntityByName($entidad)
    {
        return Entity::where('name', $entidad)->first();
    }


    /**
     * Retorna una entidad dada la referencia
     *
     * @param string $entidad
     *
     * @return self
     */
    public static function getEntityByReference($entidad)
    {
        return Entity::where('reference', $entidad)->first();
    }

    /**
     * Get name table humman
     * @param $reference
     * @return mixed|string|null
     */
    public static function getNameEntity($reference) {
        $nameEntity = null;
        try {
            $entity = self::getEntityByReference($reference);
            if (isset($entity)) {
                $nameEntity =  $entity->name;
            }
        } catch (\Exception $e) {
            $entity = self::getEntityByName($reference);
            if (isset($entity)) {
                $nameEntity = $entity->description;
            }
        }
        return $nameEntity ?? $reference;
    }
}
