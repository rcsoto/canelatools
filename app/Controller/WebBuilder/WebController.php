<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Client\Customer;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\Web;
use App\Models\Project\WebBuilder\WebState;
use App\Models\Project\WebBuilder\WebTemplate;
use App\Models\Project\WebBuilder\WebTrace;
use Canela\CanelaTools\Requests\WebBuilder\WebRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebController extends BasicController
{
    use HtmlLists;

    /**
     * WebController constructor.
     */
    public function __construct()
    {
        $this->entidad      = Web::class;
        $this->routeEntity  = 'admin.webbuilder.web';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web.report'))
            ->setUrlIndex(route('admin.webbuilder.web.index'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'customer_name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                ),
                new TableColumn(
                    table: 'web_template',
                    fieldOrder: 'web_template_name',
                    label: trans('canelatools::canelatools.entity.web.field.template_id'),
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'state_name',
                    label: trans('canelatools::canelatools.general.field.state'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'customer_id',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebState::comboTranslate(),
                ),
                new TableForm(
                    name: 'template_id',
                    label: trans('canelatools::canelatools.entity.web.field.template_id'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebTemplate::comboList(),
                ),
                new TableForm(
                    name: 'text_avise_legal',
                    label: trans('canelatools::canelatools.entity.web.field.text_avise_legal'),
                    type: TableColumnTypes::TEXTAREA,
                    max: TableColumnMaxSize::TEXT_MAX,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = Web::select([
            'web.id',
            'customer.name as customer_name',
            'web_template.name as web_template_name',
            'state.name as state_name',
            'web.id as action_web_id',
        ])
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id')
        ->join('web_template', 'web.template_id', 'web_template.id')
        ->join('state', 'web.state_id', 'state.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_web_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, false, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param $customerId
     * @throws Exception
     */
    protected function checkCustomerAlreadyHasWeb($customerId)
    {
        $customer = Customer::findOrFail($customerId);
        if (!empty($customer->web_id)) {
            throw new Exception('This customer already has one web associated.');
        }
    }


    /**
     * @param Request $request
     * @param ?int $id
     */
    protected function createAndEditInitFields(Request $request, ?int $id = null)
    {
        $this->tableRest->getTableFormItem('customer_id')->setComboList(Customer::hasNoWeb($id)->comboListWithSecurity($this));

        if (is_null($id) && $request->filled('customer_id')) {  // Comes from Customer list button link.
            $this->tableRest->getTableFormItem('customer_id')->setValue($request->customer_id);
        }

        if ($request->filled('redirect_to')) {
            $this->tableRest->addParametersHidden(['redirect_to' => $request->redirect_to]);
        }
    }


    /**
     * Create item view.
     *
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function create(Request $request): Factory|View
    {
        $this->createAndEditInitFields($request);
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return mixed|void
     */
    public function edit(Request $request, int $id)
    {
        $this->createAndEditInitFields($request, $id);
        return parent::edit($request, $id);
    }


    /**
     * @param $register
     * @param $request
     * @return Application|Factory|View
     */
    public function setParamReturn($register, $request) {
        $register->customer_id = $register->customer->id;
        return view($this->vistaEdit, compact('register'))
                ->with('urlPrefix', $this->urlPrefix)
                ->with('tableRest', $this->tableRest);
    }


    /**
     * @param Web $model
     * @param $request
     * @return Web
     * @throws Exception
     */
    public function storePre(Web $model, $request): Web
    {
        $this->checkCustomerAlreadyHasWeb($request->customer_id);
        return $model;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WebRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(WebRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request, $request->filled('redirect_to') ? $request->redirect_to : null);
    }


    /**
     * @param Web $model
     * @param $request
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function storePost(Web $model, $request): Web
    {
        // Associate created web to customer.
        $customer = Customer::findOrFail($request->customer_id);
        $customer->web_id = $model->id;
        $customer->save();

        // WebTrace
        $webtrace = new WebTrace();
        $webtrace->user_id = \Auth::id();
        $webtrace->web_id = $model->id;
        $webtrace->state_old_id = $model->state_id;
        $webtrace->state_new_id = $model->state_id;
        $webtrace->comment = trans('canelatools::canelatools.general.label.trace_new',
                                    ['model_name' => trans('canelatools::canelatools.entity.web.report')]);
        $webtrace->save();

        return $model;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param WebRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     * @throws ModelNotFoundException
     */
    public function update(WebRequest $request, int $id): JsonResponse|RedirectResponse
    {
        $model = ($this->entidad)::findOrFail($id);

        if ($model->state_id != (int)$request->state_id)
        {
            // WebTrace
            $webtrace = new WebTrace();
            $webtrace->user_id = \Auth::id();
            $webtrace->web_id = $model->id;
            $webtrace->state_old_id = $model->state_id;
            $webtrace->state_new_id = $request->state_id;
            $webtrace->comment = trans('canelatools::canelatools.general.label.trace_update',
                                        ['model_name' => trans('canelatools::canelatools.entity.web.report')]);
            $webtrace->save();
        }

        return $this->updateBasic($request, $id, $request->filled('redirect_to') ? $request->redirect_to : null);
    }
}
