<?php namespace Canela\CanelaTools\Models\General;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\State;

class Typography extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'typography';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'position',
        'name',
        'code',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'state_id'  => 'int',
        'position'  => 'int',
        'name'      => 'string',
        'code'      => 'string',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state() {
        return $this->belongsTo(State::class, 'state_id');
    }

}
