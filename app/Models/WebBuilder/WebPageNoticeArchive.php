<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebPageNoticeArchive extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_notice_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notice_id',
        'archive_id',
        'title',
        'position',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'notice_id' => 'integer',
        'archive_id' => 'integer',
        'title' => 'string',
        'position' => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function notice(): BelongsTo
    {
        return $this->belongsTo(WebPageNotice::class, 'notice_id');
    }


    /**
     * @return BelongsTo
     */
    public function archive(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'archive_id');
    }


    /**
     * @return BelongsTo
     */
    public function archiveEntity(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'archive_id');
    }



}
