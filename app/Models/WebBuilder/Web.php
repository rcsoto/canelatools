<?php namespace Canela\CanelaTools\Models\WebBuilder;


use Canela\CanelaTools\Models\BasicModel;
use App\Models\Client\Customer;
use Canela\CanelaTools\Models\State;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Web
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property int $id
 * @property int $state_id
 * @property int $template_id
 * @property string $text_avise_legal
 */
class Web extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'template_id',
        'text_avise_legal',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'state_id'          => 'int',
        'template_id'       => 'int',
        'text_avise_legal'  => 'string',
    ];


    /**
     * @return BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(WebState::class, 'state_id');
    }


    /**
     * @return BelongsTo
     */
    public function template(): BelongsTo
    {
        return $this->belongsTo(WebTemplate::class, 'template_id');
    }


    /**
     * @return HasMany
     */
    public function pages(): HasMany
    {
        return $this->hasMany(WebPage::class, 'web_id')
                    ->orderBy('position', 'asc');
    }

    /**
     * Paginas activas
     * @return Collection
     */
    public function pagesActive(): HasMany
    {
        return $this->pages()
                    ->where('state_id', State::ACTIVO);
    }


    /**
     * Paginas activas a mostrar en la cabecera
     * @return Collection
     */
    public function pagesShowHeader()
    {
        return $this->pagesActive()
            ->where('show_header', 1);
    }


    /**
     * Paginas activas a mostrar en el pie
     * @return Collection
     */
    public function pagesShowFooter()
    {
        return $this->pagesActive()
                    ->where('show_footer', 1);
    }


    /**
     * Paginas activas a mostrar en la home
     * @return Collection
     */
    public function pagesShowHome()
    {
        return $this->pagesActive()
            ->where('show_home', 1);
    }


    /**
     * @return HasMany
     */
    public function languages(): HasMany
    {
        return $this->hasMany(WebLanguage::class, 'web_id')
                    ->orderBy('position', 'asc');
    }


    /**
     * Idioma principal
     * @return Model|HasMany|null
     */
    public function language()
    {
        return $this->languages()
                    ->where('principal', 1)
                    ->first();
    }


    /**
     * @return HasOne
     */
    public function customer(): HasOne
    {
        return $this->hasOne(Customer::class, 'web_id');
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function translationLanguages(): \Illuminate\Support\Collection
    {
        return WebLanguage::webLanguages($this->id);
    }

}
