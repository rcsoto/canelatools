<?php

namespace Canela\CanelaTools\Controller\System;


use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\MessageType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use Linfo\Linfo;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;
use Symfony\Component\Console\Output\BufferedOutput;

class SystemController extends Controller
{

    /**
     * @var
     */
    protected $request;
    /**
     * @var LaravelLogViewer
     */
    private $log_viewer;
    /** @var string $menu . Nombre de la opcion de menu */
    protected $menu;

    /** @var string $menu_group . Nombre de la opcion de grupo del menu */
    protected $menu_group;

    /**
     * LogViewerController constructor.
     */
    public function __construct()
    {
        $this->log_viewer = new LaravelLogViewer();
        $this->request    = app('request');

        $this->menu       = AdminMenu::SYSTEM;
        $this->menu_group = AdminMenu::GROUP_CONFIGURATION;

        AdminMenu::setCurrent($this->menu);
        AdminMenu::setCurrentGroup($this->menu_group);
    }


    public function index()
    {
        $folderFiles = [];
        if ($this->request->input('f')) {
            $this->log_viewer->setFolder(Crypt::decrypt($this->request->input('f')));
            $folderFiles = $this->log_viewer->getFolderFiles(true);
        }
        if ($this->request->input('l')) {
            $this->log_viewer->setFile(Crypt::decrypt($this->request->input('l')));
        }

        if ($early_return = $this->earlyReturn()) {
            return $early_return;
        }

        $data = [
            'logs'           => $this->log_viewer->all(),
            'folders'        => $this->log_viewer->getFolders(),
            'current_folder' => $this->log_viewer->getFolderName(),
            'folder_files'   => $folderFiles,
            'files'          => $this->log_viewer->getFiles(true),
            'current_file'   => $this->log_viewer->getFileName(),
            'standardFormat' => true,
        ];

        if ($this->request->wantsJson()) {
            return $data;
        }

        if (is_array($data['logs'])) {
            $firstLog = reset($data['logs']);
            if (!$firstLog['context'] && !$firstLog['level']) {
                $data['standardFormat'] = false;
            }
        }

        $data['sysInfo'] = $this->systemData();

        return app('view')->make('canelatools::administrator.system.index', $data);
    }

    /**
     * @return bool|mixed
     * @throws \Exception
     */
    private function earlyReturn()
    {
        if ($this->request->input('f')) {
            $this->log_viewer->setFolder(Crypt::decrypt($this->request->input('f')));
        }

        if ($this->request->input('dl')) {
            return $this->download($this->pathFromInput('dl'));
        } elseif ($this->request->has('clean')) {
            app('files')->put($this->pathFromInput('clean'), '');
            return $this->redirect($this->request->url());
        } elseif ($this->request->has('del')) {
            app('files')->delete($this->pathFromInput('del'));
            return $this->redirect($this->request->url());
        } elseif ($this->request->has('delall')) {
            $files = ($this->log_viewer->getFolderName())
                ? $this->log_viewer->getFolderFiles(true)
                : $this->log_viewer->getFiles(true);
            foreach ($files as $file) {
                app('files')->delete($this->log_viewer->pathToLogFile($file));
            }
            return $this->redirect($this->request->url());
        }
        return false;
    }

    /**
     * @param string $input_string
     * @return string
     * @throws \Exception
     */
    private function pathFromInput($input_string)
    {
        return $this->log_viewer->pathToLogFile(Crypt::decrypt($this->request->input($input_string)));
    }

    /**
     * @param $to
     * @return mixed
     */
    private function redirect($to)
    {
        if (function_exists('redirect')) {
            return redirect($to);
        }

        return app('redirect')->to($to);
    }

    /**
     * @param string $data
     * @return mixed
     */
    private function download($data)
    {
        return response()->download($data);
    }


    public function clearCache()
    {
        set_time_limit(300);

        $output = new BufferedOutput();
        \Artisan::call('clear', ['--no-interaction' => true], $output);

        return redirect()->back()->with(MessageType::INFO, nl2br($output->fetch()));
    }

    public function clearOrders(){
        set_time_limit(300);

        $output = new BufferedOutput();
        \Artisan::call('order:cleardeleted', ['--no-interaction' => true], $output);

        return redirect()->back()->with(MessageType::INFO, nl2br($output->fetch()));
    }

    /**
     * @return Collection|RedirectResponse
     */
    private function systemData()
    {
        $settings = [
            'show'      => [
                'kernel'         => true,
                'ip'             => true,
                'os'             => true,
                'load'           => true,
                'ram'            => true,
                'mounts_options' => false,
                'webservice'     => true,
                'phpversion'     => true,
                'network'        => true,
                'uptime'         => true,
                'cpu'            => true,
                'process_stats'  => true,
                'hostname'       => true,
                'distro'         => true,
                'virtualization' => true,
            ],
            'cpu_usage' => true,
            'language'  => 'es',
        ];

        try {
            $linfo = new Linfo($settings);
            $linfo->scan();

            return collect($linfo->getInfo());
        } catch (\Exception $exception) {
            return redirect()->back()->with(MessageType::WARNING, $exception->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getSystemData()
    {
        $sysInfo = $this->systemData();

        return response()->json([
                                    'result' => true,
                                    'data'   => $sysInfo,
                                    // 'data'   => view('canelatools::administrator.system.systemData', compact('sysInfo'))->render(),
                                ]);
    }
}
