<?php

namespace Canela\CanelaTools\Enums;


abstract class TableFilterTypes
{
    const PREFIJO_FILTER            = 'filter_';
    const PREFIJO_ARRAY             = '@array:';
    const PREFIJO_ORDER_BY          = 'orderby_';
    const ITEM_PAGINATION           = 20;
    const FILTER_MAYOR_FECHA_ACTUAL = '___filter_mayor_fecha_actual__';
    const REGISTRO                  = 'registro';
    const LISTA                     = 'lista';
    const ITEMS_SHOW_PAGE           = 10;
    const SEPARADOR_CAMPOS_AUX      = '___';
    const SEPARADOR_TABLA_AUX       = '__';
    const SEPARADOR_TABLA           = '.';
    const NAME_NO_TABLA             = '123';
    const FILED_ID                  = 'id';
    const TIPO_FILTRO_CONTIENE      = 1;
    const TIPO_FILTRO_IGUAL         = 2;
    const TIPO_FILTRO_MAYOR         = 3;
    const TIPO_FILTRO_MAYOR_IGUAL   = 4;
    const TIPO_FILTRO_MENOR         = 5;
    const TIPO_FILTRO_MENOR_IGUAL   = 6;
    const TIPO_FILTRO_DISTINTO      = 7;
    const TIPO_FILTRO_NO_CONTIENE   = 8;
    const TIPO_FILTRO_VACIO         = 9;
    const TIPO_FILTRO_NO_VACIO      = 10;
    
    const FILTRO_TIPO_DATE          = 'date';
    const FILTRO_TIPO_TEXT          = 'text';
    const FILTRO_TIPO_SELECT        = 'select';
}