<?php
use App\Models\User\Profile;
?>
<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar theme-horizontal menu-light brand-blue">
    <div class="navbar-wrapper">
        <div class="navbar-content sidenav-horizontal" id="layout-sidenav">
            <ul class="nav pcoded-inner-navbar sidenav-inner">
                <li class="nav-item pcoded-menu-caption">
                    <label>Navigation</label>
                </li>

                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">@lang('canelatools::webbuilder.pages.admin.common.main-menu.configuration.label')</span></a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{route('admin.client.client.index')}}">@lang('canelatools::canelatools.entity.client.list')</a></li>
                        <li><a href="{{route('admin.client.customer.index')}}">@lang('canelatools::canelatools.entity.customer.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.weblanguage.index')}}">@lang('canelatools::canelatools.entity.web_language.list')</a></li>

                        <li><a href="{{route('admin.configuration.parameter.index')}}">@lang('canelatools::canelatools.entity.parameter.list')</a></li>
                        <li><a href="{{route('admin.configuration.language.index')}}">@lang('canelatools::canelatools.entity.language.list')</a></li>
                        <li><a href="{{route('admin.configuration.user.index')}}">@lang('canelatools::canelatools.entity.user.list')</a></li>

                        <li><a href="{{route('admin.system.index')}}">@lang('canelatools::canelatools.general.label.system-administration')</a></li>
                    </ul>
                </li>

                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">@lang('canelatools::webbuilder.pages.admin.common.main-menu.catalogue.label')</span></a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{route('admin.webbuilder.webpagecontacttype.index')}}">@lang('canelatools::canelatools.entity.web_page_contact_type.list')</a></li>
                    </ul>
                </li>

                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-globe"></i></span><span class="pcoded-mtext">@lang('canelatools::webbuilder.pages.admin.common.main-menu.web.label')</span></a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{route('admin.webbuilder.webpage.index')}}">@lang('canelatools::canelatools.entity.web_page.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagecategory.index')}}">@lang('canelatools::canelatools.entity.web_page_category.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagesection.index')}}">@lang('canelatools::canelatools.entity.web_page_section.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagenotice.index')}}">@lang('canelatools::canelatools.entity.web_page_notice.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagefaq.index')}}">@lang('canelatools::canelatools.entity.web_page_faq.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagedata.index')}}">@lang('canelatools::canelatools.entity.web_page_data.list')</a></li>
                        <li><a href="{{route('admin.webbuilder.webpagecontact.index')}}">@lang('canelatools::canelatools.entity.web_page_contact.list')</a></li>
                        <li><a href="{{route('admin.general.archive.index')}}">@lang('canelatools::canelatools.entity.archive.list')</a></li>
                    </ul>
                </li>

                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="fas fa-database"></i></span><span class="pcoded-mtext">@lang('canelatools::webbuilder.pages.admin.common.main-menu.utilities.label')</span></a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{route('admin.database.sql.index')}}">@lang('canelatools::canelatools.menu.sentenceSql')</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->
