<?php

namespace Canela\CanelaTools\Traits;

use App\Models\General\ArchiveType;
use Canela\CanelaTools\Requests\General\ListOfGenericIdsRequest;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Log;

trait ArchivesAssociatable
{
    private string $archivesEntity;

    /**
     * @param ListOfGenericIdsRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function associateArchives(ListOfGenericIdsRequest $request, int $id): JsonResponse
    {
        try {
            DB::beginTransaction();

            $model = ($this->entidad)::findOrFail($id);

            if (!method_exists($model, 'archives')) throw new Exception('archives relationship must be implemented in '.$this->entidad.' entity.');

            // Filter already existing items.
            $existingArchivesIds = $model->archives()->pluck('archive_id');
            $ids = array_diff($request->ids, $existingArchivesIds->toArray());

            // Init current positions.
            $pos = 1;
            foreach ($model->archives as $modelArchive) {
                $modelArchive->update(['position' => $pos]);
                $pos++;
            }

            $pos = $model->archives->last()?->position ?? 0;

            $positionedNewArchives = collect($ids)->map(function ($item) use ($model, &$pos) {
                $pos++;
                return [
                    'section_id'    => $model->id,
                    'type_id'       => ArchiveType::IMAGE,      // No se usa.
                    'archive_id'    => $item,
                    'position'      => $pos,
                    'archive'       => '',                      // No se usa.
                ];
            })->toArray();

            $model->archives()->createMany($positionedNewArchives);

            DB::commit();
            return response()->json([
                'result' => true,
                'message' => trans('canelatools::canelatools.general.label.success'),
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'result' => false,
                'message' => trans('canelatools::canelatools.model.update.error'),
            ]);
        }
    }


    /**
     * @param int|null $id
     * @return JsonResponse
     */
    public function disassociateArchive(int $id = null): JsonResponse
    {
        try {
            if (is_null($id)) throw new Exception('$id parameter is required.');
            if (!property_exists($this, 'archivesEntity') || empty($this->archivesEntity))
                throw new Exception('archivesEntity property must exists and not be empty in '.__CLASS__.' controller.');

            $model = ($this->archivesEntity)::findOrFail($id);

            $model->delete();

            return response()->json([
                'result' => true,
                'message' => trans('canelatools::canelatools.general.label.success'),
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'result' => false,
                'message' => trans('canelatools::canelatools.model.update.error'),
            ]);
        }
    }


    /**
     * @param ListOfGenericIdsRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sortAssociatedArchives(ListOfGenericIdsRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            if (!property_exists($this, 'archivesEntity') || empty($this->archivesEntity))
                throw new Exception('archivesEntity property must exists and not be empty in '.__CLASS__.' controller.');

            $pos = 1;
            foreach ($request->ids as $id) {
                ($this->archivesEntity)::findOrFail($id)->update(['position' => $pos]);
                $pos++;
            }

            DB::commit();
            return response()->json([
                'result' => true,
                'message' => trans('canelatools::canelatools.general.label.success'),
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'result' => false,
                'message' => trans('canelatools::canelatools.model.update.error'),
            ]);
        }
    }

}
