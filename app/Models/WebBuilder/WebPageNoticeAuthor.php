<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;

class WebPageNoticeAuthor extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_notice_author';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notice_id',
        'type_id',
        'name',
        'employee',
        'company',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'notice_id' => 'integer',
        'type_id' => 'integer',
        'name' => 'string',
        'employee' => 'string',
        'company' => 'string',
    ];

    protected $with = ['type'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notice() {
        return $this->belongsTo(WebPageNotice::class, 'notice_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type() {
        return $this->belongsTo(WebPageNoticeAuthorType::class, 'type_id');
    }

}
