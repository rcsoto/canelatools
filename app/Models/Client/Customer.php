<?php namespace Canela\CanelaTools\Models\Client;

use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Models\BasicModel;
use App\Models\Project\WebBuilder\Web;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Client\Client;

/**
 * Class Customer
 * @package Canela\CanelaTools\Models\Client
 *
 * @method static mixed comboListWithSecurity(BasicController $controller): mixed
 * @method static mixed hasNoWeb(?int $id): mixed
 * @property int $id
 * @property int $client_id
 * @property int $web_id
 * @property string $name
 * @property string $business_name
 * @property string $image_logo
 * @property float $map_latitude
 * @property float $map_longitude
 */
class Customer extends BasicModel
{
    public const DIR_LOGO = 'project/customer/logo/';
    public const LOGO_FILENAME = 'logo';
    public const LOGO_DARK_FILENAME = 'logo-dark';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'web_id',
        'name',     // nombre comercial
        'business_name', // razon solcial
        'map_latitude',
        'map_longitude',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'client_id'         => 'int',
        'web_id'            => 'int',
        'name'              => 'string',
        'business_name'     => 'string',
        'image_logo'        => 'string',
        'map_latitude'      => 'string',
        'map_longitude'     => 'string',
    ];


    /**
     * @return BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id');
    }


    /**
     * @return BelongsTo
     */
    public function web(): BelongsTo
    {
        return $this->belongsTo(Web::class, 'web_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rrss() {
        return $this->hasMany(CustomerRrss::class, 'customer_id');
    }


    /**
     * @return HasOne
     */
    public function configuration(): HasOne
    {
        return $this->hasOne(ClientConfiguration::class, 'customer_id');
    }


    /**
     * @param $query
     * @param $controller
     * @return mixed
     */
    public function scopeComboListWithSecurity($query, $controller): mixed
    {
        $query->select(['customer.id', 'customer.name'])
              ->join('client', 'customer.client_id', 'client.id')
              ->leftJoin('web', 'customer.web_id', 'web.id')
              ->groupBy(['customer.id', 'customer.name']);
        return $controller->setSecurityList($query)->get()->pluck($this->getNameFieldComboBoxAttribute(), 'id');
    }


    /**
     * @param $query
     * @param ?int $id
     * @return mixed
     */
    public function scopeHasNoWeb($query, ?int $id): mixed
    {
        $query->whereNull('customer.web_id');
        if (!is_null($id)) {
            $query->orWhere('customer.web_id', $id);
        }
        return $query;
    }

}
