<?php namespace Canela\CanelaTools\Models\AttributeFree;




use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Entity;

/**
 * App\Models\User\Profile
 *
 * @property int $id
 * @property int $attribute_free_id
 * @property int $entity_id
 * @property int $register_id
 * @property int|null $attribute_free_list_id
 * @property string|null $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\AttributeFree\AttributeFree $attribute
 * @property-read \App\Models\Admin\Entity $entity
 * @property-read mixed $created_at_format
 * @property-read mixed $updated_at_format
 * @property-read \App\Models\AttributeFree\AttributeFreeList|null $listValue
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\ModelApp comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\ModelApp comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\ModelApp combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereAttributeFreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereAttributeFreeListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereRegisterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\ModelApp comboListOrd()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeFree\AttributeFreeEntityValue query()
 * @property-read string $value_identificator
 * @property-read mixed $combo_identificator
 * @property-read mixed $created_at_short
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @property-read mixed $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 */
class AttributeFreeEntityValue extends BasicModel
{

	const DIR_MULTIMEDIA 			= 'document/attributefreeentityvalue/';
	const DIR_MULTIMEDIA_THUMBNAIL 	= 'document/attributefreeentityvalue/thumbnail/';
	const THUMBNAIL_WIDTH			= 192;
	const THUMBNAIL_HEIGHT			= 129;
	
	const CLASS_INPUT               = 'attributefreeentityvalue';
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */	
	protected $table = 'attribute_free_entity_value';
	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'attribute_free_id',
			'entity_id',
			'register_id',
			'attribute_free_list_id',
			'value',
	];

	protected $with = ['attribute'];
	
	
	/**
	 *
	 * @return
	 */
	public function attribute() {
		return $this->belongsTo(AttributeFree::class, 'attribute_free_id');
	}
	
	
	/**
	 *
	 * @return
	 */
	public function entity() {
		return $this->belongsTo(Entity::class, 'entity_id');
	}
	
	
	/**
	 *
	 * @return
	 */
	public function listValue() {
		return $this->belongsTo(AttributeFreeList::class, 'attribute_free_list_id');
	}

}
