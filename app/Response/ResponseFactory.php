<?php

namespace Canela\CanelaTools\Response;

use Illuminate\Http\JsonResponse;
use \Illuminate\Routing\ResponseFactory as BaseResponseFactory;
use Illuminate\Contracts\Routing\ResponseFactory as FactoryContract;

class ResponseFactory //extends BaseResponseFactory implements FactoryContract
{
    /**
     * Create a new JSON response instance.
     *
     * @param bool        $result
     * @param int         $code
     * @param string|null $message
     * @param  mixed      $data
     * @param  int        $status
     * @return JsonResponse
     */
    public function ajax(bool $result = true, int $code = 1, string $message = null, $data = [], $status = 200)
    {
        $data = array_merge(['result'  => $result,
                             'code'    => $code,
                             'message' => $message,
                            ], $data);
        return new JsonResponse($data, $status, $headers = [], $options = 0);
    }
}