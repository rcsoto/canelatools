@section('associated-horaries-nav-item')
    <li class="nav-item">
        <a class="nav-link text-uppercase has-ripple" id="associated-horaries-tab" data-toggle="tab" href="#associated-horaries"
            role="tab" aria-controls="associated-horaries" aria-selected="false">@lang('canelatools::canelatools.canela-rest.form.label.associated-horaries')<span class="ripple ripple-animate"></span></a>
    </li>
@endsection

@section('associated-horaries-tab-content')
    <div class="tab-pane p-t-10 fade" id="associated-horaries" role="tabpanel" aria-labelledby="associated-horaries-tab">
        <div class="m-b-20">

            <div class="m-b-20">
                <div class="btn-group">
                    <a class="btn btn-success btn-tumblr waves-effect waves-light"
                       href="{{ $webPageNoticeHoraryTableRest?->urlAdd.'?notice_id='.$register->id }}">
                        <i class="fa fa-plus"></i> {{ trans('canelatools::canelatools.canela-rest.list.btn.new',
                        ['model' => $webPageNoticeHoraryTableRest?->formTitle]) }}</a>
                </div>
            </div>

            @include('canelatools::rest.layouts.table', array('tableRest' => $webPageNoticeHoraryTableRest))

        </div>
    </div>
@endsection

@section('body-scripts')
    @parent

    <script type="text/javascript">

        jQuery(document).ready(function () {


        });
    </script>
@endsection

