<?php

namespace Canela\CanelaTools\Controller;

class DashboardController extends \Illuminate\Routing\Controller
{
    public function index() {
        return view('canelatools::administrator.dashboard');
    }
}
