@section('associated-archives-nav-item')
    <li class="nav-item">
        <a class="nav-link text-uppercase has-ripple" id="associated-archives-tab" data-toggle="tab" href="#associated-archives" role="tab" aria-controls="associated-archives" aria-selected="false">@lang('canelatools::canelatools.canela-rest.form.label.associated-archives')<span class="ripple ripple-animate"></span></a>
    </li>
@endsection

@section('associated-archives-tab-content')
    <div class="tab-pane p-t-10 fade" id="associated-archives" role="tabpanel" aria-labelledby="associated-archives-tab">
        <div class="m-b-20">
            <div class="m-b-30">
                <div class="btn-group">
                    <a class="btn btn-success btn-tumblr waves-effect waves-light has-ripple" href="{{route('admin.general.archive.index', ['selectable' => 1, 'url_select' => route($tableRest->routeEntity.'.ajax.associatearchives', $register->id), 'url_back' => url()->full()])}}">
                        <i class="fa fa-plus"></i> @lang('canelatools::canelatools.canela-rest.list.btn.new', ['model' => trans('canelatools::canelatools.canela-rest.form.label.associated-archive')])<span class="ripple ripple-animate"></span></a>
                </div>
            </div>
            <div class="row associated-items-list">
                @foreach($register->archives()->with('archiveEntity')->get() as $item)
                    <div class="col-sm-12 col-md-6 col-lg-3 col-xl-2 associated-item-container">
                        <div>
                            <div class="sortable-move-icon-header"><i class="fa fa-arrows-alt"></i></div>
                            <?php
                            /** @var \Canela\CanelaTools\Models\WebBuilder\WebPageSection $item */
                            $archive = $item->archiveEntity;
                            $archiveName = $archive?->translate('archive');
                            $archiveUrl = asset(\Canela\CanelaTools\Models\General\Archive::DIR_ARCHIVES.$archiveName);
                            ?>
                            @if (!is_null($archive))
                                @if ($archive->type_id == \Canela\CanelaTools\Models\General\ArchiveType::IMAGE)
                                    <a href="{{$archiveUrl}}" target="_blank">
                                        <img src="{{$archiveUrl}}" alt="{{$archive->translate('title')}}">
                                    </a>
                                @endif
                                <div class="item-name-container"><a href="{{$archiveUrl}}" target="_blank">{{$archiveName}}</a></div>
                            @endif
                            <div class="m-t-10">
                                <a href="{{route('admin.general.archive.edit', $archive->id)}}" class="edit-item-link"><i
                                        class="fa fa-edit"></i></a>
                                <a href="#" class="remove-item-link" data-id="{{$item->id}}"><i
                                        class="fa fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('body-scripts')
    @parent

    <script type="text/javascript">

        jQuery(document).ready(function () {

            const $associatedArchivesList = jQuery('#associated-archives .associated-items-list');
            $associatedArchivesList.find('a.remove-item-link').each(function () {
               jQuery(this).on('click', function (e) {
                   e.preventDefault()

                   Swal.fire({
                       text: '{{trans('canelatools::canelatools.canela-rest.label.question_unlink_archive')}}',
                       showCancelButton: true,
                       confirmButtonText: '{{trans('canelatools::canelatools.buttons.continue')}}',
                       cancelButtonText: '{{trans('canelatools::canelatools.buttons.cancel')}}',
                   }).then(function (result) {
                       if (result.isConfirmed) {
                           jQuery.disassociateArchive(jQuery(e.currentTarget));
                       }
                   });
               });
            });

            jQuery.disassociateArchive = function (item) {
                jQuery.ajax({
                    url: '{{route($tableRest->routeEntity.'.ajax.disassociatearchive')}}/' + item.data('id'),
                    dataType: "json",
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'PATCH'
                    },
                    type: "POST",
                    processData: true,
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        const msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            item.parent().parent().remove();
                        } else {
                            jQuery.messageWarning(data.message);
                        }
                        jQuery.loadEnd();
                    },
                });
            };

            jQuery.sortAssociatedArchives = function () {
                const ids = $associatedArchivesList.find('a.remove-item-link').map(function () {
                    return jQuery(this).data('id');
                }).toArray();

                jQuery.ajax({
                    url: '{{route($tableRest->routeEntity.'.ajax.sortassociatedarchives')}}',
                    dataType: "json",
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'PATCH',
                        'ids': ids
                    },
                    type: "POST",
                    processData: true,
                    beforeSend: function () {
                        jQuery.loadIni();
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        const msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        jQuery.messageWarning(msg);
                        jQuery.loadEnd();
                    },
                    success: function (data) {
                        if (data.result) {
                            jQuery.messageSuccess(data.message);
                        } else {
                            jQuery.messageWarning(data.message);
                        }
                        jQuery.loadEnd();
                    },
                });

            }

            $associatedArchivesList.sortable({
                update: function () {
                    jQuery.sortAssociatedArchives();
                }
            });

        });
    </script>
@endsection

