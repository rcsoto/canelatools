<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\Parameter
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property string                                 $reference
 * @property string|null                            $value
 * @property \Illuminate\Support\Carbon             $created_at
 * @property \Illuminate\Support\Carbon             $updated_at
 * @property-read string                            $created_at_format
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_format
 * @property-read string                            $updated_at_short
 * @property-read string                            $value_identificator
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Parameter whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class Parameter extends BasicModel
{
    const ENTITY_TABLE = 'parameter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'reference',
        'value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'      => 'string',
        'reference' => 'string',
        'value'     => 'string',
    ];

    /**
     * Get reference
     * @param string $reference
     * @return string|NULL
     */
    public static function getValue($reference)
    {
        $parameter = self::whereReference($reference)
                         ->first();

        return $parameter instanceof Parameter
            ? $parameter->value
            : null;
    }


    /**
     * Crea un nuevo parámetro o la actualiza si ya existe
     * @param string $reference
     * @param string $value
     * @return Parameter
     */
    public static function setValue($reference, $value): Parameter
    {
        $parameter = Parameter::whereReference($reference)
                                 ->firstOrCreate([
                                     'name'  => $reference,
                                     'value' => $value,
                                 ]);

        $parameter->value = $value;
        $parameter->save();

        return $parameter;
    }
}
