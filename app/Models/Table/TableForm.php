<?php

namespace Canela\CanelaTools\Models\Table;

use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\ArchiveType;
use Illuminate\Database\Eloquent\Scope;
use JetBrains\PhpStorm\Deprecated;
use PhpParser\Node\Expr\Cast\String_;

class TableForm
{
    /** @var bool $readOnly Campo de solo lectura */
    public $readOnly  = false;

    /** @var string $charCountVisible Contador de caracteres visible */
    public $charCountVisible = true;

    /** @var string $id Id del campo */
    public $id;

    /** @var string $name Nombre del campo */
    public $name;

    /** @var string $label Etiqueta del campo */
    public $label = null;

    /** @var Scope $comboList Informacion para los Select */
    public $comboList;

    /** @var bool $comboMultiple Indica si el selector es multiple */
    public $comboMultiple = false;

    /** @var string $comboSelectedIds Indica los valores seleccionados para la seleccion multiple */
    public $comboSelectedIds = "";

    /** @var string $urlSelectRefresh Para los select, indica el metodo a ejecutar */
    public $urlSelectRefresh = null;

    /** @var array $onInputsChangeRefresh Input que al cambiar el valor hace que se dispare la funcion de refresco */
    public $onInputsChangeRefresh = [];

    /** @var string $type Tipo de la columna */
    public $type = TableColumnTypes::TEXT;

    /**
        @var integer $col Indica el numero de columnas en el que se muestra el campo
        @Deprecated
    */
    public $col = 12;

    /** var string $containerClass Clases a aplicar al contenerdor */
    public $containerClass = "col-12";


    /** @var float $min Indica el valor minimo */
    public $min;

    /** @var float $max Indica el valor maximo */
    public $max;

    /** mixed $value Valor por defecto */
    public $value = null;

    /**
     * @var string|null $fileDir
     * Stores file directory.
     * Used with TableColumnTypes::FILE and TableColumnTypes::IMAGE_CROPPER
     */
    private $fileDir = null;


    /** @var bool $required
     * Stores if field is required.
     */
    private bool $required = false;

    /**
     * @var null $archiveType
     * Stores archive type when file field.
     */
    public $archiveType = null;

    /**
     * @var bool $nullable
     * Stores if field admits the value null.
     */
    private $nullable = false;

    /**
     * @var string|null $fileName
     * Stores filename.
     * Only used with file typed fields.
     */
    private $fileName = null;

    /**
     * @var bool
     * Stores if can be translated only in WebLanguages (false) or in all languages (true).
     */
    private $allLanguages = false;


    /**
     * Stores if component has a button to create new item.
     *
     * @var boolean
     */
    private $hasAddNewBtn = false;


    /**
     * Stores new entity to create route.
     *
     * @var ?string
     */
    private $addNewBtnCreateRoute = null;


    /**
     * Stores new entity field id.
     *
     * @var ?string
     */
    private $addNewBtnFieldId = null;


    /**
     * TableForm constructor.
     * @param string|null $id
     * @param string|null $name
     * @param string|null $label
     * @param bool|null $required
     * @param string|null $type
     * @param string|null $containerClass
     * @param float|null $min
     * @param float|null $max
     * @param string|null $fileDir
     * @param int|null $archiveType
     * @param mixed|null $comboList
     * @param bool|null $readOnly
     * @param mixed|null $value
     * @param bool $nullable
     * @param string|null $fileName
     */
    public function __construct(
        ?string $name = null,
        ?string $label = null,
        ?bool $required = null,
        ?string $type = null,
        ?string $containerClass = null,
        ?float $min = null,
        ?float $max = null,
        ?string $fileDir = null,
        mixed $comboList = null,
        ?bool $readOnly = null,
        mixed $value = null,
        ?string $id = null,
        ?int $archiveType = null,
        bool $nullable = false,
        ?string $fileName = null,
        bool $allLanguages = false,
    )
    {
        if(!is_null($name))             $this->setName($name);
        if(!is_null($label))            $this->setLabel($label);
        if(!is_null($required))         $this->setRequired($required);
        if(!is_null($type))             $this->setType($type);
        if(!is_null($containerClass))   $this->setContainerClass($containerClass);
        if(!is_null($min))              $this->setMin($min);
        if(!is_null($max))              $this->setMax($max);
        if(!is_null($fileDir))          $this->setFileDir($fileDir);
        if(!is_null($comboList))        $this->setComboList($comboList);
        if(!is_null($readOnly))         $this->setReadOnly($readOnly);
        if(!is_null($value))            $this->setValue($value);

        if(!is_null($id)) {
            $this->setId($id);
        } elseif (!is_null($name)) {
            $this->setId($name);
        }

        if(!is_null($archiveType))      $this->setArchiveType($archiveType);
        $this->setNullable($nullable);
        if(!is_null($fileName)) $this->setFileName($fileName);
        $this->setAllLanguages($allLanguages);
    }


    /**
     *
     * @param bool $value
     * @return TableForm
     */
    public function setReadOnly(bool $value): TableForm
    {
        $this->readOnly = $value;
        return $this;
    }


    /**
     *
     * @param bool $value
     * @return TableForm
     */
    public function setCharCountVisible(bool $value): TableForm
    {
        $this->charCountVisible = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableForm
     */
    public function setId(string $value): TableForm
    {
        $this->id = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableForm
     */
    public function setName(string $value): TableForm
    {
        $this->name = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableForm
     */
    public function setLabel(string $value)
    {
        $this->label = $value;
        return $this;
    }


    /**
     * @param $comboList
     * @param bool $multiple
     * @param string $selectedIds
     * @param null $urlSelectRefresh
     * @param array $onInputsChangeRefresh
     * @return TableForm
     */
    public function setComboList($comboList, bool $multiple=false, string $selectedIds="",
        $urlSelectRefresh=null, $onInputsChangeRefresh=[]): TableForm
    {
        $this->comboList                = $comboList;
        $this->comboMultiple            = $multiple;
        $this->comboSelectedIds         = $selectedIds;
        $this->urlSelectRefresh         = $urlSelectRefresh;
        $this->onInputsChangeRefresh    = $onInputsChangeRefresh;
        $this->type                     = TableColumnTypes::SELECT2;
        return $this;
    }


    /**
     * @param $comboList
     * @param bool $multiple
     * @param string $selectedIds
     * @param null $urlSelectRefresh
     * @param array $onInputsChangeRefresh
     * @return TableForm
     */
    public function setComboListJson($comboList, bool $multiple=false, string $selectedIds="",
                                     $urlSelectRefresh=null, $onInputsChangeRefresh=[]): TableForm
    {
        $this->comboList                = BasicModel::getComboListJson($comboList);
        $this->comboMultiple            = $multiple;
        $this->comboSelectedIds         = $selectedIds;
        $this->urlSelectRefresh         = $urlSelectRefresh;
        $this->onInputsChangeRefresh    = $onInputsChangeRefresh;
        $this->type                     = TableColumnTypes::SELECT2;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableForm
     */
    public function setType(string $value): TableForm
    {
        $this->type = $value;
        return $this;
    }


    /**
     *
     * @param int $value
     * @return TableForm
     * @deprecated
     */
    #[Deprecated(
        reason: '',
        replacement: 'setContainerClass(string $value): TableForm'
    )]    public function setCol(int $value): TableForm
    {
        $this->col = $value;
        return $this;
    }


    /**
     *
     * @param string $value
     * @return TableForm
     */
    public function setContainerClass(string $value): TableForm
    {
        $this->containerClass = $value;
        return $this;
    }


    /**
     *
     * @param float $value
     * @return TableForm
     */
    public function setMin(float $value): TableForm
    {
        $this->min = $value;
        return $this;
    }


    /**
     *
     * @param float $value
     * @return TableForm
     */
    public function setMax(float $value): TableForm
    {
        $this->max = $value;
        return $this;
    }


    /**
     * getOnInputsChangeRefreshJson
     *
     * @return string
     */
    public function getOnInputsChangeRefreshJson(): string
    {
        return json_encode($this->onInputsChangeRefresh);
    }


    /**
     *
     * @param $value
     * @return TableForm
     */
    public function setValue($value): TableForm
    {
        $this->value = $value;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getFileDir(): ?string
    {
        return $this->fileDir;
    }


    /**
     * @param string|null $fileDir
     * @return TableForm
     */
    public function setFileDir(?string $fileDir): TableForm
    {
        $this->fileDir = $fileDir;
        return $this;
    }

    /**
     * getArchiveType
     *
     * @return integer|null
     */
    public function getArchiveType(): ?int
    {
        return $this->archiveType;
    }

    /**
     * setArchiveType
     *
     * @param integer|null $archiveType
     * @return TableForm
     */
    public function setArchiveType(?int $archiveType): TableForm
    {
        $this->archiveType = $archiveType;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }


    /**
     * @param bool $required
     * @return TableForm
     */
    public function setRequired(bool $required): TableForm
    {
        $this->required = $required;
        return $this;
    }


    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }


    /**
     * @param bool $nullable
     * @return TableForm
     */
    public function setNullable(bool $nullable): TableForm
    {
        $this->nullable = $nullable;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }


    /**
     * @param string|null $fileName
     * @return TableForm
     */
    public function setFileName(?string $fileName): TableForm
    {
        $this->fileName = $fileName;
        return $this;
    }


    /**
     * @return bool
     */
    public function isAllLanguages(): bool
    {
        return $this->allLanguages;
    }


    /**
     * @param bool $allLanguages
     * @return TableForm
     */
    public function setAllLanguages(bool $allLanguages): TableForm
    {
        $this->allLanguages = $allLanguages;
        return $this;
    }


    /**
     * Return if component has new item button.
     *
     * @return bool
     */
    public function getHasAddNewBtn(): bool
    {
        return $this->hasAddNewBtn;
    }


    /**
     * Return new entity button route.
     *
     * @return string|null
     */
    public function getAddNewBtnCreateRoute(): ?string
    {
        return $this->addNewBtnCreateRoute;
    }


    /**
     * Set new entity button route and return itself.
     * Generally used with archives.
     *
     * @param string $route
     * @return TableForm
     */
    public function setAddNewBtnCreateRoute(?string $route = null): TableForm
    {
        if (!is_null($route)) {
            $this->hasAddNewBtn = true;
            $this->addNewBtnCreateRoute = $route;
            $this->addNewBtnFieldId = $this->id;
        }
        return $this;
    }


    /**
     * Return new entity field id.
     *
     * @return string|null
     */
    public function getAddNewBtnFieldId(): ?string
    {
        return $this->addNewBtnFieldId;
    }

}
