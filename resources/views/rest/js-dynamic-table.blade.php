﻿@php
    use Canela\CanelaTools\Manager\UtilsBasicManager;
    use Canela\CanelaTools\Enums\TableFilterTypes;

    /** @var String $nameSectionTable nombre de la seccion donde se añade el codigo */
    $nameSectionTable = $nameSectionTable ?? config('canelatools.blade.section.contentTable');

    $exportUrlQueryString = "?action=export&". http_build_query(request()->toArray());
    $modetable              = ($modetable  ?? true ) ? 1 : 0;
    $hideFilter             = ($hideFilter ?? false) ? 1 : 0;
    $groupBtn               = ($group_btn  ?? 0) ? 1 : 0;
    $gridBtn                = ($grid_btn   ?? 0) ? 1 : 0;
    $functionModeGrid       = ($functionModeGrid ?? '');
    $itemsByDefaultSelect   = ($itemsByDefaultSelect ?? 10);
    $columnsVisible         = $columnsVisible ?? $columns;

@endphp

@section('content-table')
    @parent
    {!! Form::open(array('url' => $urlentidad.$exportUrlQueryString, 'id' => $table.'-excel-report-table')) !!}
    {!! Form::hidden('filters', null, array('id' => $table.'-report-filters')) !!}
    {!! Form::hidden('orderbys', null, array('id' => $table.'-report-orderbys')) !!}
    {!! Form::hidden('titles', null, array('id' => $table.'-report-titles')) !!}
    {!! Form::hidden('visibilities', null, array('id' => $table.'-report-visibilities')) !!}
    {!! Form::hidden('modetable', $modetable, array('id' => $table.'-modetable')) !!}
    {!! Form::hidden('parameterFilters', null, array('id' => $table.'-parameter-filters')) !!}
    {!! Form::close() !!}
@endsection


<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function () {

        // ----------------------------------------------------------------------
        // Variables globales
        // ----------------------------------------------------------------------
        var _TABLE 	    = '{{$table}}';
        var _TABLE_ID 	= '#{{$table}}';


        // ----------------------------------------------------------------------
        // Crear tabla
        // ----------------------------------------------------------------------
        let langUrl = '//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json';
        @if(App::getLocale() == 'es')
            langUrl = '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json';
        @endif

        @if($modetable)
        jQuery(_TABLE_ID).DataTable({
            // set the initial value
            "bPaginate": false,
            "searching": false,
            "info": false,
            "bSort": false,
            "aoColumns": {{$columnsVisible}},
            language: {
                url: langUrl
            },
            initComplete: function (settings, json) {

            }
        });
        @endif

        // ----------------------------------------------------------------------
        // Buscar al hacer en intro en un campo de busqueda
        // ----------------------------------------------------------------------
        jQuery(_TABLE_ID+' input.search_init').keypress(function (e) {
            if (e.which === 13) {
                jQuery.buscar(_TABLE);
            }
        });


        // ----------------------------------------------------------------------
        // Cambio en select de elementos por pagina
        // ----------------------------------------------------------------------
        jQuery(_TABLE_ID+" select.search_init").change(function () {
        	jQuery.buscar(_TABLE);
        });



        // ----------------------------------------------------------------------
        // Configuar cabeceras de orden
        // ----------------------------------------------------------------------
        jQuery(_TABLE_ID+" #tr-orders").children().each(function () {
            // ----------------------------------------------------------------------
            // Pulsar en cabeceras de orden
            // ----------------------------------------------------------------------
            jQuery(this).click(function () {
                if (jQuery(this).attr('data-orderby') === 'ASC') {
                    // vaciar todos los campos de orden
                    jQuery("#tr-orders").children().each(function () {
                        jQuery(this).attr('data-orderby', '');
                        jQuery(this).children().each(function () {
                            jQuery(this).removeClass('fa-sort-asc, fa-sort-desc').addClass('fa-sort');
                        });
                    });

                    jQuery(this).children().each(function () {
                        jQuery(this).addClass('fa-sort-asc');
                        jQuery(this).removeClass('fa-sort-desc');
                    });

                    jQuery(this).attr('data-orderby', 'DESC');
                } else {
                    // vaciar todos los campos de orden
                    jQuery("#tr-orders").children().each(function () {
                        jQuery(this).attr('data-orderby', '');
                        jQuery(this).children().each(function () {
                            jQuery(this).removeClass('fa-sort-asc, fa-sort-desc').addClass('fa-sort');
                        });
                    });

                    jQuery(this).children().each(function () {
                        jQuery(this).addClass('fa-sort-desc');
                        jQuery(this).removeClass('fa-sort-asc');
                    });

                    jQuery(this).attr('data-orderby', 'ASC');
                }

                jQuery.buscar(_TABLE);
            });


            // ----------------------------------------------------------------------
            // Configuar cabeceras de orden
            // ----------------------------------------------------------------------
            if (jQuery(this).attr('id') != null) {
                jQuery(this).css('cursor', 'pointer');

                if (jQuery(this).attr('data-orderby') === 'ASC') {
                    jQuery(this).children(function () {
                        jQuery(this).removeClass('fa-sort-asc');
                        jQuery(this).addClass('fa-sort-desc');
                    });

                } else {
                    jQuery(this).children(function () {
                        jQuery(this).removeClass('fa-sort-desc');
                        jQuery(this).addClass('fa-sort-asc');
                    });
                }
            }

        });


        // ----------------------------------------------------------------------
        // CARGA INICIAL
        // ----------------------------------------------------------------------
        var parametersFilter = "";
        @if(!empty($parameterFilters))
        var jsonObj = [];
        @foreach($parameterFilters as $key => $value)
            item = {};
        item['{{$key}}'] = '{{$value}}';
        jsonObj.push(item);
        @endforeach
            parametersFilter = JSON.stringify(jsonObj);
        @endif

        const columnas = JSON.stringify({{$columnsVisible}});

        jQuery(_TABLE_ID).attr('data-urlentidad', "{{$urlentidad}}")
            .attr('data-columns', columnas)
            .attr('data-parameter-filters', parametersFilter)
            .attr('data-iniciar-busqueda', 0)
            .attr('data-modetable', '{{ $modetable }}')
            .attr('data-function-mode-grid', '{{ $functionModeGrid }}')
            .attr('data-hide-filter', '{{ $hideFilter }}')
            .attr('data-group-btn', '{{ $groupBtn }}')
            .attr('data-grid_btn', '{{ $gridBtn }}')
            .attr('data-items-by-default-select', '{{ $itemsByDefaultSelect }}')
            .attr('data-request', $.param({!! json_encode(request()->toArray()) !!}));
        jQuery.hideAdvancedFilters(_TABLE_ID);

        jQuery.busquedasAvanzadas(_TABLE);
        jQuery.createElementosPorPagina(_TABLE);
        jQuery.cookieFilters(_TABLE_ID);
        jQuery(_TABLE_ID).data('iniciar-busqueda', 1);
        jQuery.buscar(_TABLE);
        jQuery.setTableTopRightBtnContainer(_TABLE);
    });



    /* Event listener on search ends. */
    //jQuery(document).on('endDatatableSearch', function (event, data, button) {
    //  //some stuff here
    //});

</script>
