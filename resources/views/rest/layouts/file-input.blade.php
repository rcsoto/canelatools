<?php
    /** @var string $type */
    /** @var string $fileId */
    /** @var string $fileName */
    /** @var string $fileDir */
    /** @var string $fileValue */

    use Canela\CanelaTools\Enums\TableColumnTypes;

    $tableColumnTypeClass = match($type) {
        TableColumnTypes::IMAGE => ' canela-input-image',
        TableColumnTypes::IMAGE_CROPPER => ' canela-input-image-cropper',
        default => '',
    };
?>
<div class="preview-container">
    @if (\Canela\CanelaTools\Enums\TableColumnTypes::isImageType($type))
        <img src="{{empty($register) ? '' : asset($fileDir.$fileValue)}}" alt="">
    @else
        @if(!empty($register) && !empty($fileValue))
            <a href="{{asset($fileDir.$fileValue)}}" class="file-link" target="_blank">{{$fileValue}}</a>
        @endif
    @endif

    @if(!empty($register) && !empty($fileValue))
        <a href="javascript:jQuery.confirmDeleteFile('{{$fileId}}', '{{$fileName}}', '{{$fileDir}}', '{{$type}}')" class="image-action-icon-link remove-file-link"><i class="fa fa-trash-alt"></i></a>
    @endif
    <a href="javascript:jQuery.cleanFile('{{$fileId}}', '{{$type}}')" class="image-action-icon-link clean-file-link" style="display: none;"><i class="fa fa-times"></i></a>
</div>
{!! Form::file($fileName, array('class' => 'form-control canela-input-text'.$tableColumnTypeClass, 'id' => $fileId)) !!}
