<?php

namespace Canela\CanelaTools\Middleware;

use Closure;
use Illuminate\Http\Request;

class Profile
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param array ...$type
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$type): mixed
    {
        if (!\Canela\CanelaTools\Models\User\Profile::hasProfile($type)) {
            abort(401, trans('auth.unauthorized'));
        }

        return $next($request);
    }
}
