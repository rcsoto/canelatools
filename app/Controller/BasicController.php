<?php

namespace Canela\CanelaTools\Controller;

use Auth;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\HttpErrors;
use Canela\CanelaTools\Enums\MessageType;
use Canela\CanelaTools\Manager\SecurityManager\SecurityManagerTrait;
use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\Module;
use Canela\CanelaTools\Models\Table\TableRest;
use DB;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;
use Log;
use Response;
use Session;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class BasicController
 * @package Canela\CanelaTools\Controller
 */
class BasicController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests,
        TableListController, SecurityManagerTrait, ReorderableController, UpdateStatusController, DeletableController;


    /** @var Model $entidad Clase del modelo sobre el que se hacen las operaciones */
    protected $entidad;

    /** @var string $routeEntity Determina el nombre de la entidad en la ruta */
    public $routeEntity;

    /** @var string $routePrefix Determina la ruta de acceso a los metodos de la entidad */
    protected $routePrefix;

    /** @var string $urlPrefix Determina la url de acceso a los metodos de la entidad */
    protected $urlPrefix;

    /** @var string $titleEntity Ruta lang del título de la entidad */
    public $titleEntity;

    /** @var string $vistaLista Nombre de la vista asociada a la lista */
    protected $vistaLista = 'canelatools::rest.list';

    /** @var string $vistaEdit Nombre de la vista asociada a la pantalla de edit */
    protected $vistaEdit = 'canelatools::rest.addEdit';

    /** @var string $vistaAdd Nombre de la vista asociada a la pantalla nuevo */
    protected $vistaAdd = 'canelatools::rest.addEdit';

    /** @var string $vistaCopy Nombre de la vista asociada a la pantalla de duplicar un Registro */
    protected $vistaCopy;

    /** @var string $menu Nombre de la opcion de menu */
    protected $menu;

    /** @var string $menu_group Nombre de la opcion de grupo del menu */
    protected $menu_group;

    /** @var string $submenu_group Nombre de la opcion de grupo de sub menu */
    protected $submenu_group;

    /** @var TableRest $tableRest  */
    protected $tableRest;


    /**
     * AppController constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setSession();

        foreach ((new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            \Illuminate\Support\Facades\View::share($property->name, $property->getValue($this));
        }
    }

    /**
     * Set session variables.
     */
    protected function setSession()
    {
        AdminMenu::setCurrent($this->menu);
        AdminMenu::setCurrentGroup($this->menu_group);
        AdminMenu::setCurrentGroupSub($this->submenu_group);
    }


    /**
     * Index view. Show list of items.
     *
     * @return Factory|View
     */
    protected function index(): Factory|View
    {
        $this->setSession();

        // Pre Index ?
        if (method_exists($this, 'preIndex')) {
            // Em metodo ha de devolver TRUE o FALSE
            $this->preIndex();
        }

        return view($this->vistaLista)->with('tableRest', $this->tableRest)
                                      ->with('urlPrefix', $this->urlPrefix)
                                      ->with(request()->all());
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return mixed|void
     */
    public function edit(Request $request, int $id)
    {
        $this->setSession();

        try {
            /** @var Model $register */
            $register = ($this->entidad)::findOrFail($id);

            // Seguridad
            if (method_exists($this, 'securityItem')) {
                // Em metodo ha de devolver TRUE o FALSE
                if (!$this->securityItem($register)) {
                    abort(401, trans('auth.unauthorized'));
                }
            }
            if (method_exists($this, 'securityProject')) {
                // El metodo aborta o continua
                $this->securityProject($register);
            }

            if (!is_null($this->tableRest)) {
                $this->tableRest->setInitialValues($request);
            }

            // Mas parametros a retornar?
            if (method_exists($this, 'setParamReturn')) {
                // Se ha de hacer un return con la Vista y los Parametros
                return $this->setParamReturn($register, $request);
            } else {
                return view($this->vistaEdit, compact('register'))
                                    ->with('urlPrefix', $this->urlPrefix)
                                    ->with('tableRest', $this->tableRest);
            }
        } catch (HttpException $exception) {
            if (HttpErrors::isAbortError($exception)) {
                abort($exception->getStatusCode(), $exception->getMessage());
            } else {
                $msg = config('app.debug') ? $exception->getMessage() : trans('canelatools::canelatools.model.find.error');
                return redirect()->back()->withInput()->with(MessageType::WARNING, $msg);
            }
        } catch (Exception $exception) {
            $msg = config('app.debug') ? $exception->getMessage() : trans('canelatools::canelatools.model.find.error');
            return redirect()->back()->withInput()->with(MessageType::WARNING, $msg);
        }
    }

    /**
     * Create item view.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->setSession();
        // Seguridad
        if (method_exists($this, 'securityItemCreate')) {
            // Em metodo ha de devolver TRUE o FALSE
            if (!$this->securityItemCreate()) {
                abort(401);
            }
        }

        if (!is_null($this->tableRest)) {
            $this->tableRest->setInitialValues($request);
        }

        // additional params?
        if (method_exists($this, 'setParamReturnCreate')) {
            // Se ha de hacer un return con la Vista y los Parametros
            return $this->setParamReturnCreate($request);
        } else {
            return view($this->vistaAdd)
                        ->with('urlPrefix', $this->urlPrefix)
                        ->with('tableRest', $this->tableRest);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FormRequest $request
     * @param string $redirectTo Indica la url al acabar
     * @param bool $storeOnly Indica si el metodo ha de devolver el modelo tras la creacion
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function storeBasic(FormRequest $request, $redirectTo=null, $storeOnly=false)
    {
        try {
            DB::beginTransaction();
            /** @var Model $model */
            $entidad = $this->entidad;
            $model   = new $entidad($this->filterBinaries($request));

            // metodo a ejecutar previo al guardado
            if (method_exists($this, 'storePre')) {
                $model = $this->storePre($model, $request);
            }

            $model->commit(Action::INSERT, Module::WEB, (Auth::check() ? Auth::user()->id : null));

            // metodo a ejecutar post guardado
            if (method_exists($this, 'storePost')) {
                $model = $this->storePost($model, $request);
            }

            if (method_exists($this, 'storePostRequest')) {
                $request = $this->storePostRequest($model, $request);
            }

            // Guardar ficheros.
            if (method_exists($this, 'storeFilesPost')) {
                $model = $this->storeFilesPost($model, $request);
            }

            // si no es unicamente una creacion
            if ($storeOnly == false) {
                $request->ajax()
                    ?: Session::flash(MessageType::SUCCESS, trans('canelatools::canelatools.model.store.correct', ['value' => $model->value_identificator]));
            }
            DB::commit();

            if ($storeOnly)
            {
                return $model;
            }

            // respuesta a formulario

            // Ajax ??
            if ($request->ajax())
            {
                return Response::ajax(true, 1,
                                  trans('canelatools::canelatools.model.store.correct', ['value' => $model->value_identificator]),
                                  ['model' => $model->attributesToArray(),
                                   'redirectTo' => $redirectTo
                                  ]);
            }
            // Web
            else
            {
                // sin ruta especificada
                if (empty($redirectTo))
                {
                    return (is_null($this->routeEntity)
                                ? redirect()->back()
                                : redirect()->route("{$this->routeEntity}.index"));
                }
                else
                {
                    return redirect()->route($redirectTo);
                }
            }
        } catch (Exception $exception) {
            Log::info($exception);
            DB::rollBack();

            if ($storeOnly)
            {
                return null;
            }

            // respuesta a formulario

            return $request->ajax()
                ? Response::ajax(false, 0, trans('canelatools::canelatools.model.store.error'))
                : redirect()->back()->withInput()
                            ->with(MessageType::WARNING, trans('canelatools::canelatools.model.store.error'));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param FormRequest $request
     * @param int $id
     * @param string|null $redirectTo Indica la url al acabar
     * @param bool $updateOnly Indica si el método ha de devolver el modelo tras la actualización
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     * @throws ModelNotFoundException
     */
    public function updateBasic(FormRequest $request, int $id, ?string $redirectTo=null, bool $updateOnly=false): Model|JsonResponse|RedirectResponse|null
    {
        try {
            DB::beginTransaction();
            /** @var Model $model */
            $model    = ($this->entidad)::findOrFail($id);
            $modelPre = clone $model;

            if (method_exists($this, 'updateValidate')) {
                $model = $this->updateValidate($model, $request);
                if (!is_a($model, get_class($model))) {
                    throw new Exception('Error updateValidate');  // TODO: Change to expecific exception to catch errors.
                }
            }

            $model->update($this->filterBinaries($request));

            // metodo a ejecutar previos al guardado
            if (method_exists($this, 'updatePre')) {
                $model = $this->updatePre($model, $request);
            }

            $model->commit(Action::UPDATE, Module::WEB, (Auth::check() ? Auth::user()->id : null), $modelPre, true);

            // metodo a ejecutar tras el guardado
            if (method_exists($this, 'updatePost')) {
                $model = $this->updatePost($model, $request);
            }

            if (method_exists($this, 'updatePostRequest')) {
                $request = $this->updatePostRequest($model, $request);
            }

            // Guardar ficheros.
            if (method_exists($this, 'updateFilesPost')) {
                $model = $this->updateFilesPost($model, $request);
            }

            // si unicamente grabamos registro no lo hacemos
            if ($updateOnly == false) {
                $request->ajax()
                    ?: Session::flash(MessageType::SUCCESS, trans('canelatools::canelatools.model.update.correct', ['value' => $model->value_identificator]));
            }
            DB::commit();

            // solo grabamos
            if ($updateOnly)
            {
                return $model;
            }

            // respuesta de formulario

            // Ajax ??
            if ($request->ajax())
            {
                return Response::ajax(true, 1,
                        trans('canelatools::canelatools.model.update.correct', ['value' => $model->value_identificator]),
                        ['model' => $model->attributesToArray(),
                         'redirectTo' => $redirectTo]);
            }
            // Web
            else
            {
                // sin ruta especificada
                if (empty($redirectTo))
                {
                    return (is_null($this->routeEntity)
                        ? redirect()->back()
                        : redirect()->route("{$this->routeEntity}.index"));
                }
                else
                {
                    return redirect()->route($redirectTo);
                }
            }
        } catch (Exception $exception) {
            Log::info($exception);
            DB::rollBack();

            // si solo grabamos
            if ($updateOnly)
            {
                return null;
            }

            // respuesta de formulario
            return $request->ajax()
                ? Response::ajax(false, 0, trans('canelatools::canelatools.model.update.error'))
                : redirect()->back()->withInput()
                            ->with(MessageType::WARNING, trans('canelatools::canelatools.model.update.error'));
        }
    }


    /**
     * @param $request
     * @return array
     */
    private function filterBinaries($request): array {
        return collect($request->all())->filter(function ($value, $key) use ($request) {
            return !$request->hasFile($key);
        })->toArray();
    }


    /**
     * Obtiene el parametro dado
     * @param $claveParametro
     * @param string $parameter
     * @param null $valueDefault
     * @return mixed|string|null
     */
    public function getValueParameterFilters($claveParametro, string $parameter, $valueDefault=null)
    {
        $parametros   = json_decode(request()->get($claveParametro));
        if (!empty($parametros)) {
            foreach ($parametros as $value) {
                foreach ($value as $key => $val) {
                    if ($key == $parameter) {
                        return $val;
                    }
                }
            }
        }
        return $valueDefault;
    }


    /**
     * @return Entity|null
     */
    public function getEntity(): ?Entity
    {
        return Entity::getEntityByReference(($this->entidad)::getTableName());
    }

}
