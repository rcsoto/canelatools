<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\Action
 *
 * @property int                 $id
 * @property string              $name
 * @property string|null         $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null         $reference
 * @property-read string         $created_at_format
 * @property-read string         $updated_at_format
 * @property-read string         $table
 * @property-read string         $combo_identificator
 * @property-read string         $value_identificator
 * @property-read string         $created_at_short
 * @property-read string         $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboAreaList($area_id = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboSubAreaList($subAreaId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Action whereReference($value)
 * @mixin \Eloquent
 */
class Action extends BasicModel
{
    // constantes
    const LOGIN        = 1;    // Acceso a la aplicacion mediante login
    const LOGOUT       = 2;    // Cerrar sesion
    const INSERT       = 3;    // Nuevo elemento
    const UPDATE       = 4;    // Edidcion de un registro
    const DELETE       = 5;    // Eliminar un registro
    const BAJA         = 6;    // Baja logica
    const ACTIVAR      = 7;    // Activar un registro
    const DESHABILITAR = 8;    //  Deshabilitar un registro
    const LISTA        = 9;    // Visualizacion de una lista
    const CONSULTA     = 10;    // Consulta un registro

    const SEND_MAIL = 12;   // Enviar correo electronico

    const PASSWORD_RESET_SEND_MAIL = 13;    // Envio de correo para cambiar password
    const PASSWORD_RESET           = 14;    // Cambio de password

    const NOTIFICACION = 21;   // Enviar notificacion a un usuario a la APP

    const BUSQUEDA = 22;   // Busqueda de informacion

    const CAMBIO_DE_ESTADO = 23;    // Cambio de estado

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'action';

    public $comboIdentifierField = 'reference';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'reference'];
}
