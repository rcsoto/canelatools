<?php

namespace Canela\CanelaTools\Controller;


use Auth;
use Canela\CanelaTools\Enums\HttpErrors;
use Canela\CanelaTools\Models\Action;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Module;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\User\UserState;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;

trait UpdateStatusController
{
    /**
     * Deactivate registry
     *
     * @param int $id
     * @return JsonResponse
     */
    protected function deactivate(int $id): JsonResponse
    {
        return $this->changeStatus($id, State::BAJA);
    }

    /**
     * Activate registry
     *
     * @param int $id
     * @return JsonResponse
     */
    protected function activate(int $id): JsonResponse
    {
        return $this->changeStatus($id, State::ACTIVO);
    }

    /**
     * Disable registry
     *
     * @param int $id
     * @return JsonResponse
     */
    protected function disable(int $id): JsonResponse
    {
        return $this->changeStatus($id, UserState::DESHABILITADO);
    }

    /**
     * Update status.
     *
     * @param int $id
     * @param int $stateId
     * @return JsonResponse
     * @throw Exception
     */
    protected function changeStatus(int $id, int $stateId): JsonResponse
    {
        try {
            /** @var BasicModel $model */
            $model = ($this->entidad)::findOrFail($id);

            // Check if status can be changed
            if (method_exists($this, 'validateChangeStatus')) {
                if (!$this->validateChangeStatus($model, $stateId)) {
                    throw new Exception(trans('auth.unauthorized'), HttpErrors::HTTP_UNAUTHORIZED);
                }
            }

            DB::beginTransaction();

            $model->state_id = $stateId;
            if (method_exists($this, 'changeStatePost')) {
                $this->changeStatePost($model, $stateId);
            }

            method_exists($model, 'commit')
                ? $model->commit(Action::INSERT, Module::WEB, (Auth::check() ? Auth::user()?->id ?? null : null))
                : $model->save();

            DB::commit();
            return response()->json([
                'result' => true,
                'message' => trans('canelatools::canelatools.model.update_status.correct'),
                'stateId' => $model->state_id,
                'stateName' => $model->state->name,
            ]);
        }
        catch (Exception $exception) {
            $msg = config('app.debug') ? $exception->getMessage() : trans('canelatools::canelatools.model.update_status.error');

            return response()->json([
                'result' => false,
                'message' => $msg,
            ]);
        }
    }
}
