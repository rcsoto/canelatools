<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Models\Client\Customer;
use Canela\CanelaTools\Models\WebBuilder\Web;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'customer_id',
        'type_id',
        'state_id',
        'show_header',
        'show_footer',
        'show_home',
        'position',
        'slug',
        'title',
        'subtitle',
        'body',
        'file_header_id',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'external_link',
        'is_external_link',
        'open_in_new_window'
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        if ($this->request->has('customer_id')) {
            $customer = Customer::findOrFail($this->request->get('customer_id'));
            $web = Web::findOrFail($customer->web_id);
            $this->merge(['web_id' => $web->id]);
        }

        return [
            'customer_id'       => 'required|integer|min:1|exists:customer,id',
            'type_id'           => 'required|integer|min:1|exists:web_page_type,id',
            'state_id'          => 'required|integer|min:1|exists:state,id',
            'show_header'       => 'required|integer|between:0,1',
            'show_footer'       => 'required|integer|between:0,1',
            'show_home'         => 'required|integer|between:0,1',
            'position'          => 'required|integer|max:'.TableColumnMaxSize::UNSIGNED_SMALLINT_MAX,
            'slug'              => 'nullable|max:255',
            'title'             => 'required|max:255',
            'subtitle'          => 'max:4000',
            'body'              => 'max:'.TableColumnMaxSize::LONGTEXT_MAX,
            'file_header_id'    => 'nullable|integer|min:1|exists:archive,id',
            'seo_title'         => 'max:255',
            'seo_keywords'      => 'max:255',
            'seo_description'   => 'max:4000',
            'external_link'     => 'required_if:is_external_link,1|max:4000',
            'is_external_link'  => 'required|integer|between:0,1',
            'open_in_new_window'=> 'required|integer|between:0,1',
        ];
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['customer_id']  = trans('canelatools::canelatools.entity.customer.report');
        $attributes['state_id']     = trans('canelatools::canelatools.general.field.state');
        $attributes['position']     = trans('canelatools::canelatools.general.field.position');

        return $attributes;
    }

}
