<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\WebPage;
use App\Models\Project\WebBuilder\WebPageFaq;
use App\Models\Project\WebBuilder\WebPageType;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Requests\WebBuilder\WebPageFaqRequest;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageFaqController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageFaqController extends BasicController
{
    use HtmlLists;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageFaq::class;
        $this->routeEntity = 'admin.webbuilder.webpagefaq';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_faq.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_faq.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_fag',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.name'),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.page_id'),
                ),
                new TableColumn(
                    table: 'web_page_faq',
                    fieldOrder: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                ),
                new TableColumn(
                    table: 'web_page_faq',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.name'),
                ),
                new TableColumn(
                    table: 'web_page_faq',
                    fieldOrder: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.description'),
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.general.field.state'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: State::comboTranslate(),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.page_id'),
                    required: true,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.name'),
                    required: true,
                    max: 4000,
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.description'),
                    type: TableColumnTypes::TEXTAREA,
                    max: TableColumnMaxSize::TEXT_MAX,
                ),
                new TableForm(
                    name: 'open_in_new_window',
                    label: trans('canelatools::canelatools.entity.web_page_faq.open_in_new_window'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'external_link',
                    label: trans('canelatools::canelatools.entity.web_page_faq.field.external_link'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = WebPageFaq::select([
                'web_page_faq.id',
                'customer.name as customer_name',
                'web_page.title as web_page_title',
                'web_page_faq.position',
                'web_page_faq.name',
                'web_page_faq.description',
                'state.name as state_name',
                'web_page_faq.id as action_web_page_faq_id',
                'web.id as web_id',
                'state.id as state_id',
            ])
            ->join('web_page', 'web_page_faq.page_id', 'web_page.id')
            ->join('web', 'web_page.web_id', 'web.id')
            ->join('customer', 'web.id', 'customer.web_id')
            ->join('client', 'customer.client_id', 'client.id')
            ->join('state', 'web_page_faq.state_id', 'state.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'action_web_page_faq_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, true, false, true, true, false,
                        BasicModel::TYPE_CHANGE_STATE, $jsonItem->state_id,
                        true, true);
                    break;
                case 'description':
                    $txt = '<div class="td-multiple-lines">'.$value.'</div>';
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
                    break;
                default:
                    $txt = $value;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_FAQ));
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, int $id): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_FAQ));
        return parent::edit($request, $id);
    }



    /**
     * @param WebPageFaqRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageFaqRequest $request): Model|JsonResponse|RedirectResponse|null
    {
        return $this->storeBasic($request);
    }


    /**
     * @param WebPageFaqRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageFaqRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
