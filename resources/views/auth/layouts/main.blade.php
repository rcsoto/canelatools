@extends('canelatools::administrator.layouts.main', ['auth' => true])
@php
    use App\Models\Project\WebBuilder\Web;
    $web = Web::firstOrFail();
    $customer = $web->customer;
@endphp

@section('body-class', 'page-auth')

@section('content')
    <!-- [ auth-signup ] start -->
    <div class="auth-wrapper">
        <div class="auth-content">
            <div class="card">
                <div class="row align-items-center text-center">
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="logo-container"><img src="{{asset(\App\Models\General\Archive::DIR_ARCHIVES.$customer->image_logo)}}" alt="{{$customer->business_name}}" class="img-fluid mb-4"></div>
                            @yield('auth-content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ auth-signup ] end -->
@endsection

