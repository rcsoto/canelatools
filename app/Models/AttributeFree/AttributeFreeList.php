<?php namespace Canela\CanelaTools\Models\AttributeFree;


use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Canela\CanelaTools\Models\AttributeFree\AttributeFreeList
 *
 * @property int                                                         $id
 * @property string                                                      $name
 * @property string|null                                                 $position
 * @property \Carbon\Carbon|null                                         $created_at
 * @property \Carbon\Carbon|null                                         $updated_at
 * @property int                                                         $attribute_free_id
 * @property-read string                                                 $created_at_format
 * @property-read string                                                 $updated_at_format
 * @property-read string                                                 $table
 * @property-read string                                                 $combo_identificator
 * @property-read string                                                 $value_identificator
 * @property-read string                                                 $created_at_short
 * @property-read string                                                 $updated_at_short
 * @property-read \Canela\CanelaTools\Models\AttributeFree\AttributeFree $attribute
 * @method static \Illuminate\Database\Eloquent\Builder|\Illuminate\Support\Collection comboListAttribute($area_id = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList whereAttributeFreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeList query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @mixin \Eloquent
 */
class AttributeFreeList extends BasicModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_free_list';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'attribute_free_id',
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'position'          => 'integer',
        'attribute_free_id' => 'integer',
        'name'              => 'string',
    ];

    /**
     * Related attribute.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(AttributeFree::class, 'attribute_free_id');
    }

    /**
     * Generic combo.
     *
     * @param Builder      $query
     * @param integer|null $attributeId
     * @return Collection
     */
    public function scopeComboListAttribute($query, $attributeId = null)
    {
        return empty($attributeId)
            ? $this->scopeComboList($query)
            : $query->where('attribute_free_id', $attributeId)
                    ->orderBy('position')
                    ->get()
                    ->pluck('name', 'id');
    }

}
