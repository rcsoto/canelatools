<?php

namespace Canela\CanelaTools\Requests\Configuration;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class TranslationRequest
 * @package Canela\CanelaTools\Requests\Configuration
 */
class TranslationRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'language_id' => 'required|integer|exists:language,id',
                'entity_id'   => 'required|integer|exists:entity,id',
                'register_id' => 'required|integer|min:1',
                'code'        => 'required|string|max:45',
                'value'       => 'required|string|max:65535',
            ];

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'language_id'   => trans('canelatools::canelatools.entity.translation.field.language_id'),
            'entity_id'     => trans('canelatools::canelatools.entity.translation.field.entity_id'),
            'register_id'   => trans('canelatools::canelatools.entity.translation.field.register_id'),
            'code'          => trans('canelatools::canelatools.entity.translation.field.code'),
            'value'         => trans('canelatools::canelatools.entity.translation.field.value'),
        ];
    }

}
