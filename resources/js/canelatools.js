
/******************************************************************
* Funciones generales
******************************************************************/

jQuery(document).ready(function(){

    const COMBO_AJAX_PAGINATION_SIZE = 30;

    /******************************************************************
     * EFECTO CARGA
     ******************************************************************/
    jQuery.loadIni = function () {
        jQuery('.loader-bg').fadeIn(300);
    };

    jQuery.loadEnd = function () {
        jQuery('.loader-bg').fadeOut(300);
    };

    /******************************************************************
     * AJAX
     ******************************************************************/
    jQuery.procesarRespuestaAjax = function(jqXHR, exception, errorThrown)
    {
        var msg = '';
        if (jqXHR.status === 200)
        {
            msg = jqXHR.message;
        }
        else if (jqXHR.status === 0)
        {
            msg = 'Not connect.\n Verify Network.';
        }
        else if (jqXHR.status == 401)
        {
            msg = 'Not authorized. [401]';
        }
        else if (jqXHR.status == 403)
        {
            msg = 'Forbidden. [403]';
        }
        else if (jqXHR.status == 404)
        {
            msg = 'Requested page not found. [404]';
        }
        else if (jqXHR.status == 422)
        {
            //msg = 'Unprocessable Entity [422].';
            //msg = JSON.stringify(jqXHR.responseText);
            var array = jQuery.procesarJson(jqXHR);
            msg = jQuery.getString(array);

        }
        else if (jqXHR.status == 500)
        {
            msg = 'Internal Server Error [500].';
        }
        else if (exception === 'parsererror')
        {
            msg = 'Requested JSON parse failed.';
        }
        else if (exception === 'timeout')
        {
            msg = 'Time out error.';
        }
        else if (exception === 'abort')
        {
            msg = 'Ajax request aborted.';
        }
        else
        {
            msg = 'Uncaught Error.\n' + jqXHR.status;
        }

        return msg;
    };


    /**
     * Process jqXHR to get JSON errors
     *
     * @param jqXHR
     */
    jQuery.procesarJson = function(jqXHR){
        var resultado = [];
        jQuery.map(jqXHR.responseJSON, function(value, index) {
			if (typeof value == 'object')
			{
				jQuery.map(value, function(value2, index2) {
					resultado.push(value2);
				});
			}
			else
			{
				//	resultado.push(value);
        	}
        });

        return resultado;
    };

    /**
     * Prettify the array to set it ready to be printed.
     *
     * @param array
     * @returns {string}
     */
    jQuery.getString = function(array){
        var salida = "";

        array.forEach(function(entry) {
            salida += entry + '<br>';
        });
        return salida;
    };



    /**
     * Determina si un cadena es un numero
     */
    jQuery.isNumber = function(n)
    {
    	return !isNaN(parseFloat(n)) && isFinite(n);
    };



    /******************************************************************
	 * Refresco Combo by Ajax
	******************************************************************/
    jQuery.refreshSelectAjax = function(urlCP, formdata, selectId, token, byDefault)
    {
	    jQuery.ajax({
			url: urlCP,
			dataType: "json",
			data: formdata,
			processData: false,
			contentType: false,
			type: "POST",
			beforeSend: function(){
			    // Handle the beforeSend event
				jQuery.loadIni();
			},
			error: function(jqXHR, exception, errorThrown)
			{
				//jQuery('.loader-overlay').addClass('loaded');
				jQuery.messageError("Se ha producido un error. Vuelva a intentar la operación. En caso que el error persista comunique con el equipo de soporte.");
			},
		    success: function(datos) {
				// vaciamos siempre el destino
				jQuery('#'+selectId).children().remove().empty(); //.detach().
			    // ocultar espera
		    	if (datos.result == 1)
			    {
					var vacios = [{'id':0, 'text':''}];
					var valores = vacios.concat(datos.items);
					jQuery("#"+selectId).select2({
						data: valores,
					});

					if (byDefault != null)
					{
						jQuery("#"+selectId).val(byDefault).trigger('change');
                    }

                    var selector = jQuery("#"+selectId);
                    if (selector.data('modal') !== undefined) {
                        selector.select2({
                            dropdownParent: $(selector.data('modal'))
                        })
                    }
			    }
		    	else
		    	{
			    	/* Mostrar mensaje de error */
		    		//jQuery.messageWarning(datos.msg, 3000);
				}
		    	jQuery.loadEnd();
			}
		});
    };


    /**
     * AJAX select2 combo.
     *
     * @param selector
     * @param uri
     * @param placeholder
     * @param params
     */
    jQuery.select2Ajax = function (selector, uri, placeholder, params) {
        for (let i in params) {
            uri += '&' + Object.keys(params[i]) + '=' + Object.values(params[i]);
        }

        $(selector).select2({
            placeholder: placeholder,
            multiple: !!$(selector).attr('multiple'),
            ajax: {
                url: uri,
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page: page,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    let more = (page * COMBO_AJAX_PAGINATION_SIZE) < data.total_count;
                    return {results: data.items, more: more};
                },
                cache: true
            },
            initSelection: function (element, callback) {
                let data = [];
                let val = element.val().split(',');
                //parse data to array if multiple
                if (val.length > 1) {
                    let names = element.data('name').split(',');
                    val.forEach(function (value, key) {
                        data.push({id: value, text: names[key]});
                    });
                } else {
                    data = {id: element.val(), text: element.data('name')};
                }
                callback(data);
            },
            allowClear: true
        });
    };

    /**
     * Select2 combo sin carga ajax.
     *
     * @param selector
     * @param uri
     * @param placeholder
     * @param params
     * @param byDefault
     */
    jQuery.select2SinAjax = function (selector, uri, placeholder, params, byDefault) {
        for (let i in params) {
            uri += '&' + Object.keys(params[i]) + '=' + Object.values(params[i]);
        }

        let select = jQuery(selector);
        jQuery.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            url: uri,
            dataType: "json",
            processData: false,
            contentType: false,
            type: "GET",
            beforeSend: function () {
                jQuery('.loader-overlay').removeClass('loaded');
                select.select2('destroy').trigger('change.select2');
            },
            error: function (objeto1, descr_error, objeto2) {
                jQuery('.loader-overlay').addClass('loaded');
            },
            success: function (datos) {
                jQuery('.loader-overlay').addClass('loaded');
                if (datos.result == 1) {
                    select.select2({
                        placeholder: placeholder,
                        multiple: !!$(selector).attr('multiple'),
                        data: datos.items
                    });

                    if (byDefault != null) {
                        select.select2('val', byDefault);
                    }
                    else {
                        select.select2('val', '');
                    }

                    select.trigger('change');
                }
                else {
                    //
                }
            }
        });
    };



    /******************************************************************
	 * Añadir un caracter N veces a la izquirda de un texto hasta completar una longitud
	******************************************************************/
    jQuery.pad = function(str, chr, max)
    {
    	  str = str.toString();
    	  return (str.length < max ? jQuery.pad(chr + str, max, chr) : str);
    };


    /******************************************************************
	 * Dada una fecha sumar/restar un numero de dias
	******************************************************************/
    jQuery.dateAddDays = function(fecha, n)
    {
    	var newdate = new Date(fecha);
    	newdate.setDate(newdate.getDate() + n);

    	var dd = newdate.getDate();
    	var mm = newdate.getMonth() + 1;
    	var y  = newdate.getFullYear();

    	return y +'-'+ jQuery.pad(mm, 0 , 2) + '-'+ jQuery.pad(dd, 0 , 2);
    };


    /******************************************************************
	 * Formatear fecha
	******************************************************************/
    jQuery.formatDate = function (date, separator='-', format='ymd', separator_from='-', format_from='ymd')
    {
        var d = date.split(separator_from);
        var day = d[format_from.indexOf('d')];
        var month = d[format_from.indexOf('m')];
        var year = d[format_from.indexOf('y')];

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        var result = '';
        switch (format) {
            case 'dmy':
                result = [day, month, year].join(separator)
                break;
            case 'mdy':
                result = [month, day, year].join(separator)
                break;
            case 'ymd':
                result = [year, month, day].join(separator)
        };
        return result;
    };


    /******************************************************************
	 * Formatear fecha ES
	******************************************************************/
    jQuery.formatDateEs = function (date, separator='-')
    {
        return jQuery.formatDate(date, separator, 'dmy');
    };


    /******************************************************************
	 * Formatear fecha/hora
	******************************************************************/
    jQuery.formatDateTime = function (datetime, separator='-', format='ymd', separator_from='-', format_from='ymd')
    {
        var d = datetime.split(' ');
        var time = d[1];
        return jQuery.formatDate(d[0], separator, format, separator_from, format_from) + ' ' + time;;
    };


    /******************************************************************
	 * Formatear fecha/hora ES
	******************************************************************/
    jQuery.formatDateTimeEs = function (datetime, separator='-')
    {
        return jQuery.formatDateTime(datetime, separator, 'dmy');
    };


    /******************************************************************
	 * Nombre del dia de la semana
	******************************************************************/
    jQuery.getAllNameDays = function()
    {
    	return ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
    };
    jQuery.getNameDay = function(index)
    {
		return jQuery.getAllNameDays()[index];
    };

    /******************************************************************
	 * Nombre del dia de la semana abreviatura
	******************************************************************/
    jQuery.getAllNameDaysAbr = function()
    {
    	return ['L', 'M', 'X', 'J', 'V', 'S', 'D'];
    }    ;
    jQuery.getNameDayAbr = function(index)
    {
    	return jQuery.getAllNameDaysAbr()[index];
    };

    /******************************************************************
	 * Nombre del mes
	******************************************************************/
    jQuery.getAllNameMonth = function()
    {
    	return ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    };
    jQuery.getNameMonth = function(index)
    {
		return jQuery.getAllNameMonth()[index];
    };


    /******************************************************************
	 * Formatear fecha
	******************************************************************/
    jQuery.formatDateHuman = function(date)
    {
        var d = new Date(date),
            month = d.getMonth(),
            day = d.getDate(),
            year = d.getFullYear(),
            dayWeek = d.getDay() - 1;
        if (dayWeek < 0)
        { 	dayWeek = 6;  }
        // sábado 28 abril 2018
        return jQuery.getNameDay(dayWeek)+" "+day+" "+jQuery.getNameMonth(month)+" "+year;
    };



    /**
     * Obtener valor de una cookie
     */
    jQuery.cookieGetValue = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) != -1) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };


    /**
     * funcion para grabar la cookie
     */
    jQuery.cookieSetValue = function (cname, value) {
        if (value == null) {
            value = 'yes';
        }

        var today = new Date();
        var expire = new Date();
        expire.setTime(today.getTime() + 3600000 * 24 * 3650);

        // grabar la cookie
        document.cookie = cname + "=" + value + "; expires=" + expire.toGMTString() + "; path=/;";

        return jQuery.cookieGetValue(cname);
    };

    /**
     * funcion para borrar la cookie
     */
    jQuery.cookieDelete = function(cname)
    {
        jQuery.removeCookie(cname);
    };


    /******************************************************************
     * UTILIDADES
     ******************************************************************/

    /**
     * Mezcla cada datos de un array "A" con los datos de un array "B"
     * parametros: a: array uno
     *             b: array dos
     *             sepItem: separador entre el dato del array "A" y el "B"
     *             setTubla: separador entre cada la union de "A" y "B" con la siguiente tupla
     */
    jQuery.mezclarArray = function (a, b, sepItem, sepTubla) {
        var resultado = "";
        var separadorItem = "";
        var separadorTubla = "";

        if (a.length > 0 && b.length > 0) {
            a.forEach(function (elementA) {
                b.forEach(function (elementB) {
                    resultado += separadorTubla + elementA + separadorItem + elementB;
                    separadorTubla = sepTubla;
                    separadorItem = sepItem;
                });
            });

        }
        else {
            var ary = [];
            if (a.length > 0) {
                ary = a;
            }
            else {
                ary = b
            }

            ary.forEach(function (element) {
                resultado += separadorTubla + element;
                separadorTubla = sepTubla;
            });
        }

        return resultado;
    };



    /**
     * Comprueba si un string es un email valido
     */
    jQuery.validateEmail = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };


    /**
     * String con separador de miles y decimales
     * parametros: number: numero a formatear
     *             numDec: número de decimales
     *             sepDecimal: Caracter a suar de separador de los decimales
     *             sepMiles: Caracter a usar de separador de los miles
     */
    jQuery.numberFormat = function (number, numDec, sepDecimal, sepMiles) {
        if (number == null || number.length == 0) {
            return "";
        }
        number = parseFloat(Math.round(number * 100) / 100).toFixed(numDec);

        var parts = number.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, sepMiles);
        return parts.join(sepDecimal);
    };


    /**
     * Devuelve un string formateado
     * parametros: format: texto a formatear
     *             arguments: argumentos opcionales, se han de pasar tantos como marcas esten en el texto
     *             sprintf("hola %s esto es un %", 'mundo', 'ejemplo') : hola mundo esto es un ejemplo
     */
    jQuery.sprintf = function( format )
    {
      for( var i=1; i < arguments.length; i++ ) {
        format = format.replace( /%s/, arguments[i] );
      }
      return format;
    }



    /******************************************************************
     * Save data from a Form
     ******************************************************************/
    jQuery.saveForm = function (selector_id, ok_callback, fail_callback) {
        let form = $(selector_id);
        let formData = new FormData();
        let method = form.find('input[name="_method"]');
        method = method.val() === undefined ? 'POST' : method.val();

        formData.append('_method', method);

        // Clean up error labels.
        form.find('input, select, textarea').each(function () {
            clearErrors($(this));
        });

        form.find('input:not([data-do-not-send])').each(function () {
            let input = $(this);
            if (input.prop('type') === 'file') {
                if (input.hasClass('canela-input-image-cropper')) {
                    const cropperImage = cropperImages[input.prop('id')];
                    if (cropperImage !== undefined) {
                        formData.append(input.prop('name'), cropperImage.data, cropperImage.fileName);
                    }
                } else {
                    for (var i = 0; i < input.prop('files').length; i++)
                    {
                        formData.append(input.prop('name'), input.prop('files')[i]);
                    }
                }
            } else if (input.prop('type') === 'radio') {
            	if (input[0].checked)
            	{
            		formData.append(input.prop('name'), input.val());
            	}
            } else if (input.prop('type') === "checkbox") {
                formData.append(input.prop('name'), input.prop('checked')? '1' : '0');
            } else if (input.hasClass('date')) {
                formData.append(input.prop('name'), jQuery.formatDate(input.val(), '-', 'ymd', '-', 'dmy'));
            } else if (input.hasClass('datetime')) {
                formData.append(input.prop('name'), jQuery.formatDateTime(input.val(), '-', 'ymd', '-', 'dmy'));
            } else if (input.prop('name') !== "") {
                formData.append(input.prop('name'), input.val());
            }
        });

        form.find('select:not([data-do-not-send])').each(function () {
            let select = $(this);
            if (!((select.val() == null || select.val().length <= 0) && select.data('nullable') !== true))
            {
                formData.append(select.prop('name'), select.val());
            }
        });

        form.find('textarea:not([data-do-not-send])').each(function () {
            const textarea = $(this);
            if (textarea.hasClass('classic-editor')) {
                const classicEditor = classicEditors[textarea.prop('id')];
                if (classicEditor !== undefined) {
                    formData.append(textarea.prop('name'), classicEditor.getData());
                }
            } else {
                formData.append(textarea.prop('name'), textarea.val());
            }
        });

        return $.ajax({
            headers: {'X-CSRF-TOKEN': form.find('input[name="_token"]:first').val()},
            url: form.attr('action'),
            dataType: "json",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: () => {
                // Handle the beforeSend event
                jQuery.loadIni();
                /*for (let value of formData) {
                    console.log(value[0], value[1]);
                }*/
            },
            error: function (jqXHR, exception, errorThrown) {
                jQuery.loadEnd();

                let msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                jQuery.messageWarning(msg);
                if (jqXHR.status === 422) {
                    Object.entries(jqXHR.responseJSON.errors).forEach(error => {
                        let key = error[0];
                        let input = form.find(`select[name="${key}"]`);
                        if (input.length === 0) {
                            input = form.find(`input[name="${key}"]`);
                        }
                        if (input.length === 0) {
                            input = form.find(`textarea[name="${key}"]`);
                        }

                        input.parent('div').append(printErrors(error[1]));
                    });
                }
            },
            success: function (data) {
                jQuery.loadEnd();
                if (data.result) {
                    let msg = data.message === undefined ? (data.msg === undefined ? null : data.msg) : data.message;
                    if (msg) {
                        jQuery.messageInfo(data.message);
                    }
                    if (typeof ok_callback === "function") {
                        ok_callback(data);
                    }
                } else {
                    // Mostrar mensaje de error
                    let msg = data.message === undefined ? (data.msg === undefined ? null : data.msg) : data.message;
                    if (msg) {
                        jQuery.messageWarning(msg, 3000);
                    }
                    if (typeof fail_callback === "function") {
                        fail_callback(data);
                    }
                }
            }
        });

        function printErrors(errors) {
            return `<p class="form-error text-danger">${errors.join('. \n')}</p>`;
        }

        function clearErrors(node) {
            node.parent('div').find('p.form-error').remove();
        }
    };


    /******************************************************************
     * Tabs
     ******************************************************************/
    if (location.hash) {
        jQuery('a[href=\'' + location.hash + '\']').tab('show');
    }
    let activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
    	jQuery('a[href="' + activeTab + '"]').tab('show');
    }
    jQuery('body').on('click', 'a[data-toggle=\'tab\']', function (e) {
        e.preventDefault();
        let tab_name = this.getAttribute('href');
        localStorage.setItem('activeTab', tab_name);
        $(this).tab('show');
        return false;
    });
    jQuery(window).on('popstate', function () {
        let anchor = location.hash ||
            $('a[data-toggle=\'tab\']').first().attr('href');
        jQuery('a[href=\'' + anchor + '\']').tab('show');
    });


    /**
     * This function is same as PHP's nl2br() with default parameters.
     *
     * @param {string} str Input text
     * @param {boolean} replaceMode Use replace instead of insert
     * @param {boolean} isXhtml Use XHTML
     * @return {string} Filtered text
     */
    jQuery.nl2br = function(str, replaceMode, isXhtml) {

    	var breakTag = (isXhtml) ? '<br />' : '<br>';
    	var replaceStr = (replaceMode) ? '$1'+ breakTag : '$1'+ breakTag +'$2';
    	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, replaceStr);
    }


    /**
     * This function inverses text from PHP's nl2br() with default parameters.
     *
     * @param {string} str Input text
     * @param {boolean} replaceMode Use replace instead of insert
     * @return {string} Filtered text
     */
    jQuery.br2nl = function(str, replaceMode) {

    	var replaceStr = (replaceMode) ? "\n" : '';
    	// Includes <br>, <BR>, <br />, </br>
    	return str.replace(/<\s*\/?br\s*[\/]?>/gi, replaceStr);
    }


    /**
     * Detect if is the device is mobile/tablet
     */
    jQuery.isMobileTablet = function() {
        var check = false;
        (function(a){
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
                check = true;
        })(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }


    /**
     * downloadFileWithProgress
     * Needs: http://danml.com/download.html
     * Method that download a file via ajax with progress loader.
     *
     * @param url String
     * @param filename String
     * @param mimetype String
     * @return void
     */
    jQuery.downloadFileWithProgress = function (url, filename, mimetype) {
        var oReq = new XMLHttpRequest();
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";

        oReq.onload = function(oEvent) {
            downloadjs(oReq.response, filename, mimetype);
            jQuery.loadEnd();
        };

        jQuery.loadIni();
        oReq.send();
    };


    /**
     * isValidJSONString
     *
     * Method that check if the str string has correct JSON format.
     *
     * @return boolean
     */
    jQuery.isValidJSONString = function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }


    /**
     *
     * Jasny Bootstrap Fileinput.
     *
     */
    jQuery('.fileinput').each(function () {
        var oldThumbnail = jQuery(this).find('div.fileinput-new.thumbnail');
        var removeBtnOldThumbnail = jQuery(this).find('a.btn-remove-old-img');
        var removeImageInput = jQuery(this).find('input[type="hidden"][name="delete_image"]');
        var fileInput = jQuery('.fileinput').fileinput();

        fileInput.on('change.bs.fileinput', function (event) {
            removeBtnOldThumbnail.remove();
            removeImageInput.val(0);
        });
        fileInput.on('clear.bs.fileinput', function (event) {
            oldThumbnail.remove();
            removeBtnOldThumbnail.remove();
            removeImageInput.val(1);
        });
    });


    /**
     *
     * valor nulo (alternative ??)
     *
     */
    jQuery.valueOrNull = function(value) {
        return (value == null || value == undefined) ? '' : value;
    }

    /**************************************************/
    /** Función para descargar una url dinámicamente **/
    /**************************************************/
    jQuery.downloadFileFromLink = function (url, filename) {
        const linkId = 'download-file-from-here';
        jQuery('body').append('<a id="'+linkId+'" download="'+filename+'" href="'+url+'"></a>');
        const link = jQuery('#'+linkId);
        link.get(0).click();
        link.remove();
    }


    /**
     * Returns a 'size' size random string.
     * @param {int} size
     * @returns string
     */
     jQuery.generateRandomString = function (size = 10) {
        const values = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        let result = "";
        const min = 0;
        const max = values.length;
        for (let i = 0; i < size; i++)
            result += values[Math.floor(Math.random() * (max - min)) + min];

        return result;
    }


    /**
     * Funcion equivalente a htmlspecialchars de php
     */
    jQuery.htmlspecialchars = function(text, valuedefault) {
        if (text === undefined || text == null || text.length == 0) {
            return valuedefault;
        }
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }


    /**
     * Stores form current fields values into local storage
     * by key and go to url.
     * @param {string} formId
     * @param {string} key
     * @param {string} url
     */
     jQuery.storeDataLocallyAndGoToUrl = function (formId, key, url) {
        console.log('#'+formId, key, url)
        const fields = jQuery('#'+formId).serializeArray();

        // Remove _method and _token fields.
        const methodIndex = fields.findIndex(item => item.name === '_method');
        if (methodIndex > -1) {
            fields.splice(methodIndex, 1);
        }
        const tokenIndex = fields.findIndex(item => item.name === '_token');
        if (tokenIndex > -1) {
            fields.splice(tokenIndex, 1);
        }
        console.log(fields)

        // serializeArray() doesn't work with WYSIWYGs.
        jQuery('#'+formId+' textarea.classic-editor').each(function() {
            const textarea = $(this);
            console.log(textarea)
            const classicEditor = classicEditors[textarea.prop('id')];
            if (classicEditor !== undefined) {
                const fieldIndex = fields.findIndex(item => item.name === textarea.prop('name'));
                if (fieldIndex > -1) {
                    fields[fieldIndex].value = classicEditor.getData();
                }
            }
        });

        const hash = jQuery.generateRandomString(20);
        const data = {
            hash: hash,
            fields: fields
        }
        console.log(data)
        console.log(JSON.stringify(data))
        localStorage.setItem(key, JSON.stringify(data))

        console.log(url)
        let urlObject = new URL(url);
        let urlBackObject = new URL(urlObject.searchParams.get('url_back'));
        urlBackObject.searchParams.set('h', hash);
        urlObject.searchParams.set('url_back', urlBackObject.toString());
        urlObject.searchParams.set('h', hash);
        console.log(urlObject.toString())
        window.location.href = urlObject.toString();
    }


    /**
     * Update local stored field value.
     * @param {string} key
     * @param {string} hash
     * @param {string} fieldName
     * @param {any} fieldValue
     */
    jQuery.updateStoredData = function (key, hash, fieldName, fieldValue) {
        const storedDataString = localStorage.getItem(key);
        if (storedDataString !== null) {
            const storedData = JSON.parse(storedDataString);
            if (storedData.hash === hash) {
                const index = storedData.fields.findIndex(item => item.name === fieldName);
                if (index > -1) {
                    storedData.fields[index].value = fieldValue;
                    localStorage.setItem(key, JSON.stringify(storedData));
                }
            }
        }
    }


    /**
     * Set form fields with local stored values.
     * @param {string} formId
     * @param {string} key
     * @param {string} hash
     */
    jQuery.getStoredLocalData = function (formId, key, hash) {
        const storedDataString = localStorage.getItem(key);
        if (storedDataString !== null) {
            const storedData = JSON.parse(storedDataString);
            if (storedData.hash === hash) {
                // console.log('coinciden')
                storedData.fields.forEach((storedField) => {
                    console.log(storedField)
                    const formField = jQuery('#'+formId+' [name="'+storedField.name+'"]');
                    console.log(formField)
                    if (formField.length === 1) {

                        if (formField.is('input')) {
                            if (formField.hasClass('tagsinput')) {          // TAGSINPUTS
                                console.log('tagsinput')
                                storedField.value.split(',').forEach(function (item) {
                                    formField.tagsinput('add', item);
                                });
                            } else {                                        // INPUT
                                formField.val(storedField.value);
                            }
                        } else if (formField.is('textarea')) {
                            if (formField.hasClass('classic-editor')) {     // WYSIWYG
                                const classicEditor = classicEditors[formField.prop('id')];
                                if (classicEditor !== undefined) {
                                    classicEditor.setData(storedField.value);
                                }
                            } else {                                        // TEXTAREA
                                formField.val(storedField.value);
                            }
                        } else if (formField.is('select')) {                // SELECT2
                            formField.val(storedField.value);
                            formField.trigger('change');
                        }

                        console.log('cambiado')
                    }
                });
            } else {
                // Values from other old request. Remove them.
                localStorage.removeItem(key);
                // console.log('NO COINCIDEN. BORRADO')
            }
        }
    }

});
