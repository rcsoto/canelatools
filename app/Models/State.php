<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\State
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property \Illuminate\Support\Carbon|null        $created_at
 * @property \Illuminate\Support\Carbon|null        $updated_at
 * @property-read string                            $created_at_format
 * @property-read string                            $updated_at_format
 * @property-read string                            $value_identificator
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_short
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\State query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class State extends ModelCatalog
{

    /**
     *
     * Constantes de los estados
     */
    const ACTIVO = 1;
    const BAJA   = 2;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}