//Add strings
gdprCookieNoticeLocales.es = {
  description: 'Utilizamos cookies para ofrecerle una mejor experiencia de navegación, personalizar contenido y anuncios, proporcionar funciones de redes sociales y analizar nuestro tráfico. Lea sobre cómo usamos las cookies y cómo puede controlarlas haciendo clic en Configuración de cookies.',
  settings: 'Configuración de cookies',
  accept: 'Aceptar cookies',
  statement: 'Política de cookies',
  save: 'Guardar configuración',
  always_on: 'Siempre activas',
  cookie_essential_title: 'Cookies esenciales del sitio',
  cookie_essential_desc: 'Las cookies esenciales ayudan a la usabilidad de un sitio web al habilitar funciones básicas como la navegación de páginas y el acceso a áreas seguras del sitio web. El sitio web no puede funcionar correctamente sin estas cookies.',
  cookie_performance_title: 'Cookies de rendimiento',
  cookie_performance_desc: 'Estas cookies se utilizan para mejorar el rendimiento y la funcionalidad de nuestros sitios web, pero no son esenciales para su uso. Por ejemplo, almacenar su idioma preferido o la región en la que se encuentra.',
  cookie_analytics_title: 'Cookies de analítica web',
  cookie_analytics_desc: 'Utilizamos cookies de análisis para ayudarnos a medir cómo interactúan los usuarios con el contenido del sitio web, lo que nos ayuda a personalizar nuestros sitios web y aplicaciones para usted con el fin de mejorar su experiencia.',
  cookie_marketing_title: 'Cookies de marketing',
  cookie_marketing_desc: 'Estas cookies se utilizan para hacer que los mensajes publicitarios sean más relevantes para usted y sus intereses. La intención es mostrar anuncios que sean relevantes y atractivos para el usuario individual y, por lo tanto, más valiosos para los editores y anunciantes externos.'
}
