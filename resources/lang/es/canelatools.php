<?php

/***
 * IMPORTANTE
 * ESTE ARCHIVO NO SE DEBE DE MODIFICAR EN LOS PROYECTOS
 * ES MANTENIDO Y MODIFICADO EN "CANELATOOLS"
 *
 */


return [

    /*
     |--------------------------------------------------------------------------
     | Etiquetas para el mantinimiento de Tablas
     |--------------------------------------------------------------------------
     |
     | canela-rest
     |
     */

    'canela-rest' => [
        'table' => [
            'errors' => [
                'generic' => 'Se ha producido un error. Vuelva a intentar la operación. En caso que el error persista comunique con el equipo de soporte.',
            ],
            'excel' => 'Descargar tabla en Excel',
            'filter' => [
                'clear'           => 'Limpiar filtros',
                'contains'        => 'Contiene',
                'equal'           => 'Igual',
                'higher'          => 'Mayor',
                'higher_equal'    => 'Mayor o igual',
                'less'            => 'Menor',
                'less_equal'      => 'Menor o igual',
                'distinct'         => 'Distinto',
                'no_contains'     => 'No contiene',
                'empty'           => 'Vacío',
                'no_empty'        => 'No vacío',
            ],
            'register' => [
                'registers'       => 'Registros: ',
                'page'            => ' - Página ',
                'of'              => ' de ',
            ],
        ],

        'list' => [
            'btn' => [
                'new' => 'Nuev@ :model',
            ],
        ],

        'form'  => [
            'title' => [
                'edit'  => 'Editar :model',
                'new'   => 'Nuev@ :model',
            ],
            'label' => [
                'general' => 'General',
                'associated-horaries' => 'Fechas',
                'associated-archive' => 'Archivo asociado',
                'associated-archives' => 'Archivos asociados',
                'associated-sections' => 'Secciones',
            ],
            'btn' => [
                'save'   => 'Guardar',
                'cancel' => 'Cancelar',
            ],
        ],

        'label' => [
            'question_delete_file'              => '¿Quieres eliminar el fichero?',
            'question_delete_image'             => '¿Quieres eliminar la imagen?',
            'question_delete_translation_file'  => '¿Quieres eliminar la traducción del archivo?',
            'question_unlink_archive'           => '¿Quieres quitar el archivo?',
            'file_deleted_successfully'         => 'Fichero eliminado correctamente',
            'image_deleted_successfully'        => 'Imagen eliminada correctamente',
        ],
    ],



    /*
     |--------------------------------------------------------------------------
     | Etiquetas generales
     |--------------------------------------------------------------------------
     |
     | canela
     |
     */

    // GENERAL
    'general' => [
        'acciones' => 'Acciones',

        'message'  => [
            'send_correct' => 'Mensaje enviado correctamente',
            'send_error'   => 'Error al enviar mensaje',
        ],

        'field'    => [
            'created_at'    => 'Fecha de creación',
            'updated_at'    => 'Última fecha de actualización',
            'id'            => 'Id',
            'id_n'          => 'Nº',
            'email'         => 'Correo electrónico',
            'state'         => 'Estado',
            'select'        => 'Selecciona un valor ...',
            'position'      => 'Orden de visualización',
            'image'         => 'Imagen',
            'icon'          => 'Icono',
        ],
        'actions'  => [
            'exit'          => 'Salir',
            'logout'        => 'Cerrar sesión',
            'edit'          => 'Editar',
            'select'        => 'Consultar',
            'save'          => 'Guardar',
            'disabled'      => 'Baja lógica',
            'enabled'       => 'Restaurar',
            'send'          => 'Enviar',
        ],
        'label'    => [
            'attention'     => '¡Atención!',
            'information'   => 'Información',
            'success'       => 'Petición procesada correctamente',
            'trace_new'     => 'Alta de :model_name',
            'trace_update'  => 'Edición de :model_name',
            'date'          => 'Fecha de alta',
            'last_date'     => 'Fecha última modificacion',
            'loading'       => 'Cargando ...',
            'select'        => 'Seleccionar',
            'select_items_and_click_continue' => 'Selecciona los elementos y pulsa continuar.',
            'cookies-policy' => 'Política de Cookies',
            'legal-warning' => 'Aviso legal',
            'data-protection' => 'Protección de datos',
            'privacy-policy' => 'Politica de privacidad',
            'read_more' => ' ...leer más',
            'accept'        => 'Aceptar',
            'accept-privacy-policy' => 'Aceptar Política de Privacidad',
            'have-read-and-accept-privacy-policy' => 'He leído y acepto la <a href=":route" target="_blank">Política de Privacidad</a>',
            'must-accept-privacy-policy' => 'Debe aceptar la Política de Privacidad.',
            'query' => 'Consulta',
            'add-new' => 'Añadir nuev@',
            'system-administration' => 'Administración sistema',
        ],
    ],


    // Auntentificacion
    'auth'    => [
        'email' => [
            'change_password' => 'Cambiar Contraseña',
            'back_login'      => 'Volver al login',
            'send_reminder'   => 'Enviar recordatorio',
        ],
        'login' => [
            'init'               => 'Inicio de sesión',
            'remember'           => 'Recordar',
            'forgotten_password' => '¿Has olvidado la contraseña?',
            'enter'              => 'ENTRAR',
        ],
    ],


    // MENU
    'menu'    => [
        'home'              => 'Inicio',
        'configuration'     => 'Configuración',
        'users'             => 'Usuarios',
        'groupuser'         => 'Grupos de usuarios',
        'country'           => 'Países',
        'province'          => 'Provicias',
        'city'              => 'Ciudades',
        'histoty_log'       => 'Histórico de operaciones',
        'catalog'           => 'Catálogos',
        'originopportunity' => 'Origen de Oportunidad',
        'leadqualification' => 'Lead Calificación',
        'platform'          => 'Plataforma',
        'typequery'         => 'Tipo de Consulta',
        'contact'           => 'Contactos',
        'translation'       => 'Traducciones',
        'parameter'         => 'Parámetros',
        'language'          => 'Idiomas',
        'sentenceSql'       => 'Sentencias SQL',
    ],


    // Translations
    'translations' => [
        'actions' => [
            'translate'     => 'Traducir',
        ],
        'labels' => [
            'original'      => 'Original',
            'translation'   => 'Traducción',
        ],
    ],


    /*
     |--------------------------------------------------------------------------
     | Etiquetas generales
     |--------------------------------------------------------------------------
     |
     | general
     |
     */

    'bool'       => [
        'yes' => 'Si',
        'no'  => 'No',
    ],
    'ajax'       => [
        'unsupported' => 'Operación no soportada',
    ],
    'model'      => [
        'list'               => ['correct' => 'Registros listados correctamente.',
                                 'error'   => 'Se ha producido un error al listar registros.',],
        'update'             => ['correct' => ':value actualizado correctamente.',
                                 'error'   => 'Se ha producido un error al actualizar el registro',],
        'store'              => ['correct' => ':value creado correctamente.',
                                 'error'   => 'Se ha producido un error al crear el registro',],
        'find'               => ['error' => 'Registro no encontrado',],
        'delete'             => ['correct' => ':value eliminado correctamente.',
                                 'error'   => 'Se ha producido un error al eliminar el registro',],
        'copy'               => ['correct' => ':value duplicado correctamente.',
                                 'error'   => 'Se ha producido un error al duplicar el registro',],
        'alignment'          => ['center' => 'Centrado',
                                 'left'   => 'Izquierda',
                                 'right'  => 'Derecha',],
        'vertical_alignment' => ['medium' => 'Medio',
                                 'bottom' => 'Inferior/normal',
                                 'top'    => 'Superíndice',],
        'reorder'            => ['reorder_ok'    => 'Se ha reordenado correctamente.',
                                 'reorder_error' => 'Ha habido un problema al reordenar.',],
        'update_status'      => ['correct' => 'Se ha modificado el estado correctamente.',
                                 'error'   => 'Se ha encontrado un problema al actualizar el estado.',],
        'fusion'             => ['correct' => 'Se han fusionado los registros correctamente.',
                                 'error'   => 'Se ha encontrado un problema al fusionar registros.',],
        'generic_error'      => 'Se ha producido un error. Vuelva a intentar la operación. En caso que el error persista comunique con el equipo de soporte.',
    ],
    'file'       => [
        'upload_ok'    => 'Fichero Subido correctamente.',
        'upload_error' => 'Ha ocurrido un error al subir el fichero.',
    ],
    'buttons'    => [
        'accept'        => 'Aceptar',
        'approve'       => 'Aprobar',
        'back'          => 'Volver',
        'cancel'        => 'Cancelar',
        'continue'      => 'Continuar',
        'close'         => 'Cerrar',
        'copy'          => 'Copiar',
        'delete'        => 'Eliminar',
        'edit'          => 'Editar',
        'new'           => 'Nuevo',
        'preview'       => 'Previsualizar',
        'refuse'        => 'Rechazar',
        'save'          => 'Guardar',
        'send'          => 'Enviar',
        'show'          => 'Consulta',
        'update'        => 'Actualizar',
        'activate'      => 'Activar',
        'deactivate'    => 'Baja',
        'disable'       => 'Deshabilitar',
        'execute'       => 'Ejecutar',
        'upload'        => 'Subir',
        'download'      => 'Descargar',
        'share'         => 'Compartir',
        'prev'          => 'Anterior',
        'next'          => 'Siguiente',
    ],
    'cropper'    => [
        'restrictions' => [
            'restrictions' => 'Restricciones',
            'min_height'   => 'Altura mínima: :h px',
            'min_width'    => 'Anchura mínima :w px',
            'exact_height' => 'El alto de la imagen ha de ser :h px',
            'exact_width'  => 'El ancho alto de la imagen ha de ser :w px',
            'ratio'        => 'El ratio de la imagen ha de ser :ratio',
        ],
        'view'         => [
            'select_img'    => 'Subir imagen',
            'upload'        => 'Guardar',
            'height'        => 'Altura',
            'width'         => 'Anchura',
            'rotate'        => 'Rotación',
            'format'        => 'Formato del selector',
            'ratio_free'    => 'libre',
            'modify_image'  => 'Modificar imagen',
            'select_image'    => 'Seleccionar imagen',
        ],
        'buttons'   => [
            'modify'        => 'Modificar',
        ],
    ],
    'select'     => [
        'placeholder' => 'Selecciona un valor ...',
    ],


    /*
     |--------------------------------------------------------------------------
     | Page ERRORS
     |--------------------------------------------------------------------------
     */
    '401' => [
        'title' => 'No autorizado',
        'message' => 'No está autorizado para acceder a este registro.',
    ],
    '403' => [
        'title' => 'Sin permisos',
        'message' => 'No tiene permiso para acceder a este registro.',
    ],
    '404' => [
        'title' => 'Página no encontrada',
        'message' => 'Parece que esta página no existe o ha sido eliminada.',
    ],
    '419' => [
        'title' => 'Página caducada',
        'message' => 'Página caducada. Tus credenciales han expirado.',
    ],
    '429' => [
        'title' => 'Demasiadas solicitudes.',
        'message' => 'Demasiadas solicitudes.',
    ],
    '500' => [
        'title' => 'Error/Incidencia',
        'message' => 'Se ha produccido un error en el servidor. Vuelve a intentarlo. Si el error persiste, por favor, ponte en contacto con el equipo de soporte.',
    ],
    '503' => [
        'title' => 'Mantenimiento',
        'message' => 'Estamos de mantenimiento. Volveremos en unos minutos. Gracias.',
    ],
    '550' => [
        'title' => 'Error/Incidencia',
        'message' => 'Se ha produccido un error en el servidor. Vuelve a intentarlo. Si el error persiste, por favor, ponte en contacto con el equipo de soporte.',
    ],

    /*
     |--------------------------------------------------------------------------
     | Update Browser
     |--------------------------------------------------------------------------
     */

    'updateBrowser' => [
        'title'                  => ':project Navegador obsoleto',
        'obsolete_browser'       => 'Si estás viendo esta página es porque tu navegador está obsoleto',
        'update_version_browser' => 'Por tu seguridad y para ver las páginas web modernas con todo su potencial, te recomendamos que te actualices a la última versión del navegador que prefieras.',
        'important'              => 'Importante',
        'browser_funcionality'   => 'Comprueba si está activada en tu navegador la funcionalidad',
        'compability_view'       => 'Vista de compatibilidad',
        'disable'                => 'si es así, has de desactivarlo para poder ver la página.',
        'more_information'       => 'Más información aquí',
        'google_chrome'          => 'Google Chrome',
        'firefox'                => 'Firefox',
        'explorer'               => 'Microsoft Edge',
        'update_visit'           => 'Después de actualizarte, ¡no olvides volver a visitarnos!',
        'rights_reserved'        => 'TODOS LOS DERECHOS RESERVADOS',
        'show_image'             => 'Mostrar imagen',
        'url_chrome'             => 'https://www.google.com/intl/es/chrome/browser/?hl=es',
        'url_firefox'            => 'http://www.mozilla.org/es-ES/firefox/fx/',
        'url_edge'               => 'https://support.microsoft.com/es-es/help/4501095/download-the-new-microsoft-edge-based-on-chromium',
    ],



    /*
     |--------------------------------------------------------------------------
     | Update Browser
     |--------------------------------------------------------------------------
     |
     | model
     |
     */

    'tables_assoc_count'           => '{1} (:value asociado)|[2,*] (:value asociados)',
    'delete_record'                => 'Eliminar registro',
    'delete_record_question'       => '¿Desea eliminar el registro?',
    'recorded_dependencies'        => 'El registro tiene dependencias creadas',
    'recorded_dependencies_tables' => 'El registro tiene dependencias de las siguientes tablas',
    'delete_recorded_dependencies' => '¿Desea eliminar los registros asociados también?',
    'delete_anyway'                => 'Eliminar de todas formas',
    'update_status'                => 'Cambio de Estado',
    'update_status_question'       => '¿Confirma el cambio de estado?',
    'update_status_dynamic'        => "¿Confirma <span class=\"status-js\"></span> el registro?",
    'comment_not_empty'            => 'Has de indicar un motivo.',


    // ENTIDADES
    'entity'  => [

        // TRACE
        'trace'       => [
            'list'   => 'Cambios de estado',
            'report' => 'Cambio de estado',
            'field'  => [
                'date'         => 'Fecha de cambio de estado',
                'state_new_id' => 'Estado nuevo',
                'state_old_id' => 'Estado anterior',
                'user_id'      => 'Usuario',
                'comment'      => 'Comentario',
            ],
        ],


        // HISTORICO DE OPERACIONES
        'history_log' => [
            'list'   => 'Histórico de operaciones',
            'report' => 'Histórico de operación',
            'field'  => [
                'module_id'   => 'Módulo',
                'entity_id'   => 'Entidad',
                'action_id'   => 'Acción',
                'user_id'     => 'Usuario',
                'register_id' => 'Registro ID',
                'register'    => 'Registro',
                'value_pre'   => 'Valor previo',
                'value_post'  => 'Valor posterior',
                'comment'     => 'Comentario',
            ],
            'label'  => [
                'basic_data' => 'Datos básicos',
                'values'     => 'Valores',
            ],
        ],

        //Configuration
        'configuration' => [
            'list'   => 'Configuración',
            'report' => 'Configuración',
            'field'  => [
                'name'          => 'Denominación',
                'reference'     => 'Código',
                'description'   => 'Descripción',
                'value'         => 'Valor',
            ],
        ],

        //Parameter
        'parameter' => [
            'list'   => 'Parámetros',
            'report' => 'Parámetro',
            'field'  => [
                'name'          => 'Denominación',
                'reference'     => 'Código',
                'description'   => 'Descripción',
                'value'         => 'Valor',
            ],
        ],


        //Traducciones
        'translation' => [
            'list'   => 'Traducciones',
            'report' => 'Traducción',
            'field'  => [
                'language_id'       => 'Idioma',
                'entity_id'         => 'Entidad',
                'register_id'       => 'Registro',
                'entity'            => 'Entidad (texto)',
                'code'              => 'Campo',
                'value'             => 'Valor',
                'translation'       => 'Traducción',
                'archive_type_id'   => 'Tipo de archivo',
            ],
        ],


        // user
        'user'     => [
            'list'   => 'Usuarios',
            'new'    => 'Nuevo Usuario',
            'report' => 'Usuario',
            'field'  => [
                'user'              => 'Usuario',
                'login'             => 'Usuario',
                'name'              => 'Nombre',
                'email'             => 'Correo',
                'state_id'          => 'Estado',
                'profile_id'        => 'Perfil',
                'client_id'         => 'Empresa',
                'token'             => 'Token',
                'email_verified_at' => 'Fecha verificación de email',
            ],
        ],

        //GroupUser
        'groupuser' => [
            'list'   => 'Grupos de Usuarios',
            'new'    => 'Nuevo Grupo de Usuario',
            'report' => 'Grupos de Usuarios',
            'field' => [
                'name'          => 'Nombre',
                'description'   => 'Descripción',
                'users'          => 'Usuarios',
            ]
        ],

        // Language
        'language'  => [
            'list'   => 'Idiomas',
            'report' => 'Idioma',
            'field'  => [
                'name'      => 'Denominación',
                'code'      => 'Código',
                'code2'     => 'Codigo 2',
                'reference' => 'Código 3',
                'position'  => 'Order visualización',
                'state_id'  => 'Estado',
            ],
        ],

        // Pais
        'country'  => [
            'list'   => 'Países',
            'report' => 'País',
            'field'  => [
                'country'     => 'País',
                'countries'   => 'Paises',
                'name'        => 'Nombre',
                'code2'       => 'ISO 3166-1 alfa-2',
                'code3'       => 'ISO 3166-1 alfa-3',
                'code_number' => 'ISO 3166 numérico',
            ],
        ],

        // Provincia
        'province' => [
            'list'   => 'Provincias',
            'report' => 'Provincia',
            'field'  => [
                'province'   => 'Provincia',
                'provinces'  => 'Provincias',
                'country_id' => 'País',
                'name'       => 'Nombre',
                'code'       => 'Código estándar',
            ],
        ],

        // ciudades
        'city'     => [
            'list'   => 'Ciudades',
            'report' => 'Ciudad',
            'field'  => [
                'city'        => 'Ciudad',
                'cities'      => 'Ciudades',
                'country_id'  => 'País',
                'province_id' => 'Provincia',
                'name'        => 'Nombre',
                'code'        => 'Código',
            ],
        ],

        // client
        'client'     => [
            'list'   => 'Clientes',
            'report' => 'Cliente',
            'field'  => [
                'name'              => 'Denominación',
                'contact_name'      => 'Nombre de contacto',
                'cif'               => 'NIF/CIF',
                'full_address'      => 'Dirección postal completa',
                'contact_telephone' => 'Teléfono de contacto',
                'image_logo'        => 'Logo',
            ],
        ],

        // Delegación
        'customer'     => [
            'list'   => 'Delegaciones',
            'report' => 'Delegación',
            'field'  => [
                'client_id'     => 'Cliente',
                'web_id'        => 'Web asociada',
                'name'          => 'Delegación/Franquicia',
                'business_name' => 'Razón social',
                'image_logo'    => 'Logo',
                'image_logo_dark' => 'Logo para fondo oscuro',
                'map_latitude'  => 'Coordenada mapa latitud (latitude)',
                'map_longitude' => 'Coordenada mapa longitud (longitude)',
            ],
        ],

        // Web
        'web'     => [
            'list'   => 'Webs',
            'report' => 'Web',
            'field'  => [
                'template_id'       => 'Plantilla',
                'text_avise_legal'  => 'Texto aviso legal',
            ],
        ],

        // Web Template
        'web_template'     => [
            'list'   => 'Plantillas',
            'report' => 'Plantilla',
            'field'  => [
                'code'          => 'Código',
                'name'          => 'Nombre',
                'onepage'       => 'One page',
                'description'   => 'Descripción',
            ],
        ],

        // Web Page
        'web_page'     => [
            'list'   => 'Páginas',
            'report' => 'Página',
            'field'  => [
                'type_id'           => 'Tipo',
                'show_header'       => 'Mostrar en el menú superior',
                'show_footer'       => 'Mostrar en el pié de página',
                'show_home'         => 'Mostrar en la pàgina de inicio',
                'slug'              => 'Slug - URL amigable',
                'title'             => 'Título',
                'subtitle'          => 'Subtítulo',
                'body'              => 'Body',
                'file_header_id'    => 'Fichero de la cabecera',
                'seo_title'         => 'Meta title (SEO)',
                'seo_keywords'      => 'Meta keywords (SEO)',
                'seo_description'   => 'Meta description (SEO)',
                'tags'              => 'Etiquetas',
                'external_link'     => 'Url externa',
                'open_in_new_window'=> 'Abrir en una ventana nueva',
                'is_external_link'  => 'Indica que es un enlace a una página externa',
            ],
        ],


        // Web Page FAQ
        'web_page_faq'     => [
            'list'   => 'Preguntas frecuentes',
            'report' => 'Pregunta frecuente',
            'field'  => [
                'page_id'  => 'Página Web',
                'name'     => 'Denominación',
                'description'  => 'Descripción',
                'external_link'     => 'Url externa',
                'open_in_new_window'=> 'Abrir en una ventana nueva',
            ],
        ],

        // Web Page Contact
        'web_page_contact'     => [
            'list'   => 'Página de contactos',
            'report' => 'Contacto',
            'field'  => [
                'page_id'       => 'Página Web',
                'type_id'       => 'Tipo de contacto',
                'concept'       => 'Concepto',
                'value'         => 'Valor',
            ],
        ],

        // Web Page Contact Type
        'web_page_contact_type' => [
            'list'   => 'Tipos de contacto',
            'report' => 'Tipo de contacto',
            'field'  => [
                'contact'       => 'Contacto',
            ],
        ],

        // Web Page Section
        'web_page_section' => [
            'list' => 'Secciones',
            'report' => 'Sección',
            'field'  => [
                'page_id'   => 'Página Web',
                'title'     => 'Título',
                'subject'   => 'Asunto',
                'body'      => 'Cuerpo',
                'slug'              => 'Slug - URL amigable',
                'seo_title'         => 'Meta title (SEO)',
                'seo_description'   => 'Meta description (SEO)',
                'file_header_id'  => 'Imagen para mostrar en la cabecera de la página',
                'file_thumb_id'   => 'Imagen para mostrar en la galería de secciones',
                'external_link' => 'Url externa',
                'open_in_new_window'=> 'Abrir en una ventana nueva',
                'categories' => 'Categorías asociadas',
            ],
        ],

        // Web Page Notice
        'web_page_notice' => [
            'list' => 'Noticias',
            'report' => 'Noticia',
            'field'  => [
                'page_id'   => 'Página Web',
                'title'     => 'Noticia',
                'subject'   => 'Asunto',
                'author'    => 'Autor/autores de la publicación',
                'date'      => 'Fecha de la publicación',
                'body'      => 'Cuerpo de la publicación',
                'seo_title'         => 'Meta title (SEO)',
                'seo_description'   => 'Meta description (SEO)',
                'file_thumb_id'     => 'Imagen para mostrar en la galería de noticias',
                'tags'              => 'Etiquetas',
                'external_link'     => 'Url externa',
                'open_in_new_window'=> 'Abrir en una ventana nueva',
                'coming_soon'       => 'Proximamente',
                'important'         => 'Destacado',
                'is_live'           => 'En directo',
                'file_thumbnail_id' => 'Imagen en miniatura',
                'type_id'           => 'Tipo',
                'category'          => 'Categorías asociadas',
                'slug'              => 'Slug - URL amigable',
            ],
        ],

        // Web Page Notice Archive
        'web_page_notice_archive' => [
            'list' => 'Noticias - Archivos',
            'report' => 'Noticia Archivo',
            'field'  => [
                'notice_id' => 'Noticia',
                'title'     => 'Título',
                'type_id'   => 'Tipo de archivo asociado',
                'archive'           => 'Archivo asociado',
                'duration'  => 'Duración del archivo (si procede)',
                'position' => 'Orden de visualización',
            ],
        ],

        // Web Page Section
        'web_page_category' => [
            'list' => 'Categorías asociadas a la página',
            'report' => 'Categoría de página',
            'field'  => [
                'page_id'   => 'Página Web',
                'name'     => 'Categoría',
                'abbreviation'  => 'Abreviatura',
                'description'   => 'Descripción larga',
                'color_text'    => 'Color de texto dentro de la pastilla',
                'color_border'  => 'Color del borde de la pastilla',
                'color_bg'      => 'Color de fondo de la pastilla',
            ],
        ],



        // Web Page Notice Horary
        'web_page_notice_horary' => [
            'list' => 'Fechas Noticias',
            'report' => 'Fecha Noticia',
            'field'  => [
                'page_id'   => 'Página Web',
                'title'     => 'Noticia/Evento',
                'notice_id' => 'Noticia/Evento',
                'date_from' => 'Fecha inicio',
                'date_to'   => 'Fecha fin',
                'time_from' => 'Hora inicio',
                'time_to'   => 'Hora fin',
                'comment'   => 'Comentarios',
            ],
            'labels' => [
                'date_from_to' => 'Del :from al :to',
                'date_from_to_eqal_year_distint_month' => 'Del :day de :month al :to',
            ],
        ],

        // Web Page Data
        'web_page_data'     => [
            'list'   => 'Datos / Parámetros',
            'report' => 'Dato / Parámetro',
            'field'  => [
                'page_id'   => 'Página Web',
                'date'      => 'Fecha',
                'name'      => 'Denominación',
                'description'     => 'Descripción',
                'value'     => 'Valor',
                'prefix'    => 'Prefijo',
                'suffix'    => 'Sufijo',
            ],
        ],

        // rrss
        'rrss'     => [
            'list'   => 'RRSS',
            'report' => 'RRSS',
            'field'  => [
                'name'     => 'Denominación de la Red Social',
                'icon'     => 'Icono representativo',
            ],
        ],

        // rrss
        'web_language'     => [
            'list'   => 'Web - Idiomas',
            'report' => 'Web - Idioma',
            'field'  => [
                'web_id'    => 'Web',
                'language_id' => 'Idioma',
                'principal' => 'Idioma principal',
                'position' => 'Orden de visualización',
            ],
        ],

        // Archive
        'archive'     => [
            'list'   => 'Archivos',
            'report' => 'Archivo',
            'field'  => [
                'type_id' => 'Tipo de archivo',
                'available_in_main_language' => 'Disponible en el idioma principal de la página',
                'duration' => 'Duración',
                'archive' => 'Archivo',
                'title' => 'Título',
                'name_download' => 'Nombre descarga',
                'description' => 'Descripción',
                'reference' => 'Referencia',
            ],
        ],

    ],

    /*
     |--------------------------------------------------------------------------
     | DATABASE
     |--------------------------------------------------------------------------
     |
     | database
     |
     */
    'database' => [
        'sql' => [
            'title' => 'Ejecución de Sentencias SQL',
            'avise' => 'En caso de dudas, por favor, no usar.',
            'sentences' => '* Sentencias a ejecutar. En caso de ser más de una han de estar separadas por "punto y coma" ";"',
            'registers' => 'Registros afectados: :value',
        ],
    ],

];
