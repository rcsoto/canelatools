<?php

namespace Canela\CanelaTools\Models\Location;



use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Support\Facades\DB;

/**
 * Canela\CanelaTools\Models\Location\City
 *
 * @property int                                               $id
 * @property int                                               $province_id
 * @property string                                            $name
 * @property string|null                                       $code
 * @property \Illuminate\Support\Carbon|null                   $created_at
 * @property \Illuminate\Support\Carbon|null                   $updated_at
 * @property-read mixed                                        $combo_identificator
 * @property-read mixed                                        $created_at_format
 * @property-read mixed                                        $created_at_short
 * @property-read \Canela\CanelaTools\Models\Entity            $table
 * @property-read mixed                                        $updated_at_format
 * @property-read mixed                                        $updated_at_short
 * @property-read string                                       $value_identificator
 * @property-read \Canela\CanelaTools\Models\Location\Province $province
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\City whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class City extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'city';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['province_id', 'name', 'code'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'province_id' => 'integer',
        'name'        => 'string',
        'code'        => 'string',
    ];


    //
    //public $comboIdentifierField = 'value_identificator';


    /**
     *
     * {@inheritDoc}
     * @see \Canela\CanelaTools\Models\BasicModel::getValueIdentificatorAttribute()
     */
    public function getValueIdentificatorAttribute()
    {
        return $this->province->name . " - ". $this->name;
    }

    /**
     * Related province.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }


    /**
     * Id country
     * @return NULL|number
     */
    public function getCountryIdAttribute()
    {
        return empty($this->province_id) ? null : $this->province->country_id;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Canela\CanelaTools\Models\BasicModel::scopeComboList()
     */
    /*
    public function scopeComboList($query)
    {
        return $query->select(DB::raw("concat(`province`.`name`,' - ', `city`.`name`) as name"))
                     ->join('province', 'city.province_id', 'provicne.id')
                     ->get()
                     ->pluck('name', 'id');
    }
    */

}
