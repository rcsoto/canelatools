<?php

namespace Canela\CanelaTools\Controller\Client;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Client\Client;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Client\ClientRequest;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use stdClass;

/**
 * Class ClientController
 * @package Canela\CanelaTools\Controller\Client
 */
class ClientController extends BasicController
{
    use HtmlLists, Fileable;

    /**
     * ClientController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Client::class;
        $this->routeEntity = 'admin.client.client';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;
        $this->menu_group = AdminMenu::GROUP_CLIENT;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.client.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.client.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'id',
                )),
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.client.field.name'),
                )),
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'cif',
                    label: trans('canelatools::canelatools.entity.client.field.cif'),
                )),
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'contact_name',
                    label: trans('canelatools::canelatools.entity.client.field.contact_name'),
                )),
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'contact_telephone',
                    label: trans('canelatools::canelatools.entity.client.field.contact_telephone'),
                )),
                (new TableColumn(
                    table: 'client',
                    fieldOrder: 'full_address',
                    label: trans('canelatools::canelatools.entity.client.field.full_address'),
                )),
                (new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                )),
            ])
            ->setTableForm([
                (new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.client.field.name'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    max: 255,
                )),
                (new TableForm(
                    name: 'cif',
                    label: trans('canelatools::canelatools.entity.client.field.cif'),
                    containerClass: 'col-12 col-sm-6',
                    max: 255,
                )),
                (new TableForm(
                    name: 'contact_name',
                    label: trans('canelatools::canelatools.entity.client.field.contact_name'),
                    containerClass: 'col-12 col-sm-6',
                    max: 255,
                )),
                (new TableForm(
                    name: 'contact_telephone',
                    label: trans('canelatools::canelatools.entity.client.field.contact_telephone'),
                    containerClass: 'col-12 col-sm-6',
                    max: 45,
                )),
                (new TableForm(
                    name: 'full_address',
                    label: trans('canelatools::canelatools.entity.client.field.full_address'),
                    containerClass: 'col-12',
                    max: 2000,
                )),
                (new TableForm(
                    name: 'image_logo',
                    label: trans('canelatools::canelatools.entity.client.field.image_logo'),
                    type: TableColumnTypes::IMAGE_CROPPER,
                    fileDir: Client::DIR_LOGO,
                )),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $consulta = Client::select([  'client.id',
                                      'client.name',
                                      'client.cif',
                                      'client.contact_name',
                                      'client.contact_telephone',
                                      'client.full_address',
                                      'client.id as action_client_id',
                                   ]);

        return $this->setSecurityList($consulta);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_client_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                                              true, false, false, false,
                                              false, false, 0, 0, true,
                                              true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(ClientRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     * @param int $id
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function update(ClientRequest $request, int $id): Model|JsonResponse|RedirectResponse|null
    {
        return $this->updateBasic($request, $id);
    }

}
