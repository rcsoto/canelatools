<?php

namespace Canela\CanelaTools\Manager;



use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Svrnm\ExcelDataTables\ExcelDataTable;

/**
 * Class ExcelManager
 * @package Canela\CanelaTools\Manager
 */
class ExcelManager extends ExcelDataTable
{

    /**
     *
     * @return string
     */
    private static function getFileTmp()
    {
        return public_path(config('canelatools.document_root') . "/sample.xlsx");
    }


    /**
     * @param Collection $data
     * @param string     $filename
     * @param array      $headers
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|null
     */
    public static function buildExcel(Collection $data, $filename, array $headers)
    {
        try {
            $dataTable = new static();
            /** @var string $path . Fake Document used as base */
            $path = self::getFileTmp();
            $dataTable->showHeaders();
            $dataTable->addRows($data->toArray());
            //mini hack
            $dataTable->headerNames   = [];
            $dataTable->headerLabels  = [];
            $dataTable->headerNumbers = [];
            $dataTable->setHeaders(
                array_filter(array_combine($headers, $headers), function ($value) {
                    return !empty($value);
                })
            );
            $xlsx = $dataTable->fillXLSX($path);

            return Response::make($xlsx, 200, array(
                'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename="' . $filename . '.xlsx"',
                'Content-Length'      => strlen($xlsx),
            ));
        } catch (\Exception $e) {
            Log::error($e);
            return null;
        }
    }


    /**
     * Genera un archivo Excel a partir de un modelo o cosulta
     * @param string $nameFile
     * @param mixed  $consulta
     * @param bool   $isModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public static function showExcel(string $nameFile, $consulta, bool $isModel = false)
    {
        try {
            // aumentamos el tiempo de ejecucion a 10 min
            set_time_limit(3600);
            $data = ($isModel ? $consulta->get()->toArray() : DB::select($consulta));
            $path = self::getFileTmp();

            $dataTable = new static();
            $dataTable->showHeaders()->addRows($data);

            $dataTable->headerNames   = [];
            $dataTable->headerLabels  = [];
            $dataTable->headerNumbers = [];

            $headers = self::headings($data);
            $dataTable->setHeaders(array_combine($headers, $headers));
            $xlsx = $dataTable->fillXLSX($path);

            return Response::make($xlsx, 200, array(
                'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename="' . $nameFile . '"',
                'Content-Length'      => strlen($xlsx)
            ));
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors($e->getMessage());
        }
    }


    /**
     * @param array $data
     * @return array
     */
    public static function headings($data): array
    {
        if (count($data)) {
            $item = json_decode(json_encode($data[0]), true);
            return array_map(['self', 'changeHeading'], array_keys($item));
        } else {
            return [];
        }
    }


    /**
     * @param string
     * @return string
     */
    private static function changeHeading($head_item)
    {
        if (strlen($head_item) > 0) {
            if (($head_item[0] == '"') || ($head_item[0] == "'")) {
                $head_item = substr($head_item, 1);
            }

            $last = substr($head_item, -1);
            if (($last == '"') || ($last == "'")) {
                $head_item = substr($head_item, 0, strlen($head_item) - 1);
            }
        }

        return $head_item;

    }
}
