<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

/**
 * Class WebPageFaq
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property WebPage page
 *
 */
class WebPageData extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'position',
        'date',
        'name',
        'description',
        'value',
        'prefix',
        'suffix',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id'           => 'int',
        'position'          => 'int',
        // 'date'              => 'date',   // Returns date with 00:00:00 time.
        'name'              => 'string',
        'description'       => 'string',
        'value'             => 'string',
        'prefix'            => 'string',
        'suffix'            => 'string',
    ];


    /**
     * @return BelongsTo
     */
    public function page(): BelongsTo
    {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages($this->page->web_id);
    }

}
