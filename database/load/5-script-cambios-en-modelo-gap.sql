-- ---------------------------------------------------------------------------------------------
-- Web Notice Category - asociada a cliente
-- ---------------------------------------------------------------------------------------------
ALTER TABLE `web_notice_category` 
ADD COLUMN `client_id` INT(10) UNSIGNED NULL AFTER `id`,
ADD UNIQUE INDEX `idx_unique` (`client_id` ASC, `name` ASC) ,
ADD INDEX `fk_web_notice_category_client1_idx` (`client_id` ASC) ,
DROP INDEX `name_UNIQUE` ;

ALTER TABLE `web_notice_category` 
ADD CONSTRAINT `fk_web_notice_category_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `client` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

update  `web_notice_category` 
set `client_id` = (select `id` from `client`);

ALTER TABLE `web_notice_category` 
DROP FOREIGN KEY `fk_web_notice_category_client1`;

ALTER TABLE `web_notice_category` 
CHANGE COLUMN `client_id` `client_id` INT(10) UNSIGNED NOT NULL ;

ALTER TABLE `web_notice_category` 
ADD CONSTRAINT `fk_web_notice_category_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `client` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




-- -----------------------------------------------------
--
-- Cambios al modelo original
--
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `web_page_section`
-- -----------------------------------------------------
ALTER TABLE `web_page_section` 
ADD COLUMN `slug` VARCHAR(255) NULL AFTER `position`;

-- -----------------------------------------------------
-- Table `web_page_type`
-- -----------------------------------------------------
ALTER TABLE `web_page_type` 
ADD COLUMN `with_data` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `with_horary`;

-- -----------------------------------------------------
-- Table `web_page_type`
-- -----------------------------------------------------
 INSERT INTO `web_page_type`(`id`,`state_id`,`is_home`,`with_section`,`with_slider`,`with_gallery`,`with_map`,`with_data_contact`,`with_plan`,`with_notice`,`with_faq`,`with_data`,`multiple`,`name`,`description`) values
 ('10',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'0',	'Datos/Parémetros',	'Informar de datos/parámetros');


-- -----------------------------------------------------
-- Table `web_page_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_data` ;

CREATE TABLE IF NOT EXISTS `web_page_data` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` INT UNSIGNED NOT NULL,
  `position` SMALLINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `date` DATE NULL,
  `name` VARCHAR(255) NULL,
  `description` VARCHAR(4000) NULL,
  `value` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_web_page_data_web_page1_idx` (`page_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_web_page_data_web_page1`
    FOREIGN KEY (`page_id`)
    REFERENCES `web_page` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web_page_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_category` ;

CREATE TABLE IF NOT EXISTS `web_page_category` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` INT UNSIGNED NOT NULL,
  `position` SMALLINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `name` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_web_page_category_web_page1_idx` (`page_id` ASC),
  CONSTRAINT `fk_web_page_category_web_page1`
    FOREIGN KEY (`page_id`)
    REFERENCES `web_page` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web_page_section_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_section_category` ;

CREATE TABLE IF NOT EXISTS `web_page_section_category` (
  `category_id` INT UNSIGNED NOT NULL,
  `section_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`category_id`, `section_id`),
  INDEX `fk_web_page_section_category_web_page_section1_idx` (`section_id` ASC),
  CONSTRAINT `fk_web_page_section_category_web_page_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `web_page_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_section_category_web_page_section1`
    FOREIGN KEY (`section_id`)
    REFERENCES `web_page_section` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


insert into `entity` (`name`,`reference`,`model`) values
('Web Página Datos','web_page_data', null),
('Web Página Categorías','web_page_category',null);


-- -----------------------------------------------------
-- Table `web_page_contact_type`
-- -----------------------------------------------------
insert into `web_page_contact_type` (`id`,`state_id`,`position`,`icon`,`contact`) values				
('6',	'1',	'6',	null,	'Email'),
('7', 	'1', 	'7', 	NULL, 	'Horario');

-- -----------------------------------------------------
-- Table `web_page_section_archive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web_page_section_archive` ;

CREATE TABLE IF NOT EXISTS `web_page_section_archive` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `section_id` INT UNSIGNED NOT NULL,
  `type_id` INT UNSIGNED NOT NULL,
  `position` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1,
  `duration` SMALLINT(5) UNSIGNED NULL,
  `archive` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_web_page_section_archive_web_page_section1_idx` (`section_id` ASC),
  INDEX `fk_web_page_section_archive_archive_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_web_page_section_archive_web_page_section1`
    FOREIGN KEY (`section_id`)
    REFERENCES `web_page_section` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_section_archive_archive_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `archive_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`model`) values
('Web Página Secciones - Archivos','web_page_section_archive',null);


-- ---------------------------------------------------------------------------------------------
-- `web_page_category`
-- ---------------------------------------------------------------------------------------------
ALTER TABLE `web_page_category` 
ADD UNIQUE INDEX `idx_unique` (`page_id` ASC, `name` ASC),
DROP INDEX `name_UNIQUE` ;


-- ---------------------------------------------------------------------------------------------
-- `web_page_category`
-- ---------------------------------------------------------------------------------------------
ALTER TABLE `web_page_category` 
ADD COLUMN `abbreviation` VARCHAR(45) NULL AFTER `name`,
ADD COLUMN `description` VARCHAR(255) NULL AFTER `abbreviation`,
ADD COLUMN `color_text` VARCHAR(45) NULL AFTER `description`,
ADD COLUMN `color_border` VARCHAR(45) NULL AFTER `color_text`,
ADD COLUMN `color_bg` VARCHAR(45) NULL AFTER `color_border`;

ALTER TABLE `web_page_notice_category` 
DROP FOREIGN KEY `fk_web_page_notice_category_web_notice_category1`;
ALTER TABLE `web_page_notice_category` 
ADD INDEX `fk_web_page_notice_category_web_notice_category1_idx` (`category_id` ASC) ,
DROP INDEX `fk_web_page_notice_category_web_notice_category1_idx` ;

ALTER TABLE `web_page_notice_category` 
ADD CONSTRAINT `fk_web_page_notice_category_web_notice_category1`
  FOREIGN KEY (`category_id`)
  REFERENCES `web_page_category` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

drop table `web_notice_category`;


-- -----------------------------------------------------
-- Table `archive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `archive` ;

CREATE TABLE IF NOT EXISTS `archive` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` INT UNSIGNED NOT NULL,
  `duration` SMALLINT(5) UNSIGNED NULL,
  `archive` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NULL,
  `name_download` VARCHAR(255) NULL,
  `description` VARCHAR(4000) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_archive_archive_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_archive_archive_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `archive_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into `entity` (`name`,`reference`,`model`) values
('Archivos','archive',null);

ALTER TABLE `web_page_section_archive` 
ADD COLUMN `archive_id` INT(10) UNSIGNED NULL AFTER `type_id`,
ADD INDEX `fk_web_page_section_archive_archive1_idx` (`archive_id` ASC) ;

ALTER TABLE `web_page_section_archive` 
ADD CONSTRAINT `fk_web_page_section_archive_archive1`
  FOREIGN KEY (`archive_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `web_page_notice_archive` 
ADD COLUMN `archive_id` INT(10) UNSIGNED NULL AFTER `type_id`,
ADD INDEX `fk_web_page_notice_archive_archive1_idx` (`archive_id` ASC) ;

ALTER TABLE `web_page_notice_archive` 
ADD CONSTRAINT `fk_web_page_notice_archive_archive1`
  FOREIGN KEY (`archive_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table `customer` logo para fondos oscuros
-- -----------------------------------------------------
ALTER TABLE `customer` 
ADD COLUMN `image_logo_dark` VARCHAR(255) NULL AFTER `image_logo`;

-- -----------------------------------------------------
-- Table `web_page` video de cabecera
-- -----------------------------------------------------
ALTER TABLE `web_page` 
ADD COLUMN `file_header_id` INT(10) UNSIGNED NULL AFTER `state_id`,
ADD INDEX `fk_web_page_archive1_idx_idx` (`file_header_id` ASC) ,
ADD INDEX `fk_web_page_archive1_idx` (`file_header_id` ASC) ;
ALTER TABLE `web_page` 
ADD CONSTRAINT `fk_web_page_archive1`
  FOREIGN KEY (`file_header_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table `archive_type` tipos de archivos
-- -----------------------------------------------------

INSERT INTO `archive_type` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES ('6', 'Otro', 'Otro tipo de documento', now(), now());

ALTER TABLE `archive_type` 
ADD COLUMN `is_generic` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `description`,
ADD COLUMN `is_video` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_generic`,
ADD COLUMN `is_image` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_video`,
ADD COLUMN `is_pdf` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_image`,
ADD COLUMN `is_url` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_pdf`;

UPDATE `archive_type` SET `is_video` = '1' WHERE (`id` = '1');
UPDATE `archive_type` SET `is_pdf` = '1' WHERE (`id` = '2');
UPDATE `archive_type` SET `is_image` = '1' WHERE (`id` = '3');
UPDATE `archive_type` SET `is_url` = '1' WHERE (`id` = '4');
UPDATE `archive_type` SET `is_url` = '1' WHERE (`id` = '5');
UPDATE `archive_type` SET `is_generic` = '1' WHERE (`id` = '6');


-- -----------------------------------------------------
-- Relacion con archivos
-- -----------------------------------------------------

-- web_page_notice_archive
ALTER TABLE `web_page_notice_archive` 
DROP FOREIGN KEY `fk_web_page_notice_archive_archive_type1`;
ALTER TABLE `web_page_notice_archive` 
DROP COLUMN `archive`,
DROP COLUMN `duration`,
DROP COLUMN `type_id`,
DROP INDEX `fk_web_page_notice_archive_archive_type1_idx` ;

-- web_page_section_archive
ALTER TABLE `web_page_section_archive` 
DROP FOREIGN KEY `fk_web_page_section_archive_archive_type1`;
ALTER TABLE `web_page_section_archive` 
DROP COLUMN `archive`,
DROP COLUMN `duration`,
DROP COLUMN `type_id`,
DROP INDEX `fk_web_page_section_archive_archive_type1_idx` ;

-- archive
ALTER TABLE `archive` 
ADD COLUMN `reference` VARCHAR(45) NULL AFTER `description`;

-- web_page
ALTER TABLE `web_page` 
DROP COLUMN `image_header`;

-- web_page_section
ALTER TABLE `web_page_section` 
DROP COLUMN `image_thumb`,
DROP COLUMN `image_header`,
ADD COLUMN `file_header_id` INT(10) UNSIGNED NULL AFTER `state_id`,
ADD COLUMN `file_thumb_id` INT(10) UNSIGNED NULL AFTER `file_header_id`,
ADD INDEX `fk_web_page_section_archive1_idx` (`file_header_id` ASC) ,
ADD INDEX `fk_web_page_section_archive2_idx` (`file_thumb_id` ASC) ;
ALTER TABLE `web_page_section` 
ADD CONSTRAINT `fk_web_page_section_archive1`
  FOREIGN KEY (`file_header_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_web_page_section_archive2`
  FOREIGN KEY (`file_thumb_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

-- web_page_notice
ALTER TABLE `web_page_notice` 
DROP COLUMN `image_thumb`,
DROP COLUMN `image_thumbnail`,
ADD COLUMN `file_thumb_id` INT(10) UNSIGNED NULL AFTER `state_id`,
ADD COLUMN `file_thumbnail_id` INT(10) UNSIGNED NULL AFTER `file_thumb_id`,
ADD INDEX `fk_web_page_notice_archive1_idx` (`file_thumbnail_id` ASC) ,
ADD INDEX `fk_web_page_notice_archive2_idx` (`file_thumb_id` ASC) ;
ALTER TABLE `web_page_notice` 
ADD CONSTRAINT `fk_web_page_notice_archive1`
  FOREIGN KEY (`file_thumbnail_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_web_page_notice_archive2`
  FOREIGN KEY (`file_thumb_id`)
  REFERENCES `archive` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

-- -----------------------------------------------------
-- web_page_notice_horary
-- -----------------------------------------------------
insert into `entity` (`name`,`reference`,`model`) values
('Web página noticias horarios','web_page_notice_horary',null);


-- -----------------------------------------------------
-- web_page -> mostrar en footer t en la home
-- -----------------------------------------------------
ALTER TABLE `web_page` 
ADD COLUMN `show_footer` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `show_header`,
ADD COLUMN `show_home` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `show_footer`;

-- -----------------------------------------------------
-- web_page -> mostrar en footer t en la home
-- -----------------------------------------------------
ALTER TABLE `archive` 
ADD COLUMN `available_in_main_language` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Disponible en el idioma principal de la pagina' AFTER `type_id`;

DROP TABLE IF EXISTS `web_page_archive` ;

CREATE TABLE IF NOT EXISTS `web_page_archive` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` INT UNSIGNED NOT NULL,
  `archive_id` INT UNSIGNED NULL,
  `position` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1,
  `title` VARCHAR(255) NULL,
  `description` VARCHAR(4000) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_web_page_archive_web_page1_idx` (`page_id` ASC),
  INDEX `fk_web_page_archive_archive1_idx` (`archive_id` ASC),
  CONSTRAINT `fk_web_page_archive_web_page1`
    FOREIGN KEY (`page_id`)
    REFERENCES `web_page` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_web_page_archive_archive1`
    FOREIGN KEY (`archive_id`)
    REFERENCES `archive` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


insert into `entity` (`name`,`reference`,`model`) values
('Web página archivos','web_page_archive',null);


-- -----------------------------------------------------
-- Table `web_page_section_archive` cascade
-- -----------------------------------------------------
ALTER TABLE `web_page_section_archive` 
DROP FOREIGN KEY `fk_web_page_section_archive_web_page_section1`;
ALTER TABLE `web_page_section_archive` 
ADD CONSTRAINT `fk_web_page_section_archive_web_page_section1`
  FOREIGN KEY (`section_id`)
  REFERENCES `web_page_section` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
-- -----------------------------------------------------
-- `web_page_section_archive` eliminar campos repetidos
-- -----------------------------------------------------  
ALTER TABLE `web_page_section_archive` 
DROP COLUMN `description`,
DROP COLUMN `title`;
  
