{{--
Param: $publicKey
JS: ha de existir un archivo denominado "serviceWorker.js" en la carpeta publica
--}}


<script type="text/javascript">
    jQuery(window).on("load", function() {

        let swRegistration = null;

        // registrar Service Worker
        jQuery.registerServiceWorker = function () {
            if ('serviceWorker' in navigator && 'PushManager' in window) {
                //console.log('Service Worker and Push is supported');
                navigator.serviceWorker.register('serviceWorker.js')
                    .then(function (swReg) {
                        //console.log('Service Worker is registered', swReg);
                        swRegistration = swReg;
                        jQuery.initialiseUI();
                    }).catch(function (error) {
                    //console.error('Service Worker Error', error);
                });
            } else {
                //console.warn('Push messaging is not supported');
            }
        }

        jQuery.initialiseUI =function() {
            swRegistration.pushManager.getSubscription()
                .then(function(subscription) {
                    if (subscription) {
                        //console.log('User IS subscribed:', subscription);
                    } else {
                        jQuery.subscribeUser();
                    }
                });
        }

        jQuery.subscribeUser = function() {
            const applicationServerKey =
                window.toUint8Array('{{ $publicKey }}');
            swRegistration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: applicationServerKey
            }).then(function(subscription) {
                //console.log('User is subscribed:', subscription);
                jQuery.registerEndpoint(subscription);
            }).catch((err)=> {
                //console.log('Failed to subscribe the user: ', err);
            });
        }

        //const subscriptionDetails = document.querySelector('#subcription-details');
        jQuery.registerEndpoint = function(subscription) {
            if (subscription) {
                formdata = new FormData();
                formdata.append("_token", '{{ csrf_token() }}');
                formdata.append("endpoint", subscription.endpoint);
                var urlCP = "{{ route('addendpoint') }}";
                jQuery.ajax({
                    url: urlCP,
                    dataType: "json",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    error: function (jqXHR, exception, errorThrown) {
                    },
                    success: function (datos) {
                    }
                });
            }
        }

        // Solicitar permiso para notificar
        Notification.requestPermission(function(result) {
            if (result === 'denied') {
                //console.log('Permission wasn\'t granted. Allow a retry.');
                return;
            } else if (result === 'default') {
                //console.log('The permission request was dismissed.');
                return;
            }
            // Hacer algo con el permiso concedido.
            else if (result === 'granted') {
                jQuery.registerServiceWorker();
            }
        });

    });

</script>
