@section('body-scripts')
    @parent
    <script type="text/javascript">
        jQuery(document).ready(function () {

            /***** File Delete *****/
            @if(!empty($register))
                jQuery.confirmDeleteFile = function (fileId, fileName, type) {
                    Swal.fire({
                        text: (type === '{{\Canela\CanelaTools\Enums\TableColumnTypes::FILE}}') ? '{{trans('canelatools::canelatools.canela-rest.label.question_delete_file')}}' : '{{trans('canelatools::canelatools.canela-rest.label.question_delete_image')}}',
                        showCancelButton: true,
                        confirmButtonText: '{{trans('canelatools::canelatools.buttons.continue')}}',
                        cancelButtonText: '{{trans('canelatools::canelatools.buttons.cancel')}}',
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jQuery.deleteFile(fileId, fileName, type);
                        }
                    });
                }

                jQuery.deleteFile = function(fileId, fileName, dir, type) {
                    jQuery.loadIni();

                    const formdata = new FormData();
                    formdata.append('_token', '{{ csrf_token() }}');
                    formdata.append('id', {{$register->id}});
                    formdata.append('field_name', fileName);
                    formdata.append('dir', dir);

                    jQuery.ajax({
                        url: '{{$deleteFileUrl}}',
                        dataType: "json",
                        data: formdata,
                        type: "POST",
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            jQuery.loadIni();
                        },
                        error: function (jqXHR, exception, errorThrown) {
                            var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                            jQuery.messageWarning(msg);
                            jQuery.loadEnd();
                        },
                        success: function (data) {
                            if (data.result) {
                                const container = jQuery('#canela-input-file-container-'+fileId);
                                container.find('.preview-container img').prop('src', '');
                                container.find('a.file-link').remove();
                                container.find('a.remove-file-link').remove();

                                jQuery.messageInfo((type === '{{\Canela\CanelaTools\Enums\TableColumnTypes::FILE}}')
                                                    ? '{{trans('canelatools::canelatools.canela-rest.label.file_deleted_successfully')}}'
                                                    : '{{trans('canelatools::canelatools.canela-rest.label.image_deleted_successfully')}}');
                            } else {
                                jQuery.messageWarning(data.message);
                            }
                            jQuery.loadEnd();
                        },
                    });
                };
            @endif
            /***** /File Delete *****/

        });
    </script>
@endsection
