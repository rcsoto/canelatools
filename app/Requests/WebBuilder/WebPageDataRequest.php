<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageFaqRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageDataRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'position',
        'date',
        'name',
        'description',
        'value',
        'prefix',
        'suffix',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_data.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'name'              => 'nullable|max:255',
            'position'          => 'required|integer|min:1|max:99999',
            'description'       => 'nullable|max:4000',
            'value'             => 'nullable|max:45',
            'prefix'            => 'nullable|max:45',
            'suffix'            => 'nullable|max:45',
            'date'              => 'nullable|date'

        );

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');

        return $attributes;
    }

}
