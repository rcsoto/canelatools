<?php

namespace Canela\CanelaTools\Models;

use Canela\CanelaTools\Models\WebBuilder\WebLanguage;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * Canela\CanelaTools\Models\Language
 *
 * @property int                                   $id
 * @property int                                   $state_id
 * @property string                                $name
 * @property string                                $code
 * @property string                                $code2
 * @property string                                $reference
 * @property int                                   $position
 * @property \Carbon\Carbon|null                   $created_at
 * @property \Carbon\Carbon|null                   $updated_at
 * @property-read string                           $created_at_format
 * @property-read string                           $table
 * @property-read string                           $updated_at_format
 * @property-read string                           $combo_identificator
 * @property-read string                           $value_identificator
 * @property-read string                           $created_at_short
 * @property-read string                           $updated_at_short
 * @property-read \Canela\CanelaTools\Models\State $state
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereCode2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language comboListActive()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language comboListActiveOrderType()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Language whereStateId($value)
 * @mixin \Eloquent
 */
class Language extends BasicModel
{
    const ENTITY_TABLE = 'language';

    /**
     *
     * Constantes para los estados
     */
    const SPAIN        = 1;
    const ID_DEFAULT   = 1;
    const DEFAULT_CODE = 'es';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'name',
        'code',
        'code2',
        'reference',
        'position',
        'flag',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'        => 'int',
        'state_id'  => 'int',
        'name'      => 'string',
        'code'      => 'string',
        'code2'     => 'string',
        'reference' => 'string',
        'position'  => 'int',
        'flag'      => 'string'
    ];


    /** @var string $routePath Model route path */
    protected static string $routePath = 'admin.configuration.language';


    /** @var string $resourceTransPath Model resource translations path */
    protected static string $resourceTransPath = 'canelatools::canelatools.entity.language.';


    /**
     *
     * Estado del idioma
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }


    /**
     * @return HasOne
     */
    public function webLanguage(): HasOne
    {
        return $this->hasOne(WebLanguage::class, 'language_id');
    }


    /**
     * Return register actives
     * @return \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\Language[]
     */
    public static function actives()
    {
        return self::where('state_id', State::ACTIVO)
                   ->orderBy('position', 'asc')
                   ->get();
    }


    /**
     * @return Language[]|\Illuminate\Database\Eloquent\Collection|Builder[]|\Illuminate\Support\Collection
     */
    public static function activesCheckingPrincipal(bool $withoutPrincipal = true)
    {
        $query = self::leftJoin('web_language', 'language.id', 'web_language.language_id')
                        ->where('language.state_id', State::ACTIVO);
        if ($withoutPrincipal) {
            $query->where(function ($query) {
                $query->where('web_language.principal', 0)
                        ->orWhereNull('web_language.principal');
            });
        }
        $query->orderBy('language.position')
                ->select(['language.*', 'web_language.principal']);
        return $query->get();
    }


    /**
     * Combo for active languages.
     *
     * @param Builder $query
     * @return \Illuminate\Support\Collection
     */
    public function scopeComboListActive(Builder $query)
    {
        return $query->where('state_id', State::ACTIVO)
                     ->orderBy('name', 'asc')
                     ->pluck('name', 'id');
    }

    /**
     * @param Builder $query
     * @return \Illuminate\Support\Collection
     */
    public function scopeComboListActiveOrderType(Builder $query)
    {
        return $query->where('state_id', State::ACTIVO)
                     ->orderBy('name', 'asc');
    }

}

