<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class WebPage
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property int web_id
 * @method static mixed comboListWithSecurity($controller, $type=null)
 */
class WebPage extends BasicModel
{
    public const DIR_HEADER = 'project/page/header/';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page';

    public $tableIdentifierField = 'title';
    public $comboIdentifierField = 'title';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'web_id',
        'type_id',
        'state_id',
        'file_header_id',
        'show_header',
        'show_footer',
        'show_home',
        'position',
        'slug',
        'title',
        'subtitle',
        'body',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'tags',
        'external_link',
        'is_external_link',
        'open_in_new_window',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'web_id'            => 'int',
        'type_id'           => 'int',
        'state_id'          => 'int',
        'file_header_id'    => 'int',
        'show_header'       => 'bool',
        'show_footer'       => 'bool',
        'show_home'         => 'bool',
        'position'          => 'int',
        'slug'              => 'string',
        'title'             => 'string',
        'subtitle'          => 'string',
        'body'              => 'string',
        'seo_title'         => 'string',
        'seo_keywords'      => 'string',
        'seo_description'   => 'string',
        'tags'              => 'string',
        'external_link'     => 'string',
        'is_external_link'  => 'bool',
        'open_in_new_window'=> 'bool',
    ];


    /**
     * @return BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * @return BelongsTo
     */
    public function fileHeader(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'file_header_id');
    }


    /**
     * @return BelongsTo
     */
    public function web(): BelongsTo
    {
        return $this->belongsTo(Web::class, 'web_id');
    }

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(WebPageType::class, 'type_id');
    }


    /**
     * @return HasMany
     */
    public function sections(): HasMany
    {
        return $this->hasMany(WebPageSection::class, 'page_id')
            ->orderBy('position', 'asc');
    }


    /**
     * Secciones activas a mostrar
     * @return mixed
     */
    public function sectionsActive(): HasMany
    {
        return $this->sections()
            ->where('state_id', State::ACTIVO);
    }

    /**
     * @return HasMany
     */
    public function archives() {
        return $this->hasMany(WebPageArchive::class, 'page_id')
            ->orderBy('position');
    }


    /**
     * @return HasMany
     */
    public function galleries(): HasMany
    {
        return $this->hasMany(WebPageGallery::class, 'page_id')
                    ->orderBy('position', 'asc');
    }


    /**
     * @return HasMany
     */
    public function contacts(): HasMany
    {
        return $this->hasMany(WebPageContact::class, 'page_id')
                    ->orderBy('position', 'asc');
    }


    /**
     * @return HasMany
     */
    public function data(): HasMany
    {
        return $this->hasMany(WebPageData::class, 'page_id');
    }


    /**
     * @return HasMany
     */
    public function dataSorted(): HasMany
    {
        return $this->data()
            ->orderBy('position');
    }


    /**
     * @return HasMany
     */
    public function categories(): HasMany
    {
        return $this->hasMany(WebPageCategory::class, 'page_id')
                    ->orderBy('position');
    }


    /**
     * @return HasMany
     */
    public function notices(): HasMany
    {
        return $this->hasMany(WebPageNotice::class, 'page_id')
                    ->orderBy('date', 'desc');
    }
    /**
     * @return HasMany
     */
    public function noticesAsc(): HasMany
    {
        return $this->hasMany(WebPageNotice::class, 'page_id')
            ->orderBy('date', 'asc');
    }



    /**
     * Noticias activas
     * @return mixed
     */
    public function noticesActive(): HasMany
    {
        return $this->notices()
                    ->where('state_id', State::ACTIVO);
    }
    public function noticesActiveAsc(): HasMany
    {
        return $this->noticesAsc()
            ->where('state_id', State::ACTIVO);
    }


    /**
     * @return HasMany
     */
    public function faqs(): HasMany
    {
        return $this->hasMany(WebPageFaq::class, 'page_id')
                    ->orderBy('position', 'asc');
    }


    /**
     * FAQ activas
     * @return mixed
     */
    public function faqsActive(): HasMany
    {
        return $this->faqs()
                    ->where('state_id', State::ACTIVO);
    }


    /**
     * Return register actives
     * @return \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\Language[]
     */
    public static function actives()
    {
        return self::where('state_id', State::ACTIVO)
            ->orderBy('position')
            ->get();
    }


    /**
     * @param $query
     * @param $controller
     * @param string|null $type Nombre del campo
     * @return mixed
     */
    public function scopeComboListWithSecurity($query, $controller, $type=null): mixed
    {
        $query->select(['web_page.id', 'web_page.title'])
            ->join('web_page_type', 'web_page.type_id', 'web_page_type.id')
            ->join('web', 'web_page.web_id', 'web.id')
            ->join('customer', 'web.id', 'customer.web_id')
            ->join('client', 'customer.client_id', 'client.id')
            ->groupBy(['web_page.id', 'web_page.title']);
        if (!empty($type)) {
            $query->whereRaw('`web_page_type`.`'.$type.'` = 1');
        }

        return $controller->setSecurityList($query)->get()->pluck($this->getNameFieldComboBoxAttribute(), 'id');
    }


    /**
     * @param Builder $query
     * @return Collection
     */
    public function scopeActivesShowHeader($query)
    {
        return $query->where('state_id', State::ACTIVO)
            ->where('show_header', 1)
            ->orderBy('position', 'asc')
            ->get();
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages($this->web_id);
    }


    /**
     * Recupera la primera página de un tipo
     *
     * @param int $webId
     * @param string $attribute
     * @param int $value
     * @return WebPage|Model|object|null
     */
    public static function getPageAttribute(int $webId, string $attribute, $value=1) {
        return WebPage::select(['web_page.*'])
            ->where('web_page.state_id', State::ACTIVO)
            ->where('web_page.web_id', $webId)
            ->whereExists(function($query) use($attribute, $value) {
                $query->select(DB::raw(1))
                    ->from('web_page_type')
                    ->whereRaw('web_page.type_id = web_page_type.id')
                    ->where('web_page_type.'.$attribute, $value);
                })
            ->first();
    }
}
