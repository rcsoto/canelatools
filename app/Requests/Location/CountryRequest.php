<?php

namespace Canela\CanelaTools\Requests\Location;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class CountryRequest
 * @package Canela\CanelaTools\Requests\Location
 */
class CountryRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'name'        => 'required|string|min:1|unique:country,name',
            'code2'       => 'required|max:2|unique:country,code2',
            'code3'       => 'required|max:3|unique:country,code3',
            'code_number' => 'required|integer|min:1|max:999|unique:country,code_number',
        );

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id = $this->route('country');

            $rules['name']        .= ",$id";
            $rules['code2']       .= ",$id";
            $rules['code3']       .= ",$id";
            $rules['code_number'] .= ",$id";
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'        => trans('canelatools::canelatools.entity.country.field.name'),
            'code2'       => trans('canelatools::canelatools.entity.country.field.code2'),
            'code3'       => trans('canelatools::canelatools.entity.country.field.code3'),
            'code_number' => trans('canelatools::canelatools.entity.country.field.code_number'),
        ];
    }
}
