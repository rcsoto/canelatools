@php
    $currentLocale = app()->getLocale();
@endphp

<!-- GDPR Cookie Notice -->
<script src="{{asset('plugins/gdpr-cookie-notice/gdpr-cookie-notice.js')}}"></script>
<script src="{{asset('plugins/gdpr-cookie-notice/langs/'.$currentLocale.'.js')}}"></script>

<script>
    jQuery(function () {

        document.addEventListener('gdprCookiesEnabled', function (e) {
            if (e.detail.analytics) { //checks if analytics cookies are enabled
                if (typeof(addTagManagerScript) !== 'undefined') {
                    addTagManagerScript();
                }
            }
        });

        gdprCookieNotice({
            locale: '{{$currentLocale}}', //This is the default value
            timeout: 500, //Time until the cookie bar appears
            expiration: 30, //This is the default value, in days
            domain: window.location.hostname, //If you run the same cookie notice on all subdomains, define the main domain starting with a .
            implicit: false, // debe se false, porque de lo contrario no cumples
            statement: '{{route('cookies-policy')}}', //Link to your cookie statement page
            performance: {!! json_encode($performance) !!}, //Cookies in the performance category.
            analytics: {!! json_encode($analytics) !!}, //Cookies in the analytics category.
            marketing: {!! json_encode($marketing) !!} //Cookies in the marketing category.
        });
    });
</script>
