<?php namespace Canela\CanelaTools\Models\Client;

use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Query\Builder;
use App\Models\Client\Customer;

class ClientConfiguration extends BasicModel
{
    const DEFAULT_DATE_FORMAT           = 'dd-mm-yyyy';
    const DEFAULT_TIME_FORMAT           = 'HH:ii';
    const DEFAULT_CURRENCY              = '€';
    const DEFAULT_CURRENCY_BEFORE       = '0';
    const DEFAULT_DECIMAL_SEPARATOR     = ',';
    const DEFAULT_THOUSAND_SEPARATOR    = '.';
    const DEFAULT_TIMEZONE              = 'Europe/Madrid';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_configuration';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'date_format',
        'time_format',
        'currency',
        'currency_before',
        'time_zone',
        'decimal_separator',
        'thousand_separator',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'customer_id'           => 'int',
        'date_format'           => 'string',
        'time_format'           => 'string',
        'currency'              => 'string',
        'currency_before'       => 'string',
        'time_zone'             => 'string',
        'decimal_separator'     => 'string',
        'thousand_separator'    => 'string',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }


    /**
     * Formatos de fechas
     * @param Builder $query
     * @return mixed
     */
    public function scopeDateFormatList($query)
    {
        return collect(['dd-mm-yyyy' => 'dd-mm-yyyy',
                        'dd/mm/yyyy' => 'dd/mm/yyyy',
                        'yyyy-mm-dd' => 'yyyy-mm-dd',
                        'yyyy/mm/dd' => 'yyyy/mm/dd',]);
    }

    /**
     * Formatos de horas
     * @param Builder $query
     * @return mixed
     */
    public function scopeTimeFormatList($query)
    {
        return collect(['hh:ii' => '(12)hh:mm',
            'HH:ii', '(24)hh:ii']);
    }


    /**
     * Zonas horarias validas
     * @param Builder $query
     * @return mixed
     */
    public function scopeTimeZoneList($query)
    {
        return collect(['0' => 'Definir la función scopeTimeZoneList']);
    }
}
