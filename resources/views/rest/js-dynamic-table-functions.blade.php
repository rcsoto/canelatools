﻿@php
    /** @var String $nameSectionTable nombre de la seccion donde se añade el codigo */
    $nameSectionTable = $nameSectionTable ?? config('canelatools.blade.section.contentTable');

    /** @var String $modalSectionName nombre de la seccion donde se añaden los modales.
     *                                Por ejemplo 'body-modals'
     */
    $modalSectionName = $modalSectionName ?? 'body-modals';
@endphp


@section('content-table')
    @parent
    @include('canelatools::deleteListRecord', ['modalSectionName' => $modalSectionName])
    @include('canelatools::updateStatus')
@endsection


<script type="text/javascript" charset="utf-8">

    jQuery(document).ready(function () {
        // ----------------------------------------------------------------------
        // Variables globales
        // ----------------------------------------------------------------------
        var _ACTIVE_TABLE 		= '{{\Canela\CanelaTools\Manager\UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeTable';
        var _ACTIVE_FILTERS_DATA 	= '{{\Canela\CanelaTools\Manager\UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeFiltersData';
        var _ACTIVE_FILTERS_CONDITION   = '{{\Canela\CanelaTools\Manager\UtilsBasicManager::sanearString(strtolower(config("app.name")))}}-activeFiltersCondition';

        // ----------------------------------------------------------------------
        // construir ID para la tabla
        // ----------------------------------------------------------------------
        jQuery.getTableId = function(tablename) {
            return "#"+tablename;
        }


        // ----------------------------------------------------------------------
        // recoge valor de paginacion
        // ----------------------------------------------------------------------
        jQuery.getPaginacion = function (tableid) {
            var valor = jQuery(tableid+'-select-pagination').val();
            if (valor == null) {
                valor = 10;
            }
            var pageTmp = jQuery(tableid+'_paginate').data('page');
            if (pageTmp == null) {
                pageTmp = 1;
            }
            // TODO no entiendo por que se hace esto
            //jQuery(tableid+'_paginate').data('page', 1);
            return '&page=' + pageTmp + '&pagination=' + valor;
        };


        // ----------------------------------------------------------------------
        // recogemos filtros
        // ----------------------------------------------------------------------
        jQuery.getFiltros = function (tableid) {
            var jsonObj = [];

            jQuery(tableid+" #tr-filters").children().each(function () {
                let item = {};
                let existe = false;
                jQuery(this).children().each(function () {
                    item['type'] = null;
                    if (jQuery(this).hasClass('search_init')) {
                        existe = true;
                        item['id'] 			= jQuery(this).attr('name');
                        item['table'] 		= jQuery(this).data('table');
                        item['field'] 		= jQuery(this).data('field');
                        item['filter_sql'] 	= jQuery(this).data('filtersql');
                        item['filter_sql_combo'] = jQuery(this).data('filtersqlcombo');
                        item['filter_sql_combo_table'] = jQuery(this).data('filtersqlcombotable');
                        item['value'] 		= null;
                        item['data_type'] 	= (typeof jQuery(this).attr('data-type') !== 'undefined' ? jQuery(this).attr('data-type') : null);
                        let variable = jQuery(this).val();
                        if (!("undefined" === typeof variable || variable === '' || variable == null)) {
                            item['value'] = variable;
                        }
                    } else if (jQuery(this).hasClass('select-filter-opt')) {
                        item['type'] = jQuery(this).val();
                    }
                });
                if (existe) {
                    jsonObj.push(item);
                }
            });
            return JSON.stringify(jsonObj);
        };


        // ----------------------------------------------------------------------
        // recoge orderBy
        // ----------------------------------------------------------------------
        jQuery.getOrderBy = function (tableid) {
            var jsonObj = [];
            jQuery(tableid+" #tr-orders").children().each(function () {
                var variable = jQuery(this).attr('data-orderby');
                if (!("undefined" === typeof variable || variable == '')) {
                    item = {};
                    item['id'] = jQuery(this).attr('id');
                    item['value'] = variable;
                    jsonObj.push(item);
                }
            });

            return JSON.stringify(jsonObj);
        };


        // ----------------------------------------------------------------------
        // limpiar filtros
        // ----------------------------------------------------------------------
        jQuery.limpiarFiltros = function (tablename) {
            const tableid = jQuery.getTableId(tablename);

            jQuery(tableid+" #tr-filters").children().each(function () {
                jQuery(this).children().each(function () {
                    if (jQuery(this).hasClass('search_init'))
                        if (jQuery(this).hasClass('select2'))
                            jQuery(this).val('').trigger('change.select2');
                        else
                            jQuery(this).val('');
                    else if (jQuery(this).hasClass('select-filter-opt')) {
                        jQuery(this)[0].selectedIndex = 0;
                    }
                });
                jQuery.saveCookieFilters(tableid);
            });
            jQuery.buscar(tablename);
        };


        // ----------------------------------------------------------------------
        // BUSQUEDA
        // ----------------------------------------------------------------------
        jQuery.buscar = function (tablename) {
            const tableid = jQuery.getTableId(tablename);
            const iniciarBusqueda = jQuery(tableid).data('iniciar-busqueda');
			if (iniciarBusqueda === 1)
			{
    			// guardar filtros
            	jQuery.saveCookieFilters(tableid);
                // Elementos a mostrar
                pagination = jQuery.getPaginacion(tableid);

                // Filtros
                filters = jQuery.getFiltros(tableid);


                // order bys
                orderbys = jQuery.getOrderBy(tableid);

                // recargar
                formdata = new FormData();
                formdata.append("_token", '{{ csrf_token() }}');
                formdata.append("filters", filters);
                formdata.append("orderbys", orderbys);

                {{-- Create Order from other --}}
                @if (!empty($inputs) && !empty($inputs['create_from_other']))
                    formdata.append("create_from_other", '1');
                @endif
                {{-- /Create Order from other --}}

                {{-- From Section --}}
                @if (!empty($archive_target))
                    formdata.append("archive_target", '{{$archive_target}}');
                @endif
                {{-- /From Section --}}

                // parametros desde el controlador
                const  parametersFilter = jQuery(tableid).data('parameter-filters');
                if (parametersFilter !== null) {
                    formdata.append("parameterFilters", JSON.stringify(parametersFilter));
                }

                var pagination;
                var columnas;
                const urlentidad = jQuery(tableid).data('urlentidad');
                var urlCP = urlentidad+"?" + pagination + "&" +  jQuery(tableid).data('request');

                jQuery.ajax({
                    url: urlCP,
                    dataType: "json",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        // Handle the beforeSend event
                        jQuery.loadIni();
                        pagination = null;
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        jQuery.loadEnd();
                        jQuery.messageError("Se ha producido un error. Vuelva a intentar la operación. En caso que el error persista comunique con el equipo de soporte.");
                        var msg = jQuery.procesarRespuestaAjax(jqXHR, exception, errorThrown);
                        console.error(msg);
                    },
                    success: function (datos) {
                        const modetable = jQuery(tableid).data('modetable');
                        // ocultar espera
                        if (datos.result) {
                            pagination = datos.pagination;
                            columnas = datos.columnas;

                            // TABLA
                            if (modetable) {
                                var dtable = jQuery(tableid).DataTable().table();
                                // Estamos pintando una tabla
                                dtable.clear();
                                dtable.rows.add(datos.data);
                                dtable.draw();

                                // personalizar lista
                                if (jQuery.isFunction(jQuery.personalizarLista)) {
                                    jQuery.personalizarLista(dtable);
                                }
                                // Refrescar los tooltips de la tabla
                                jQuery(tableid+'.popover-dynamic').popover({
                                    trigger: 'focus'    // Dismiss on next click
                                });

                            }
                            // GRID
                            else {
                                // estamos pintado un grid, ejecutamos la funcion que hemos recibido
                                // por parametro
                                const functionModeGrid = jQuery(tableid).data('function-mode-grid');
                                if (functionModeGrid != null) {
                                    eval(functionModeGrid)(datos);
                                }
                            }

                            // actualizar informacion de la tabla
                            jQuery.createBotonesNavegacion(tablename, pagination, columnas);

                            jQuery.updateElementosPorPagina(tableid, pagination);

                            const groupBtn = jQuery(tableid).data('group-btn');
                            if (groupBtn) {
                                jQuery.groupActionBtn(tableid);
                            }

                            const gridBtn = jQuery(tableid).data('grid_btn');
                            if (gridBtn) {
                                jQuery.gridActionBtn(tableid);
                            }

                            if (typeof loadButtons === 'function') {
                                loadButtons();
                            }

                            if (typeof loadStatusButtons === 'function') {
                                loadStatusButtons();
                            }
                            jQuery(document).trigger('endDatatableSearch', []);

                        } else {
                            // Mostrar mensaje de error
                            jQuery.messageWarning(datos.msg, 3000);
                        }

                        jQuery.loadEnd();
                    },
                    type: "POST"
                });
			}
        };


        // ----------------------------------------------------------------------
        // Imagen a mostrar
        // ----------------------------------------------------------------------
        jQuery.configureBtnOrder = function (tipoOrden) {
            if (tipoOrder == 'ASC') {
                return 'fa-sort-asc';
            } else if (tipoOrder = 'DESC') {
                return 'fa-sort-desc';
            }
            return 'fa-order';
        }


        // ----------------------------------------------------------------------
        // Crea la barra de navegacion por paginas
        // ----------------------------------------------------------------------
        jQuery.createBotonesNavegacion = function (tablename, pagination, columnas) {
            const tableid = jQuery.getTableId(tablename);
            var tablita = jQuery(tableid);

            jQuery(tableid+" tfoot").remove();
            /*<span>Registros: listItem->total() - Página listItem->currentPage() de listItem->lastPage() </span>*/

            var txtDisabledPagePrev = (pagination.current_page == 1 ? 'disabled' : '');
            var txtDisabledPageNext = (pagination.current_page == pagination.last_page ? 'disabled' : '');


            var txt = '<div class="dataTables_paginate paging_simple_numbers" id="'+tablename+'_paginate" data-page="1">' +
                '<ul class="pagination">' +
                '<li class="paginate_button previous page-item previous ' + txtDisabledPagePrev + '" aria-controls="tabla" tabindex="0">' +
                '<a class="page-link" href="javascript:jQuery.goPage(' + (pagination.current_page - 1) + ',\''+tablename+'\');">' +
                '<i class="fa fa-angle-left"></i>' +
                '</a>' +
                '</li>';

            // siempre boton primera pagina
            txt += '<li class="paginate_button page-item ' + (pagination.current_page == 1 ? 'active' : '') + ' aria-controls="tabla" tabindex="0">' +
                '<a class="page-link" href="javascript:jQuery.goPage(1,\''+tablename+'\');">1</a>' +
                '</li>';

            // boton "..." por no se correlativo
            if (pagination.current_page >= 4 && pagination.last_page > 5) {
                txt += '<li class="paginate_button page-item disabled" aria-controls="tabla" tabindex="0">' +
                    '<a href="#">…</a>' +
                    '</li>';
            }

            // botones intermedios
            var ii = Math.max(2, pagination.current_page - 1);
            var i = Math.min(ii, Math.max(5, (pagination.last_page - 3)));
            var paso = 0;
            while (i < pagination.last_page && paso < 3) {
                txt += '<li class="paginate_button page-item ' + (pagination.current_page == i ? 'active' : '') + ' aria-controls="tabla" tabindex="0">' +
                    '<a class="page-link" href="javascript:jQuery.goPage(' + i + ',\''+tablename+'\');">' + i + '</a>' +
                    '</li>';
                paso++;
                i++;
            }


            // boton "..." por no se correlativo
            if ((pagination.last_page - 3) >= pagination.current_page && pagination.last_page > 5) {
                txt += '<li class="paginate_button page-item disabled" aria-controls="tabla" tabindex="0">' +
                    '<a class="page-link" href="#">…</a>' +
                    '</li>';
            }


            // siempre boton ultima pagina (menos si ya lo hemos pintado)
            if (pagination.last_page != paso && pagination.last_page != 1) {
                txt += '<li class="paginate_button page-item ' + (pagination.current_page == pagination.last_page ? 'active' : '') + ' aria-controls="tabla" tabindex="0">' +
                    '<a class="page-link" href="javascript:jQuery.goPage(' + pagination.last_page + ',\''+tablename+'\');">' + pagination.last_page + '</a>' +
                    '</li>';
            }

            txt += '<li class="paginate_button page-item next ' + txtDisabledPageNext + '" aria-controls="tabla" tabindex="0">' +
                '<a class="page-link" href="javascript:jQuery.goPage(' + (pagination.current_page + 1) + ',\''+tablename+'\');">' +
                '<i class="fa fa-angle-right"></i>' +
                '</a>' +
                '</li>' +
                '</ul>' +
                '</div>';

            tablita.append('<tfoot class="t-right"><tr><td colspan="' + columnas + '">' + txt + '</td></tr></tfoot>');

        }


        // ----------------------------------------------------------------------
        // Va a la pagina seleccionada
        // ----------------------------------------------------------------------
        jQuery.goPage = function (page, tablename) {
            const tableid = jQuery.getTableId(tablename);
            jQuery(tableid+'_paginate').data('page', page);
            jQuery.buscar(tablename);
        }


        // ----------------------------------------------------------------------
        // Crea el combo box de elementos por pagina
        // ----------------------------------------------------------------------
        jQuery.createElementosPorPagina = function (tablename) {
            const tableid = jQuery.getTableId(tablename);
            var tablita = jQuery(tableid);

            const hideCombo = jQuery(tableid).data('hide-filter');
            const porDefecto = jQuery(tableid).data('items-by-default-select');

            jQuery(tableid+"-combo-items").remove();

            var txt = '<div class="row" id="'+tablename+'-combo-items">' +
                            '<div class="col-6">' +
                                '<div class="row">' +
                                    '<div class="col-6 col-sm-4 col-md-3">' +
                                        '<select id="'+tablename+'-select-pagination" class="form-control" data-style="white">' +
                                            '<option value="10">10</option>' +
                                            '<option value="100">100</option>' +
                                            '<option value="500">500</option>' +
                                            '<option value="1000">1000</option>' +
                                            '<option value="10000">10000</option>' +
                                        '</select> ' +
                                    '</div>	' +
                                    '<div class="col-6">' +
                                        '<span id="'+tablename+'-combo-items-select-pagination-info"></span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';

            tablita.before(txt);

            const paginationSelect = jQuery(tableid+"-select-pagination");
            paginationSelect.val(porDefecto).trigger('change');

            if (hideCombo === 1) {
                paginationSelect.addClass('hide');
                jQuery(".select-pagination-span").removeClass('hide');
            } else {
                paginationSelect.removeClass('hide');
                jQuery(".select-pagination-span").addClass('hide');
            }

            // Cambio en paginador
            paginationSelect.change(function () {
                jQuery.buscar(tablename);
            });
        }


        // ----------------------------------------------------------------------
        // Crea un combo debajo de cada filtro con las opciones de busqueda
        // ----------------------------------------------------------------------
        jQuery.busquedasAvanzadas = function (tablename) {
            const tableid = jQuery.getTableId(tablename);
            const hideCombo = jQuery(tableid).data('hide-filter');

            // ocultar
            const classHide = hideCombo === 1 ? " hide " : "";


            var txtCombo = "<select class='select-filter-opt form-control mt-1 "+ classHide +"' onchange='javascript:jQuery.buscar(\""+tablename+"\");'>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_CONTIENE }}'>@lang('canelatools::canelatools.canela-rest.table.filter.contains')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.equal')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_MAYOR }}'>@lang('canelatools::canelatools.canela-rest.table.filter.higher')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_MAYOR_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.higher_equal')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_MENOR }}'>@lang('canelatools::canelatools.canela-rest.table.filter.less')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_MENOR_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.less_equal')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_DISTINTO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.distinct')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_NO_CONTIENE }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_contains')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.empty')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_NO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_empty')</option>" +
                "</select>";

            var txtCombo2 = "<select class='select-filter-opt form-control mt-1 "+ classHide +"' onchange='javascript:jQuery.buscar(\""+tablename+"\");'>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_IGUAL }}'>@lang('canelatools::canelatools.canela-rest.table.filter.equal')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_DISTINTO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.distinct')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.empty')</option>" +
                "<option value='{{ \Canela\CanelaTools\Enums\TableFilterTypes::TIPO_FILTRO_NO_VACIO }}'>@lang('canelatools::canelatools.canela-rest.table.filter.no_empty')</option>" +
                "</select>";


            jQuery(tableid+" #tr-filters").children().each(function () {
                let isSelect = false;
                let existeControl = false;
                jQuery(this).children().each(function () {
                    isSelect = isSelect || jQuery(this).is('select');
                    existeControl = existeControl || jQuery(this).is('select') || jQuery(this).is('input');
                });
                if (existeControl) {
                    if (isSelect) {
                        jQuery(this).append(txtCombo2);
                    } else {
                        jQuery(this).append(txtCombo);
                    }
                }
            });
        }


        /**
         * Must hide advanced filters?
         */
        jQuery.hideAdvancedFilters = function(tableid) {
            const hideFilter = jQuery(tableid).data('hide-filter');
            if (hideFilter) {
                jQuery(tableid+' #tr-filters').hide();
            }
        }


        // ----------------------------------------------------------------------
        // Actualiza elementos visualizados
        // ----------------------------------------------------------------------
        jQuery.updateElementosPorPagina = function (tableid, pagination) {
            jQuery(tableid+"-combo-items-select-pagination-info")
                .html("@lang('canelatools::canelatools.canela-rest.table.register.registers')" +
                    pagination.total +
                    "@lang('canelatools::canelatools.canela-rest.table.register.page')" +
                    pagination.current_page +
                    "@lang('canelatools::canelatools.canela-rest.table.register.of')" + pagination.last_page)
        }


        // ----------------------------------------------------------------------
        // AÑADIR BOTÓN EXPORTAR EXCEL
        // ----------------------------------------------------------------------
        jQuery.setExportBtn = function (container, tableid) {
            var excelbtn = "<a role='button' class='btn btn-sm btn-success dt-action' style='float: right;' " +
                "title='@lang('canelatools::canelatools.canela-rest.table.excel')' onclick='jQuery.exportToExcel(\""+tableid+"\")'>" +
                "<i class='fa fa-file-excel'></i></a>";

            container.prepend(excelbtn);
        };


        // ----------------------------------------------------------------------
        // EXPORTAR EXCEL
        // ----------------------------------------------------------------------
        jQuery.exportToExcel = function (tableid) {
            // order bys
            jQuery(tableid+"-report-filters").val(jQuery.getFiltros(tableid));
            // Filtros
            jQuery(tableid+"-report-orderbys").val(jQuery.getOrderBy(tableid));
            //column titles
            var titles = [];
            jQuery(tableid+' tr#tr-orders th').each(function () {
                titles.push(jQuery(this).text().trim());
            });
            jQuery(tableid+'-report-titles').val(JSON.stringify(titles));

            // columnas
            var columns = jQuery(tableid).data('columns');
            let visibilities = columns.map(function(item) { return item.bVisible});
            jQuery(tableid+'-report-visibilities').val(JSON.stringify(visibilities));

            // modetable
            jQuery(tableid+'-modetable').val(jQuery(tableid).data('modetable'));

            // parametros desde el controlador
            const  parametersFilter = jQuery(tableid).data('parameter-filters');
            if (parametersFilter !== null && parametersFilter.length > 0) {
                jQuery(tableid+'-parameter-filters').val(JSON.stringify(parametersFilter));
            }

            // execute
            jQuery(tableid+'-excel-report-table').submit();
        };




        // ----------------------------------------------------------------------
        // AÑADIR BOTÓN LIMPIAR FILTROS
        // ----------------------------------------------------------------------
        jQuery.setClearFiltersBtn = function (container, tablename) {
            var clearFiltersBtn = "<a role='button' class='btn btn-sm btn-warning dt-action' style='float: right;margin-right: 10px;' " +
                "title='@lang('canelatools::canelatools.canela-rest.table.filter.clear')' onclick='jQuery.limpiarFiltros(\""+tablename+"\")'>" +
                "<i class='fa fa-filter'></i></a>";

            container.prepend(clearFiltersBtn);
        };


        // ----------------------------------------------------------------------
        // AÑADIR BOTONES LIMPIAR FILTROS Y EXPORTAR EXCEL
        // ----------------------------------------------------------------------
        jQuery.setTableTopRightBtnContainer = function (tablename) {
            const tableid = jQuery.getTableId(tablename);
            var container = jQuery("<div id='"+tablename+"-table-top-right-btn-container' class='col-6 float-right'></div>");
            jQuery(tableid+'-combo-items').append(container);
            jQuery.setClearFiltersBtn(container, tablename);
            jQuery.setExportBtn(container, tableid);
        };


        // ----------------------------------------------------------------------
        // GUARDAR FILTROS EN COOKIE
        // ----------------------------------------------------------------------


        /**
         * Get filters from storage
         */
        jQuery.cookieFilters = function(tableid) {
            const table = jQuery(tableid);
            const idTable = jQuery.getCodeTable(tableid);
            let activeTable = localStorage.getItem(_ACTIVE_TABLE);
            if (activeTable && activeTable === idTable) {
                let filtersData 		= JSON.parse(localStorage.getItem(_ACTIVE_FILTERS_DATA));
                let filtersCondition	= JSON.parse(localStorage.getItem(_ACTIVE_FILTERS_CONDITION));
                var contadorCondition = 0;
                jQuery(tableid+" #tr-filters").children().each(function () {
                    jQuery(this).children().each(function () {
                        if (jQuery(this).hasClass('search_init'))
                        {
                            const input = jQuery(this);
                            var nameInput = input.attr('name');
                            filtersData.forEach(function(data, index)
        					{
        						if (data.name == nameInput)
        						{
        							if (input.hasClass('select2'))
        							{
        	                            input.val(data.value).trigger('change');
        	                        } else {
        	                        	input.val(data.value);
        	                        }
        						}
        					});
                        }
                        else if (jQuery(this).hasClass('select-filter-opt'))
                        {
                            if (filtersCondition != null)
                            {
                                var valueCondition = filtersCondition[contadorCondition];
    							if (!("undefined" === typeof valueCondition || valueCondition == '' || valueCondition == null))
    							{
                                    jQuery(this).val(valueCondition).trigger('change');
    							}
                            }
                        	contadorCondition++;
                        }
                    });
                });
            } else {
                localStorage.setItem(_ACTIVE_TABLE, idTable);
                jQuery.saveCookieFilters(tableid);
            }
        }


        /**
         * Save active filters to cookie
         */
         jQuery.saveCookieFilters = function(tableid) {
            let filtersData = [];
            let filtersCondition = [];
            jQuery(tableid+" #tr-filters").children().each(function () {
                jQuery(this).children().each(function () {
                    if (jQuery(this).hasClass('search_init'))
                    {
                        var name = jQuery(this).attr('name');
                        if (!("undefined" === typeof name || name == '' || name == null))
                        {
                        	var variable = jQuery(this).val();
                        	if ("undefined" === typeof variable || variable == '' || variable == null)
                        	{
                            	variable = "";
                        	}
                    		filtersData.push({name: name, value: variable});
                        }
                    }
                    else if (jQuery(this).hasClass('select-filter-opt'))
                    {
                    	filtersCondition.push(jQuery(this).val());
                    }
                });
            });

			// save values
            localStorage.setItem(_ACTIVE_TABLE, jQuery.getCodeTable(tableid));
            localStorage.setItem(_ACTIVE_FILTERS_DATA, JSON.stringify(filtersData));
            localStorage.setItem(_ACTIVE_FILTERS_CONDITION, JSON.stringify(filtersCondition));
        }


        /**
         * Codigo hash que identifica a una tabla
         */
        jQuery.getCodeTable = function(tableid)
        {
        	const table = jQuery(tableid);
        	var tt = table.find("thead:first tr th");
    		var codigoTabla = "";
    		tt.each(function(index, e) {
				var id = jQuery(e).attr('id');
				if (id != undefined && id != "")
    			{
	    			codigoTabla += jQuery(e).attr('id');
	    		}
        	});
    		return JSON.stringify(codigoTabla);
    		//return jQuery.hashCode(JSON.stringify(codigoTabla));
        }

        /**
         * Hash codigo
         */
        jQuery.hashCode = function(string) {
        	var hash = 0, i, chr;
        	if (string.length === 0) return hash;
        	for (i = 0; i < string.length; i++) {
        	    chr   = string.charCodeAt(i);
        	    hash  = ((hash << 5) - hash) + chr;
        	    hash |= 0; // Convert to 32bit integer
        	}
      		return hash;
		};

        // ----------------------------------------------------------------------
        // Change urlentidad
        // ----------------------------------------------------------------------
        jQuery.changeUrlEntidad = function (tableid, url, params) {
            _URL_ENTIDAD = url;
            jQuery(tableid+'-excel-report-table').attr('action', _URL_ENTIDAD + "{{ $exportUrlQueryString ?? "" }}");
            jQuery.setParameterFilters(tableid, params);
        }

        // ----------------------------------------------------------------------
        // Set parameter filters
        // ----------------------------------------------------------------------
        jQuery.setParameterFilters = function (tableid, params) {
            jQuery(tableid).attr('data-parameter-filters', params);
        }


        /**
         * Group action buttons
         */
        jQuery.groupActionBtn = function(tableid) {
            jQuery(tableid+' tbody tr').each(function () {
                let actions = jQuery(this).find('td:last-child');
                let html = actions.html();
                actions.html('<div class="btn-group">'+ html +'</div>');
            });
        }

        /**
         * Group action buttons
         */
        jQuery.gridActionBtn = function(tableid) {
            jQuery(tableid+' tbody tr').each(function () {
                let actions = jQuery(this).find('td:last-child');
                actions.css('display', 'grid');
            });
        }


        // ----------------------------------------------------------------------
        // Reload table on status change
        // TODO: ¿como hacerla multiple?
        // ----------------------------------------------------------------------
        jQuery(document).on('endChangeStatus', function (event, data, button) {
            //jQuery.buscar();
        });

    });

</script>
