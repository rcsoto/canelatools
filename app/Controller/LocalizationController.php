<?php

namespace Canela\CanelaTools\Controller;

use Canela\CanelaTools\Manager\LocalizationManager;
use Exception;
use Illuminate\Http\RedirectResponse;

class LocalizationController extends BasicController
{
    /**
     * @param $locale string
     * @return RedirectResponse
     * @throws Exception
     */
    public function changeLocale(string $locale): RedirectResponse
    {
        LocalizationManager::storeLocaleInSession($locale);
        return redirect()->back();
    }
}
