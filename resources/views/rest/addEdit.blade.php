<?php
use Canela\CanelaTools\Manager\TranslationManager;
use Canela\CanelaTools\Models\BasicModel;

/** @var Canela\CanelaTools\Models\Table\TableRest $tableRest */
/** @var BasicModel $register */

// valores por defecto
$title = $tableRest->title ?? "??";
$comesFromSelect = request()->has('url_back') && request()->has('field_id') && request()->has('k') && request()->has('h');
$recoverModifiedData = request()->has('h') && !request()->has('url_back') && !request()->has('field_id') && !request()->has('k');
$redirectUrl = $comesFromSelect ? request()->get('url_back') : $tableRest->urlIndex;
$entityId = !empty($register) ? $register->entityModel()?->id : null;
$webLanguages = !empty($register) ? $register->translationLanguages() : collect();
$translatedLanguages = !empty($register) ? TranslationManager::getTranslatedLanguages($entityId, $register->id) : collect();

    /** @var String $nameSectionBreadcrumbs nombre de la seccion de las migas de pan */
    $nameSectionBreadcrumbs = $nameSectionBreadcrumbs ?? config('canelatools.blade.section.breadcrumbs');

    /** @var String $nameSectionContent nombre de la seccion principal */
    $nameSectionContent = $nameSectionContent ?? config('canelatools.blade.section.content');

    /** @var String $nameSectionScript nombre de la seccion donde se a�ade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
?>

@extends($tableRest->layoutsMaster)


@section('breadcrumbs')
    @include('canelatools::rest.layouts.addEdit-breadcrumbs')
@endsection


@section('content')

    @yield('custom-content')

    @sectionMissing('custom-content')
        <div class="card">
            <div class="card-block">
                @include('canelatools::rest.layouts.form', compact('tableRest', 'entityId', 'webLanguages', 'translatedLanguages'))
            </div>
        </div>

        @isset($register)
            <div class="row">
                <div class="col-12">
                    @include($tableRest->layoutsDateState, array('registro' => $register))
                </div>
            </div>
        @endisset
    @endif
@stop

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">
        jQuery(document).ready(function () {

            @if ($recoverModifiedData)
                jQuery.getStoredLocalData("modelForm", "{{$tableRest->getLocalStorageEntityKeyName()}}", "{{ request()->get('h') }}");
            @endif

        	jQuery('#modelForm').find('a[data-action="submit"]').click(function(event) {
                event.preventDefault();
                jQuery.saveForm('#modelForm', function(data) {
                    @if ($comesFromSelect)
                        jQuery.updateStoredData("{{ request()->get('k') }}", "{{ request()->get('h') }}", "{{ request()->get('field_id') }}", data.model.id);
                    @endif
                    window.location.href = "{{ $redirectUrl }}";
                }, null, true);
            });

        });
    </script>
@endsection
