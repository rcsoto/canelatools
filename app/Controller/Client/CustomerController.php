<?php

namespace Canela\CanelaTools\Controller\Client;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Client\Client;
use Canela\CanelaTools\Models\Client\Customer;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Client\CustomerRequest;
use Canela\CanelaTools\Traits\Fileable;
use DB;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use stdClass;

/**
 * Class CustomerController
 * @package Canela\CanelaTools\Controller\Client
 */
class CustomerController extends BasicController
{
    use HtmlLists, Fileable;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->entidad      = Customer::class;
        $this->routeEntity  = 'admin.client.customer';

        $this->vistaLista   = 'canelatools::rest.list';
        $this->vistaEdit    = 'canelatools::rest.addEdit';
        $this->vistaAdd     = 'canelatools::rest.addEdit';

        $this->menu         = AdminMenu::CUSTOMER;
        $this->menu_group   = AdminMenu::GROUP_CLIENT;

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.customer.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.customer.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'id'
                ),
                new TableColumn(
                    table: 'client',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.client_id'),
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.name'),
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'map_latitude',
                    label: trans('canelatools::canelatools.entity.customer.field.map_latitude'),
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'map_longitude',
                    label: trans('canelatools::canelatools.entity.customer.field.map_longitude'),
                ),
                new TableColumn(
                    table: 'web',
                    fieldOrder: 'id',
                    label: trans('canelatools::canelatools.entity.customer.field.web_id'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'client_id',
                    label: trans('canelatools::canelatools.entity.customer.field.client_id'),
                    required: true,
                    comboList: Client::comboList(),
                    readOnly: true,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.name'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'business_name',
                    label: trans('canelatools::canelatools.entity.customer.field.business_name'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'map_latitude',
                    label: trans('canelatools::canelatools.entity.customer.field.map_latitude'),
                    type: TableColumnTypes::REAL,
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'map_longitude',
                    label: trans('canelatools::canelatools.entity.customer.field.map_longitude'),
                    type: TableColumnTypes::REAL,
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'image_logo',
                    label: trans('canelatools::canelatools.entity.customer.field.image_logo'),
                    type: TableColumnTypes::IMAGE_CROPPER,
                    fileDir: Customer::DIR_LOGO,
                    fileName: Customer::LOGO_FILENAME,
                ),
                new TableForm(
                    name: 'image_logo_dark',
                    label: trans('canelatools::canelatools.entity.customer.field.image_logo_dark'),
                    type: TableColumnTypes::IMAGE_CROPPER,
                    fileDir: Customer::DIR_LOGO,
                    fileName: Customer::LOGO_DARK_FILENAME,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $consulta = Customer::select([  'customer.id',
                                        'client.name as client_name',
                                        'customer.name',
                                        'customer.map_latitude',
                                        'customer.map_longitude',
                                        DB::raw('IF(customer.web_id IS NULL,0,1) as has_web'),
                                        'customer.id as action_customer_id',
                                        'customer.web_id as customer_web_id',
                                    ])
                          ->join('client', 'customer.client_id','client.id');

        return $this->setSecurityList($consulta);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            switch ($key) {
                case 'action_customer_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, false, false, false,
                        false, false, 0, 0, true,
                        true);

                    // Add web button.
                    $redirectTo = route('admin.client.customer.index');
                    $url = empty($jsonItem->customer_web_id)
                            ? route('admin.webbuilder.web.create', ['customer_id' => $jsonItem->id, 'redirect_to' => $redirectTo])
                            : route('admin.webbuilder.web.edit', ['web' => $jsonItem->customer_web_id, 'redirect_to' => $redirectTo]);
                    $txt .= $this->addGenericButton(url: $url, title: trans('canelatools::canelatools.entity.customer.field.web_id'), colour: 'btn-warning', showLabels: true);
                    break;
                case 'has_web':
                    $txt = Customer::booleanToTextStatic($value);
                    break;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CustomerRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(CustomerRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CustomerRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(CustomerRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }

}
