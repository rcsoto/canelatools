<?php

namespace Canela\CanelaTools\Enums;

use ReflectionClass;

/**
 * Class Enum
 * @package App\Http\Enums
 */
abstract class Enum
{
    /**
     * Get all class const keys.
     *
     * @return array
     * @throws \ReflectionException
     */
    static function getKeys()
    {
        $class = new ReflectionClass(get_called_class());
        return array_keys($class->getConstants());
    }


    /**
     * isInEnum
     *
     * @param mixed $value
     * @param array $enum
     * @return bool
     * @throws \ReflectionException
     */
    static function isInEnum($value, $enum)
    {
        $class = new ReflectionClass(get_called_class());
        // return in_array($value, $class->getStaticPropertyValue($enum)); //TODO: TEST
        return in_array($value, $class->getStaticProperties());
    }
}
