<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 *
 * @property int $customer_id
 * @property int $state_id
 * @property int $template_id
 * @property string redirect_to
 */
class WebRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'customer_id',
        'state_id',
        'template_id',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'customer_id'       => 'required|integer|min:1|exists:customer,id',
            'state_id'          => 'required|integer|min:1|exists:web_state,id',
            'template_id'       => 'required|integer|min:1|exists:web_template,id',
        );

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['customer_id'] = trans('canelatools::canelatools.entity.customer.report');
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');

        return $attributes;
    }

}
