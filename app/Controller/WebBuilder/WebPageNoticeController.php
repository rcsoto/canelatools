<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use App\Models\Client\Customer;
use App\Models\General\Archive;
use App\Models\General\ArchiveType;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\WebNoticeType;
use App\Models\Project\WebBuilder\WebPage;
use App\Models\Project\WebBuilder\WebPageCategory;
use App\Models\Project\WebBuilder\WebPageNotice;
use App\Models\Project\WebBuilder\WebPageNoticeArchive;
use App\Models\Project\WebBuilder\WebPageType;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Requests\WebBuilder\WebPageNoticeRequest;
use Canela\CanelaTools\Traits\ArchivesAssociatable;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageNoticeController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageNoticeController extends BasicController
{
    use HtmlLists, Fileable, ArchivesAssociatable;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageNotice::class;
        $this->routeEntity = 'admin.webbuilder.webpagenotice';
        $this->archivesEntity = WebPageNoticeArchive::class;

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_notice.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_notice.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_notice',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: Customer::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.page_id'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: WebPage::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_notice_type',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_page.field.type_id'),
                    fieldFilter: 'id',
                    comboList: WebNoticeType::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page_notice',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.title'),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'subject',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.subject'),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'date',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.date'),
                    type: TableFilterTypes::FILTRO_TIPO_DATE,
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.language.field.state_id'),
                    fieldFilter: 'id',
                    comboList: State::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'image_thumb',
                    label: trans('canelatools::canelatools.general.field.image'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.page_id'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'type_id',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.type_id'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                    comboList: WebNoticeType::comboTranslate(),
                ),
                new TableForm(
                    name: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.title'),
                    required: true,
                    type: TableColumnTypes::TEXT,
                    max: 255,
                ),
                new TableForm(
                    name: 'subject',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.subject'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
                new TableForm(
                    name: 'author',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.author'),
                    type: TableColumnTypes::TEXT,
                    containerClass: 'col-6 mb-2',
                    max: 255,
                ),
                new TableForm(
                    name: 'date',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.date'),
                    type: TableColumnTypes::DATE,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'body',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.body'),
                    type: TableColumnTypes::WYSIWYG,
                ),

                new TableForm(
                    name: 'coming_soon',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.coming_soon'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'important',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.important'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'is_live',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.is_live'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'open_in_new_window',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.open_in_new_window'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'external_link',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.external_link'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'slug',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.slug'),
                    max: 255,
                ),
                (new TableForm(
                    name: 'categories',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.category'),
                    required: false,
                    containerClass: 'col-12 col-sm-6',
                ))->setComboList(WebPageCategory::comboTranslate(['page_id is null']), true, 'categoryIds'),
                new TableForm(
                    name: 'tags',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.tags'),
                    type: TableColumnTypes::TAGS,
                ),
                new TableForm(
                    name: 'seo_title',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.seo_title'),
                    type: TableColumnTypes::TEXT,
                    max: 255,
                ),
                new TableForm(
                    name: 'seo_description',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.seo_description'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
                (new TableForm(
                    name: 'file_thumb_id',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.file_thumb_id'),
                    containerClass: 'col-12 col-sm-6',
                    comboList: Archive::comboTranslate(['type_id='.ArchiveType::IMAGE]),
                    archiveType: ArchiveType::IMAGE,
                    nullable: true,
                ))->setAddNewBtnCreateRoute('admin.general.archive.create'),
                (new TableForm(
                    name: 'file_thumbnail_id',
                    label: trans('canelatools::canelatools.entity.web_page_notice.field.file_thumbnail_id'),
                    containerClass: 'col-12 col-sm-6',
                    comboList: Archive::comboTranslate(['type_id='.ArchiveType::IMAGE]),
                    archiveType: ArchiveType::IMAGE,
                    nullable: true,
                ))->setAddNewBtnCreateRoute('admin.general.archive.create'),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang     = App::getLocale();
        $query = WebPageNotice::select([
            'web_page_notice.id',
            'customer.name as customer_name',
            'web_notice_type.name as web_notice_type_name',
            'web_page.title as web_page_title',
            'web_page_notice.title as web_page_section_title',
            'web_page_notice.subject',
            DB::Raw('DATE_FORMAT(web_page_notice.date, "'.WebPageNotice::formatDate().'") as web_page_notice_date'),
            DB::Raw((Language::DEFAULT_CODE == $lang ? "state.name" : "trans('" . $lang . "', 'state', state.id, 'name', state.name)") . " as state_name"),
            'archive.archive as image_thumb',
            'web_page_notice.id as action_web_page_notice_id',
            'state.id as state_id',
        ])
        ->join('web_page', 'web_page_notice.page_id', 'web_page.id')
        ->join('web', 'web_page.web_id', 'web.id')
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id')
        ->join('state', 'web_page_notice.state_id', 'state.id')
        ->leftJoin('web_notice_type', 'web_page_notice.type_id', 'web_notice_type.id')
        ->leftJoin('archive', 'web_page_notice.file_thumbnail_id', 'archive.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch($key) {
                case 'action_web_page_notice_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, true, false, true, true, false,
                        BasicModel::TYPE_CHANGE_STATE, $jsonItem->state_id,
                        true, true);
                    break;
                case 'image_thumb':
                    if (isset($value)) {
                        $txt = '<div class="text-center">'.$this->getListImage(url: asset(config('canelatools.folder.storage').Archive::DIR_ARCHIVES.$value), classes: 'list-image-thumb').'</div>';
                    }
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
                    break;
                default:
                    $txt = $value;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_NOTICE));
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, $id)
    {
        $webPageNotice = WebPageNotice::findOrFail($id);
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_NOTICE));
        return parent::edit($request, $id);
    }


    /**
     * getWebPageNoticeHoraryTableRest
     *
     * @return TableRest
     */
    private function getWebPageNoticeHoraryTableRest() {
        return (new WebPageNoticeHoraryController())->tableRest;
    }


    /**
     * @param $register
     * @param $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setParamReturn($register, $request) {
        $webPageNoticeHoraryTableRest = $this->getWebPageNoticeHoraryTableRest();
        $webPageNoticeHoraryTableRest->parameterFilters['notice_id'] = $register->id;
        return view($this->vistaEdit, compact('register', 'webPageNoticeHoraryTableRest'))
                ->with('urlPrefix', $this->urlPrefix)
                ->with('tableRest', $this->tableRest);
    }


    /**
     * @param WebPageNoticeRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageNoticeRequest $request) {
        return $this->storeBasic($request);
    }
    public function storePre($model, $request) {
        return $this->saveAttr($model, $request);
    }


    /**
     * @param WebPageNoticeRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageNoticeRequest $request, int $id) {
        return $this->updateBasic($request, $id);
    }
    public function updatePre($model, $request) {
        return $this->saveAttr($model, $request);
    }

    /**
     * Guardado de relaciones
     * @param $model
     * @param $request
     * @return mixed
     */
    public function saveAttr(WebPageNotice $model, $request) {
        // slug
        $model->slug = empty($model->slug) ? Str::slug($model->title) : Str::slug($model->slug);
        //categories
        $model->syncBasic($request, 'categories', 'categories');

        return $model;
    }
}
