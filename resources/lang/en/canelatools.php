<?php

return [

    /*
     |--------------------------------------------------------------------------
     | General Tags
     |--------------------------------------------------------------------------
     |
     | canela
     |
     */

    // GENERAL
    'general' => [

                'message' => [
                        'send_correct' => 'Message sent correctly',
            'send_error' => 'Error sending message',
        ],

        'actions' => [
            'exit' => 'Exit',
            'logout' => 'Logout',
            'edit' => 'Edit',
                        'select' => 'Select',
                        'save' => 'Save',
            'disabled' => 'Marked for deletion',
            'enabled' => 'Restore',
                        'send' => 'Send',
        ],
                'label' => [
                        'attention' => 'Attention!',
                        'information' => 'Information',
            'success' => 'Request processed successfully',
                        'trace_new' => 'Registration of :model_name',
            'trace_update' => 'Edit :model_name',
            'date' => 'Date of registration',
            'last_date' => 'Last modified date',
            'loading' => 'Loading ...',
            'select' => 'Select',
            'select_items_and_click_continue' => 'Select items and click continue.',
                        'cookie-policy' => 'Cookie Policy',
                        'legal-warning' => 'Legal notice',
            'data-protection' => 'Data protection',
                        'privacy-policy' => 'Privacy policy',
            'read_more' => ' ...read more',
            'accept' => 'Accept',
            'accept-privacy-policy' => 'Accept Privacy Policy',
                        'have-read-and-accept-privacy-policy' => 'I have read and accept the <a href=":route" target="_blank">Privacy Policy</a>',
            'must-accept-privacy-policy' => 'You must accept the Privacy Policy',
            'query' => 'Query',
        ],
    ],


        // Authentication
    'auth' => [
        'email' => [
            'change_password' => 'Change Password',
            'back_login' => 'Back to login',
            'send_reminder' => 'Send reminder',
        ],
        'login' => [
            'init' => 'Login',
            'remember' => 'Remember',
            'forgotten_password' => 'Forgot password',
            'enter' => 'ENTER',
        ],
    ],


    /*
     |--------------------------------------------------------------------------
     | General Tags
     |--------------------------------------------------------------------------
     |
     | general
     |
     */

    'bool' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'ajax' => [
        'unsupported' => 'Operation not supported',
    ],
        'model' => [
                'list' => ['correct' => 'Logs listed correctly.',
                                 'error' => 'An error occurred while listing logs.',],
                'update' => ['correct' => ':value updated successfully.',
                                 'error' => 'An error occurred while updating the log',],
                'store' => ['correct' => ':value created successfully.',
                                 'error' => 'An error occurred while creating the log',],
                'find' => ['error' => 'Log not found',],
                'delete' => ['correct' => ':value deleted successfully.',
                                 'error' => 'An error occurred while deleting the log',],
                'copy' => ['correct' => ':value duplicated correctly.',
                                 'error' => 'An error occurred while duplicating the log',],
        'alignment' => ['centre' => 'Centred',
                                 'left' => 'Left',
                                 'right' => 'Right',],
                'vertical_alignment' => ['medium' => 'Medium',
                                 'bottom' => 'Bottom/normal',
                                 'top' => 'Superscript',],
        'reorder' => ['reorder_ok' => 'Reordered successfully.',
                                                                  'reorder_error' => 'An error occurred while reordering.',],
                'update_status' => ['correct' => 'Status has been updated successfully.',
                                 'error' => 'An error occurred while updating the status.',],
                'merge' => ['correct' => 'Logs were merged successfully.',
                                 'error' => 'An error occurred while merging logs.',],
        'generic_error' => 'An error has occurred. Please try the operation again. If the error persists, please contact support',
    ],
    'file' => [
                'upload_ok' => 'File successfully uploaded',
        'upload_error' => 'An error occurred while uploading the file.',
    ],
    'buttons' => [
        'accept' => 'Accept',
        'approve' => 'Approve',
        'back' => 'Back',
        'cancel' => 'Cancel',
        'continue' => 'Continue',
        'close' => 'Close',
        'copy' => 'Copy',
        'delete' => 'Delete',
        'edit' => 'Edit',
        'new' => 'New',
        'preview' => 'Preview',
        'refuse' => 'Refuse',
        'save' => 'Save',
        'send' => 'Send',
        'show' => 'Query',
        'update' => 'Update',
        'activate' => 'Activate',
        'deactivate' => 'Low',
        'disable' => 'Disable',
        'execute' => 'Execute',
        'upload' => 'Upload',
        'download' => 'Download',
        'share' => 'Share',
                'prev' => 'Previous',
        'next' => 'Next',
    ],

    'select' => [
                'placeholder' => 'Select a value ...',
    ],


    /*
     |--------------------------------------------------------------------------
     | Page ERRORS
     |--------------------------------------------------------------------------
     */
    '401' => [
                'title' => 'Unauthorised',
                'message' => 'You are not authorised to access this log.',
    ],
    '403' => [
                'title' => 'No permissions',
                'message' => 'You do not have permission to access this log.',
    ],
    '404' => [
                'title' => 'Page not found',
                'message' => 'This page does not seem to exist or has been deleted.',
    ],
    '419' => [
                'title' => 'Page expired',
                'message' => 'Page has expired. Your credentials have expired.',
    ],
    '429' => [
                'title' => 'Too many requests',
                'message' => 'Too many requests.',
    ],
    '500' => [
                'title' => 'Error/Incident',
        'message' => 'An error has occurred on the server. Please try again. If the error persists, please contact support',
    ],
    '503' => [
                'title' => 'Maintenance',
                'message' => 'We are currently down for maintenance. We will be back in a few minutes. Thank you',
    ],
    '550' => [
                'title' => 'Error/Incident',
        'message' => 'An error has occurred on the server. Please try again. If the error persists, please contact support',
    ],

    /*
     |--------------------------------------------------------------------------
     | Update Browser
     |--------------------------------------------------------------------------
     */

    'updateBrowser' => [
        'title' => ':project Obsolete browser',
        'obsolete_browser' => 'If you are viewing this page it is because your browser is obsolete',
                'update_version_browser' => 'For your safety and to view modern web pages to their full potential, we recommend that you upgrade to the latest version of your preferred browser.',
                'important' => 'Important',
        'browser_funcionality' => 'Check if the functionality is enabled in your browser',
                'compability_view' => 'Compatibility view',
        'disable' => 'if so, you must disable it to view the page',
        'more_information' => 'More information here',
        'google_chrome' => 'Google Chrome',
                'firefox' => 'Firefox',
        'explorer' => 'Microsoft Edge',
                'update_visit' => "After you've updated, don't forget to come back and visit us!",
                'rights_reserved' => 'ALL RIGHTS RESERVED',
        'show_image' => 'Show image',
                'url_chrome' => 'https://www.google.com/intl/es/chrome/browser/?hl=es',
                'url_firefox' => 'http://www.mozilla.org/es-ES/firefox/fx/',
                'url_edge' => 'https://support.microsoft.com/es-es/help/4501095/download-the-new-microsoft-edge-based-on-chromium',
    ],


];
