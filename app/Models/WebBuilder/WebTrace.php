<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\User\User;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class WebTrace
 * @package Canela\CanelaTools\Models\WebBuilder
 *
 * @property int $web_id
 * @property int $state_old_id
 * @property int $state_new_id
 * @property int $user_id
 * @property string $comment
 */
class WebTrace extends BasicModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_trace';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'web_id',
        'state_old_id',
        'state_new_id',
        'user_id',
        'comment',
    ];

    /**
     * Related order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function web()
    {
        return $this->belongsTo(Web::class, 'web_id');
    }


    /**
     * Related state old.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stateOld()
    {
        return $this->belongsTo(WebState::class, 'state_old_id');
    }


    /**
     * Related state new.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stateNew()
    {
        return $this->belongsTo(WebState::class, 'state_new_id');
    }

    /**
     * Related user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * Get last trace model.
     *
     * @param Builder $query
     * @param integer $orderId
     * @param integer $stateId
     * @return OrderTrace|null
     */
    public function scopeTraceLastState($query, $webId, $stateId)
    {
        return $query->where('web_id', $webId)
                     ->where('state_new_id', $stateId)
                     ->orderBy('id', 'desc')
                     ->first();
    }

}
