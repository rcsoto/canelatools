<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;

class WebPageGallery extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'position',
        'mame',
        'subtitle',
        'tags',
        'text_external_link',
        'external_link',
        'open_in_new_window',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id'           => 'int',
        'position'          => 'int',
        'name'              => 'string',
        'subtitle'          => 'string',
        'tags'              => 'string',
        'text_external_link'=> 'string',
        'external_link'     => 'string',
        'open_in_new_window'=> 'bool',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page() {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * getUsersIdsAtttribute
     *
     * @return array
     */
    public function getTagIdsAttribute()
    {
        return $this->users->tags('id')->toJson();
    }


}
