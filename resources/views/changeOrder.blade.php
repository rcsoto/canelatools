﻿{{----------------------------------------------------------------------------------------------------------
                                                TRAIT DOCS:

   ----- CONTROLLER -----

   Model:: (...data...)
            ->each(function (Model $model) {
                  $model->img        = 'url';
                  $model->titleRow   = 'title';
                  $model->background = 'color bg';
                  $model->color      = $brand->app_brand_title_color;
              });

        return view('administrator.brand.changeOrder', compact('items'));


   ----- PARAMETERS -----
   * img: Required. image src.
   * titleRow: Required. Text for title
   * background: Optional. Default white
   * color: Optional. Default black

----------------------------------------------------------------------------------------------------------}}
@php
    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
@endphp


<div class="row sortable">
    @foreach($items as $item)
        <div class="col-md-2 col-sm-4" data-id="{{$item->id}}">
            <div class="panel"
                 style="background-color: {{$item->background ? $item->background : 'transparent'}}">
                <div class="panel-header text-center"
                     style="color: {{$item->color ? $item->color : 'inherit'}};
                             height: 40px;">
                    <h3><i class="fa fa-arrows" aria-hidden="true"></i>
                        {{ucwords($item->titleRow)}}</h3>
                </div>
                <div class="panel-content p-t-10">
                    <div class="text-center" style="height: {{isset($item->img)? '150' : '50'}}px">
                        @if(isset($item->img) && !empty($item->img))
                            <img src="{{$item->img}}" alt="{{ucwords($item->titleRow)}}" class="img-responsive"
                                 style="max-height: 150px; margin: auto">
                        @elseif(isset($item->description))
                            <span style="margin: auto">{{ucwords($item->description)}}</span>
                        @else
                            <span style="margin: auto">{{ucwords($item->titleRow)}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {
            jQuery('.loader-overlay').addClass('loaded');

            $('.sortable').sortable({
                connectWith: "ul",
                handle: ".panel-header",
                start: function (event, ui) {
                    var start_pos = ui.item.index();
                    ui.item.data('start_pos', start_pos);
                },
                stop: function (event, ui) {
                    newOrder(ui);
                }
            });

            function newOrder(ui) {
                var elementList = [];
                $('.sortable').find('.panel').each(function (pos) {
                    elementList.push({item: $(this).parent().attr('data-id'), position: ++pos});
                });
                var url = '{{$url}}';
                var formdata = new FormData;
                formdata.append("_token", window.Laravel.csrfToken);
                formdata.append("item_list", JSON.stringify(elementList));

                jQuery.ajax({
                    url: url,
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: formdata,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    type: "POST",
                    beforeSend: function () {
                        jQuery('.loader-overlay').removeClass('loaded');
                    },
                    error: function (object1, descr_error, object2) {
                        var msg = jQuery.procesarRespuestaAjax(object1, descr_error, object2);
                        jQuery('.loader-overlay').addClass('loaded');
                        jQuery.messageError(msg);
                    },
                    success: function (data) {
                        jQuery('.loader-overlay').addClass('loaded');
                        if (data.error === false) {
                            jQuery.messageInfo(data.msg, 4000);
                        }
                        else {
                            jQuery.messageError(data.msg);
                        }
                    }
                });
            }
        });
    </script>
@endsection
