<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\State;
use Illuminate\Support\Collection;

class WebPageContactType extends BasicModel
{
    const PHONE = 1;
    const FAX = 2;
    const WHATSAPP = 3;
    const ADDRESS = 4;
    const CONTACT_PERSON = 5;
    const EMAIL = 6;
    const TIMETABLE = 7;

    /**
     * Database table name.
     * @var string
     */
    const ENTITY_TABLE = 'web_page_contact_type';


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = self::ENTITY_TABLE;

    public $tableIdentifierField = 'contact';
    public $comboIdentifierField = 'contact';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'icon',
        'position',
        'contact',
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'state_id'          => 'int',
        'position'          => 'int',
        'icon'              => 'string',
        'contact'           => 'string',
    ];


    /** @var string $routePath Model route path */
    protected static string $routePath = 'admin.webbuilder.webpagecontacttype';


    /** @var string $resourceTransPath Model resource translations path */
    protected static string $resourceTransPath = 'canelatools::canelatools.entity.web_page_contact_type.';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state() {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages(Web::firstOrFail()->id);
    }

}
