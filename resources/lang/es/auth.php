<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API header
    |--------------------------------------------------------------------------
    |
    | Headers validation
    |
    */
    'api_no_compat'       => 'Versión de Api no soportada',
    'compilation_invalid' => 'Por favor, actualice su aplicación para poder continuar',
    'auth_invalid'        => 'Falta el tipo de autorización',
    'headers_incomplete'  => 'Las cabeceras de la petición están incompletas',
    'headers_error'       => 'Hay un problema en las cabeceras de la petición',

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'             => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle'           => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'unauthorized'       => 'No está autorizado para acceder a los registros.',
    'password_pernod'    => 'Su Password debe ser modificado en su cuenta de Pernod Ricard',
    'signed_url_expired' => 'La url ha caducado. Vuelva a solicitar acceso.',

    /*
     |--------------------------------------------------------------------------
     | User
     |--------------------------------------------------------------------------
     |
     */
    "verification"       => [
        "error"    => "No se ha podido activar el usuario",
        "correcto" => "Usuario activado correctamente",
    ],
    "login"              => [
        "disabled"     => "El usuario está deshabilitado.",
        "invalidCred"  => "credenciales de usuario inválidas.",
        "userNoExist"  => "No existe ningún usuario con ese nombre.",
        "noToken"      => "No se puede generar el token de usuario.",
        "invalidEmail" => "El email es inválido.",
        "blocked"      => "El usuario se encuentra bloqueado.",
    ],

    'messages' => [
        'remember_send_ok' => 'Te hemos mandado un correo de recordatorio de contraseña. ' .
                              'Por favor, revisa tu bandeja de entrada.',
    ],
    'view'     => [
        'login'           => 'Iniciar sesión',
        'keep_session'    => 'Mantener la sesión abierta',
        'forgot_pwd'      => '¿Olvidaste la contraseña?',
        'log_in'          => 'Entrar',
        'already_account' => '¿Tienes una cuenta? Accede',
        'send_reminder'   => 'Enviar recordatorio',
    ],
    'password' => [
        'new_password' => 'Iniciar sesión',
    ],
    "role"     => [
        "forbidden" => "No tiene privilegios para realizar esta acción.",
    ],
];
