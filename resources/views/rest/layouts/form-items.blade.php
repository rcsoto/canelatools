<?php
use \Canela\CanelaTools\Models\Language;
/* @var Canela\CanelaTools\Models\Table\TableRest $tableRest */
$allLanguages = Language::activesCheckingPrincipal();
?>

<div class="row">
    @foreach($tableRest->tableForm as $info)
        <?php /* @var Canela\CanelaTools\Models\Table\TableForm $info */ ?>
        @if($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::CHECKBOX)

            <div class="form-group form-check {{ $info->containerClass }}">

                {!! Form::checkbox($info->name, 1, $info->value, ['class' => 'form-check-input', 'id' => $info->id, 'disabled' => $info->readOnly]) !!}
                {!! Form::label($info->id, $info->label, array('class' => 'form-check-label'.($info->isRequired() ? ' required' : ''))) !!}

            </div>

        @else

            <div id="canela-input-file-container-{{$info->id}}"
                class="form-group {{ $info->containerClass }}{{ \Canela\CanelaTools\Enums\TableColumnTypes::isFileType($info->type) ? ' canela-input-file-container' : '' }}{{ $info->type == \Canela\CanelaTools\Enums\TableColumnTypes::HIDDEN ? 'm-0' : '' }}">
                @if(!is_null($info->label))
                    {!! Form::label($info->id, $info->label, array('class' => 'canela-input-text'.($info->isRequired() ? ' required' : ''))) !!}
                @endif

                @if (\Canela\CanelaTools\Enums\TableColumnTypes::countable($info->type) && $info->charCountVisible)
                    <span class="float-right canela-input-text" data-name="{{$info->id}}"></span>
                @endif

                @if(isset($info->max))
                    <span class="float-right">&nbsp;max: {{$info->max}}&nbsp;</span>
                @endif
                @if(isset($info->min))
                    <span class="float-right">&nbsp;min: {{$info->min}}&nbsp;</span>
                @endif

                <?php $field = $info->name ?>
                @if($info->readOnly)
                    @if ($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::SELECT2)
                        {{ Form::select($info->name, $info->comboList, $info->value,
                                    array('class' => 'form-control select2', 'data-search' => 'true', 'id' => $info->id, 'disabled' => 'disabled')) }}
                                    <a href="#">Nuevo</a>
                    @else
                        <p class='form-control input-no-edit'>{{ empty($register) ? "" : $register->$field }}</p>
                    @endif
                @else
                    @if($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::HIDDEN)

                        {!! Form::hidden($info->name, $info->value, array('class' => 'form-control canela-input-text', 'id' => $info->id,
                            'maxlength' => ($info->max ?? null) )) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::TEXT)

                        {!! Form::text($info->name, $info->value, array('class' => 'form-control canela-input-text', 'id' => $info->id,
                            'maxlength' => ($info->max ?? null) )) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::TAGS)
                        {!! Form::text($info->name, $info->value, array('class' => 'form-control canela-input-text tagsinput', 'id' => $info->id,
                           'data-role' => 'tagsinput' )) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::DATE)

                        {!! Form::date($info->name, $info->value, array('class' => 'form-control', 'id' => $info->id)) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::TIME)

                        {!! Form::time($info->name, $info->value, array('class' => 'form-control', 'id' => $info->id)) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::TEXTAREA)

                        {!! Form::textarea($info->name, $info->value, array('class' => 'form-control canela-input-text', 'id' => $info->id, 'rows' => '3',
                            'maxlength' => ($info->max ?? null) )) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::WYSIWYG)

                        {!! Form::textarea($info->name, $info->value, array('class' => 'form-control canela-input-text classic-editor', 'id' => $info->id,
                            'maxlength' => ($info->max ?? null) )) !!}

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::SELECT2)

                        <div class="combo-with-btn-add-new-container">
                            @if($info->comboMultiple)
                                <?php $fieldIds = $info->comboSelectedIds ?>
                                {{ Form::select($info->name.'[]', $info->comboList,
                                    (empty($register) ? ($info->value ?? []) : $register->$fieldIds),
                                        array('class' => 'form-control select2', 'data-search' => 'true', 'id' => $info->id, 'multiple' => 'multiple')) }}
                            @else
                                <?php
                                    $selectAttributes = ['class' => 'form-control select2', 'data-search' => 'true', 'id' => $info->id, 'placeholder' => trans('canelatools::canelatools.select.placeholder')];
                                    if ($info->isNullable()) {
                                        $selectAttributes['data-nullable'] = 'true';
                                    }
                                ?>
                                {{ Form::select($info->name, $info->comboList, $info->value, $selectAttributes) }}
                            @endif

                            @if ($info->getHasAddNewBtn())
                                <a href='javascript:jQuery.storeDataLocallyAndGoToUrl("modelForm", "{{$tableRest->getLocalStorageEntityKeyName()}}", "{{route($info->getAddNewBtnCreateRoute(), ['field_id' => $info->getAddNewBtnFieldId(), 'url_back' => url()->current(), 'k' => $tableRest->getLocalStorageEntityKeyName()])}}")'
                                    class="btn btn-success btn-sm btn-add-new" title="@lang('canelatools::canelatools.general.label.add-new')"><i class="fa fa-plus"></i></a>
                            @endif
                        </div>

                    @elseif(\Canela\CanelaTools\Enums\TableColumnTypes::isFileType($info->type))

                        @include('canelatools::rest.layouts.file-input', [
                            'type' => $info->type,
                            'fileId' => $info->id,
                            'fileName' => $info->name,
                            'fileDir' => $info->getFileDir(),
                            'fileValue' => (empty($register) ? null : $register->$field)
                        ])

                    @elseif($info->type == \Canela\CanelaTools\Enums\TableColumnTypes::REAL || $info->type == \Canela\CanelaTools\Enums\TableColumnTypes::INT)

                        {!! Form::number($info->name, $info->value, array('class' => 'form-control text-right', 'id' => $info->id,
                            'min' => ($info->min ?? null), 'max' => ($info->max ?? null) )) !!}

                    @endif

                    @if(!empty($register) && \Canela\CanelaTools\Enums\TableColumnTypes::translatable($info->type))
                        <ul class="languages-flags-list">
                            <?php
                            /** @var \Illuminate\Support\Collection $webLanguages */
                            $languages = $info->isAllLanguages() ? $allLanguages : $webLanguages;
                            ?>
                            @foreach($languages as $language)
                                <?php
                                    /** @var \Illuminate\Support\Collection $translatedLanguages */
                                    /** @var  Language $language */
                                $translationIconClass = $translatedLanguages->contains(function ($item) use ($info, $language) {
                                    return $item->attribute == $info->name && $item->id == $language->id;
                                }) ? ' fa-check text-success' : ' fa-times text-danger' ?>
                                <li><a id="{{$info->id}}-{{$language->code}}" class="translate-link" title="{{$language->name}}"
                                       data-id="{{$info->id}}" data-type="{{$info->type}}" data-attribute="{{$info->name}}"
                                       data-label="{{$info->label}}" data-language="{{$language->code}}"
                                       {{is_null($info->archiveType) ? '' : 'data-archive-type-id="'.$info->archiveType.'"'}}
                                       href="#"><span class="flag-icon flag-icon-{{$language->code}}"></span><i class="fa{{$translationIconClass}}"></i></a></li>
                            @endforeach
                        </ul>
                    @endif

                @endif
            </div>

        @endif

    @endforeach

    @if($tableRest->hasTableColumnTypesFields(\Canela\CanelaTools\Enums\TableColumnTypes::fileTypes))
        @include('canelatools::rest.layouts.file-preview')
        @include('canelatools::rest.layouts.file-delete', ['deleteFileUrl' => route($tableRest->routeEntity.'.ajax.deletefile')])
        @include('canelatools::rest.layouts.image-cropper')
    @endif

    @if(!empty($register))
        @include('canelatools::rest.layouts.input-translation', ['entityId' => $entityId])
    @endif

</div>
