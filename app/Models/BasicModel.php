<?php

namespace Canela\CanelaTools\Models;


use Canela\CanelaTools\Contracts\DeletableContract;
use Canela\CanelaTools\Manager\TranslationManager;
use Canela\CanelaTools\Traits\Deletable;
use Canela\CanelaTools\Traits\EloquentModelBread;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Eloquent;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

/**
 * App\Models\Base\ModelApp
 *
 * @property-read mixed                             $created_at_format
 * @property-read mixed                             $updated_at_format
 * @property-read mixed                             $value_identificator
 * @property-read mixed                             $combo_identificator
 * @property-read mixed                             $created_at_short
 * @property-read mixed                             $updated_at_short
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin Eloquent
 */
class BasicModel extends Eloquent implements DeletableContract
{
    use FormAccessible, EloquentModelBread, Deletable;

    const TYPE_CHANGE_STATE      = 1;
    const TYPE_CHANGE_USER_STATE = 2;

    public $tableIdentifierField = 'name';
    public $comboIdentifierField = 'name';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];


    /** @var string $routePath Model route path */
    protected static string $routePath = '';


    /** @var string $resourceTransPath Model resource translations path */
    protected static string $resourceTransPath = '';


    /**
     * @param string|null $path
     * @return string
     */
    public static function getRoutePath(?string $path = ''): string
    {
        return static::$routePath.$path;
    }


    /**
     * @param string|null $path
     * @return string
     */
    public static function getResourceTransPath(?string $path = ''): string
    {
        return static::$resourceTransPath.$path;
    }


    /**
     * @param string $path
     * @return string
     */
    public static function getResourceTrans(string $path): string
    {
        return trans(static::$resourceTransPath.$path);
    }


    /**
     * @param string $field
     * @return string
     */
    public static function getResourceFieldTrans(string $field): string
    {
        return static::getResourceTrans('field.'.$field);
    }


    /**
     * Returns table name.
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return with(new static)->table;
    }

    /**
     * Get entity related model.
     *
     * @return Entity|null
     */
    public function entityModel()
    {
        return Entity::whereReference(self::getTableName())->first();
    }

    /**
     * Get entity related model (static).
     *
     * @return Entity|null
     */
    public static function entityModelStatic()
    {
        return Entity::whereReference(self::getTableName())->first();
    }


    /**
     * Formatea un numero.
     *
     * @param float  $number        Numero
     * @param int    $decimals      Numero de decimales
     * @param string $dec_sep       Separador de decimales
     * @param string $thousands_sep Separado de miles
     * @return string
     */
    public static function getNumberFormat($number, $decimals = 2, $dec_sep = ',', $thousands_sep = '.')
    {
        if (empty($number)) {
            return null;
        }

        return number_format($number, $decimals, $dec_sep, $thousands_sep);
    }

    /**
     * Returns table row identifier name.
     *
     * @return string
     */
    public function getValueIdentificatorAttribute()
    {
        return $this->getValueIdentifierAttribute();
    }

    /**
     * Returns table row identifier name.
     *
     * @return string
     */
    public function getValueIdentifierAttribute()
    {
        $attribute = $this->tableIdentifierField;
        $valor     = $this->$attribute;
        return (empty($valor) ? $this->id : $valor);
    }

    /**
     * return combo identifier attribute.
     *
     * @return mixed
     */
    public function getComboIdentificatorAttribute()
    {
        $attribute = $this->comboIdentifierField;
        return $this->$attribute;
    }

    /**
     * return combo identifier attribute.
     *
     * @return mixed
     */
    public function getComboIdentifierAttribute()
    {
        $attribute = $this->comboIdentifierField;
        return $this->$attribute;
    }

    /**
     * @return \Canela\CanelaTools\Models\Entity
     */
    public function getTableAttribute()
    {
        return Entity::getEntityByName($this->getTable());
    }

    /**
     * Get translated atribute.
     *
     * @param string $attribute
     * @param string|null $lang
     * @param string|null $default
     * @param bool $returnNullWhenNotFound
     * @return string|null
     */
    public function translate(string $attribute, ?string $lang = null, ?string $default = null, bool $returnNullWhenNotFound = false): ?string
    {
        return TranslationManager::translate($this, $this->getTable(), $attribute, $default ?? $this->$attribute ?? '', $this->id, $lang, $returnNullWhenNotFound);
    }

    /**
     * Normal combo.
     *
     * @param Builder $query
     * @return mixed
     */
    public function scopeComboList($query)
    {
        return $query->get()->pluck($this->getNameFieldComboBoxAttribute(), 'id');
    }


    /**
     * @param mixed $list
     * @return false|string
     */
    public static function getComboListJson($list)
    {
        $tmp = [];
        $list->each(function ($item, $key) use (&$tmp) {
            $tmp[] = [$key => $item];
        });
        return json_encode($tmp);
    }


    /**
     * Translated combo.
     * @param Builder $query
     * @param array   $filters Lista con los filtros a ejecutar. Se ha de poner la sentencia completa (campo = valor)
     * @return mixed
     */
    public function scopeComboTranslate($query, $filters = [])
    {
        // tiene filtros
        if (!empty($filters)) {
            foreach ($filters as $filter) {
                $query->whereRaw($filter);
            }
        }

        $attr = $this->getNameFieldComboBoxAttribute();
        return $query->get()->each(function (BasicModel $model) use ($attr) {
            $model->idtTranslateField = $model->translate($attr);
        })->pluck('idtTranslateField', 'id');
    }


    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeComboBoolean($query)
    {
        return collect(array(1 => trans('canelatools::canelatools.bool.yes'), 0 => trans('canelatools::canelatools.bool.no')));
    }


    /**
     * Combo for range.
     *
     * @param Builder $query
     * @param int     $start
     * @param int     $end
     * @param int     $step
     * @return \Illuminate\Support\Collection
     */
    public static function scopeComboRange($query, $start = 0, $end = 60, $step = 10)
    {
        return collect(range($start, $end, $step))->mapWithKeys(function ($value) use ($step) {
            return [($value - 1) => "{$value} - " . ($value + --$step)];
        });
    }


    /**
     * Converts boolean to text
     *
     * @param boolean $value
     *
     * @return string
     */
    public function booleanToText($value)
    {
        return self::booleanToTextStatic($value);
    }
    public static function booleanToTextStatic($value)
    {
        return ($value == 1 ? trans('canelatools::canelatools.bool.yes') : trans('canelatools::canelatools.bool.no'));
    }



    /**
     * Combo search except $filters
     *
     * @param Builder $query
     * @param Array $filters
     * @return mixed
     */
    public function scopeCombolistFilter($query, $filters = [])
    {
        // has filters?
        if (!empty($filters)) {
            foreach ($filters as $filter) {
                $query->whereRaw($filter);
            }
        }

        return $query->get()
                     ->pluck($this->getNameFieldComboBoxAttribute(), 'id');
    }



    /**
     * Combo search except provided ID
     * @param Builder $query
     * @param integer $id
     * @return mixed
     */
    public function scopeComboListFilterId($query, $id)
    {
        return $query->where('id', '<>', $id)
                     ->orderBy($this->getNameFieldComboBoxAttribute())
                     ->get()
                     ->pluck($this->getNameFieldComboBoxAttribute(), 'id');
    }

    /**
     * Establece el nombre de la columna que se usa en los ComboBox
     * @return string
     */
    protected function getNameFieldComboBoxAttribute()
    {
        return $this->comboIdentifierField;
    }

    /**
     * Formato para fecha Larga.
     *
     * @param string $fecha
     * @return null|string
     * @throws \Exception
     */
    public function formatFechaLarga($fecha)
    {
        if (empty($fecha)) {
            return null;
        }

        $fecha = new \DateTime($fecha);
        return $fecha->format('d-m-Y H:i:s');
    }


    /**
     * Formato para fecha Larga.
     *
     * @param string $fecha
     * @param string $sep
     * @return string
     * @throws \Exception
     */
    public function formatFechaShort($fecha, $sep = '-')
    {
        return self::formatFechaShortStatic($fecha, $sep);
    }


    /**
     * formatFechaShortStatic
     *
     * @param string $fecha
     * @param string $sep
     * @return string
     * @throws \Exception
     */
    public static function formatFechaShortStatic($fecha, $sep = '-')
    {
        return empty($fecha)
            ? null
            : (new \DateTime($fecha))->format('Y' . $sep . 'm' . $sep . 'd');
    }


    /**
     * Formato para fecha Larga.
     *
     * @param string $fecha
     * @param string $sep
     * @return string
     * @throws \Exception
     */
    public function formatFechaCorta($fecha, $sep = '-')
    {
        return empty($fecha)
            ? null
            : (new \DateTime($fecha))->format('d' . $sep . 'm' . $sep . 'Y');
    }

    /**
     * Format localized datetime.
     *
     * @param string $fecha
     * @return string
     */
    public function getDateFormatHuman($fecha)
    {
        return self::getDateFormatHumanStatic($fecha);
    }
    public static function getDateFormatHumanStatic($fecha)
    {
        return empty($fecha)
                ? null
                : (Carbon::parse($fecha))->translatedFormat(trans('project.formats.date'));
    }



    /**
     *
     * @param int $actionId Action
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getHistoricalEntity($actionId = null)
    {
        $entityModel = $this->entityModel();

        if ($entityModel instanceof Entity) {
            $query = HistoryLog::select(['history_log.id',
                                         'module.name as module_name',
                                         'entity.name as entity_name',
                                         'action.name as action_name',
                                         'user.name as user_name',
                                         'history_log.register_id',
                                         'history_log.register',
                                         'history_log.comment',
                                         'history_log.value_pre',
                                         'history_log.value_post',
                                         'history_log.created_at',
                                         'history_log.updated_at',
                                         DB::Raw("DATE_FORMAT(history_log.created_at, '".HistoryLog::formatDate()."') as created_at_fmt"),
                                         DB::Raw("DATE_FORMAT(history_log.updated_at, '".HistoryLog::formatDate()."') as updated_at_fmt"),
                                        ])
                               ->join('module', 'history_log.module_id', 'module.id')
                               ->join('action', 'history_log.action_id', 'action.id')
                               ->join('entity', 'history_log.entity_id', 'entity.id')
                               ->leftJoin('user', 'history_log.user_id', 'user.id')
                               ->where('history_log.register_id', $this->id);

            if (!is_null($actionId)) {
                $query->where('entity.id', $entityModel->id);
            }

            return $query->orderBy('history_log.created_at', 'desc')->get();
        } else {
            return null;
        }
    }


    /**
     * Guardar relaciones. compruba si va por ajax o no
     * @param Request $request
     * @param $attribute Nombre del campo del modelo donde se guardan los datos
     * @param $field Nombre del atributo del request de donde se obtienen los datos
     */
    public function syncBasic($request, $attribute, $field)
    {
        $ids = [];
        if ($request->has($field) && !empty($request->get($field))) {
            if (is_array($request->get($field))) {
                $ids = $request->get($field);
            } else {
                $ids = array_map('intval', explode(',', $request->$field));
            }
            $ids = array_filter(array_unique($ids));
        }

        $this->$attribute()->sync($ids);
    }



    /*__________________________________
    |                                   |
    |       Mutator functions           |
    |___________________________________*/

    /**
     * fecha de alta con formato
     * @throws \Exception
     */
    public function getCreatedAtFormatAttribute()
    {
        return $this->formatFechaLarga($this->created_at);
    }

    /**
     * fecha de alta con formato corto
     * @throws \Exception
     */
    public function getCreatedAtShortAttribute()
    {
        return $this->formatFechaShort($this->created_at);
    }

    /**
     * fecha de alta con formato
     * @throws \Exception
     */
    public function getUpdatedAtFormatAttribute()
    {
        return $this->formatFechaLarga($this->updated_at);
    }

    /**
     * fecha de alta con formato corta
     * @throws \Exception
     */
    public function getUpdatedAtShortAttribute()
    {
        return $this->formatFechaShort($this->updated_at);
    }


    /**
     * Formato para las fechas
     * @return \Illuminate\Contracts\Translation\Translator|string|array|NULL
     */
    public static function formatDate()
    {
        return Lang::has('project.date') ? trans('project.date') : '%Y-%m-%d';
    }

    /**
     * Formato para las fechas-hora
     * @return \Illuminate\Contracts\Translation\Translator|string|array|NULL
     */
    public static function formatDateTime()
    {
        return Lang::has('project.datetime') ? trans('project.datetime') : '%Y-%m-%d %H:%i:%s';
    }


    /**
     * Devuelve un array con los idiomas a los que se pueden
     * traducir los atributos de texto de la entidad que sean traducibles.
     *
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return collect();
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('state_id', State::ACTIVO);
    }

}
