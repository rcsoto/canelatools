<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use App\Models\Client\Customer;
use App\Models\General\Archive;
use App\Models\General\ArchiveType;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\WebPage;
use App\Models\Project\WebBuilder\WebPageType;
use Canela\CanelaTools\Requests\WebBuilder\WebPageRequest;
use Canela\CanelaTools\Traits\ArchivesAssociatable;
use Canela\CanelaTools\Traits\Fileable;
use Canela\CanelaTools\Traits\SectionsSortable;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageController extends BasicController
{
    use HtmlLists, Fileable, SectionsSortable, ArchivesAssociatable;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPage::class;
        $this->routeEntity = 'admin.webbuilder.webpage';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: Customer::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page.field.title'),
                ),
                new TableColumn(
                    table: 'web_page_type',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_page.field.type_id'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: WebPageType::comboTranslate(),
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.general.field.state'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: State::comboTranslate(),
                ),
                new TableColumn(
                    label: '',
                    withOrderBy: false,
                    withFilter: false,
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'customer_id',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                    required: true,
                ),
                new TableForm(
                    name: 'title',
                    label: trans('canelatools::canelatools.entity.web_page.field.title'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'subtitle',
                    label: trans('canelatools::canelatools.entity.web_page.field.subtitle'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'body',
                    label: trans('canelatools::canelatools.entity.web_page.field.body'),
                    type: TableColumnTypes::WYSIWYG,
                    max: TableColumnMaxSize::LONGTEXT_MAX,
                ),
                new TableForm(
                    name: 'tags',
                    label: trans('canelatools::canelatools.entity.web_page.field.tags'),
                    type: TableColumnTypes::TAGS,
                    max: TableColumnMaxSize::TEXT_MAX,
                ),
                new TableForm(
                    name: 'type_id',
                    label: trans('canelatools::canelatools.entity.web_page.field.type_id'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPageType::comboTranslate(),
                ),
                (new TableForm(
                    name: 'file_header_id',
                    label: trans('canelatools::canelatools.entity.web_page.field.file_header_id'),
                    containerClass: 'col-12 col-sm-6',
                    comboList: Archive::comboTranslate(['type_id='.ArchiveType::VIDEO]),
                    archiveType: ArchiveType::VIDEO,
                    nullable: true,
                ))->setAddNewBtnCreateRoute('admin.general.archive.create'),
                new TableForm(
                    name: 'show_header',
                    label: trans('canelatools::canelatools.entity.web_page.field.show_header'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'show_footer',
                    label: trans('canelatools::canelatools.entity.web_page.field.show_footer'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'show_home',
                    label: trans('canelatools::canelatools.entity.web_page.field.show_home'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'is_external_link',
                    label: trans('canelatools::canelatools.entity.web_page.field.is_external_link'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'slug',
                    label: trans('canelatools::canelatools.entity.web_page.field.slug'),
                    containerClass: 'col-12 col-sm-6',
                    max: 255,
                ),
                new TableForm(
                    name: 'open_in_new_window',
                    label: trans('canelatools::canelatools.entity.web_page.field.open_in_new_window'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'external_link',
                    label: trans('canelatools::canelatools.entity.web_page.field.external_link'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    containerClass: 'col-12 col-sm-6',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'seo_title',
                    label: trans('canelatools::canelatools.entity.web_page.field.seo_title'),
                    max: 255,
                ),
                new TableForm(
                    name: 'seo_keywords',
                    label: trans('canelatools::canelatools.entity.web_page.field.seo_keywords'),
                    type: TableColumnTypes::TAGS,
                    max: 255,
                ),
                new TableForm(
                    name: 'seo_description',
                    label: trans('canelatools::canelatools.entity.web_page.field.seo_description'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = WebPage::select([
            'web_page.id',
            'customer.name as customer_name',
            'web_page.title',
            'web_page_type.name as web_page_type_name',
            'state.name as state_name',
            'archive.archive as image_header',
            'web_page.id as action_web_page_id',
            'archive_type.id as archive_type_id',
        ])
        ->join('web', 'web_page.web_id', 'web.id')
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id')
        ->join('web_page_type', 'web_page.type_id', 'web_page_type.id')
        ->join('state', 'web_page.state_id', 'state.id')
        ->leftJoin('archive', 'web_page.file_header_id', 'archive.id')
        ->leftJoin('archive_type', 'archive.type_id', 'archive_type.id')
        ->groupBy('web_page.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'image_header':
                    $txt = !is_null($value) && !is_null($jsonItem->archive_type_id) && $jsonItem->archive_type_id == ArchiveType::IMAGE
                        ? '<div class="text-center" style="min-width: 100px;">'.$this->getListImage(url: asset(config('canelatools.folder.storage'). \Canela\CanelaTools\Models\General\Archive::DIR_ARCHIVES.$value), classes: 'list-image-thumb').'</div>'
                        : '';
                    break;
                case 'action_web_page_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, false, false, false,
                        false, false, 0, 0, true,
                        true);
                    break;
                default:
                    $txt = $value;
            }

            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->loadCustomerCombolist();
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, $id)
    {
        $this->loadCustomerCombolist();
        return parent::edit($request, $id);
    }


    /**
     * @param $register
     * @param $request
     * @return Application|Factory|View
     */
    public function setParamReturn($register, $request) {
        $register->customer_id = $register->web->customer->id;
        return view($this->vistaEdit, compact('register'))
            ->with('urlPrefix', $this->urlPrefix)
            ->with('tableRest', $this->tableRest);
    }


    /**
     *
     */
    private function loadCustomerCombolist()
    {
        $customerTableFormItem = $this->tableRest->getTableFormItem('customer_id');
        if (!empty($customerTableFormItem)) {
            $customerTableFormItem->setComboList(Customer::comboListWithSecurity($this));
        }
    }


    /**
     * Store a newly created item.
     *
     * @param WebPageRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(WebPageRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request);
    }
    public function storePre($model, $request) {
        return $this->saveRelation($model, $request);
    }


    /**
     * Update the specified item.
     *
     * @param WebPageRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
    public function updatePre($model, $request) {
        return $this->saveRelation($model, $request);
    }


    /**
     * Guardado de relaciones
     * @param $model
     * @param $request
     * @return mixed
     */
    public function saveRelation($model, $request) {
        $model->slug = empty($model->slug) ? Str::slug($model->title) :  Str::slug($model->slug);
        return $model;
    }
}
