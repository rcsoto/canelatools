<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\General\Typography;


class WebTemplateSectionStyle extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_template_section_style';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id',
        'section_id',
        'typography_id',
        'color',
        'color_backgroup',
        'text_size',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'template_id'       => 'int',
        'section_id'        => 'int',
        'typography_id'     => 'int',
        'color'             => 'string',
        'color_backgroup'   => 'string',
        'text_size'         => 'string',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template() {
        return $this->belongsTo(WebTemplate::class, 'template_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section() {
        return $this->belongsTo(WebTemplateSection::class, 'section_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typography() {
        return $this->belongsTo(Typography::class, 'typography_id');
    }
}
