<?php

if (!function_exists('blade')) {
    /**
     * Get the blade tools instance
     *
     * @return \Canela\CanelaTools\BladeResolver
     */
    function blade()
    {
        return app(\Canela\CanelaTools\BladeResolver::class);
    }
}
