/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

global.$ = global.jQuery = require('jquery');
window.$ = window.jQuery = require('jquery');

require('../vendor/growl/javascripts/jquery.growl');
require('noty');
require('cropperjs');
const downloadjs = require('downloadjs');

require( 'datatables.net-bs4' )();
require( 'datatables.net-responsive-bs4' )();
require( 'datatables.net-scroller-bs4' )();

require('./bootstrap');
require('jquery-slimscroll');
require('bonzo');
require('classie');
require('sweetalert2');
//require('bootstrap-multiselect/dist/js/bootstrap-multiselect');
//require('bootstrap-multiselect/dist/js/bootstrap-multiselect-collapsible-groups');
require('jquery.nicescroll');
require('select2/dist/js/select2.full');
require('select2/dist/js/i18n/es');
require('node-waves/dist/waves.min');
require('bootstrap-daterangepicker/daterangepicker');
require('bootstrap-datepicker/dist/js/bootstrap-datepicker');
require('bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min');
require('../../node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
require('../../www/assets/js/main.js');

window.URI = require('urijs');

window.moment = require('moment');
require('moment/locale/es');

require('./load');
require('./canela');
require('../../vendor/canela/canelatools/resources/js/canelatools');

window.Vue = require('vue');

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

window.i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

import CanelaInput from '../../vendor/canela/canelatools/resources/js/components/utils/form/CanelaInput';
Vue.component('canela-input', CanelaInput);
