<?php namespace Canela\CanelaTools\Models\AttributeFree;


use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\Entity;


/**
 * Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification
 *
 * @property int                                                         $id
 * @property int                                                         $attribute_free_id
 * @property int                                                         $entity_destination_id Entidad sobre la que se muestra el atributo
 * @property int                                                         $entity_origin_id      Entidad sobre la que se aplica condicion para mostrar atributo
 * @property int|null                                                    $register_id
 * @property int                                                         $is_apply              Indica si la condicion es para mostrar (1) o para ocultar (0)
 * @property int                                                         $required              Indica si es obligatorio
 * @property int                                                         $multiple              Indica si se pueden seleccionar multiples valores. Unicamente si el tipo de dato es Lista
 * @property int|null                                                    $position
 * @property \Carbon\Carbon|null                                         $created_at
 * @property \Carbon\Carbon|null                                         $updated_at
 * @property-read \Canela\CanelaTools\Models\AttributeFree\AttributeFree $attribute
 * @property-read \Canela\CanelaTools\Models\Entity                      $entityDestination
 * @property-read \Canela\CanelaTools\Models\Entity                      $entityOrigin
 * @property-read string                                                 $created_at_format
 * @property-read string                                                 $updated_at_format
 * @property-read string                                                 $table
 * @property-read string                                                 $combo_identificator
 * @property-read string                                                 $value_identificator
 * @property-read string                                                 $created_at_short
 * @property-read string                                                 $updated_at_short
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereAttributeFreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereEntityDestinationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereEntityOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereIsApply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereMultiple($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereRegisterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeEntity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\AttributeFree\AttributeFreeClassification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListOrd()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel modelJoin($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoin($relationSegments, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withJoinLeft($relationSegments, $rightAlias = null, $decorators = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel withSelect($relationSegments, $columns, $rightAlias = null, $decorators = null, $join = 'join', $operator = '=')
 * @mixin \Eloquent
 */
class AttributeFreeEntity extends BasicModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_free_entity';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attribute_free_id',
        'entity_destination_id',
        'entity_origin_id',
        'register_id',
        'is_apply',
        'required',
        'multiple',
        'position',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                    => 'integer',
        'attribute_free_id'     => 'integer',
        'entity_destination_id' => 'integer',
        'entity_origin_id'      => 'integer',
        'register_id'           => 'integer',
        'is_apply'              => 'boolean',
        'required'              => 'boolean',
        'multiple'              => 'boolean',
        'position'              => 'integer',
    ];

    protected $with = ['attribute'];

    
    /**
     * Related attribute.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(AttributeFree::class, 'attribute_free_id');
    }


    /**
     * Related destination entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityDestination()
    {
        return $this->belongsTo(Entity::class, 'entity_destination_id');
    }


    /**
     * Related origin Entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityOrigin()
    {
        return $this->belongsTo(Entity::class, 'entity_origin_id');
    }


}
