<?php namespace Canela\CanelaTools\Models\Client;

use Canela\CanelaTools\Models\BasicModel;

class Rrss extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rrss';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'name',
        'icon',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'state_id'          => 'int',
        'name'              => 'string',
        'icon'              => 'string',
    ];
}
