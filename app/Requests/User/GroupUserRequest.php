<?php

namespace Canela\CanelaTools\Requests\User;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class GroupUserRequest
 * @package App\Http\Requests\User
 */
class GroupUserRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'              => 'required|max:255|unique:group_user,name',
            'description'       => 'nullable|string|min:1|max:4000',
            'user_group_user'   => 'nullable|array',
            'user_group_user.*' => 'exists:user,id',
        ];

        if ($this->method() == 'PUT' || $this->method() == 'PATCH')
        {
            $id                = $this->route('groupuser');
            $rules['name']     .= ',' . $id;
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'              => trans('canelatools::canelatools.entity.groupuser.field.name'),
            'description'       => trans('canelatools::canelatools.entity.groupuser.field.description'),
            'user_group_user'   => trans('canelatools::canelatools.entity.groupuser.field.user'),
            'user_group_user.*' => trans('canelatools::canelatools.entity.groupuser.field.user'),
        ];
    }
}
