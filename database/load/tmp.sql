ALTER TABLE `client` 
CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ;
ALTER TABLE `client` 
CHANGE COLUMN `full_address` `full_address` VARCHAR(2000) NULL DEFAULT NULL ;

-- - otro cambio  

ALTER TABLE `customer` 
DROP FOREIGN KEY `fk_customer_store_customer_brand1`;
ALTER TABLE `customer` 
DROP COLUMN `customer_brand_id`,
ADD COLUMN `client_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
ADD INDEX `fk_customer_client1_idx` (`client_id` ASC),
DROP INDEX `fk_customer_store_customer_brand1_idx` ;
;
ALTER TABLE `customer` 
ADD CONSTRAINT `fk_customer_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `client` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

drop table `customer_brand`

-- - otro cambio
ALTER TABLE `customer` 
CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ;
