<?php

namespace Canela\CanelaTools\Routing;

/**
 * Class Router
 * @package App\Routing
 */
class RouterOld extends \Illuminate\Routing\Router
{
    /**
     * Configuration Routes.
     *
     * @param array $options
     */
    public function configuration(array $options = [])
    {
        $this->group(array('namespace' => 'Configuration',
                           'prefix'    => 'configuration',
                           'as'        => 'configuration.'), function () {

            // History Log
            $this->resource('historylog', 'HistoryLogController')->only(['index', 'edit']);
            $this->group(['prefix' => 'historylog', 'as' => 'historylog.'], function () {
                $this->post('list/ajax', 'HistoryLogController@listAjax')->name('listAjax');
                $this->post('list/ajax/export', 'HistoryLogController@exportAjax')->name('exportAjax');
            });

        });
    }

    /**
     * Location routes.
     *
     * @param array $options
     */
    public function location(array $options = [])
    {
        $this->group(array('namespace' => 'Location',
                           'prefix'    => 'location',
                           'as'        => 'location.'), function () {

            $this->resource('country', 'CountryController')->except(['show']);
            $this->group(['prefix' => 'country', 'as' => 'country.'], function () {
                $this->post('list/ajax', 'CountryController@listAjax')->name('listAjax');
                $this->post('list/ajax/export', 'CountryController@exportAjax')->name('exportAjax');
            });

            $this->resource('province', 'ProvinceController')->except(['show']);
            $this->group(['prefix' => 'province', 'as' => 'province.'], function () {
                $this->post('list/ajax', 'ProvinceController@listAjax')->name('listAjax');
                $this->post('list/ajax/export', 'ProvinceController@exportAjax')->name('exportAjax');
            });

            $this->resource('city', 'CityController')->except(['show']);
            $this->group(['prefix' => 'city', 'as' => 'city.'], function () {
                $this->post('list/ajax', 'CityController@listAjax')->name('listAjax');
                $this->post('list/ajax/export', 'CityController@exportAjax')->name('exportAjax');
            });
        });
    }
}
