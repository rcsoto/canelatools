<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Carbon\Carbon;

class WebPageNoticeHorary extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_notice_horary';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notice_id',
        'date_from',
        'date_to',
        'time_from',
        'time_to',
        'comment'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'notice_id'       => 'int',
        //'date_from'       => 'date',
        //'date_to'         => 'date',
        //'time_from'       => 'time',
        //'time_to'         => 'time',
        'comment'         => 'string',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notice() {
        return $this->belongsTo(WebPageNotice::class, 'notice_id');
    }


    /**
     * @return string
     */
    public function getHoraryText() {
        $dateFrom = isset($this->date_from) ? Carbon::parse($this->date_from) : null;
        $dateTo   = isset($this->date_to) ? Carbon::parse($this->date_to) : null;

        // ambas fechas informadas
        if (isset($dateFrom) && isset($dateTo)) {
            // son del mismo ano
            if ($dateFrom->year == $dateTo->year) {
                if ($dateFrom->month == $dateTo->month) {
                    return trans('canelatools::canelatools.entity.web_page_notice_horary.labels.date_from_to',
                        ['from' => $dateFrom->translatedFormat('l d'),
                            'to' => self::getDateFormatHumanStatic($dateTo)]);
                } else {
                    return trans('canelatools::canelatools.entity.web_page_notice_horary.labels.date_from_to_eqal_year_distint_month',
                        ['day' => $dateFrom->translatedFormat('l d'),
                            'month' => $dateFrom->translatedFormat('F'),
                            'to' => self::getDateFormatHumanStatic($dateTo)]);
                }
            }
            // ambas fechas
            else {
                return trans('canelatools::canelatools.entity.web_page_notice_horary.labels.date_from_to',
                    ['from' => self::getDateFormatHumanStatic($dateFrom),
                        'to' => self::getDateFormatHumanStatic($dateTo)]);
            }
        }
        // fecha inicio
        else if (isset($dateFrom)) {
            return self::getDateFormatHumanStatic($dateFrom);
        }
        // fecha fin
        else if (isset($dateTo)) {
            return self::getDateFormatHumanStatic($dateTo);
        }
        return null;
    }
}
