<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <title>@lang('canelatools::canelatools.updateBrowser.title', ['project' => config('app.name')])</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<link rel="shortcut icon" type="image/x-icon" href="copito.ico" />
	<style>
	
        body {
        	font-family: Arial, sans-serif;
        	margin: 0;
        	text-align: center;
        	text-shadow: 0px 1px 1px #fff;
        	font-size: 13px ;
        	line-height: 22px; 
        	color: #333; 
        }
        
        /* Container */
        .container {
        	margin-right: auto;
        	 margin-left: auto;
        	*zoom: 1;
        	width: 940px;
        	text-align: center;
        	padding: 30px 0;
        }
        
        /* Columns */
        .span12 { width: 940px; }
        .span8 { width: 620px; }
        .span2 { width: 140px; }
        .row { margin-left: -20px; *zoom: 1; }
        [class*="span"] { float: left; min-height: 1px;  margin-left: 20px; }
        
        /* Links */
        a {
        	color: #666; 
        	text-decoration: none;
        	-moz-transition: all 0.3s ease;
        	-webkit-transition: all 0.3s ease;
        	-o-transition: all 0.3s ease;
        	transition: all 0.3s ease;
        	outline: 0!important;
        	text-decoration: none; 
        }
        a:hover { color: #222; }
        
        /* Jigsaw Piece */
        #whoops { margin: 0 auto 20px; }
        
        h1 { color: #333; font-size: 24px; line-height: 26px; }
        
        hr {
        	margin: 15px 0 0px;
        	border: 0 none;
        	border-top: 1px solid #bbb;
        	border-bottom: 1px solid #fff;
        }
        
        /* Jigsaw Swing Animation */
        .animated{
        	-webkit-animation-fill-mode:both;
        	-moz-animation-fill-mode:both;
        	-ms-animation-fill-mode:both;
        	-o-animation-fill-mode:both;
        	animation-fill-mode:both;
        	-webkit-animation-duration:2s;
        	-moz-animation-duration:2s;
        	-ms-animation-duration:2s;
        	-o-animation-duration:2s;
        	animation-duration:2s;
        	-webkit-animation-delay: 2s;
        	-moz-animation-delay: 2s;
        	-ms-animation-delay: 2s;
            animation-delay: 2s;
        }
        
        @-webkit-keyframes swing {
        	20%, 40%, 60%, 80%, 100% { -webkit-transform-origin: top center; }	20% { -webkit-transform: rotate(15deg); }	
        	40% { -webkit-transform: rotate(-10deg); }
        	60% { -webkit-transform: rotate(5deg); }	
        	80% { -webkit-transform: rotate(-5deg); }	
        	100% { -webkit-transform: rotate(0deg); }
        }
        
        @-moz-keyframes swing {
        	20% { -moz-transform: rotate(15deg); }	
        	40% { -moz-transform: rotate(-10deg); }
        	60% { -moz-transform: rotate(5deg); }	
        	80% { -moz-transform: rotate(-5deg); }	
        	100% { -moz-transform: rotate(0deg); }
        }
        
        @-o-keyframes swing {
        	20% { -o-transform: rotate(15deg); }	
        	40% { -o-transform: rotate(-10deg); }
        	60% { -o-transform: rotate(5deg); }	
        	80% { -o-transform: rotate(-5deg); }	
        	100% { -o-transform: rotate(0deg); }
        }
        
        @keyframes swing {
        	20% { transform: rotate(15deg); }	
        	40% { transform: rotate(-10deg); }
        	60% { transform: rotate(5deg); }	
        	80% { transform: rotate(-5deg); }	
        	100% { transform: rotate(0deg); }
        }
        
        .swing {
        	-webkit-transform-origin: top center;
        	-moz-transform-origin: top center;
        	-o-transform-origin: top center;
        	transform-origin: top center;
        	-webkit-animation-name: swing;
        	-moz-animation-name: swing;
        	-o-animation-name: swing;
        	animation-name: swing;
        }
        
        /* Fade In Animation */
        @-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
        @-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
        @keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
         
        .fade-in {
            opacity:0; 
            -webkit-animation:fadeIn ease-in 1;  
            -moz-animation:fadeIn ease-in 1;
            animation:fadeIn ease-in 1;
         
            -webkit-animation-fill-mode:forwards;  
            -moz-animation-fill-mode:forwards;
            animation-fill-mode:forwards;
         
            -webkit-animation-duration:1s;
            -moz-animation-duration:1s;
            animation-duration:1s;
        }
         
        .fade-in.one {
        -webkit-animation-delay: 0.2s;
        -moz-animation-delay: 0.2s;
        animation-delay: 0.2s;
        }
         
        .fade-in.two {
        -webkit-animation-delay: 1.0s;
        -moz-animation-delay:1.0s;
        animation-delay: 1.0s;
        }
         
        .fade-in.three {
        -webkit-animation-delay: 1.6s;
        -moz-animation-delay: 1.6s;
        animation-delay: 1.6s;
        }
        
        .title-project {
            color: grey;
            font-size: 30px;
        }
        
        /* Media Queries */
        
        @media (max-width: 767px) 
        {
            body { padding-left: 20px; padding-right: 20px; }
            .container { width: auto; }
            .row{ margin-left: 0; }
            [class*="span"] {
                float: none;
                display: block;
                width: 100%;
                margin-left: 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .span12{ width: 100%; -webkit-box-sizing: border-box;  -moz-box-sizing: border-box; box-sizing: border-box; }
        }
        
        @media (min-width: 768px) and (max-width: 979px)
        {
            .row { margin-left: -20px; *zoom: 1; }
            
            [class*="span"] { float: left;  min-height: 1px; margin-left: 20px; }
            .container {  width: 724px; }
            .span12 { width: 724px; }
            .span8 { width: 476px; }
            .span2 { width: 104px; }
        }
        
        @media (min-width: 1200px)
        {
            .row { margin-left: -30px; *zoom: 1; }
            
            [class*="span"] { float: left; min-height: 1px;  margin-left: 30px; }
            .container{ width: 1170px; }
            .span12 { width: 1170px; }
            .span8 { width: 770px;  }
            .span2 { width: 170px; }
        }
        
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		
		<div class="span12">
			<div class="fade-in one">
				<div id="whoops">
					<p class="animated swing title-project">{{ config('app.name')}}</p>
				</div>
			</div>
		</div>
			
		<div class="span2"></div>
		<div class="span8">

		<h1 class="fade-in two">@lang('canelatools::canelatools.updateBrowser.obsolete_browser')</h1>
		
		<div class="fade-in three">
		<p>
		@lang('canelatools::canelatools.updateBrowser.update_version_browser')
		<br/>
		<br/>
		<strong>@lang('canelatools::canelatools.updateBrowser.important'):</strong>&nbsp;@lang('canelatools::canelatools.updateBrowser.browser_funcionality') <strong>"@lang('canelatools::canelatools.updateBrowser.compability_view')"</strong>, @lang('canelatools::canelatools.updateBrowser.disable') <a href="http://windows.microsoft.com/es-xl/internet-explorer/use-compatibility-view#ie=ie-9">@lang('canelatools::canelatools.updateBrowser.more_information')</a>.
		<br/>
		<br/>
		<a href="@lang('canelatools.updateBrowser.url_chrome')"><strong>@lang('canelatools::canelatools.updateBrowser.google_chrome')</strong></a>
		<br/>
		<a href="@lang('canelatools.updateBrowser.url_firefox')"><strong>@lang('canelatools::canelatools.updateBrowser.firefox')</strong></a>
		<br/>
		<a href="@lang('canelatools.updateBrowser.url_edge')"><strong>@lang('canelatools::canelatools.updateBrowser.explorer')</strong></a>
		<br/>
		<br/>
		</p>
		<h2>@lang('canelatools::canelatools.updateBrowser.update_visit')</h2>
		
	
		<hr>
		<br>
		
		<span>&copy; &laquo;<a href="#">{{ config('app.name') }}</a>&raquo;.@lang('canelatools::canelatools.updateBrowser.rights_reserved')</span>
		
		</div> 
		
		<div class="span2"></div>
		</div>
	</div>  
</div>  
</body>
</html>
