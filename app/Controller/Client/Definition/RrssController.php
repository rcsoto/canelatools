<?php

namespace Canela\CanelaTools\Controller\Client\Definition;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Client\Rrss;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Requests\Client\RrssRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class ConfigurationController
 * @package App\Http\Controllers\Admin\Configuration
 */
class RrssController extends BasicController
{
    use HtmlLists;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Rrss::class;
        $this->routeEntity = 'client.rrss';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::RRSS;
        $this->menu_group = AdminMenu::GROUP_CLIENT;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools.entity.rrss.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([  (new TableColumn())->setTable('rrss')->setFieldOrder('id'),
                                (new TableColumn())->setTable('rrss')->setFieldOrder('name')
                                    ->setLabel(trans('canelatools::canelatools.entity.rrss.field.name')),
                                (new TableColumn())->setTable('rrss')->setFieldOrder('icon')
                                    ->setLabel(trans('canelatools::canelatools.entity.rrss.field.icon')),
                                (new TableColumn())->setTable('state')->setFieldOrder('name')->setFieldFilter('id')
                                    ->setLabel(trans('canelatools::canelatools.general.field.state')),
                                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)
                                    ->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([(new TableForm())->setName('name')->setMax(45)
                                ->setLabel(trans('canelatools::canelatools.entity.customer.field.name'))->setCol(6),
                            (new TableForm())->setName('icon')->setMax(45)
                                ->setLabel(trans('canelatools::canelatools.entity.customer.field.map_longitude'))->setCol(6),
                            (new TableForm())->setName('state_id')->setComboList(State::comboList())
                                ->setLabel(trans('canelatools::canelatools.general.field.state'))->setCol(6),
                            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param Request $request
     * @return Builder|Rrss
     */
    public function getConsultaPersonalizada($request)
    {
        return Rrss::select(['rrss.id',
                             'rrss.name',
                             'rrss.icon',
                             'state.name as state_name' ,
                             'rrss.id as action_rrss_id',
                            ])
                   ->join('state', 'rrss.state_id','state.id');
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_rrss_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                                              true, false, false, false,
                                              false, false, 0, 0, true,
                                              true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param RrssRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(RrssRequest $request)
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param RrssRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(RrssRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
