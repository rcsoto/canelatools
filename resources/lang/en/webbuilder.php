<?php

return [
    'common' => [
        'fields' => [
            'email' => 'Email',
            'password' => 'Password',
        ],
        'buttons' => [
            'send' => 'Send',
            'back' => 'Back',
        ],
        'labels' => [
            'legal-conditions' => 'Legal conditions',
            'monday-to-friday' => 'Monday - Friday',
            'saturday-and-sunday' => 'Saturday - Sunday',
            'phone' => 'Phone',
            'see-more' => 'See more',
            'home' => 'Home',
            'about-me' => 'About me',
            'query' => 'Query',
            'download' => 'Download',
            'profile' => 'Profile',
        ],
    ],
    'pages' => [
        'auth'=> [
            'login' => [
                'labels' => [
                    'login' => 'Login',
                ],
                'forms' => [
                    'login' => [
                        'fields' => [
                            'remember-me' => 'Remember me',
                        ],
                        'links' => [
                            'forgot-password' => 'Forgot password?',
                        ],
                        'buttons' => [
                            'login' => 'Login',
                        ],
                    ]
                ]
            ],
            'forgot-password' => [
                'labels' => [
                    'forgot-your-password-no-problem' => 'Forgot password? No problem. Just give us your email and we will send you a link so you can change it.',
                ],
                'forms' => [
                    'forgot-password' => [
                        'fields' => [
                        ],
                    ],
                ]
            ],
            'reset-password' => [
                'forms' => [
                    'reset-password' => [
                        'fields' => [
                            'password_confirmation' => 'Password confirmation',
                        ],
                        'buttons' => [
                            'reset-password' => 'Reset password',
                        ]
                    ]
                ]
            ],
        ],
        'admin' => [
            'common' => [
                'main-menu' => [
                    'configuration' => [
                        'label' => 'Configuration',
                    ],
                    'catalogue' => [
                        'label' => 'Catalogue',
                    ],
                    'web' => [
                        'label' => 'Web',
                    ],
                    'utilities' => [
                        'label' => 'Utilities',
                        'items' => [
                            'sql' => 'SQL statements',
                        ]
                    ],
                ],
            ],
            'dashboard' => [
                'labels' => [
                ],
                'forms' => [
                ],
                'menus' => [
                ],
            ],
        ],
    ]
];
