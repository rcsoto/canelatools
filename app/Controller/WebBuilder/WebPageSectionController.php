<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Client\Customer;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\General\ArchiveType;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\WebPage;
use Canela\CanelaTools\Models\WebBuilder\WebPageCategory;
use Canela\CanelaTools\Models\WebBuilder\WebPageSection;
use Canela\CanelaTools\Models\WebBuilder\WebPageSectionArchive;
use Canela\CanelaTools\Models\WebBuilder\WebPageType;
use Canela\CanelaTools\Requests\WebBuilder\WebPageSectionRequest;
use Canela\CanelaTools\Traits\ArchivesAssociatable;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use phpDocumentor\Reflection\Types\Nullable;
use stdClass;

/**
 * Class WebPageSectionController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageSectionController extends BasicController
{
    use HtmlLists, Fileable, ArchivesAssociatable;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageSection::class;
        $this->routeEntity = 'admin.webbuilder.webpagesection';
        $this->archivesEntity = WebPageSectionArchive::class;

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_section.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_section.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: Customer::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.page_id'),
                    type: TableFilterTypes::FILTRO_TIPO_SELECT,
                    fieldFilter: 'id',
                    comboList: WebPage::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.title'),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'subject',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.subject'),
                ),
                new TableColumn(
                    table: 'state',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.language.field.state_id'),
                    fieldFilter: 'id',
                    comboList: State::comboTranslate(),
                ),
                new TableColumn(
                    table: 'web_page_section',
                    fieldOrder: 'image_thumb',
                    label: trans('canelatools::canelatools.general.field.image'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.page_id'),
                    required: true,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    containerClass: 'col-6 mb-2',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.title'),
                    required: true,
                    max: 255,
                ),
                new TableForm(
                    name: 'subject',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.subject'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
                new TableForm(
                    name: 'body',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.body'),
                    type: TableColumnTypes::WYSIWYG,
                    max: TableColumnMaxSize::LONGTEXT_MAX,
                ),
                new TableForm(
                    name: 'slug',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.slug'),
                    containerClass: 'col-12 col-sm-6',
                    max: 255,
                ),
                new TableForm(
                    name: 'open_in_new_window',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.open_in_new_window'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: WebPage::comboBoolean(),
                ),
                new TableForm(
                    name: 'external_link',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.external_link'),
                    max: 4000,
                ),
                new TableForm(
                    name: 'seo_title',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.seo_title'),
                    max: 255,
                ),
                new TableForm(
                    name: 'seo_description',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.seo_description'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
                (new TableForm(
                    name: 'categories',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.categories'),
                    required: false,
                ))->setComboList(WebPageCategory::comboTranslate(['page_id is null']), true, 'categoryIds')
                    ->setAddNewBtnCreateRoute('admin.webbuilder.webpagecategory.create'),
                (new TableForm(
                    name: 'file_header_id',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.file_header_id'),
                    containerClass: 'col-12 col-sm-6',
                    comboList: Archive::comboTranslate(['type_id='.ArchiveType::IMAGE]),
                    archiveType: ArchiveType::IMAGE,
                    nullable: true,
                ))->setAddNewBtnCreateRoute('admin.general.archive.create'),
                (new TableForm(
                    name: 'file_thumb_id',
                    label: trans('canelatools::canelatools.entity.web_page_section.field.file_thumb_id'),
                    containerClass: 'col-12 col-sm-6',
                    comboList: Archive::comboTranslate(['type_id='.ArchiveType::IMAGE]),
                    archiveType: ArchiveType::IMAGE,
                    nullable: true,
                ))->setAddNewBtnCreateRoute('admin.general.archive.create'),
                new TableForm(
                    name: 'state_id',
                    label: trans('canelatools::canelatools.general.field.state'),
                    required: true,
                    containerClass: 'col-12 col-sm-6',
                    comboList: State::comboTranslate(),
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang     = App::getLocale();
        $query = WebPageSection::select([
            'web_page_section.id',
            'customer.name as customer_name',
            'web_page.title as web_page_title',
            'web_page_section.title as web_page_section_title',
            'web_page_section.subject',
            DB::Raw((Language::DEFAULT_CODE == $lang ? "state.name" : "trans('" . $lang . "', 'state', state.id, 'name', state.name)") . " as state_name"),
            'archive.archive as image_thumb',
            'web_page_section.id as action_web_page_section_id',
            'state.id as state_id',
        ])
        ->join('web_page', 'web_page_section.page_id', 'web_page.id')
        ->join('web', 'web_page.web_id', 'web.id')
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id')
        ->join('state', 'web_page_section.state_id', 'state.id')
        ->leftJoin('archive', 'web_page_section.file_thumb_id', 'archive.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'action_web_page_section_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, true, false, true, true, false,
                        BasicModel::TYPE_CHANGE_STATE, $jsonItem->state_id,
                        true, true);
                    break;
                case 'subject':
                    $txt = '<div class="td-multiple-lines">'.$value.'</div>';
                    break;
                case 'image_thumb':
                    if (isset($value)) {
                        $txt = '<div class="text-center">'.$this->getListImage(url: asset(config('canelatools.folder.storage').Archive::DIR_ARCHIVES.$value), classes: 'list-image-thumb').'</div>';
                    }
                    break;
                case 'state_name':
                    $txt = '<span id="column-state-name-' . $jsonItem->id . '">' . $value . '</span>';
                    break;
                default:
                    $txt = $value;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_SECTION));
        return parent::create($request);
    }


    /**
     * @param $register
     * @param $request
     * @return Application|Factory|View
     */
    public function setParamReturn($register, $request) {
        $this->tableRest->getTableFormItem('categories')->setComboList(WebPageCategory::comboTranslate(['page_id = '. $register->page_id]));
        return view($this->vistaEdit, compact('register'))
            ->with('urlPrefix', $this->urlPrefix)
            ->with('tableRest', $this->tableRest);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, $id): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_SECTION));
        return parent::edit($request, $id);
    }


    /**
     * @param WebPageSectionRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageSectionRequest $request) {
        return $this->storeBasic($request);
    }
    public function storePre($model, $request) {
        return $this->saveAttrbutes($model, $request);
    }


    /**
     * @param WebPageSectionRequest $request
     * @param $id
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function update(WebPageSectionRequest $request, $id) {
        return $this->updateBasic($request, $id);
    }
    public function updatePre($model, $request) {
        $model = $this->saveAttrbutes($model, $request);
        return $this->saveRelation($model, $request);
    }


    /**
     * @param $model
     * @param $request
     * @return mixed
     */
    public function saveAttrbutes($model, $request) {
        $model->slug = empty($model->slug) ? Str::slug($model->title) :  Str::slug($model->slug);
        return $model;
    }

    /**
     * Guardado de relaciones
     * @param $model
     * @param $request
     * @return mixed
     */
    public function saveRelation($model, $request) {
        // relaciones
        $model->syncBasic($request, 'categories', 'categories');

        return $model;
    }

}
