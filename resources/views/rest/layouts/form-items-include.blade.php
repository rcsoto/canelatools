﻿
@if(empty($register))
    {!! Form::open(['id' => 'modelForm', 'route' => $tableRest->urlStore, 'method' => 'POST', 'role' => 'form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
@else
    @if(($tableRest->showBtnEdit ?? false))
        {!! Form::model($register, ['id' => 'modelForm', 'route' => [$tableRest->urlUpdate, $register], 'method' => 'PUT', 'role' => 'form', 'class' => 'form-horizontal']) !!}
    @endif
@endif

    <!-- Parametros ocultos -->
    @foreach($tableRest->getParametersHidden() as $key => $value)
        {!! Form::hidden($key, $value, array('id' => $key)) !!}
    @endforeach

    @include('canelatools::rest.layouts.form-items')

    <div class="col-12 card-block button-list text-center">
        @if(($tableRest->showBtnEdit ?? false))
            <a class="btn btn-primary" data-action="submit">@lang('canelatools::canelatools.canela-rest.form.btn.save')</a>
        @endif
        <a class="btn btn-secondary" type="button"
           href="{{ $redirectUrl }}">@lang('canelatools::canelatools.canela-rest.form.btn.cancel')</a>
    </div>


@if(empty($register) || ($tableRest->showBtnEdit ?? false))
    {!! Form::close() !!}
@endif
