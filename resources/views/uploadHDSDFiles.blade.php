﻿@php
    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
@endphp
<button type="button" class="btn btn-s-md btn-success btn-embossed btn-upload-js"
        data-type="file-hd">
    <i class="fa fa-cloud-upload"></i> @lang('tag.bladeTraits.view.upload_pdf_hd')
</button>
<button type="button" class="btn btn-s-md btn-success btn-embossed btn-upload-js"
        data-type="file">
    <i class="fa fa-cloud-upload"></i> @lang('tag.bladeTraits.view.upload_pdf')
</button>

@section('body-scripts-extra')
    @parent

    <div id="upFile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadFile">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(array('route' => $upFileRoute, 'enctype' => "multipart/form-data", 'method' => 'put')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group input-file">
                                        <span class="input-group-btn">
                                            <button class="btn btn-blue btn-choose"
                                                    type="button">@lang('tag.bladeTraits.view.select_pdf')</button>
                                        </span>
                            <input type="text" class="form-control" id="font-file"
                                   placeholder="Cargar fichero...">
                        </div>
                        {!! Form::hidden('file_type', '') !!}
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">@lang('canelatools::canelatools.buttons.close')</button>
                    <button type="submit" class="btn btn-blue">@lang('canelatools::canelatools.buttons.upload')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            /**
             * Subir ficheros de carta PDF
             */
            $('.btn-upload-js').click(function () {
                var btn = $(this);

                $(".input-file").before(function () {
                        if (!$(this).prev().hasClass('input-ghost')) {
                            var element = $("<input type='file' name='pdf_file' class='input-ghost' accept='application/pdf' style='visibility:hidden; height:0'>");
                            element.attr("name", $(this).attr("name"));
                            element.change(function () {
                                element.next(element).find('input').val((element.val()).split('\\').pop());
                            });
                            $(this).find("button.btn-choose").click(function () {
                                element.click();
                            });
                            $(this).find('input').css("cursor", "pointer");
                            $(this).find('input').mousedown(function () {
                                $(this).parents('.input-file').prev().click();
                                return false;
                            });
                            return element;
                        }
                    }
                );
                var modal = jQuery('#upFile');
                modal.find('input[name=file_type]:first').val(btn.attr('data-type'));
                var title = '@lang('tag.bladeTraits.view.upload_pdf')';
                modal.find('.modal-title').html(title + (btn.attr('data-type') === 'file-hd' ? ' HD' : ''));
                modal.modal('show');
            });

            jQuery('#upFile').find('button[type=submit]:first').click(function () {
                jQuery(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>\n' +
                    '<span class="sr-only">@lang("tag.bladeTraits.view.upload_pdf")</span>');
                jQuery('.loader-overlay').removeClass('loaded');
            });
        });
    </script>
@endsection
