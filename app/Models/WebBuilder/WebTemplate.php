<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

class WebTemplate extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_template';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'onepage',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'code'              => 'string',
        'name'              => 'string',
        'onepage'           => 'bool',
        'image_sample'      => 'string',
        'image_thumb'       => 'string',
        'description'       => 'string',
    ];


    /**
     * @return HasMany
     */
    public function sectionStyles(): HasMany
    {
        return $this->hasMany(WebTemplateSectionStyle::class, 'template_id');
    }

}
