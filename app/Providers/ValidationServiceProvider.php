<?php

namespace Canela\CanelaTools\Providers;


use Carbon\Carbon;
use DB;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->oneDecimalValidator();
        $this->dayMonthValidator();
        $this->uniqueXFieldsValidator();
        $this->pregMatchValidator();
        $this->valueDistinctValidator();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function oneDecimalValidator()
    {
        \Validator::extend('one_decimal', function ($attribute, $value, $parameters, $validator) {

            $precio_separado = explode(".", strval(round($value, 2)));
            if (isset($precio_separado[1])) {
                return (strlen($precio_separado[1]) <= 1 ? true : false);
            }
            return true;
        });

        \Validator::replacer('one_decimal', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', trans('validation.attributes.' . $attribute), $message);
        });
    }

    protected function dayMonthValidator()
    {
        \Validator::extend('day_month', function ($attribute, $value, $parameters, $validator) {
            $dt    = Carbon::now();
            $month = substr($value, 3, 2);
            $day   = substr($value, 0, 2);
            $year  = $dt->year;

            return (is_numeric($month) && is_numeric($day) && checkdate($month, $day, $year));
        });

        \Validator::replacer('day_month', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', trans('validation.attributes.' . $attribute), $message);
        });
    }

    /**
     * Validar valor unico X campos de una tabla
     * He de recibir los siguientes parametros
     * - Nombre de la tabla
     * - Nombre del campo sobre el que esta configurada la validacion (lo normal es que es valor coincida con "attribute")
     * - Nombre del segundo campo sobre el que se hace la validación
     * - Valor del segundo campo sobre el que se hace la validación
     * - etc .. (se pueden enviar tantos campos como se necesite para hacer la vlidacion)
     * - ID del registro que se quiere excluir (opcional)
     */
    protected function uniqueXFieldsValidator()
    {

        \Validator::extend('unique_x_fields', function ($attribute, $value, $parameters, $validator) {
            $table       = $parameters[0];
            $field1      = $parameters[1];
            $valueField1 = $value;

            $consulta = DB::table($table);

            // Campo 1
            if (empty($valueField1)) {
                $consulta->whereNull($field1);
            } else {
                $consulta->where($field1, $valueField1);
            }

            // Resto de campos
            $indice = 2;
            while ($indice < count($parameters)) {
                if (isset($parameters[$indice]) && isset($parameters[$indice + 1])) {
                    $field      = $parameters[$indice++];
                    $valueField = $parameters[$indice++];
                    // Campo 1
                    if (empty($valueField)) {
                        $consulta->whereNull($field);
                    } else {
                        $consulta->where($field, $valueField);
                    }
                } else {
                    if (isset($parameters[$indice])) {
                        $idExcluyed = empty($parameters[$indice]) ? null : $parameters[$indice++];
                        if (!empty($idExcluyed)) {
                            $consulta->where('id', '<>', $idExcluyed);
                        }
                    }
                }
            }

            return $consulta->count() == 0 ? true : false;
        });

        \Validator::replacer('unique_x_fields', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':values', implode(', ', array_slice($parameters, 1)), $message);
        });
    }

    protected function valueDistinctValidator()
    {
        // Valida que dos campos no tengan el mismo valor
        // He de recibir los siguientes parametros
        // - Nombre del segundo campo sobre el que se hace la validación
        // - Valor del segundo campo sobre el que se hace la validación

        \Validator::extend('value_distinct', function ($attribute, $value, $parameters, $validator) {
            $field1      = $attribute;
            $field2      = $parameters[0];
            $valueField1 = $value;
            $valueField2 = $parameters[1];

            return ($valueField1 != $valueField2);
        });

        \Validator::replacer('value_distinct', function ($message, $attribute, $rule, $parameters) {
            $replacer = str_replace(':attribute', $attribute, $message);
            return str_replace(':attribute2', $parameters[0], $replacer);
        });
    }


    /**
     * Validacion preg_match
     * He de recibir los siguientes parametros
     * - Validacion regex a realizar
     */
    protected function pregMatchValidator()
    {
        \Validator::extend('preg_match', function ($attribute, $value, $parameters, $validator) {
            $regex = $parameters[0];
            return preg_match("/" . $regex . "/", $value);
        });

        \Validator::replacer('preg_match', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', $attribute, $message);
        });
    }
}
