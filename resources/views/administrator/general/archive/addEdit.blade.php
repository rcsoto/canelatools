@extends('canelatools::rest.addEdit')

@section('body-scripts')
    @parent
    <script type="text/javascript">
        const archiveTypes = {!!\App\Models\General\ArchiveType::archiveTypeListJson()!!};
        var selectedArchiveTypeId = null;

        jQuery(document).ready(function () {

            const $typeId = jQuery('#type_id');
            const $archiveText = jQuery('#archive_text');
            const $archiveFile = jQuery('#archive_file');
            const $archiveImageCropper = jQuery('#archive_image_cropper');
            const $archiveTextContainer = jQuery('#canela-input-file-container-archive_text');
            const $archiveFileContainer = jQuery('#canela-input-file-container-archive_file');
            const $archiveImageCropperContainer = jQuery('#canela-input-file-container-archive_image_cropper');

            jQuery.showHideField = function (field, container, show) {
                if (show) {
                    field.removeAttr( 'data-do-not-send');
                    container.show();
                } else {
                    field.attr( 'data-do-not-send', true);
                    container.hide();
                }
            };

            jQuery.hideArchiveFields = function () {
                // Hide all.
                jQuery.showHideField($archiveText, $archiveTextContainer, false);
                jQuery.showHideField($archiveFile, $archiveFileContainer, false);
                jQuery.showHideField($archiveImageCropper, $archiveImageCropperContainer, false);
            };

            jQuery.showArchiveTextField = function () {
                // Show text field.
                jQuery.showHideField($archiveText, $archiveTextContainer, true);
                jQuery.showHideField($archiveFile, $archiveFileContainer, false);
                jQuery.showHideField($archiveImageCropper, $archiveImageCropperContainer, false);
            }

            jQuery.showArchiveFileField = function () {
                // Show file field.
                jQuery.showHideField($archiveText, $archiveTextContainer, false);
                jQuery.showHideField($archiveFile, $archiveFileContainer, true);
                jQuery.showHideField($archiveImageCropper, $archiveImageCropperContainer, false);
            }

            jQuery.showArchiveImageCropperField = function () {
                // Show image cropper field.
                jQuery.showHideField($archiveText, $archiveTextContainer, false);
                jQuery.showHideField($archiveFile, $archiveFileContainer, false);
                jQuery.showHideField($archiveImageCropper, $archiveImageCropperContainer, true);
            }

            jQuery.showHideArchiveField = function (e = null) {
                const typeId = e !== null ? e.currentTarget.value : $typeId.val();
                if (typeId.length === 0) {
                    selectedArchiveTypeId = null;
                    // Hide all.
                    jQuery.hideArchiveFields();
                } else {
                    // Show/Hide correct field.
                    const type = archiveTypes.find(function (item) { return item.id == typeId; });
                    if (typeof type === "undefined") {
                        selectedArchiveTypeId = null;
                        // Hide all.
                        jQuery.hideArchiveFields();
                    } else if (type.is_text_input) {
                        selectedArchiveTypeId = typeId;
                        jQuery.showArchiveTextField();
                    } else if (type.is_file_input) {
                        selectedArchiveTypeId = typeId;
                        jQuery.showArchiveFileField();
                    } else if (type.is_image_cropper_input) {
                        selectedArchiveTypeId = typeId;
                        jQuery.showArchiveImageCropperField();
                    } else {
                        selectedArchiveTypeId = null;
                        // Hide all.
                        jQuery.hideArchiveFields();
                    }
                }
            };

            $typeId.on('change', function (e) {
                jQuery.showHideArchiveField(e);
            });

            jQuery.showHideArchiveField();

            @php
                $field = \App\Models\General\Archive::ARCHIVE_FIELD_NAME;
            @endphp
            @if(!empty($register) && !empty($register->$field) && \App\Models\General\ArchiveType::isTextInputArchiveTypeById($register->type_id))
                jQuery('.remove-file-link').remove();
            @endif

        });
    </script>
@endsection
