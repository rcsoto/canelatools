<?php

return [


    'tables_assoc_count'           => '{1} (:value associated)|[2,*] (:value associated)',
    'delete_record'                => 'Delete record',
    'delete_record_question'       => 'Do you confirm the removal of the record?',
    'recorded_dependencies'        => 'The record has dependencies created',
    'recorded_dependencies_tables' => 'The record has dependencies from the following tables',
    'delete_recorded_dependencies' => 'Do you confirm the removal of the associated records too?',
    'delete_anyway'                => 'Delete anyway',
    'update_status'                => 'Update status',
    'update_status_question'       => 'Do you confirm update status?',
    'update_status_dynamic'        => "Do you confirm <span class=\"status-js\"></span> the record?",
    'comment_not_empty'            => 'You must provide a reason.',
];