<?php

return [
    'api_no_compat'       => 'Api Version is not supported',
    'compilation_invalid' => 'Please, update your application to continue',
    'auth_invalid'        => 'Lack of type of authorization',
    'headers_incomplete'  => 'The requested headers are incomplete',
    'headers_error'       => 'There is a problem on the requested headers',
    'failed'              => 'These credentials do not match our records.',
    'throttle'            => 'Too many login attempts. Please try again in :seconds seconds.',
    'unauthorized'        => 'You are not authorised for access to registers.',
    'password_pernod'     => 'Your password must be updated in your Pernod Ricard account.',
    'signed_url_expired'  => 'the url has expired. Please, re-request access.',

    'verification' => [
        'error'    => 'The user could not be activated',
        'correcto' => 'The user has been activated correctly',
    ],
    'login'        => [
        'disabled'     => 'The user is disabled.',
        'invalidCred'  => 'User credentials are not valid.',
        'userNoExist'  => 'There is not any user with this name.',
        'noToken'      => 'The user token can not be generated.',
        'invalidEmail' => 'The email is not valid.',
        'blocked'      => 'The user is locked.',
    ],
    'messages'     => [
        'remember_send_ok' => 'We have sent you a reminder email of the password. Please, check your email inbox.',
    ],
    'view'         => [
        'login'           => 'Log in',
        'keep_session'    => 'Leave the session open',
        'forgot_pwd'      => 'Did you forget the password?',
        'log_in'          => 'Open',
        'already_account' => 'Do you have an account? Access',
        'send_reminder'   => 'Send reminder',
    ],
    'password'     => [
        'new_password' => 'User Login',
    ],
    "role"         => [
        "forbidden" => "You don't have enough privileges for this action",
    ],
];
