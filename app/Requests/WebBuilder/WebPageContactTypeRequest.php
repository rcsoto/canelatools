<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageContactTypeRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageContactTypeRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'state_id',
        'icon',
        'position',
        'contact',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_contact_type.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'state_id'          => 'required|integer|min:1|exists:state,id',
            'icon'              => 'max:45',
            'position'          => 'required|integer|min:1|max:99999',
            'contact'           => 'required|max:45',
        );
        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');
        $attributes['icon']     = trans('canelatools::canelatools.general.field.icon');
        $attributes['position'] = trans('canelatools::canelatools.general.field.position');

        return $attributes;
    }

}
