<div class="page-header-title">
    <h5 class="m-b-10">{{ trans((empty($register) ? "canelatools::canelatools.canela-rest.form.title.new" : "canelatools::canelatools.canela-rest.form.title.edit"), ['model' => $tableRest->formTitle ?? $title]) }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="feather icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $tableRest->urlIndex }}">{{ $title }}</a></li>
    @if(!empty($register))
        <li class="breadcrumb-item"><a href="#">{{ $register->value_identifier }}</a></li>
    @endif
</ul>
