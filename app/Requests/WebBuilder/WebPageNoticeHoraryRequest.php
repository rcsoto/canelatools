<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageNoticeRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageNoticeHoraryRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'notice_id',
        'date_from',
        'date_to',
        'time_from',
        'time_to',
        'comment'
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_notice_horary.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'notice_id'         => 'required|integer|min:1|exists:web_page_notice,id',
            'date_from'         => 'required|date',
            'date_to'           => 'nullable|date',
            'time_from'         => 'nullable|date_format:H:i',
            'time_to'           => 'nullable|date_format:H:i',
            'comment'           => 'nullable|max:4000',
        );
        return $rules;
    }

}
