<?php

namespace Canela\CanelaTools\Enums;

final class HttpMethod extends \BenSampo\Enum\Enum
{
    const GET = 'get';
    const POST = 'post';
    const PUT = 'put';
    const PATCH = 'patch';
    const DELETE = 'delete';
}
