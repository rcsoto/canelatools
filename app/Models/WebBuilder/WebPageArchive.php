<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use App\Models\Project\WebBuilder\WebPage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebPageArchive extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'archive_id',
        'position',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id' => 'integer',
        'archive_id' => 'integer',
        'position' => 'integer',
    ];


    /**
     * @return BelongsTo
     */
    public function page(): BelongsTo
    {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * @return BelongsTo
     */
    public function archive(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'archive_id');
    }


    /**
     * @return BelongsTo
     */
    public function archiveEntity(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'archive_id');
    }



}
