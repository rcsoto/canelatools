<?php

namespace Canela\CanelaTools\Contracts;


interface DeletableContract
{
    /**
     * Check if a model can be deleted, or it has dependency (hasMany) relations created.
     *
     * @param array $tables_with_relations
     * @return bool
     */
    public function canDelete(&$tables_with_relations = []): bool;

    /**
     * Delete model and its Associated (hasMany) models.
     *
     * @return bool|null
     */
    public function delete();
}