<?php

namespace Canela\CanelaTools\Requests\Configuration;

use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class ParameterRequest
 * @package Canela\CanelaTools\Requests\Configuration
 */
class ParameterRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'name',
        'reference',
        'value',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.parameter.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'name'        => 'required|string|max:255',
                'reference'   => 'required|string|max:45|unique:parameter,reference',
                'value'       => 'nullable|string',
            ];

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $id                 = $this->route('parameter');
            $rules['reference'] = $rules['reference'] . ',' . $id;

        }

        return $rules;
    }

}
