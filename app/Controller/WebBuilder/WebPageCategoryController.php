<?php

namespace Canela\CanelaTools\Controller\WebBuilder;

use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\WebPage;
use App\Models\Project\WebBuilder\WebPageCategory;
use Canela\CanelaTools\Requests\WebBuilder\WebPageCategoryRequest;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use stdClass;

/**
 * Class WebPageCategoryController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageCategoryController extends BasicController
{
    use HtmlLists, Fileable;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageCategory::class;
        $this->routeEntity = 'admin.webbuilder.webpagecategory';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_category.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_category.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'customer_name',
                    label: trans('canelatools::canelatools.entity.customer.report'),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page.field.title'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.name'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'abbreviation',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.abbreviation'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.description'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'color_text',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_text'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'color_border',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_border'),
                ),
                new TableColumn(
                    table: 'web_page_category',
                    fieldOrder: 'color_bg',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_bg'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.page_id'),
                    required: true,
                    comboList: WebPage::comboList(),
                    readOnly: false,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.name'),
                    required: true,
                    type: TableColumnTypes::TEXT,
                    max: 255,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    max: 99999,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'abbreviation',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.abbreviation'),
                    required: false,
                    type: TableColumnTypes::TEXT,
                    max: 45,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.description'),
                    required: false,
                    type: TableColumnTypes::TEXT,
                    max: 255,
                ),
                new TableForm(
                    name: 'color_text',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_text'),
                    required: false,
                    type: TableColumnTypes::TEXT,
                    max: 45,
                    containerClass: 'col-4 mb-2',
                ),
                new TableForm(
                    name: 'color_border',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_border'),
                    required: false,
                    type: TableColumnTypes::TEXT,
                    max: 45,
                    containerClass: 'col-4 mb-2',
                ),
                new TableForm(
                    name: 'color_bg',
                    label: trans('canelatools::canelatools.entity.web_page_category.field.color_bg'),
                    required: false,
                    type: TableColumnTypes::TEXT,
                    max: 45,
                    containerClass: 'col-4 mb-2',
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = WebPageCategory::select([
            'web_page_category.id',
            'customer.name as customer_name',
            'web_page.title as web_page_title',
            'web_page_category.position',
            'web_page_category.name',
	        'web_page_category.abbreviation',
            'web_page_category.description',
            'web_page_category.color_text',
            'web_page_category.color_border',
            'web_page_category.color_bg',
            'web_page_category.id as action_web_page_category_id',
            ])
            ->join('web_page', 'web_page_category.page_id', 'web_page.id')
            ->join('web', 'web_page.web_id', 'web.id')
            ->join('customer', 'web.id', 'customer.web_id')
            ->join('client', 'customer.client_id', 'client.id')
            ->groupBy('web_page_category.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_web_page_category_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, true, true, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param WebPageCategoryRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageCategoryRequest $request) {
        return $this->storeBasic($request);
    }


    /**
     * @param WebPageCategoryRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageCategoryRequest $request, int $id) {
        return $this->updateBasic($request, $id);
    }

}
