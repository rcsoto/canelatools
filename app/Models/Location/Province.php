<?php

namespace Canela\CanelaTools\Models\Location;


use Canela\CanelaTools\Models\BasicModel;

/**
 * Canela\CanelaTools\Models\Location\Province
 *
 * @property int                                                                                      $id
 * @property int                                                                                      $country_id
 * @property string                                                                                   $name
 * @property string|null                                                                              $code
 * @property \Illuminate\Support\Carbon|null                                                          $created_at
 * @property \Illuminate\Support\Carbon|null                                                          $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\Location\City[] $cities
 * @property-read \Canela\CanelaTools\Models\Location\Country                                         $country
 * @property-read mixed                                                                               $combo_identificator
 * @property-read mixed                                                                               $created_at_format
 * @property-read mixed                                                                               $created_at_short
 * @property-read \Canela\CanelaTools\Models\Entity                                                   $table
 * @property-read mixed                                                                               $updated_at_format
 * @property-read mixed                                                                               $updated_at_short
 * @property-read string                                                                              $value_identificator
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Province whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Province extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'province';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country_id', 'name', 'code'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'         => 'integer',
        'country_id' => 'integer',
        'name'       => 'string',
        'code'       => 'string',
    ];

    /**
     * Related country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Associated cities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class, 'province_id');
    }
}