<?php

namespace Canela\CanelaTools\Requests\Client;

use Canela\CanelaTools\Models\WebBuilder\WebLanguage;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebLanguageRequest
 * @package Canela\CanelaTools\Requests\Client
 */
class WebLanguageRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'web_id',
        'language_id',
        'principal',
        'position',
    ];


    /**
     * @var string|null
     * Stores request entity when adding/editing model throw form. Null otherwise.
     */
    protected ?string $entity = WebLanguage::class;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'web_id'            => 'required|integer|exists:web,id',
            'language_id'       => 'required|integer|exists:language,id|unique_x_fields:web_language,language_id,web_id,'.$this->get('web_id'),

            'principal'         => 'required|integer|in:0,1',
            'position'          => 'required|integer|min:1|max:99999',

        ];
        if ($this->method() == 'PUT' || $this->method() == 'PATCH')
        {
            $id = $this->route('weblanguage');
            $rules['language_id'] = $rules['language_id'].','.$id;
        }
        return $rules;
    }

}
