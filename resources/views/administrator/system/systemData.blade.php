<?php
function getcolor($usage)
{
    switch ($usage) {
        case $usage <= 30:
            return 'green';
            break;
        case $usage > 30 && $usage <= 70:
            return 'orange';
            break;
        case $usage > 70:
            return 'red';
            break;
        default:
            return 'inherit';
            break;
    }
}

?>

<table class="table table-striped">
    <tr>
        <td>
            @if(windows_os())
                <i class="fa fa-windows" aria-hidden="true"></i>
            @else
                <i class="fa fa-linux" aria-hidden="true"></i>
            @endif
        </td>
        @if(!windows_os())
        <td>
            {{$sysInfo['Kernel'] ?? ''}} {{$sysInfo['Distro']['name']??''}} -
            {{$sysInfo['Distro']['version']??''}}
        </td>
        @endif
    </tr>
	@if(!windows_os())
    <tr>
        <td>
            <i class="fa fa-wifi" aria-hidden="true"></i> Conectividad
        </td>
        <td>
            <ul>
                <li>
                    IP: {{$sysInfo['AccessedIP'] ?? 'N/D'}}
                </li>
                <li>
                    ETH0:
                    @if($sysInfo['Network Devices']['eth0']['state'] ?? '-' == 'up')
                        <i class="fa fa-circle" style="color: green" aria-hidden="true"></i>
                    @else
                        {{$sysInfo['Network Devices']['eth0']['state'] ?? '-'}} -
                    @endif
                    {{$sysInfo['Network Devices']['eth0']['port_speed'] ?? 0}} Mb/s
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            {{round(($sysInfo['Network Devices']['eth0']['recieved']['bytes'] ?? 0) / pow(1024,3) , 2)}}
                            GB
                            recibidos
                        </div>
                        <div class="col-sm-4">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            {{round(($sysInfo['Network Devices']['eth0']['sent']['bytes'] ?? 0) / pow(1024,3) ,2)}} GB
                            enviados
                        </div>
                    </div>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-cog" aria-hidden="true"></i> Memoria
        </td>
        <td>
            <ul>
                <li>Total: {{round(($sysInfo['RAM']['total'] ?? 0)  / pow(1024,3) ,2)}} GB</li>
                <li>Libre: {{round(($sysInfo['RAM']['free'] ?? 0) / pow(1024,3) ,2)}} GB</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Carga
        </td>
        <td>
            <ul>
                <li>Ahora: {{$sysInfo['Load']['now'] ?? ''}}</li>
                <li>5 mins: {{$sysInfo['Load']['5min'] ?? ''}}</li>
                <li>15 mins: {{$sysInfo['Load']['15min'] ?? ''}}</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-microchip" aria-hidden="true"></i> CPU
            @php($globalUsage = floatval($sysInfo['cpuUsage'] ?? 0))
            <span style="color: {!! getcolor($globalUsage) !!}">{{$globalUsage}} %</span>
        </td>
        <td>
            <div class="row">
                @foreach($sysInfo['CPU'] as $cpu)
                    <div class="col-md-4">
                        <i class="fa fa-microchip" aria-hidden="true"></i>
                        {{$cpu['Model'] ?? ''}} - {{$cpu['MHz'] ?? ''}}
                        <br>
                        <hr>
                        <i class="fa fa-tachometer" aria-hidden="true"></i>
                        @php($usage = floatval($cpu['usage_percentage'] ?? 0))
                        <span style="color: {!! getcolor($usage) !!}">{{$usage}} %</span>
                    </div>
                @endforeach
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-power-off" aria-hidden="true"></i> Uptime
        </td>
        <td>
            {{$sysInfo['UpTime']['text'] ?? 0}}
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-server" aria-hidden="true"></i> Servidor
        </td>
        <td>
            <ul>
                <li>Apache: {{$sysInfo['webService'] ?? ''}}</li>
                <li>PHP: {{$sysInfo['phpVersion'] ?? ''}}</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-sitemap" aria-hidden="true"></i> Virtualización
        </td>
        <td>
            @if($sysInfo['virtualization']['method'] ?? '' == 'Docker')
                <i class="fa fa-ship" aria-hidden="true"></i> Docker
            @else
                {{$sysInfo['virtualization']['method'] ?? ''}}
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <i class="fa fa-clock-o" aria-hidden="true"></i> Hora sistema
        </td>
        <td>
            {{$sysInfo['timestamp'] ?? ''}}
        </td>
    </tr>
    @endif
</table>
