<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use App\Models\Project\WebBuilder\WebPage;
use App\Models\Project\WebBuilder\WebPageData;
use App\Models\Project\WebBuilder\WebPageFaq;
use App\Models\Project\WebBuilder\WebPageType;
use Canela\CanelaTools\Requests\WebBuilder\WebPageDataRequest;
use Canela\CanelaTools\Requests\WebBuilder\WebPageFaqRequest;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageFaqController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageDataController extends BasicController
{
    use HtmlLists;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageData::class;
        $this->routeEntity = 'admin.webbuilder.webpagedata';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_data.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_data.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.name'),
                ),
                new TableColumn(
                    table: 'web_page',
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.page_id'),
                ),
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                ),
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.name'),
                ),
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'date',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.date'),
                    type: TableFilterTypes::FILTRO_TIPO_DATE,
                ),
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'value',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.value'),
                ),
                new TableColumn(
                    table: 'web_page_data',
                    fieldOrder: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.description'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'page_id',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.page_id'),
                    required: true,
                ),
                new TableForm(
                    name: 'name',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.name'),
                    required: false,
                    containerClass: 'col-6 mb-2',
                    max: 255,
                ),
                new TableForm(
                    name: 'value',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.value'),
                    required: false,
                    containerClass: 'col-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'prefix',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.prefix'),
                    required: false,
                    containerClass: 'col-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'suffix',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.suffix'),
                    required: false,
                    containerClass: 'col-6 mb-2',
                    max: 45,
                ),
                new TableForm(
                    name: 'date',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.date'),
                    required: false,
                    type: TableColumnTypes::DATE,
                    containerClass: 'col-6 mb-2',
                    max: 255,
                ),
                new TableForm(
                    name: 'position',
                    label: trans('canelatools::canelatools.general.field.position'),
                    required: true,
                    type: TableColumnTypes::INT,
                    containerClass: 'col-6 mb-2',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    name: 'description',
                    label: trans('canelatools::canelatools.entity.web_page_data.field.description'),
                    required: false,
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $query = WebPageData::select([
                'web_page_data.id',
                'customer.name as customer_name',
                'web_page.title as web_page_title',
                'web_page_data.position',
                'web_page_data.name',
                DB::Raw('DATE_FORMAT(web_page_data.date, "'.WebPageData::formatDate().'") as web_page_data_date'),
                'web_page_data.value',
                'web_page_data.description',
                'web_page_data.id as action_web_page_data_id',
                'web.id as web_id',
            ])
            ->join('web_page', 'web_page_data.page_id', 'web_page.id')
            ->join('web', 'web_page.web_id', 'web.id')
            ->join('customer', 'web.id', 'customer.web_id')
            ->join('client', 'customer.client_id', 'client.id')
            ->groupBy('web_page_data.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_web_page_data_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, true, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_DATA));
        return parent::create($request);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, int $id): Factory|View
    {
        $this->tableRest->getTableFormItem('page_id')->setComboList(WebPage::comboListWithSecurity($this, WebPageType::WITH_DATA));
        return parent::edit($request, $id);
    }



    /**
     * @param WebPageDataRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageDataRequest $request): Model|JsonResponse|RedirectResponse|null
    {
        return $this->storeBasic($request);
    }


    /**
     * @param WebPageDataRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageDataRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
}
