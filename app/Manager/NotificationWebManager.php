<?php

namespace Canela\CanelaTools\Manager;


use App\Models\Project\WebBuilder\WebPageNotice;
use Canela\CanelaTools\Models\General\NotificationWeb;
use Minishlink\WebPush\Subscription;
use Minishlink\WebPush\WebPush;

/**
 * Class TranslationManager
 * @package App\Http\Manager
 */
class NotificationWebManager
{
    /**
     * Enviar notificaciones Web
     * @param $payload
     * @param $subject
     * @param $publicKey
     * @param $privateKey
     */
    public static function sendNotification($payload, $subject, $publicKey, $privateKey) {
        $notificationsWeb = NotificationWeb::all();
        $notifications = [];
        foreach ($notificationsWeb as $notificationWeb) {
            $notifications[] = (object)['subscription' => Subscription::create(
                ['endpoint' => $notificationWeb->endpoint,
                    'publicKey' => $publicKey]),
                'payload' => $payload];
        }
        if (!empty($notifications)) {
            $auth = [
                'VAPID' => [
                    'subject' => $subject,
                    'publicKey' => $publicKey,
                    'privateKey' => $privateKey,
                ]
            ];
            $webPush = new WebPush($auth);
            foreach ($notifications as $notification) {
                $webPush->queueNotification($notification->subscription, $notification->payload);
            }
            // handle eventual errors
            foreach ($webPush->flush() as $report) {
                $report->getRequest()->getUri()->__toString();
            }
        }
    }
}
