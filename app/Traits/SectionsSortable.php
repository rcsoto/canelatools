<?php

namespace Canela\CanelaTools\Traits;

use Canela\CanelaTools\Requests\General\ListOfGenericIdsRequest;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Log;

trait SectionsSortable
{
    /**
     * @param int $id
     * @param ListOfGenericIdsRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sortAssociatedSections(int $id, ListOfGenericIdsRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $sectionClassName = get_class(($this->entidad)::findOrFail($id)->sections()->getRelated());

            $pos = 1;
            foreach ($request->ids as $sectionId) {
                $section = ($sectionClassName)::findOrFail($sectionId);
                if ($section->page_id != $id) {
                    throw new Exception('Section id '.$section->id.' does not belong to the page id '.$id.'.');
                }
                ($sectionClassName)::findOrFail($sectionId)->update(['position' => $pos]);
                $pos++;
            }

            DB::commit();
            return response()->json([
                'result' => true,
                'message' => trans('canelatools::canelatools.general.label.success'),
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'result' => false,
                'message' => trans('canelatools::canelatools.model.update.error'),
            ]);
        }
    }

}
