<?php

namespace Canela\CanelaTools\Models\Location;


use Canela\CanelaTools\Models\BasicModel;


/**
 * Canela\CanelaTools\Models\Location\Country
 *
 * @property int                                                                                          $id
 * @property string                                                                                       $name
 * @property string                                                                                       $code2
 * @property string                                                                                       $code3
 * @property string                                                                                       $code_number
 * @property \Illuminate\Support\Carbon|null                                                              $created_at
 * @property \Illuminate\Support\Carbon|null                                                              $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\Location\City[]     $cities
 * @property-read mixed                                                                                   $combo_identificator
 * @property-read mixed                                                                                   $created_at_format
 * @property-read mixed                                                                                   $created_at_short
 * @property-read \Canela\CanelaTools\Models\Entity                                                       $table
 * @property-read mixed                                                                                   $updated_at_format
 * @property-read mixed                                                                                   $updated_at_short
 * @property-read string                                                                                  $value_identificator
 * @property-read \Illuminate\Database\Eloquent\Collection|\Canela\CanelaTools\Models\Location\Province[] $provinces
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereCode2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereCode3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereCodeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\Location\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Country extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'country';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code2', 'code3', 'code_number'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'name'        => 'string',
        'code2'       => 'string',
        'code3'       => 'string',
        'code_number' => 'string',
    ];

    /**
     * Associated provinces.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function provinces()
    {
        return $this->hasMany(Province::class, 'country_id');
    }

    /**
     * Associated cities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function cities()
    {
        return $this->hasManyThrough(City::class, Province::class);
    }
}