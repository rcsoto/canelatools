/******************************************************************
 * Animaciones
 ******************************************************************/
const _ANIMATIONS = [
    'bounce','flash','pulse','rubberBand','shake','swing','tada','wobble','jello','heartBeat',

    'bounceIn','bounceInDown','bounceInLeft','bounceInRight','bounceInUp',

    'fadeIn','fadeInDown','fadeInDownBig','fadeInLeft','fadeInLeftBig','fadeInRight','fadeInRightBig','fadeInUp','fadeInUpBig',

    'flip','flipInX','flipInY',

    'lightSpeedIn',

    'rotateIn','rotateInDownLeft','rotateInDownRight','rotateInUpLeft','rotateInUpRight',

    'slideInUp','slideInDown','slideInLeft','slideInRight',

    'zoomIn','zoomInDown','zoomInLeft','zoomInRight','zoomInUp',

    'jackInTheBox','rollIn',
];

jQuery.getAnimations = function()
{
    return _ANIMATIONS;
}


/******************************************************************
 * NOTIFICACIONES
 ******************************************************************/

/**
 * Mostrar mensaje success durante un tiempo
 */
jQuery.messageSuccess = function (msg, tiempo) {
    let animation = _ANIMATIONS[Math.floor(Math.random() * _ANIMATIONS.length)];
    jQuery.notify({
        // options
        message: msg
    },{
        // settings
        type: 'success',
        placement: {
            from: "top",
            align: "center"
        },
        animate: {
            enter: 'animate__animated animate__'+animation,
            exit: 'animate__animated animate__fadeOutDown'
        },
        showProgressbar: true,
        mouse_over: 'pause',
        newest_on_top: true
    });
};

/**
 * Mostrar mensaje de información durante un tiempo
 */
jQuery.messageInfo = function (msg, tiempo) {
    let animation = _ANIMATIONS[Math.floor(Math.random() * _ANIMATIONS.length)];
    jQuery.notify({
        // options
        message: msg
    },{
        // settings
        type: 'info',
        placement: {
            from: "top",
            align: "center"
        },
        animate: {
            enter: 'animate__animated animate__'+animation,
            exit: 'animate__animated animate__fadeOutDown'
        },
        showProgressbar: true,
        mouse_over: 'pause',
        newest_on_top: true
    });
};

/**
 * Mostrar mensaje de advertencia durante un tiempo
 */
jQuery.messageWarning = function (msg, tiempo) {
    let animation = _ANIMATIONS[Math.floor(Math.random() * _ANIMATIONS.length)];
    jQuery.notify({
        // options
        message: msg
    },{
        // settings
        type: 'warning',
        placement: {
            from: "top",
            align: "center"
        },
        animate: {
            enter: 'animate__animated animate__'+animation,
            exit: 'animate__animated animate__fadeOutDown'
        },
        showProgressbar: true,
        mouse_over: 'pause',
        newest_on_top: true
    });
};

/**
 * Mostrar mensaje de error
 */
jQuery.messageError = function (msg) {
    let animation = _ANIMATIONS[Math.floor(Math.random() * _ANIMATIONS.length)];
    jQuery.notify({
        // options
        message: msg
    },{
        // settings
        type: 'danger',
        delay: 0,
        placement: {
            from: "top",
            align: "center"
        },
        animate: {
            enter: 'animate__animated animate__'+animation,
            exit: 'animate__animated animate__fadeOutDown'
        },
        newest_on_top: true
    });
};
