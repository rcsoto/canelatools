<?php

namespace Canela\CanelaTools\Models;

use Canela\CanelaTools\Models\User\User;

/**
 * Canela\CanelaTools\Models\HistoryLog
 *
 * @property int                                       $id
 * @property int                                       $module_id
 * @property int                                       $action_id
 * @property int                                       $entity_id
 * @property int                                       $user_id
 * @property string|null                               $register
 * @property int|null                                  $register_id
 * @property string|null                               $comment
 * @property string|null                               $value_pre
 * @property string|null                               $value_post
 * @property \Illuminate\Support\Carbon|null           $created_at
 * @property \Illuminate\Support\Carbon|null           $updated_at
 * @property-read \Canela\CanelaTools\Models\Action    $action
 * @property-read \Canela\CanelaTools\Models\Entity    $entity
 * @property-read string                               $created_at_format
 * @property-read string                               $created_at_short
 * @property-read string                               $updated_at_format
 * @property-read string                               $updated_at_short
 * @property-read string                               $value_identificator
 * @property-read string                               $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Module    $module
 * @property-read \Canela\CanelaTools\Models\User\User $user
 * @property-read \Canela\CanelaTools\Models\Entity    $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereActionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereRegister($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereRegisterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereValuePost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel whereValuePre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class HistoryLog extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'history_log';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['module_id', 'entity_id', 'action_id', 'user_id', 'register_id', 'register', 'value_pre', 'value_post', 'comment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class, 'entity_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function action()
    {
        return $this->belongsTo(Action::class, 'action_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * Operaciones sobre un registro para una accion
     * @param string $entityName
     * @param int    $actionId
     * @param int    $registerId
     * @return HistoryLog[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getOperations($entityName, $actionId, $registerId)
    {
        $entity = Entity::getEntityByReference($entityName);

        return HistoryLog::where('entity_id', $entity->id)
                         ->where('action_id', $actionId)
                         ->where('register_id', $registerId)
                         ->get();
    }


    /**
     * Log interactions with DB
     * @param string  $entityName
     * @param integer $moduleId
     * @param integer $actionId
     * @param integer $userId
     * @param mixed   $registrePost
     * @param mixed   $registrePre
     * @param null    $comment
     */
    public static function log($entityName, $moduleId, $actionId, $userId, $registrePost = null, $registrePre = null, $comment = null, $saveValues = true)
    {
        try {
            // Obtenemos la entidad a partir del nombre
            $entity   = Entity::whereReference($entityName)->first();
            $entityId = null;
            if ($entity instanceof Entity) {
                $entityId = $entity->id;
            }

            // Creamos un nuevo registro
            $historyLog = new HistoryLog();

            $historyLog->module_id   = $moduleId;
            $historyLog->entity_id   = $entityId;
            $historyLog->action_id   = $actionId;
            $historyLog->user_id     = empty($userId) ? null : $userId;
            $historyLog->register_id = (empty($registrePost) ? (empty($registrePre) ? null : $registrePre->id) : $registrePost->id);
            $historyLog->register    = (empty($registrePost) ? (empty($registrePre) ? null : $registrePre->valueIdentifier) : $registrePost->valueIdentifier);
            if ($saveValues) {
                $historyLog->value_pre  = self::entityToString($registrePre);
                $historyLog->value_post = self::entityToString($registrePost);
            }

            $historyLog->comment = $comment;

            $historyLog->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
    }


    /**
     *
     * Enter description here ...
     * @param mixed $registre
     * @return string
     */
    private static function entityToString($registre)
    {
        if (empty($registre)) {
            return "";
        }

        $valorEntidad = "";
        foreach ($registre->toArray() as $clave => $valor) {
            try {
                if (!is_array($valor)) {
                    $valorEntidad .= "[" . $clave . "]:" . $valor . ";;";
                }
            } catch (\Exception $e) {
                $valorEntidad .= "[" . $clave . "]:error;;";
            }
        }

        return json_encode(addslashes(html_entity_decode($valorEntidad)));
    }
}
