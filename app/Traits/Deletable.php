<?php

namespace Canela\CanelaTools\Traits;


use Composer\Autoload\ClassMapGenerator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

trait Deletable
{
    private $appModels;

    /**
     * Check if a model can be deleted, or it has dependency (hasMany) relations created.
     *
     * @param array $tables_with_relations
     * @return bool
     */
    public function canDelete(&$tables_with_relations = []): bool
    {
        set_time_limit(600);
        $canDelete = true;

        try {
            foreach ($this->getTableReferences() as $relation) {
                $tableSchema     = new TableSchema($relation);
                $modelCollection = $this->getSchemaRelation($tableSchema);
                if ($modelCollection->isNotEmpty()) {
                    $canDelete                = false;
                    $msg                      = "{$this->transTableName($tableSchema->getTableName())} " .
                                                trans_choice("canelatools::canelatools.tables_assoc_count",
                                                             $modelCollection->count(),
                                                             ['value' => $modelCollection->count()]);
                    $tables_with_relations [] = $msg;
                }
            }
            return $canDelete;

        } catch (\Exception $exception) {
            \Log::error($exception);
            return false;
        }
    }

    /**
     * Delete model and its Associated (hasMany) models.
     *
     * @return bool|null
     */
    public function delete()
    {
        set_time_limit(600);
        try {
            foreach ($this->getTableReferences() as $relation) {
                $tableSchema     = new TableSchema($relation);
                $modelCollection = $this->getSchemaRelation($tableSchema);

                $modelCollection->each(function ($model) use ($tableSchema) {
                    if ($model instanceof \stdClass) { //->isSubclassOf('Illuminate\Database\Eloquent\Model')
                        $local_column = $tableSchema->referenced_column_name;
                        \DB::table($tableSchema->table_name)
                           ->where($tableSchema->column_name, $this->$local_column)
                           ->delete();
                    } else {
                        $model->forceDelete();
                    }
                });
            }
            return parent::delete();

        } catch (\Exception $exception) {
            \Log::error($exception);
            return false;
        }
    }

    /**
     * Get table relations with other tables.
     *
     * @return array
     */
    private function getTableReferences()
    {
        set_time_limit(600);
        return \DB::select(/** @lang SQL */
            "SELECT *
                  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                      where CONSTRAINT_SCHEMA = '" . config('canelatools.database') . "'
                      and REFERENCED_TABLE_NAME = '{$this->getTable()}'
                  ");
    }

    /**
     * Load hasMany related table.
     *
     * @param TableSchema $schema
     * @return \Illuminate\Support\Collection
     */
    private function getSchemaRelation(TableSchema $schema)
    {
        set_time_limit(600);
        $local_column = $schema->referenced_column_name;

        //Find associated class
        $className = ucfirst(Str::camel($schema->table_name));
        $hasModel  = null;
        $this->getAppModels()
             ->map(function ($model) use ($className, &$hasModel) {
                 if ($className === array_values(array_slice(explode('\\', $model), -1))[0]) {
                     $hasModel = $model;
                 }
             });

        if ($hasModel) {
            return $hasModel::where($schema->column_name, $this->$local_column)
                            ->get();
        } else {
            return \DB::table($schema->table_name)
                      ->where($schema->column_name, $this->$local_column)
                      ->get();
        }
    }

    /**
     * Get all class models in app.
     *
     * @return \Illuminate\Support\Collection
     */
    private function getAppModels()
    {
        set_time_limit(600);

        if (empty($this->appModels)) {
            $models = collect();
            $dir    = base_path() . '/' . config('canelatools.model_locations');
            if (file_exists($dir)) {
                foreach (ClassMapGenerator::createMap($dir) as $model => $path) {
                    $models->push($model);
                }
            }
            return $models;
        } else {
            return $this->appModels;
        }
    }

    /**
     * Get table name.
     *
     * @param string $name
     * @return string
     */
    private function transTableName(string $name): string
    {
        set_time_limit(600);

        if (!empty(config('canelatools.entity_table'))) {
            $consulta = "select `". config('canelatools.entity_name') ."` as name
                         from `".config('canelatools.entity_table')."`
                         where `". config('canelatools.entity_reference') ."` = '".$name."'";
            $table = DB::select($consulta);
            return empty($table) ? $name : (collect($table)->first())->name;
        }
        else {
            return trans("canelatools::canelatools.{$name}");
        }
    }
}

class TableSchema
{
    public $table_name;
    public $column_name;
    public $referenced_table_name;
    public $referenced_column_name;

    /**
     * TableSchema constructor.
     * @param \stdClass $args
     */
    public function __construct(\stdClass $args)
    {
        $this->table_name             = $args->TABLE_NAME;
        $this->column_name            = $args->COLUMN_NAME;
        $this->referenced_table_name  = $args->REFERENCED_TABLE_NAME;
        $this->referenced_column_name = $args->REFERENCED_COLUMN_NAME;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * @param string $table_name
     * @return TableSchema
     */
    public function setTableName($table_name)
    {
        $this->table_name = $table_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getColumnName()
    {
        return $this->column_name;
    }

    /**
     * @param string $column_name
     * @return TableSchema
     */
    public function setColumnName($column_name)
    {
        $this->column_name = $column_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferencedTableName()
    {
        return $this->referenced_table_name;
    }

    /**
     * @param string $referenced_table_name
     * @return TableSchema
     */
    public function setReferencedTableName($referenced_table_name)
    {
        $this->referenced_table_name = $referenced_table_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferencedColumnName()
    {
        return $this->referenced_column_name;
    }

    /**
     * @param string $referenced_column_name
     * @return TableSchema
     */
    public function setReferencedColumnName($referenced_column_name)
    {
        $this->referenced_column_name = $referenced_column_name;
        return $this;
    }
}
