<?php

namespace Canela\CanelaTools\Requests\General;

use Canela\CanelaTools\Enums\HttpMethod;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\General\ArchiveType;
use Canela\CanelaTools\Requests\GenericRequest;
use Validator;

/**
 * Class ArchiveRequest
 * @package Canela\CanelaTools\Requests\General
 */
class ArchiveRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'type_id',
        'available_in_main_language',
        'duration',
        'archive',
        'title',
        'name_download',
        'description',
        'reference',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.archive.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function rules(): array
    {
        $rules = [
            'type_id'           => 'required|integer|min:1|exists:archive_type,id',
            'available_in_main_language' => 'required|integer|between:0,1',
            'duration'          => 'nullable|integer|min:1|max:99999',
            'title'             => 'nullable|max:255',
            'name_download'     => 'nullable|max:255',
            'description'       => 'nullable|max:4000',
            'reference'         => 'nullable|max:45',
        ];

        $validator = Validator::make($this->request->all(), $rules);
        $validator->setAttributeNames($this->attributes());
        $validator->validate();

        $archiveRule = null;
        if ($this->isMethod(HttpMethod::POST)) {
            $archiveRule = 'required';

            ArchiveType::addArchiveRuleDependingOnArchiveType($this->request->get('type_id'), $archiveRule);

        } elseif($this->isMethod(HttpMethod::PUT)) {

            // Check if edited archive has archive value filled.
            $archive = Archive::findOrFail($this->route('archive'));
            $archiveRule = empty($archive->archive) ? 'required' : 'nullable';

            ArchiveType::addArchiveRuleDependingOnArchiveType($this->request->get('type_id'), $archiveRule);

        }

        if (!is_null($archiveRule)) {
            $rules = array_merge([
                'archive'      => $archiveRule,
            ], $rules);
        }

        return $rules;
    }

}
