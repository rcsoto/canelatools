<?php

namespace Canela\CanelaTools\Enums;


abstract class TableColumnTypes
{
    const DATE              = 'date';
    const TIME              = 'time';
    const DATETIME          = 'datetime';
    const TEXT              = 'text';
    const TEXTAREA          = 'textarea';
    const WYSIWYG           = 'wysiwyg';
    const TAGS              = 'tags';
    const FILE              = 'file';
    const IMAGE             = 'image';
    const IMAGE_CROPPER     = 'image_cropper';
    /** @deprecated */
    const SELECT            = 'select';
    const SELECT2           = 'select2';
    const REAL              = 'real';
    const INT               = 'int';
    const CHECKBOX          = 'checkbox';
    const HIDDEN            = 'hidden';

    const countables        = [
        self::TEXT,
        self::TEXTAREA,
    ];

    const imageTypes        = [
        self::IMAGE,
        self::IMAGE_CROPPER,
    ];

    const fileTypes         = [
        self::FILE,
        ...self::imageTypes
    ];

    const translatable      = [
        self::TEXT,
        self::TEXTAREA,
        self::WYSIWYG,
        self::TAGS,
        ...self::fileTypes,
    ];

    /**
     * @param string $type
     * @return bool
     */
    public static function countable(string $type): bool
    {
        return in_array($type, self::countables);
    }


    /**
     * @param string $type
     * @return bool
     */
    public static function translatable(string $type): bool
    {
        return in_array($type, self::translatable);
    }


    /**
     * @param string $type
     * @return bool
     */
    public static function isImageType(string $type): bool
    {
        return in_array($type, self::imageTypes);
    }


    /**
     * @param string $type
     * @return bool
     */
    public static function isFileType(string $type): bool
    {
        return in_array($type, self::fileTypes);
    }
}
