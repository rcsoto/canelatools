<?php


namespace Canela\CanelaTools\Models\User;

use Canela\CanelaTools\Models\BasicModel;

/**
 * Class GroupUser
 * @package Canela\CanelaTools\Models\User
 */
class GroupUser extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'name'        => 'string',
        'description' => 'string',
    ];

    /**
     * Related users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_group_user', 'group_id', 'user_id');
    }


    /**
     * getUsersIdsAtttribute
     *
     * @return array
     */
    public function getSelectedUsersIdsAttribute()
    {
        return $this->users->pluck('id')->toJson();
    }

}
