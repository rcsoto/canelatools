<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use App\Models\General\ArchiveType;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebPageSectionArchive extends BasicModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_section_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_id',
        'archive_id',
        'position',
        'title',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'section_id' => 'integer',
        'archive_id' => 'integer',
        'position' => 'integer',
        'title' => 'string',
        'description' => 'string',
    ];


    protected $with = ['archiveEntity'];

    /**
     * @return BelongsTo
     */
    public function section(): BelongsTo
    {
        return $this->belongsTo(WebPageSection::class, 'section_id');
    }


    /**
     * @return BelongsTo
     */
    public function archiveEntity(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'archive_id');
    }

}
