<?php

namespace Canela\CanelaTools\Controller\General;


use App\Models\General\Archive;
use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\General\ArchiveType;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\Translation;
use Canela\CanelaTools\Requests\General\ArchiveRequest;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use stdClass;
use Storage;

/**
 * Class ArchiveController
 * @package Canela\CanelaTools\Controller\General
 */
class ArchiveController extends BasicController
{
    use HtmlLists, Fileable;

    /**
     * ArchiveController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Archive::class;
        $this->routeEntity = 'admin.general.archive';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::administrator.general.archive.addEdit';
        $this->vistaAdd    = 'canelatools::administrator.general.archive.addEdit';

        $this->menu       = AdminMenu::ARCHIVE;
        $this->menu_group = AdminMenu::GROUP_CONFIGURATION;

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.archive.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.archive.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: Archive::ARCHIVE_FIELD_NAME,
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'archive_type',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.archive.field.type_id'),
                    fieldFilter: 'id',
                    comboList: ArchiveType::comboTranslate(),
                ),
                new TableColumn(
                    table: Archive::ENTITY_TABLE,
                    fieldOrder: 'duration',
                    label: trans('canelatools::canelatools.entity.archive.field.duration'),
                ),
                new TableColumn(
                    table: Archive::ENTITY_TABLE,
                    fieldOrder: Archive::ARCHIVE_FIELD_NAME,
                    label: trans('canelatools::canelatools.entity.archive.field.archive'),
                ),
                new TableColumn(
                    table: Archive::ENTITY_TABLE,
                    fieldOrder: 'title',
                    label: trans('canelatools::canelatools.entity.archive.field.title'),
                ),
                new TableColumn(
                    table: Archive::ENTITY_TABLE,
                    fieldOrder: 'reference',
                    label: trans('canelatools::canelatools.entity.archive.field.reference'),
                ),
                new TableColumn(
                    table: Archive::ENTITY_TABLE,
                    fieldOrder: 'description',
                    label: trans('canelatools::canelatools.entity.archive.field.description'),
                ),
                new TableColumn(
                    label: '',
                    withOrderBy: false,
                    withFilter: false,
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'type_id',
                    label: trans('canelatools::canelatools.entity.archive.field.type_id'),
                    required: true,
                    containerClass: 'col-12 col-sm-4',
                    comboList: ArchiveType::comboTranslate(),
                ),
                new TableForm(
                    name: 'available_in_main_language',
                    label: trans('canelatools::canelatools.entity.archive.field.available_in_main_language'),
                    required: true,
                    containerClass: 'col-12 col-sm-4',
                    comboList: Archive::comboBoolean(),
                ),
                new TableForm(
                    name: 'duration',
                    label: trans('canelatools::canelatools.entity.archive.field.duration'),
                    type: TableColumnTypes::INT,
                    containerClass: 'col-12 col-sm-4 mb-2',
                    min: 1,
                    max: 99999,
                ),
                new TableForm(
                    id: 'archive_text',
                    name: Archive::ARCHIVE_FIELD_NAME,
                    label: trans('canelatools::canelatools.entity.archive.field.archive'),
                    required: true,
                    containerClass: 'col-12 canela-input-file-container',
                    max: 255,
                    allLanguages: true,
                ),
                new TableForm(
                    id: 'archive_file',
                    name: Archive::ARCHIVE_FIELD_NAME,
                    label: trans('canelatools::canelatools.entity.archive.field.archive'),
                    required: true,
                    type: TableColumnTypes::FILE,
                    fileDir: Archive::DIR_ARCHIVES,
                    allLanguages: true,
                ),
                new TableForm(
                    id: 'archive_image_cropper',
                    name: Archive::ARCHIVE_FIELD_NAME,
                    label: trans('canelatools::canelatools.entity.archive.field.archive'),
                    required: true,
                    type: TableColumnTypes::IMAGE_CROPPER,
                    fileDir: Archive::DIR_ARCHIVES,
                    allLanguages: true,
                ),
                new TableForm(
                    name: 'title',
                    label: trans('canelatools::canelatools.entity.archive.field.title'),
                    max: 255,
                ),
                new TableForm(
                    name: 'name_download',
                    label: trans('canelatools::canelatools.entity.archive.field.name_download'),
                    max: 255,
                ),
                new TableForm(
                    name: 'description',
                    label: trans('canelatools::canelatools.entity.archive.field.description'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
                new TableForm(
                    name: 'reference',
                    label: trans('canelatools::canelatools.entity.archive.field.reference'),
                    max: 45,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index view. Show list of items.
     *
     * @return Factory|View
     */
    protected function index(): Factory|View
    {
        if (request()->has('selectable') && request()->has('url_select') && request()->has('url_back')) {
            array_pop($this->tableRest->tableColumn);
            array_unshift($this->tableRest->tableColumn, new TableColumn(
                label: trans('canelatools::canelatools.general.label.select'),
                withOrderBy: false,
                withFilter: false,
            ));
        }

        return parent::index();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $lang     = App::getLocale();
        $select = [
            'archive.id',
            'archive_type.name as archive_type_name',
            'archive.duration',
            'archive.archive',
            'archive.title',
            'archive.reference',
            'archive.description',
            'archive.archive as preview',
        ];

        /* Add selectable functionality */
        if ($request->has('selectable')) {
            array_unshift($select, 'archive.id as selectable_id');
        } else {
            $select = array_merge($select, ['archive.id as action_archive_id']);
        }
        /* End Add selectable functionality */

        $select = array_merge($select, ['archive_type.id as archive_type_id']);

        $query = Archive::select($select)
                        ->join('archive_type', 'archive.type_id', 'archive_type.id');

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            switch ($key) {
                case 'selectable_id':
                    $txt = '<div class="custom-control custom-checkbox text-center"><input type="checkbox" id="item_'.$value.'" name="item_'.$value.'" class="custom-control-input selectable" style="position: static;" value="'.$value.'"><label class="custom-control-label" for="item_'.$value.'"></label></div>';
                    break;
                case 'description':
                    $txt = '<div class="td-multiple-lines">'.$value.'</div>';
                    break;
                case 'preview':
                    $txt = $jsonItem->archive_type_id == ArchiveType::IMAGE
                        ? '<div class="text-center" style="min-width: 100px;">'.$this->getListImage(url: asset(config('canelatools.folder.storage'). \Canela\CanelaTools\Models\General\Archive::DIR_ARCHIVES.$value), classes: 'list-image-thumb').'</div>'
                        : '';
                    break;
                case 'action_archive_id':
                    $txt = $this->addActionButton($value, $this->routeEntity, null,
                        true, true, false, false,
                        false, false, 0, 0, true,
                        true);
                    break;
                default:
                    $txt = $value;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * @param $register
     * @param $request
     * @return Application|Factory|View
     */
    public function setParamReturn($register, $request) {
        return view($this->vistaEdit, compact('register'))
            ->with('urlPrefix', $this->urlPrefix)
            ->with('tableRest', $this->tableRest);
    }


    /**
     * @param ArchiveRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(ArchiveRequest $request) {
        return $this->storeBasic($request);
    }


    /**
     * updatePre
     *
     * @param Archive $model
     * @param ArchiveRequest $request
     * @return Archive
     */
    public function updateValidate(Archive $model, ArchiveRequest $request): Archive {
        if (!$model->type->isTextInputArchiveType() && ArchiveType::isTextInputArchiveTypeById($request->type_id)) {
            // Remove previous file.
            $this->deleteFile(Archive::ARCHIVE_FIELD_NAME, Archive::DIR_ARCHIVES, $model);
        }
        return $model;
    }


    /**
     * @param ArchiveRequest $request
     * @param $id
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function update(ArchiveRequest $request, $id) {
        return $this->updateBasic($request, $id);
    }


    /**
     * updatePostRequest
     *
     * @param Archive $model
     * @param ArchiveRequest $request
     * @return ArchiveRequest
     */
    public function storePostRequest(Archive $model, ArchiveRequest $request): ArchiveRequest {
        return $this->removeRequestArchiveParamIfTextType($request);
    }


    /**
     * updatePostRequest
     *
     * @param Archive $model
     * @param ArchiveRequest $request
     * @return ArchiveRequest
     */
    public function updatePostRequest(Archive $model, ArchiveRequest $request): ArchiveRequest {
        return $this->removeRequestArchiveParamIfTextType($request);
    }


    /**
     * @param ArchiveRequest $request
     * @return ArchiveRequest
     */
    private function removeRequestArchiveParamIfTextType(ArchiveRequest $request): ArchiveRequest {
        if (ArchiveType::isTextInputArchiveTypeById($request->type_id)) {
            // Archive request entry is plain text. It's not a file.
            // Remove archive request entry to avoid storing file in next method.
            $request->request->remove(Archive::ARCHIVE_FIELD_NAME);
        }
        return $request;
    }


    /**
     * @param $model
     */
    public function destroyPre($model) {
        // Delete all translations and it's associated files.
        $archiveEntity = Entity::where('reference', Archive::ENTITY_TABLE)->firstOrFail();
        $translations = Translation::where('entity_id', $archiveEntity->id)
                                    ->where('register_id', $model->id);

        if (!$model->type->isTextInputArchiveType()) {
            $archiveFieldName = Archive::ARCHIVE_FIELD_NAME;
            foreach ($translations->with('language')->get()->filter(function ($item) use ($archiveFieldName) {
                return $item->code == $archiveFieldName;
            }) as $translation) {
                // Delete translation associated file if exists.
                $archivesLocaleDir = Archive::DIR_ARCHIVES.$translation->language->code;
                $path = $archivesLocaleDir.'/'.$translation->value;
                if (Storage::exists($path)) {
                    Storage::delete($path);
                }
            }
        }

        $translations->delete();

        if (!$model->type->isTextInputArchiveType()) {
            // Delete archive associated file.
            $this->deleteFile(Archive::ARCHIVE_FIELD_NAME, Archive::DIR_ARCHIVES, $model);
        }
    }
}
