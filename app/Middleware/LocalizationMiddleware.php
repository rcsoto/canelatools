<?php

namespace Canela\CanelaTools\Middleware;

use Canela\CanelaTools\Manager\LocalizationManager;
use Closure;
use Exception;
use Illuminate\Http\Request;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        if (session()->has(LocalizationManager::SESSION_LANG)) {
            app()->setLocale(session()->get(LocalizationManager::SESSION_LANG));
        } else {
            $locale = LocalizationManager::getBrowserLocaleOrDefault($request);
            LocalizationManager::storeLocaleInSession($locale, false);
            app()->setLocale($locale);
        }
        return $next($request);
    }
}
