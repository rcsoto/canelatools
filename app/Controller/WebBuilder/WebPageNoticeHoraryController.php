<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\WebPageNotice;
use Canela\CanelaTools\Models\WebBuilder\WebPageNoticeHorary;
use Canela\CanelaTools\Requests\WebBuilder\WebPageNoticeHoraryRequest;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use stdClass;

/**
 * Class WebPageNoticeHoraryController
 * @package Canela\CanelaTools\Controller\WebBuilder
 */
class WebPageNoticeHoraryController extends BasicController
{
    use HtmlLists, Fileable;

    /**
     * WebPageController constructor.
     */
    public function __construct()
    {
        $this->entidad     = WebPageNoticeHorary::class;
        $this->routeEntity = 'admin.webbuilder.webpagenoticehorary';

        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::CLIENT;          // TODO: CAMBIAR!!!
        $this->menu_group = AdminMenu::GROUP_CLIENT;    // TODO: CAMBIAR!!!

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools::canelatools.entity.web_page_notice_horary.list'))
            ->setFormTitle(trans('canelatools::canelatools.entity.web_page_notice_horary.report'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setTableColumn([
                new TableColumn(
                    table: 'web_page_notice_horary',
                    fieldOrder: 'id',
                ),
                new TableColumn(
                    table: 'web_page_notice_horary',
                    fieldOrder: 'date_from',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.date_from'),
                    type: TableFilterTypes::FILTRO_TIPO_DATE,
                ),
                new TableColumn(
                    table: 'web_page_notice_horary',
                    fieldOrder: 'date_to',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.date_to'),
                    type: TableFilterTypes::FILTRO_TIPO_DATE,
                ),
                new TableColumn(
                    table: 'web_page_notice_horary',
                    fieldOrder: 'time_from',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.time_from'),
                ),
                new TableColumn(
                    table: 'web_page_notice_horary',
                    fieldOrder: 'time_to',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.time_to'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'notice_id',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.notice_id'),
                    required: true,
                    comboList: WebPageNotice::comboTranslate(),
                ),
                new TableForm(
                    name: 'date_from',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.date_from'),
                    required: true,
                    type: TableColumnTypes::DATE,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'date_to',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.date_to'),
                    required: false,
                    type: TableColumnTypes::DATE,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'time_from',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.time_from'),
                    required: false,
                    type: TableColumnTypes::TIME,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'time_to',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.time_to'),
                    required: false,
                    type: TableColumnTypes::TIME,
                    containerClass: 'col-6 mb-2',
                ),
                new TableForm(
                    name: 'comment',
                    label: trans('canelatools::canelatools.entity.web_page_notice_horary.field.comment'),
                    type: TableColumnTypes::TEXTAREA,
                    max: 4000,
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $noticeId  = $this->getValueParameterFilters('parameterFilters', 'notice_id', null);
        $query = WebPageNoticeHorary::select([
            'web_page_notice_horary.id',
            DB::Raw('DATE_FORMAT(web_page_notice_horary.date_from, "'.WebPageNoticeHorary::formatDate().'") as web_page_notice_horary_date_from'),
            DB::Raw('DATE_FORMAT(web_page_notice_horary.date_to, "'.WebPageNoticeHorary::formatDate().'") as web_page_notice_horary_date_to'),
            DB::Raw('DATE_FORMAT(web_page_notice_horary.time_from, "'.WebPageNoticeHorary::formatDateTime().'") as web_page_notice_horary_time_from'),
            DB::Raw('DATE_FORMAT(web_page_notice_horary.time_to, "'.WebPageNoticeHorary::formatDateTime().'") as web_page_notice_horary_time_to'),
            'web_page_notice_horary.id as action_web_page_notice_horary_id',
        ])
        ->join('web_page_notice', 'web_page_notice_horary.notice_id', 'web_page_notice.id')
        ->join('web_page', 'web_page_notice.page_id', 'web_page.id')
        ->join('web', 'web_page.web_id', 'web.id')
        ->join('customer', 'web.id', 'customer.web_id')
        ->join('client', 'customer.client_id', 'client.id');

        if (isset($noticeId)) {
            $query->where('web_page_notice.id', $noticeId);
        }

        return $this->setSecurityList($query);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_web_page_notice_horary_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                    true, true, false, false,
                    false, false, 0, 0, true,
                    true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setParamReturnCreate($request)
    {
        $webPageNotice = null;
        if ($request->filled('notice_id')) {
            $webPageNotice = WebPageNotice::findOrFail($request->get('notice_id'));
            $this->tableRest
                    ->setUrlIndex(route('admin.webbuilder.webpagenotice.edit', $webPageNotice->id))
                    ->getTableFormItem('notice_id')->value = $webPageNotice->id
                    ->getTableFormItem('notice_id')->setReadOnly(true);
        }

        return view($this->vistaAdd)->with('tableRest', $this->tableRest)
                                    ->with('webPageNotice', $webPageNotice);
    }

    /**
     * @param $register
     * @param $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setParamReturn($register, $request) {
        $this->tableRest
                ->setUrlIndex(route('admin.webbuilder.webpagenotice.edit', $register->notice_id))
                ->setParametersFilters(['notice_id' => $register->notice_id])
                ->getTableFormItem('notice_id')->setReadOnly(true);
        return view($this->vistaEdit, compact('register'))
            ->with('tableRest', $this->tableRest)
            ->with('webPageNotice', $register->notice);
    }


    /**
     * @param WebPageNoticeHoraryRequest $request
     * @return Model|JsonResponse|RedirectResponse|null
     * @throws Exception
     */
    public function store(WebPageNoticeHoraryRequest $request) {

        return $this->storeBasic($request, route('admin.webbuilder.webpagenotice.edit', $request->get('notice_id')));
    }


    /**
     * @param WebPageNoticeHoraryRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebPageNoticeHoraryRequest $request, int $id) {
        return $this->updateBasic($request, $id, route('admin.webbuilder.webpagenotice.edit', $request->get('notice_id')));
    }

}
