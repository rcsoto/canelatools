@php
    use App\Models\Project\WebBuilder\Web;
    $web = Web::firstOrFail();
    $customer = $web->customer;

    if (empty($auth)) { $auth = false; }
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>{{$customer->business_name}}</title>

        <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.Laravel = {'csrfToken': '{{csrf_token()}}'};
        </script>

        <!-- Favicon icon -->
        @include('layouts.favicon')
        <link rel="stylesheet" href="{{asset('css/flags/flag-icon.min.css')}}">

        <!-- Cropper.css -->
        <link rel="stylesheet" href="{{asset('js/admin/plugins/cropper/cropper.min.css')}}">

        <!-- Select2 css -->
        <link rel="stylesheet" href="{{asset('css/admin/plugins/select2.min.css')}}">

        <!-- AnimateCss -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset(mix('css/admin/app.min.css')) }}">

        @yield('styles')

    </head>
    <body class="@yield('body-class')">

        <!-- [ app ] start -->
        <div id="app">

            <!-- [ Pre-loader ] start -->
            <div class="loader-bg">
                <div class="loader-track">
                    <div class="loader-fill"></div>
                </div>
            </div>
            <!-- [ Pre-loader ] End -->

            <!-- Messages container -->
            @include('canelatools::layouts.messages.messages')
            <!-- /Messages container -->

            @includeWhen(!$auth, 'canelatools::administrator.layouts.navigation-menu')

            @includeWhen(!$auth, 'canelatools::administrator.layouts.header')

            @if($auth)
                <div class="container">
                    <!-- [ Main Content ] start -->
                    @yield('content')
                    <!-- [ Main Content ] end -->
                </div>
            @else
                <div class="pcoded-main-container">
                    <div class="pcoded-wrapper fluid-container">
                        <div class="pcoded-content">
                            <div class="pcoded-inner-content">
                                <div class="main-body">
                                    <div class="page-wrapper">
                                        <div class="page-header">
                                            <div class="page-block">
                                                <div class="row align-items-center">
                                                    <div class="col-md-12">

                                                        @yield('breadcrumbs')

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- [ Main Content ] start -->
                                    @yield('content')
                                    <!-- [ Main Content ] end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
        <!-- [ app ] end -->

        @include('layouts.older-ie-warning-message')

        @yield('body-modals')

        @yield('global-scripts')

        <!-- Required Js -->
        <script src="{{asset('js/admin/vendor-all.min.js')}}"></script>
        <script src="{{asset('js/admin/plugins/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/admin/plugins/bootstrap-tagsinput.min.js')}}"></script>
        <script src="{{asset('js/admin/plugins/bootstrap-notify.min.js')}}"></script>
        <script src="{{asset('js/admin/ripple.js')}}"></script>
        <script src="{{asset(mix('js/admin/pcoded.min.js'))}}"></script>
        <script src="{{asset('js/admin/plugins/apexcharts.min.js')}}"></script>
        <script src="{{asset('js/admin/plugins/cropper/cropper.min.js')}}"></script>

        <!-- CKEditor 4 Full  -->
        <script src="https://cdn.ckeditor.com/4.16.2/full/ckeditor.js"></script>

        <script src="{{asset('js/admin/plugins/jquery-ui.min.js')}}"></script>

        <!-- Sweet alert 2 -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

        <!-- Select2 js -->
        <script src="{{asset('js/admin/plugins/select2.full.min.js')}}"></script>

        <!-- DataTables -->
        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

        <!-- es6-promise -->
        <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

        <!-- Vue.js, development version, includes helpful console warnings -->
{{--        <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>--}}

        <!-- Vue.js, production version, optimized for size and speed -->
{{--        <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>--}}

        <!-- Axios -->
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>--}}

        <!-- Canelatools -->
        <script src="{{asset(mix('js/canelatools.min.js'))}}"></script>

        <script src="{{asset('js/admin/horizontal-menu.js')}}"></script>
        <script>
            jQuery(document).ready(function() {
                jQuery("#pcoded").pcodedmenu({
                    themelayout: 'horizontal',
                    MenuTrigger: 'hover',
                    SubMenuTrigger: 'hover',
                });
                jQuery('.select2').select2();
            });
        </script>

        @include('canelatools::rest.js-dynamic-table-functions')

        @yield('content-table', '')

        @yield('body-modals')

        @yield('body-scripts')
        @yield('body-scripts-extra')
        @yield('scripts-popup', '')

        @yield('vue-script')

        <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery.loadEnd();
            })
        </script>

    </body>
</html>
