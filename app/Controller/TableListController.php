<?php

namespace Canela\CanelaTools\Controller;


use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\Manager\ExcelManager;
use Canela\CanelaTools\Manager\UtilsBasicManager;
use Canela\CanelaTools\Models\Entity;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Canela\CanelaTools\Models\BasicModel;

trait TableListController
{
    public static $TABLE_FILTER_PREFIX = "filter_";

    /**
     *
     * @param Request $request
     * @return string
     */
    protected function listAjax(Request $request)
    {
        try {
            if ($request->ajax()) {//Si es ajax es listar

                if (method_exists($this, 'getConsultaPersonalizada')) {
                    $listItem = $this->getConsultaPersonalizada($request);
                } else {
                    throw new \Exception('getConsultaPersonalizada not found', 500);
                }

                $listItem = $this->buildFilterAdmin($request, $listItem);
                $datos    = [];
                $columnas = 0;
                foreach ($listItem as $items) {
                    $jsonItem = json_decode($items);
                    $fila     = [];

                    // anadimos informacion en la columna de acciones
                    if (method_exists($this, 'personalizaLista')) {
                        $fila = $this->personalizaLista($jsonItem);
                    }            // si no existe metodo personalizado anadimos toda la lista
                    else {
                        foreach ($jsonItem as $key => $value) {
                            $fila [] = $value;
                        }
                    }

                    $datos [] = $fila;
                }

                if (!empty ($datos)) {
                    $columnas = count($datos [0]);
                }

                // datos paginacion
                $pagination = self::getPagination($listItem);

                return response()->json(array(
                                       'result'     => 1,
                                       'msg'        => 'OK',
                                       'data'       => $datos,
                                       'pagination' => $pagination,
                                       'columnas'   => $columnas,
                                   ));
            } else { // Si es llamada post estándar devolver el excel
                // aumentamos el tiempo de ejecucion a 10 min
                set_time_limit(3600);
                ini_set('memory_limit', '512M');

                if (method_exists($this, 'getConsultaPersonalizada')) {
                    /** @var Builder $listItem */
                    $listItem = $this->getConsultaPersonalizada($request);
                } else {
                    throw new \Exception('getConsultaPersonalizada not found', 500);
                }

                $listItem = $this->buildFilterAdmin($request, $listItem, false);

                $listItem = $listItem->get();

                // Personalizar datos
                if (method_exists($this, 'personalizaLista')) {
                    foreach ($listItem as $item) {
                        $arrayItem = $item->attributesToArray();
                        $customItem = $this->personalizaLista((object)$arrayItem);
                        foreach (array_combine(array_keys($arrayItem), $customItem) as $key => $value) {
                            // Check if html tags
                            if($value === strip_tags($value)) {
                                $item[$key] = $value;
                            }
                        }
                    }
                }

                //Make Hidden some attributes
                if (!empty($listItem->count())) {
                    $keys   = array_keys($listItem->first()->attributesToArray());

                    // Visibilities
                    if ($request->has('visibilities')) {
                        $visibilities = json_decode($request->visibilities);

                        for($i=0;$i<count($keys)-1;$i++) {
                            if ($i<=count($visibilities)-1) {
                                if (!$visibilities[$i]) {
                                    $listItem->makeHidden($keys[$i]);
                                }
                            } else {
                                $listItem->makeHidden($keys[$i]);
                            }
                        }
                    }

                    // Hide columns after "action_"
                    $search = preg_grep("/action_.+/", $keys);
                    for ($x = key($search); $x < count($keys); $x++) {
                        $listItem->makeHidden($keys[$x]);
                    }

                    //titles substitution
                    $titles2 = json_decode($request->input('titles'));

                    // Remove "Acciones" title
                    if (($key = array_search(trans('general.common.actions'), $titles2)) !== false) {
                        unset($titles2[$key]);
                    }

                    $titles  = [];
                    for ($x = 0; $x < min(count($titles2), key($search)); $x++) {
                        $titles[] = $titles2[$x];
                    }

                    $tableName = Entity::getNameEntity($listItem->first()->getTable());
                    return ExcelManager::buildExcel($listItem, $tableName, $titles);
                } else {
                    return redirect()->back()->withInput()->with('msgWarning', 'No se puede exportar una tabla vacía.');
                }

            }
        } catch (\Exception $exception) {
            \Log::error($exception);
            if ($request->ajax()) {
                return response()->json(array(
                                       'result' => false,
                                       'msg'    => $exception->getMessage(),
                                   ));
            } else {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('msgWarning', 'Por favor vuelve a intentar realizar la misma opración pasados unos minutos.');

            }
        }
    }

    /**
     * Apply filters, order and pagination.
     *
     * @param Request              $request
     * @param QueryBuilder|Builder $consulta
     * @param bool                 $paginate
     * @return QueryBuilder|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function buildFilterAdmin(Request $request, $consulta, $paginate = true)
    {
        if ($request->has('filters'))
        {
            $filtros = json_decode($request->get('filters'));
            foreach ($request->all() as $key => $value){
                if (Str::startsWith($key, self::$TABLE_FILTER_PREFIX)){
                    $filter = new \stdClass();
                    $filter->id = $key;
                    $filter->value = $value;
                    $filter->type = TableFilterTypes::TIPO_FILTRO_IGUAL;
                    $filter->data_type = null;
                    array_push($filtros, $filter);
                }
            }

            foreach ($filtros as $filtro) {
                // comprobamos que existe el parametro
                if (!empty($filtro->id) || !(empty($filtro->table) || empty($filtro->field)) || !empty($filtro->filter_sql))
                {
                    $value      = $filtro->value;
                    $tipoFiltro = $filtro->type;
                    $tipoDato   = (empty($filtro->data_type) ? null : $filtro->data_type);

                    // nombre del campo
                    $aryCampo = $this->getNameFieldValid($filtro, TableFilterTypes::PREFIJO_FILTER, $tipoDato);

                    // Filtro especial por Valores SQL en combo
                    // filter_sql_combo == 1
                    if (isset($filtro->filter_sql_combo) && isset($filtro->filter_sql_combo_table)
                            && $filtro->filter_sql_combo == 1) {
                        if (is_array($value)) // || $pos === true)
                        {
                            $sentence = UtilsBasicManager::canelaSplit($value, ' or ');
                            $sentence = ' ( '.$filtro->filter_sql_combo_table. ' and ('.$sentence.') )';
                            if ($tipoFiltro == TableFilterTypes::TIPO_FILTRO_DISTINTO) {
                                $consulta = $consulta->whereRaw(' not exists '.$sentence);
                            } else {
                                $consulta = $consulta->whereRaw(' exists '.$sentence);
                            }

                        } else {

                        }
                    } else {
                        // esta informado el campo por el que filtrar
                        // y es un tipo de busqueda por operador de comparacion
                        if (!empty ($value) && $this->typeFilterWithValue($tipoFiltro)) {
                            // es un array ?
                            if (is_array($value)) // || $pos === true)
                            {
                                if ($tipoFiltro == TableFilterTypes::TIPO_FILTRO_DISTINTO) {
                                    $consulta = $consulta->whereNotIn(\DB::Raw($aryCampo ['campo']), $value);
                                } else {
                                    $consulta = $consulta->whereIn(\DB::Raw($aryCampo ['campo']), $value);
                                }
                            } else {
                                if ($tipoFiltro == TableFilterTypes::TIPO_FILTRO_CONTIENE) {
                                    $consulta = $consulta->where(\DB::Raw($aryCampo ['campo']), 'like', '%' . $value . '%');
                                } else {
                                    if ($tipoFiltro == TableFilterTypes::TIPO_FILTRO_NO_CONTIENE) {
                                        $consulta = $consulta->where(\DB::Raw($aryCampo ['campo']), 'not like', '%' . $value . '%');
                                    } else {
                                        $consulta = $consulta->where(\DB::Raw($aryCampo ['campo']), $this->getOperatorByTypeFilter($tipoFiltro), $value);
                                    }
                                }
                            }
                        } // Null o Not Null
                        else {
                            if (!$this->typeFilterWithValue($tipoFiltro)) {
                                if (TableFilterTypes::TIPO_FILTRO_VACIO == $tipoFiltro) {
                                    $consulta = $consulta->whereNull(\DB::Raw($aryCampo ['campo']));
                                } else {
                                    if (TableFilterTypes::TIPO_FILTRO_NO_VACIO == $tipoFiltro) {
                                        $consulta = $consulta->whereNotNull(\DB::Raw($aryCampo ['campo']));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Order by?
        if ($request->has('orderbys'))
        {
            $orderbys = json_decode($request->get('orderbys'));
            foreach ($orderbys as $item) {
                // comprobamos que existe el parametro
                if (!empty ($item->id) || !empty($item->field))
                {
                    $value    = $item->value;
                    $aryCampo = $this->getNameFieldValid($item, TableFilterTypes::PREFIJO_ORDER_BY, null);
                    if ($aryCampo ['is_array']) {
                        $consulta = $consulta->orderBy(\DB::Raw($aryCampo ['campo']), $value);
                    } else {
                        $consulta = $consulta->orderBy($aryCampo ['campo'], $value);
                    }
                }
            }
        }

        if ($paginate) {// Paginacion ?
            $consulta = (!$request->has('pagination') && $request->get('pagination') == null)
                ? $consulta->paginate(10)
                : $consulta->paginate($request->get('pagination'));
        }
        return $consulta;
    }

    /**
     * Determina si el tipo de fitro va con valores o solo
     *
     * @param string $typeFilter
     * @return boolean
     */
    private function typeFilterWithValue($typeFilter)
    {
        if (in_array($typeFilter, array(
            TableFilterTypes::TIPO_FILTRO_VACIO,
            TableFilterTypes::TIPO_FILTRO_NO_VACIO,
        ))) {
            return false;
        }
        return true;
    }

    /**
     * Retorna el tipo de operador segun el tipo de filtro
     *
     * @param string $typeFilter
     * @return string
     */
    private function getOperatorByTypeFilter($typeFilter): string
    {

        switch ($typeFilter) {
            case TableFilterTypes::TIPO_FILTRO_CONTIENE;
                return "like";
                break;
            case TableFilterTypes::TIPO_FILTRO_IGUAL:
                return "=";
                break;
            case TableFilterTypes::TIPO_FILTRO_MAYOR:
                return ">";
                break;
            case TableFilterTypes::TIPO_FILTRO_MAYOR_IGUAL:
                return ">=";
                break;
            case TableFilterTypes::TIPO_FILTRO_MENOR:
                return "<";
                break;
            case TableFilterTypes::TIPO_FILTRO_MENOR_IGUAL:
                return "<=";
                break;
            case TableFilterTypes::TIPO_FILTRO_DISTINTO:
                return "<>";
                break;
            default:
                return "=";
                break;
        }
    }

    /**
     * @param Object $item
     * @param string $prefix
     * @param string $tipoDato
     * @return array
     */
    private function getNameFieldValid($item, $prefix, $tipoDato): array
    {
        // filtro especial
        if (!empty($item->filter_sql))
        {
            return array(
                'is_array'      => '0',
                'campo'         => $item->filter_sql,
                'field'         => $item->filter_sql,
            );
        }
        // filtro por campo y tabla
        if (!empty($item->field))
        {
            $nameField = self::getNombreCampo($item->table, $item->field, $tipoDato);
            return array(
                'is_array' => '0',
                'campo'    => $nameField,
                'field'    => $item->field,
            );
        }

        // filtro por id (tabla __ campo)
        $campo = str_replace($prefix, '', $item->id);

        $aryFields = explode(TableFilterTypes::SEPARADOR_CAMPOS_AUX, $campo);
        if (count($aryFields) > 1) {
            $cadena = "concat(";
            $sep    = "";
            foreach ($aryFields as $item) {
                $aryTablaCampo = $this->nombreTablaCampo($item, $tipoDato);
                $cadena        .= $sep . $aryTablaCampo ['table_field'];
                $sep           = ",' ',";
            }

            return array(
                'is_array' => '1',
                'campo'    => $cadena . ')',
                'field'    => $aryTablaCampo ['field'],
            );
        } else {
            $aryTablaCampo = $this->nombreTablaCampo($campo, $tipoDato);
            return array(
                'is_array' => '0',
                'campo'    => $aryTablaCampo ['table_field'],
                'field'    => $aryTablaCampo ['field'],
            );
        }
    }

    /**
     * Separamos el nombre de la Tabla y el del campo
     *
     * @param string $cadena
     * @param string $tipoDato
     * @return string[]
     */
    private function nombreTablaCampo($cadena, $tipoDato)
    {
        $filterDateFormat = BasicModel::formatDate();

        $tabla      = substr($cadena, 0, strpos($cadena, TableFilterTypes::SEPARADOR_TABLA_AUX));
        $campo      = substr($cadena, strpos($cadena, TableFilterTypes::SEPARADOR_TABLA_AUX) + strlen(TableFilterTypes::SEPARADOR_TABLA_AUX));
        $tablaCampo = str_replace(TableFilterTypes::SEPARADOR_TABLA_AUX, TableFilterTypes::SEPARADOR_TABLA, $cadena);

        // Es una fecha
        if ($tipoDato != null && $tipoDato == TableFilterTypes::FILTRO_TIPO_DATE) {
            $tablaCampo = "date_format(" . $tabla . "." . $campo . ", '".$filterDateFormat."')";
            $campo      = "date_format(" . $campo . ", '".$filterDateFormat."')";
        }

        if ($tabla == TableFilterTypes::NAME_NO_TABLA) {
            $tabla      = "";
            $tablaCampo = $campo;
        }

        return array(
            'table'       => $tabla,
            'table_field' => $tablaCampo,
            'field'       => $campo,
        );
    }


    /**
     * Retorna el nombre del campo a usar en la consulta
     * @param string $campo
     * @param string $tipoDato
     * @return string
     */
    private function getNombreCampo($table, $campo, $tipoDato)
    {
        $filterDateFormat = BasicModel::formatDate();
        // Es una fecha
        $result = (empty($table) ? '' : ($table.'.')).$campo;
        if ($tipoDato != null && $tipoDato == TableFilterTypes::FILTRO_TIPO_DATE) {
            $result = "date_format(" . $result . ", '".$filterDateFormat."')";
        }

        return $result;
    }



    /**
     * Set pagination data details.
     *
     * @param \Illuminate\Contracts\Pagination\LengthAwarePaginator $registros
     * @return array
     */
    protected function getPagination($registros)
    {
        return array(
            'total'         => $registros->total(),
            'per_page'      => $registros->perPage(),
            'current_page'  => $registros->currentPage(),
            'last_page'     => $registros->lastPage(),
            'next_page_url' => $registros->nextPageUrl(),
            'prev_page_url' => $registros->previousPageUrl(),
            'from'          => $registros->firstItem(),
            'to'            => $registros->lastItem(),
        );
    }
}
