<?php

namespace Canela\CanelaTools\Requests\WebBuilder;

use Canela\CanelaTools\Enums\TableColumnMaxSize;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class WebPageNoticeRequest
 * @package Canela\CanelaTools\Requests\WebBuilder
 */
class WebPageNoticeRequest extends GenericRequest
{
    /**
     * @var array $fields
     * Stores form fields name.
     */
    protected array $fields = [
        'page_id',
        'type_id',
        'title',
        'subject',
        'author',
        'date',
        'body',
        'file_thumb_id',
        'file_thumbnail_id',
        'seo_title',
        'seo_description',
        'external_link',
        'coming_soon',
        'important',
        'is_live',
        'open_in_new_window',
        'slug',
    ];


    /**
     * @var string $transPath
     * Stores translation path.
     */
    protected string $transPath = 'canelatools::canelatools.entity.web_page_notice.field.';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'page_id'           => 'required|integer|min:1|exists:web_page,id',
            'type_id'           => 'required|integer|min:1|exists:web_notice_type,id',
            'state_id'          => 'required|integer|min:1|exists:state,id',
            'title'             => 'required|max:255',
            'subject'           => 'nullable|max:4000',
            'author'            => 'nullable|max:255',
            'date'              => 'nullable|date',
            'body'              => 'nullable|max:'.TableColumnMaxSize::LONGTEXT_MAX,
            'file_thumb_id'     => 'nullable|integer|min:1|exists:archive,id',
            'file_thumbnail_id' => 'nullable|integer|min:1|exists:archive,id',
            'seo_title'         => 'nullable|max:255',
            'seo_description'   => 'nullable|max:4000',
            'tags'              => 'nullable|max:'.TableColumnMaxSize::TEXT_MAX,
            'external_link'     => 'max:4000',
            'slug'              => 'nullable|max:255',
            'coming_soon'       => 'required|integer|between:0,1',
            'important'         => 'required|integer|between:0,1',
            'is_live'           => 'required|integer|between:0,1',
            'open_in_new_window'=> 'required|integer|between:0,1',
        );
        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes['state_id'] = trans('canelatools::canelatools.general.field.state');

        return $attributes;
    }

}
