<?php

namespace Canela\CanelaTools\Enums;

use Illuminate\Support\Facades\Session;

/**
 * Class AdminMenu
 * @package App\Http\Enums
 */
abstract class AdminMenu extends Enum
{
    const HOME                  = 'home';

    const USERS                 = 'users';
    const HISTORY_LOG           = 'history_log';

    const COUNTRY               = 'country';
    const PROVINCE              = 'province';
    const CITY                  = 'city';

    const CONFIGURATION         = 'configuration';
    const PARAMETER             = 'parameter';
    const TRANSLATION           = 'translation';
    const LANGUAGE              = 'language';

    const CLIENT                = 'client';
    const CUSTOMER              = 'customer';

    const RRSS                  = 'rrss';

    const SQL                   = 'sql';

    const ARCHIVE               = 'archive';

    const SYSTEM                = 'system';

    const GROUP_CATALOGUE       = 'group_catalogue';
    const GROUP_CONFIGURATION   = 'group_configuration';
    const GROUP_USER            = 'group_user';
    const GROUP_USERS           = 'group_users';
    const GROUP_CONFIG          = 'group_config';
    const GROUP_LOCATION        = 'group_location';

    const GROUP_CLIENT          = 'group_client';

    const WEB_PAGE_CONTACT_TYPE = 'web_page_contact_type';


    //Used by model to get control
    const SESSION_CURRENT           = 'current';
    const SESSION_CURRENT_MENU      = 'current_menu';
    const SESSION_CURRENT_SUBMENU   = 'current_submenu';

    private static $pageActive = 0;

    /**
     * Active getter.
     *
     * @return int
     */
    public static function getPageActive()
    {
        return self::$pageActive;
    }

    /**
     * Active setter.
     *
     * @param $pagAct
     */
    public static function setPageActive($pagAct)
    {
        self::$pageActive = $pagAct;
    }

    /**
     * Set session and active var.
     *
     * @param $pagAct
     */
    public static function setCurrent($pagAct)
    {
        self::$pageActive = $pagAct;
        Session::put(self::SESSION_CURRENT, self::getPageActive());
    }

    /**
     * Get current active menu.
     *
     * @return mixed
     */
    public static function getCurrent()
    {
        return Session::get(self::SESSION_CURRENT);
    }

    /**
     * Set session active menu
     *
     * @param $menuAct
     */
    public static function setCurrentGroup($menuAct)
    {
        Session::put(self::SESSION_CURRENT_MENU, $menuAct);
    }

    public static function getCurrentGroup()
    {
        return Session::get(self::SESSION_CURRENT_MENU);
    }


    /**
     * Set session active menu
     *
     * @param $menuAct
     */
    public static function setCurrentGroupSub($menuAct)
    {
        Session::put(self::SESSION_CURRENT_SUBMENU, $menuAct);
    }

    public static function getCurrentGroupSub()
    {
        return Session::get(self::SESSION_CURRENT_SUBMENU);
    }

}
