<?php

namespace Canela\CanelaTools\Controller\WebBuilder;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\WebBuilder\Web;
use Canela\CanelaTools\Models\WebBuilder\WebLanguage;
use Canela\CanelaTools\Requests\Client\WebLanguageRequest;
use Canela\CanelaTools\Traits\Fileable;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use stdClass;

/**
 * Class CustomerController
 * @package Canela\CanelaTools\Controller\Client
 */
class WebLanguageController extends BasicController
{
    use HtmlLists;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->entidad      = WebLanguage::class;
        $this->menu         = AdminMenu::LANGUAGE;
        $this->menu_group   = AdminMenu::GROUP_CONFIG;

        // Información de la tabla
        $this->tableRest = (new TableRest())
            ->init($this->entidad)
            ->setTableColumn([
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'id'
                ),
                new TableColumn(
                    table: 'client',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.client.field.name'),
                ),
                new TableColumn(
                    table: 'customer',
                    fieldOrder: 'name',
                    label: trans('canelatools::canelatools.entity.customer.field.name'),
                ),
                new TableColumn(
                    table: 'language',
                    fieldOrder: 'name',
                    label: ($this->entidad)::getResourceFieldTrans('language_id'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'principal',
                    label: ($this->entidad)::getResourceFieldTrans('principal'),
                ),
                new TableColumn(
                    table: ($this->entidad)::ENTITY_TABLE,
                    fieldOrder: 'position',
                    label: ($this->entidad)::getResourceFieldTrans('position'),
                ),
                new TableColumn(
                    label: trans('canelatools::canelatools.general.acciones'),
                    withOrderBy: false,
                    withFilter: false,
                ),
            ])
            ->setTableForm([
                new TableForm(
                    name: 'web_id',
                    label: ($this->entidad)::getResourceFieldTrans('web_id'),
                    required: true,
                    comboList: Web::comboList(),
                    readOnly: true,
                ),
                new TableForm(
                    name: 'language_id',
                    label: ($this->entidad)::getResourceFieldTrans('language_id'),
                    required: true,
                    comboList: Language::comboTranslate(),
                ),
                new TableForm(
                    name: 'principal',
                    label: ($this->entidad)::getResourceFieldTrans('principal'),
                    required: true,
                    comboList: WebLanguage::comboBoolean(),
                    containerClass: 'col-12 col-sm-6',
                ),
                new TableForm(
                    name: 'position',
                    label: ($this->entidad)::getResourceFieldTrans('position'),
                    type: TableColumnTypes::INT,
                    containerClass: 'col-12 col-sm-6',
                ),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function getConsultaPersonalizada($request): mixed
    {
        $consulta = WebLanguage::select(['web_language.id',
                                         'client.name as client_name',
                                         'customer.name',
                                         'language.name as language_name',
                                         'web_language.principal',
                                         'web_language.position',
                                         'web_language.id as action_web_language_id',
                                         'web.id as customer_web_id',
                                        ])
                          ->join('language','web_language.language_id','language.id')
                          ->join('web', 'web_language.web_id','web.id')
                          ->join('customer', 'web.id','customer.web_id')
                          ->join('client', 'customer.client_id','client.id')
                          ->groupBy('web_language.id');

        return $this->setSecurityList($consulta);
    }


    /**
     * Transform buttons.
     *
     * @param stdClass $jsonItem
     * @return array
     */
    public function personalizaLista(stdClass $jsonItem): array
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            switch ($key) {
                case 'action_web_language_id':
                    $txt = $this->addActionButton($value, ($this->entidad)::getRoutePath(), null,
                        true, true, false, false,
                        false, false, 0, 0, true,
                        true);

                    break;
                case 'principal':
                    $txt = WebLanguage::booleanToTextStatic($value);
                    break;
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WebLanguageRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(WebLanguageRequest $request): JsonResponse|RedirectResponse
    {
        return $this->storeBasic($request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param WebLanguageRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(WebLanguageRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }

}
