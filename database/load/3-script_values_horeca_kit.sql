	
-- -----------------------------------------------------
-- DELETES
-- -----------------------------------------------------
delete from `allergen`;
delete from `price`;
delete from `menu_template_format`;
delete from `menu_template_format_category`;
delete from `menu_type`;
delete from `menu_style_section`;
delete from `menu_section`;
delete from `menu_style_qr`;
delete from `menu_style`;
delete from `menu_presentation`;
delete from `menu_state`;

delete from `rrss`;
delete from `web_page_contact_type`;
delete from `web_page_type_image`;
delete from `web_page_type`;
delete from `web_template_section_style`;
delete from `web_template_section`;
delete from `web_template`;
delete from `web_state`;

delete from `entity`;
delete from `typography`;

-- -----------------------------------------------------
-- Table `entity`
-- -----------------------------------------------------
-- INSERT INTO `typography` (`state_id`,`position`,`name`,`archive`,`code`) values

-- -----------------------------------------------------
-- Table `entity`
-- -----------------------------------------------------
insert into `entity` (`name`,`reference`,`model`) values
('Acción',	'action',	null),
('Publicidad',	'advertising',	null),
('Alergenos',	'allergen',	null),
('Atributo libre',	'attribute_free',	null),
('Atributo libre clasificación',	'attribute_free_classification',	null),
('Atributo libre entidad',	'attribute_free_entity',	null),
('Atributo libre valor',	'attribute_free_entity_value',	null),
('Atributo libre lista',	'attribute_free_list',	null),
('Atributo libre perfil',	'attribute_free_profile',	null),
('Atributo libre tipo',	'attribute_free_type',	null),
('Calendario',	'calendar',	null),
('Municipios',	'city',	null),
('Cliente horeca',	'client',	null),
('Cliente configuración',	'client_configuration',	null),
('Configuración',	'configuration',	null),
('Países',	'country',	null),
('Cliente',	'customer',	null),
('Cliente grupo de marcas',	'customer_brand',	null),
('Cliente RRSS',	'customer_rrss',	null),
('Entidades',	'entity',	null),
('Grupo usuarios',	'group_user',	null),
('Histórico de cambios',	'history_log',	null),
('Factura',	'invoice',	null),
('Factura líneas',	'invoice_line',	null),
('Idiomas',	'language',	null),
('Menú',	'menu',	null),
('Menú categoría',	'menu_category',	null),
('Menú categoría formato',	'menu_category_format',	null),
('Menú categoría imagen',	'menu_category_image',	null),
('Menú presentación',	'menu_presentation',	null),
('Menú precio',	'menu_price',	null),
('Menú productos',	'menu_product',	null),
('Menú producto alergenos',	'menu_product_allergen',	null),
('Menú producto comentario',	'menu_product_comment',	null),
('Menú producto formato',	'menu_product_format',	null),
('Menú producto imagen',	'menu_product_image',	null),
('Menú producto precio formato',	'menu_product_price_format',	null),
('Menú sección',	'menu_section',	null),
('Menú estado',	'menu_state',	null),
('Menú cliente',	'menu_store',	null),
('Menú cliente idiomas',	'menu_store_language',	null),
('Menú estilo',	'menu_style',	null),
('Menú estilo QR',	'menu_style_qr',	null),
('Menú estilo sección',	'menu_style_section',	null),
('Menú plantilla formato',	'menu_template_format',	null),
('Menú plantilla formato categoría',	'menu_template_format_category',	null),
('Menú traza',	'menu_trace',	null),
('Menú tipo',	'menu_type',	null),
('Módulo',	'module',	null),
('Notificaciones',	'notifications',	null),
('Parámetros',	'parameter',	null),
('Password resets',	'password_resets',	null),
('Plan',	'plan',	null),
('Plan cliente',	'plan_client',	null),
('Plan cupón',	'plan_coupon',	null),
('Plan línea',	'plan_item',	null),
('Precio',	'price',	null),
('Perfil',	'profile',	null),
('Provincia',	'province',	null),
('RRSS',	'rrss',	null),
('Estado',	'state',	null),
('Traducciones',	'translation',	null),
('Tipografía',	'typography',	null),
('Usuario',	'user',	null),
('Usuarios grupo - Usuario',	'user_group_user',	null),
('Usuario estado',	'user_state',	null),
('Web',	'web',	null),
('Web idiomas',	'web_language',	null),
('Web página',	'web_page',	null),
('Web página contacto',	'web_page_contact',	null),
('Web página contacto tipo',	'web_page_contact_type',	null),
('Web página galería',	'web_page_gallery',	null),
('Web pçagina FAQ',	'web_page_faq',	null),
('Web página noticias',	'web_page_notice',	null),
('Web página noticias etiqueta',	'web_page_notice_tag',	null),
('Web página sección',	'web_page_section',	null),
('Web página tipo',	'web_page_type',	null),
('Web página tipo imágenes',	'web_page_type_image',	null),
('Web estado',	'web_state',	null),
('Web plantilla',	'web_template',	null),
('Web plantilla sección',	'web_template_section',	null),
('Web plantilla sección estilo',	'web_template_section_style',	null),
('Web traza',	'web_trace',	null);

-- -----------------------------------------------------
-- Table `web_state`
-- -----------------------------------------------------
insert into `web_state` (`id`,`name`,`description`) values		
('1',	'Elaboración',	null),
('2',	'Publicado',	null),
('3',	'Revisión',	null),
('4',	'Borrado',	null);

-- -----------------------------------------------------
-- Table `web_template`
-- -----------------------------------------------------
insert into `web_template` (`code`,`name`,`description`,`image_sample`,`image_thumb`) values				
('1',	'Ejemplo',	'Plantilla de ejemplo',	'1.jpg',	'1.jpg');


-- -----------------------------------------------------
-- Table `web_template_section`
-- -----------------------------------------------------
-- insert into `web_template_section` (`name`,`code`) values


-- -----------------------------------------------------
-- Table `web_template_section_style`
-- -----------------------------------------------------
-- insert into `web_template_section_style` (`template_id`,`section_id`,`typography_id`,`color`,`color_backgroup`,`text_size`) values

-- -----------------------------------------------------
-- Table `web_page_type`
-- -----------------------------------------------------
INSERT INTO `web_page_type`(`id`,`state_id`,`is_home`,`with_section`,`with_slider`,`with_gallery`,`with_map`,`with_data_contact`,`with_plan`,`with_notice`,`with_faq`,`multiple`,`name`,`description`) values													
('1',	'1',	'1',	'0',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'Inicio',	'Página de incio'),
('2',	'1',	'0',	'0',	'0',	'1',	'0',	'0',	'0',	'0',	'0',	'1',	'Galería',	'Galería de fotos'),
('3',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'0',	'0',	'Noticias/Blog',	'Página de noticias / Página blog'),
('4',	'1',	'0',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'Secciones',	'Página en la que se pueden definir secciones'),
('5',	'1',	'0',	'0',	'0',	'0',	'1',	'1',	'0',	'0',	'0',	'0',	'Contacto',	'Datos de contacto y mapa'),
('6',	'2',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'0',	'0',	'0',	'Planes de venta',	'Definiciónd de planes de venta'),
('7',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'Página general',	'Página sin ninguna sección'),
('8',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'0',	'0',	'Eventos',	'Página para eventos'),
('9',	'1',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'0',	'1',	'0',	'FAQ',	'Preguntas frecuentes');

-- -----------------------------------------------------
-- Table `web_page_type_image`
-- -----------------------------------------------------
-- insert into `web_page_type_image` (`type_id`,`position`,`image_header`,`imagen_thumb`) values


-- -----------------------------------------------------
-- Table `web_page_contact_type`
-- -----------------------------------------------------
insert into `web_page_contact_type` (`state_id`,`position`,`icon`,`contact`) values				
('1',	'1',	null,	'Teléfono'),	
('1',	'2',	null,	'Fax'),	
('1',	'3',	null,	'WhatsApp'),	
('1',	'4',	null,	'Dirección'),	
('1',	'5',	null,	'Persona de contacto');


-- -----------------------------------------------------
-- Table `rrss`
-- -----------------------------------------------------
insert into `rrss` (`state_id`,`name`,`icon`) values		
('1',	'Facebook',	null),
('1',	'Youtube',	null),
('1',	'WhatsApp',	null),
('1',	'Instagram',	null),
('1',	'Twitter',	null),
('1',	'Pinterest',	null),
('1',	'Tiktok',	null),
('1',	'LinkedIn',	null);


-- -----------------------------------------------------
-- Table `menu_state`
-- -----------------------------------------------------
insert into `menu_state` (`id`,`name`) values	
('1',	'Elaboración'),
('2',	'Publicado'),
('3',	'Revisión'),
('4',	'Borrado');


-- -----------------------------------------------------
-- Table `menu_presentation`
-- -----------------------------------------------------
-- insert into `menu_presentation` (`id`,`state_id`,`position`,`name`,`code`,`image_sample`,`image_thumb`) values


-- -----------------------------------------------------
-- Table `menu_style`
-- -----------------------------------------------------
-- insert itno `menu_style` (`id`,`state_id`,`position`,`name`,`code`,`image_sample`,`image_thumb`,`image_style`) values



-- -----------------------------------------------------
-- Table `menu_style_qr`
-- -----------------------------------------------------
-- insert itno `menu_style_qr` (`id`,`state_id`,`position`,`name`,`description`,`color`,`image_sample`,`image_thumb`,`image_style`) values


-- -----------------------------------------------------
-- Table `menu_section`
-- -----------------------------------------------------
-- insert into `menu_section` (`id`,`name`,`code`) values


-- -----------------------------------------------------
-- Table `menu_style_section`
-- -----------------------------------------------------
-- insert into `menu_style_section` (`style_id`,`section_id`,`typography_id`,`color`,`color_backgroup`,`text_size`) values


-- -----------------------------------------------------
-- Table `menu_type`
-- -----------------------------------------------------
insert into `menu_type` (`state_id`,`name`) values	
('1',	'Menú'),
('1',	'Carta'),
('1',	'Vinos'),
('1',	'Cervezas'),
('1',	'Espirituosos');


-- -----------------------------------------------------
-- Table `menu_template_format_category`
-- -----------------------------------------------------
insert into `menu_template_format_category` (`state_id`,`position`,`name`) values		
('1',	'1',	'Platos'),
('1',	'2',	'Pizza'),
('1',	'3',	'Botellas'),
('1',	'4',	'Latas'),
('1',	'5',	'Grifo');


-- -----------------------------------------------------
-- Table `menu_template_format`
-- -----------------------------------------------------
INSERT INTO `menu_template_format`(`state_id`,`category_id`,`position`,`name`) values
('1',	(select `id` from `menu_template_format_category` where `name` = 'Platos'),	'1',	'Ración'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Platos'),	'2',	'1/2 Ración'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Platos'),	'3',	'Tapa'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Platos'),	'4',	'1/2 Tapa'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Pizza'),	'1',	'Pequeña'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Pizza'),	'2',	'Mediana'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Pizza'),	'3',	'Familiar'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'1',	'Botella 20cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'2',	'Botella 25cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'3',	'Botella 30cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'4',	'Botella 33cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'5',	'Botella 50cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'6',	'Botella 1L'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Botellas'),	'7',	'Botella 1/2L'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Latas'),	'1',	'Lata'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Latas'),	'2',	'Lata 33cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Latas'),	'3',	'Lata 50cl'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'1',	'Corto'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'1',	'Zurito'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'2',	'Caña'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'3',	'Caña doble'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'4',	'Copa'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'5',	'Copa doble'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'6',	'Jarra'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'7',	'Jarra doble'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'8',	'1/2 Pinta'),
('1',	(select `id` from `menu_template_format_category` where `name` = 'Grifo'),	'9',	'Pinta');

-- -----------------------------------------------------
-- Table `price`
-- -----------------------------------------------------
insert into `price` (`id`,`state_id`,`is_default`,`position`,`name`,`description`) values					
('1',	'1',	'1',	'1',	'Normal',	'Precio por defecto'),
('2',	'1',	'0',	'2',	'Terraza',	'Precio para productos servidos en la terraza'),
('3',	'1',	'0',	'3',	'Mesa',	'Precio para productos servidoe en la mesa'),
('4',	'1',	'0',	'4',	'Barra',	'Precio para productos servidos en la barra'),
('5',	'1',	'0',	'5',	'Noche',	'Precio para productos servidos por la noche');


-- -----------------------------------------------------
-- Table `allergen`
-- -----------------------------------------------------
insert into `allergen` (`id`,`state_id`,`position`,`name`,`image`,`image_thumb`) values					
('1',	'1',	'1',	'Gluten',	'alerg-1.png',	'alerg-1.png'),
('2',	'1',	'2',	'Crustáceos',	'alerg-2.png',	'alerg-2.png'),
('3',	'1',	'3',	'Huevos',	'alerg-3.png',	'alerg-3.png'),
('4',	'1',	'4',	'Pescado',	'alerg-4.png',	'alerg-4.png'),
('5',	'1',	'5',	'Cacahuetes',	'alerg-5.png',	'alerg-5.png'),
('6',	'1',	'6',	'Soja',	'alerg-6.png',	'alerg-6.png'),
('7',	'1',	'7',	'Lacteos',	'alerg-7.png',	'alerg-7.png'),
('8',	'1',	'8',	'Apio',	'alerg-8.png',	'alerg-8.png'),
('9',	'1',	'9',	'Frutos de cáscara',	'alerg-9.png',	'alerg-9.png'),
('10',	'1',	'10',	'Mostaza',	'alerg-10.png',	'alerg-10.png'),
('11',	'1',	'11',	'Granos de sésamo',	'alerg-11.png',	'alerg-11.png'),
('12',	'1',	'12',	'Dióxido de azufre y sulfitos',	'alerg-12.png',	'alerg-12.png'),
('13',	'1',	'13',	'Altramuces',	'alerg-13.png',	'alerg-13.png'),
('14',	'1',	'14',	'Moluscos',	'alerg-14.png',	'alerg-14.png');
