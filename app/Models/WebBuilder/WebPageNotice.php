<?php namespace Canela\CanelaTools\Models\WebBuilder;

use Canela\CanelaTools\Models\BasicModel;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\State;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property mixed $page
 * @property mixed $state
 * @property mixed $horaries
 */
class WebPageNotice extends BasicModel
{

    public const DIR_IMAGE_THUMB        = 'project/notice/thumb/';
    public const DIR_IMAGE_THUMBNAIL    = 'project/notice/thumbnail/';
    public const DIR_ARCHIVE            = 'project/notice/archive/';


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_page_notice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'state_id',
        'file_thumb_id',
        'file_thumbnail_id',
        'title',
        'subject',
        'author',
        'date',
        'body',
        'seo_title',
        'seo_description',
        'slug',
        'tags',
        'external_link',
        'coming_soon',
        'important',
        'is_live',
        'open_in_new_window',
        'type_id',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'page_id'           => 'int',
        'state_id'          => 'int',
        'file_thumb_id'     => 'int',
        'file_thumbnail_id' => 'int',
        'title'             => 'string',
        'subject'           => 'string',
        'author'            => 'string',
        //'date'              => 'date',
        'body'              => 'string',
        'seo_title'         => 'string',
        'seo_description'   => 'string',
        'external_link'     => 'string',
        'coming_soon'       => 'bool',
        'important'         => 'bool',
        'is_live'           => 'bool',
        'open_in_new_window'=> 'bool',
        'type_id'           => 'int',
    ];

    public $comboIdentifierField = 'title';

    protected $with = ['horaries', 'archives', 'categories', 'authors', 'type'];

    /**
     * @return BelongsTo
     */
    public function page(): BelongsTo
    {
        return $this->belongsTo(WebPage::class, 'page_id');
    }


    /**
     * @return BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_id');
    }


    /**
     * @return BelongsTo
     */
    public function fileThumb(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'file_thumb_id');
    }


    /**
     * @return BelongsTo
     */
    public function fileThumbnail(): BelongsTo
    {
        return $this->belongsTo(Archive::class, 'file_thumbnail_id');
    }


    /**
     * @return HasMany
     */
    public function horaries(): HasMany
    {
        return $this->hasMany(WebPageNoticeHorary::class, 'notice_id')
            ->orderBy('date_from', 'asc');
    }


    /**
     * @return Collection
     */
    public function translationLanguages(): Collection
    {
        return WebLanguage::webLanguages($this->page->web_id);
    }


    /**
     * @return HasMany
     */
    public function archives() {
        return $this->hasMany(WebPageNoticeArchive::class, 'notice_id')
            ->orderBy('position', 'asc');
    }

    /**
     * @return BelongsTo
     */
    public function type() {
        return $this->belongsTo(WebNoticeType::class, 'type_id');
    }

    /**
     * Related categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(WebPageCategory::class, 'web_page_notice_category', 'notice_id', 'category_id');
    }
    public function getCategoryIdsAttribute() {
        return $this->categories()->pluck('id')->toArray();
    }


    /**
     * @return HasMany
     */
    public function authors() {
        return $this->hasMany(WebPageNoticeAuthor::class, 'notice_id');
    }
}
