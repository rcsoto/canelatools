<?php

namespace Canela\CanelaTools\Providers;

use Canela\CanelaTools\BladeResolver;
use Canela\CanelaTools\Console\ClearAllCommand;
use Canela\CanelaTools\Response\ResponseFactory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Canela\CanelaTools\Console\DocsCommand;

class ToolsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        //Load routes
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        //Load views
        if ($this->app->has('view')) {
            $viewPath = __DIR__ . '/../../resources/views';
            $this->loadViewsFrom($viewPath, 'canelatools');
        }

        //Export configuration
        $this->exportConfiguration();

        //Export language files
//        $this->exportLanguage();
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'canelatools');

        //Export views
        $this->exportViews();

        //Export JS
        $this->exportJs();

        //Export CSS
        $this->exportCss();

        //Export Plugins
        $this->exportPlugins();

        //Export Document
        $this->exportDocument();

        //Export database diagrams
        $this->exportDatabase();

        //Export requests
        $this->exportRequests();

        //Export Generic email
        $this->exportMails();

        $this->app->alias(BladeResolver::class, 'blade');

        $commands = [ClearAllCommand::class];   // ClearAllCommand is also called from web.
        if ($this->app->runningInConsole()) {
            $commands[] = DocsCommand::class;
        }
        $this->commands($commands);

        \Response::macro('ajax', function (bool $result = true, int $code = 1, string $message = null, $data = [], $status = 200) {
            return (new ResponseFactory)->ajax($result, $code, $message, $data, $status);
        });

        Schema::defaultStringLength(191);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
//        $configPath = __DIR__ . '/../../config/tools.php';
//        $this->mergeConfigFrom($configPath, 'tools');
    }


    /**
     * Export database diagrams
     */
    private function exportDatabase()
    {
        $databasePath = __DIR__ . '/../../database';
        if (function_exists('database_path')) {
            $publishPath = database_path();
        } else {
            $publishPath = base_path('database');
        }
        $this->publishes([$databasePath => $publishPath], 'canelatools-database');
    }

    /**
     * Export Document
     */
    private function exportDocument()
    {
        $documentPath = __DIR__ . '/../../resources/document';
        if (function_exists('app_public_path')) {
            $publishPath = app_public_path(config('canelatools.document_root'));
        } else {
            $publishPath = app('path.public') . '/' . config('canelatools.document_root');
        }
        $this->publishes([$documentPath => $publishPath], 'canelatools-public');
    }

    /**
     * Export JS
     */
    private function exportJs()
    {
        // JS
        $jsPath = __DIR__ . '/../../public/js';
        if (function_exists('app_public_path')) {
            $publishPath = app_public_path('js');
        } else {
            $publishPath = app('path.public') . '/js';
        }
        $this->publishes([$jsPath => $publishPath], 'canelatools-public');

        // Vue Components
        // $componentsPath = __DIR__ . '/../../resources/js/components';
        // if (function_exists('resource_path')) {
        //     $publishPath = resource_path('js/vendor/canelatools/components');
        // } else {
        //     $publishPath = base_path('resources/js/vendor/canelatools/components');
        // }
        // $this->publishes([$componentsPath => $publishPath], 'vue-components');

        //mix manifest
//        $mixPath = __DIR__ . '/../../public/mix-manifest.json';
//        if (function_exists('public_path')) {
//            $publicPath = public_path('mix-manifest.json');
//        } else {
//            $publicPath = app('path.public').'/mix-manifest.json';
//        }
//        $this->publishes([$mixPath => $publicPath], 'public');

        // para copiar a la vez ...
        // $this->publishes([$jsPath => $publishPath, $mixPath => $publicPath], 'public');
    }

    /**
     * Export CSS
     */
    private function exportCss()
    {
        // CSS
        $cssPath = __DIR__ . '/../../public/css';
        if (function_exists('app_public_path')) {
            $publishPath = app_public_path('css');
        } else {
            $publishPath = app('path.public') . '/css';
        }
        $this->publishes([$cssPath => $publishPath], 'canelatools-public');

        /*
        $publicPath = __DIR__ . '/../../public/css';
        if (function_exists('public_path')) {
            $publishPath = public_path('css');
        } else {
            $publishPath = app('path.public') . '/css';
        }
        $this->publishes([$publicPath => $publishPath], 'public');
        */
    }

    /**
     * Export Plugins
     */
    private function exportPlugins()
    {
        // plugins path
        $pluginsPath = __DIR__ . '/../../public/plugins';
        if (function_exists('app_public_path')) {
            $publishPath = app_public_path('plugins');
        } else {
            $publishPath = app('path.public') . '/plugins';
        }
        $this->publishes([$pluginsPath => $publishPath], 'canelatools-public');
    }

    /**
     * Export views
     */
    private function exportViews()
    {
        // Vistas
        // Se han de usar del Vendor
        /*
        $viewPath = __DIR__ . '/../../resources/views';
        if (function_exists('resource_path')) {
            $publishPath = resource_path('views/vendor/canelatools');
        } else {
            $publishPath = base_path('resources/views/vendor/canelatools');
        }
        $this->publishes([$viewPath => $publishPath], 'views');
        */
    }

    /**
     * Export language files
     */
    private function exportLanguage()
    {
        // directorio orgien/destino
        $langPath = __DIR__ . '/../../resources/lang';
        if (function_exists('resource_path')) {
            $publishPath = resource_path('lang');
        } else {
            $publishPath = base_path('resources/');
        }
/*
        // idiomas mantenidos
        $folders = ['es', 'en'];
        // archivos que se sobreescriben siempre
        $files = ['canelatools.php'];
        // eliminamos los archivos que son de "canelatools"
        foreach ($folders as $folder)
        {
            foreach ($files as $file)
            {
                $archivo = $publishPath.'/'.$folder.'/'.$file;
                if (file_exists($archivo))
                {
                    unlink($archivo);
                }
            }
        }
*/
        // publicar
        $this->publishes([$langPath => $publishPath], 'canelatools-lang');
    }

    /**
     * Export configuration
     */
    private function exportConfiguration()
    {
        $files = [];
        foreach(['canelatools'] as $filename) {
            $configPath = __DIR__ . '/../../config/'.$filename.'.php';
            if (function_exists('config_path')) {
                $publishPath = config_path($filename.'.php');
            } else {
                $publishPath = base_path('config/'.$filename.'.php');
            }
            $files[$configPath] = $publishPath;
        }

        $this->publishes($files, 'canelatools-config');
    }

    /**
     * Export request
     */
    private function exportRequests()
    {
        $requestPath = __DIR__ . '/../Requests/GenericRequest.php';
        if (function_exists('request_path')) {
            $publishPath = request_path('GenericRequest.php');
        } else {
            $publishPath = base_path('app/Http/Requests/GenericRequest.php');
        }
        $this->publishes([$requestPath => $publishPath], 'canelatools-request');
    }

    private function exportMails()
    {
        $mailPath = __DIR__ . '/../Mails/GenericMail.php';
        if (function_exists('mail_path')) {
            $publishPath = mail_path('GenericMail.php');
        } else {
            $publishPath = base_path('app/Mails/GenericMail.php');
        }
        $this->publishes([$mailPath => $publishPath], 'canelatools-mail');
    }
}
