<?php

namespace Canela\CanelaTools;


use Canela\CanelaTools\Models\BasicModel;
use Canela\CanelaTools\Models\State;
use Canela\CanelaTools\Models\User\UserState;
use Illuminate\Support\Arr;

trait HtmlLists
{

    /**
     * Generador de Botones HTML para los listados de administración.
     *
     * @param int         $itemId
     *                            Id del registro a Editar o sobre el que se va hacer un cambio de estado
     * @param string      $resource
     *                            Nombre del recurso sobre el que generar la ruta
     * @param string|null $prefix
     *                            Texto a concatenar al nombre del botón. Se ha de informar cuando en la misma página se va a mostrar varios botones de cambio de estado
     * @param bool        $showBtnEdit
     *                            Indica si se ha de mostrar/ocultar el botón de edición
     * @param bool        $showDeleteBtn
     *                            Indica si se ha de mostrar/ocultar el botón de Borrar/Delete
     * @param bool        $stop_on_created_relations
     * @param bool        $showLabels
     *                            Mostrar texto de title en el botón.
     * @param bool        $groupButtons
     *                            Mostrar botones agrupados o independientes.
     * @param string      $titleEdit
     *                            Texto a mostrar sobre el botón Editar
     * @param string      $titleDelete
     *                            Texto a mostrar sobre el botón eliminar
     * @return string
     * @deprecated
     *
     */
    public function addActionBtn(int $itemId, string $resource, string $prefix = null,
                                 bool $showBtnEdit = true, bool $showDeleteBtn = false,
                                 bool $stop_on_created_relations = false, bool $showLabels = false,
                                 bool $groupButtons = false, string $titleEdit = null,
                                 $titleDelete = null): string
    {

        $type = count(explode('/', $resource)) > 1 ? 'url' : 'route';

        $txt = $groupButtons ? "<div class=\"btn-group\">" : "";

        if ($showBtnEdit) {
            $url = $type === 'url' ? url("{$resource}/{$itemId}/edit") : route("{$resource}.edit", $itemId);
            $txt .= $this->getEditButton($url, $itemId, $showLabels, $titleEdit, $prefix);
        }

        if ($showDeleteBtn) {
            $url = $type === 'url' ? url("{$resource}/{$itemId}") : route("{$resource}.destroy", $itemId);
            $txt .= $this->getDeleteButton($url, $itemId, $stop_on_created_relations, $showLabels, $titleDelete);
        }

        $txt .= $groupButtons ? "</div>" : "";
        return $txt;
    }


    /**
     * CRUD & update status list buttons
     *
     * @param int         $itemId
     *                            Id del registro a Editar o sobre el que se va hacer un cambio de estado
     * @param string      $resource
     *                            Nombre del recurso sobre el que generar la ruta
     * @param string|null $prefix
     *                            Texto a concatenar al nombre del botón. Se ha de informar cuando en la misma página se va a mostrar varios botones de cambio de estado
     * @param bool        $showBtnEdit
     *                            Indica si se ha de mostrar/ocultar el botón de edición
     * @param bool        $showDeleteBtn
     *                            Indica si se ha de mostrar/ocultar el botón de Borrar/Delete
     * @param bool        $stop_on_created_relations
     * @param bool        $showActivateBtn
     * @param bool        $showDeactivateBtn
     * @param bool        $showDisableBtn
     * @param int         $statusType
     * @param int         $stateId
     * @param bool        $showLabels
     *                            Mostrar texto de title en el botón.
     * @param bool        $groupButtons
     *                            Mostrar botones agrupados o independientes.
     * @param string      $titleEdit
     *                            Texto a mostrar sobre el botón Editar
     * @param string      $titleDelete
     *                            Texto a mostrar sobre el botón eliminar
     * @param string|null $titleActivate
     * @param string|null $titleDeactivate
     * @param string|null $titleDisable
     * @return string
     */
    public function addActionButton(int $itemId, string $resource, string $prefix = null,
                                    bool $showBtnEdit = true, bool $showDeleteBtn = false,
                                    bool $stop_on_created_relations = false, bool $showActivateBtn = false,
                                    bool $showDeactivateBtn = false, bool $showDisableBtn = false,
                                    int $statusType = 0, int $stateId = 0, bool $showLabels = false,
                                    bool $groupButtons = false, string $titleEdit = null, $titleDelete = null,
                                    string $titleActivate = null, string $titleDeactivate = null,
                                    string $titleDisable = null): string
    {

        $type = count(explode('/', $resource)) > 1 ? 'url' : 'route';
        $txt  = $groupButtons ? "<div class=\"btn-group\">" : "";

        if ($showBtnEdit) {
            $url = $type === 'url' ? url("{$resource}/{$itemId}/edit") : route("{$resource}.edit", $itemId);
            $txt .= $this->getEditButton($url, $itemId, $showLabels, $titleEdit, $prefix);
        }

        if ($showActivateBtn) {
            $url = $type === 'url' ? url("{$resource}/activate/{$itemId}") : route("{$resource}.activate", $itemId);
            $txt .= $this->getActivateButton($url, $statusType, $stateId, $itemId, $showLabels, $titleActivate, $prefix);
        }

        if ($showDeactivateBtn) {
            $url = $type === 'url' ? url("{$resource}/deactivate/{$itemId}") : route("{$resource}.deactivate", $itemId);
            $txt .= $this->getDeactivateButton($url, $statusType, $stateId, $itemId, $showLabels, $titleDeactivate, $prefix);
        }

        if ($showDisableBtn) {
            $url = $type === 'url' ? url("{$resource}/disable/{$itemId}") : route("{$resource}.disable", $itemId);
            $txt .= $this->getDisableButton($url, $statusType, $stateId, $itemId, $showLabels, $titleDisable, $prefix);
        }

        if ($showDeleteBtn) {
            $url = $type === 'url' ? url("{$resource}/{$itemId}") : route("{$resource}.destroy", $itemId);
            $txt .= $this->getDeleteButton($url, $itemId, $stop_on_created_relations, $showLabels, $titleDelete);
        }

        $txt .= $groupButtons ? "</div>" : "";
        return $txt;
    }

    /**
     * Parse edit button to HTML.
     *
     * @param string      $url
     * @param int|null    $id
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    public function getEditButton(string $url, int $id = null, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.edit') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-edit-{$id}\"
                class=\"btn btn-sm btn-primary btn-embossed m-0 m-b-5\" href=\"{$url}\" title=\"$title\">
                <i class=\"fa fa-pencil-square-o\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    /**
     * Parse delete button to HTML.
     *
     * @param string      $url
     * @param int         $id
     * @param bool        $stop_on_created_relations
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    public function getDeleteButton(string $url, int $id, bool $stop_on_created_relations = false, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.delete') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-delete-{$id}\"
                class=\"btn btn-sm btn-danger btn-embossed m-0 m-b-5\" title=\"{$title}\"
                data-item-id=\"{$id}\" data-action=\"delete\" data-stop-on-relations=\"{$stop_on_created_relations}\"
                data-url=\"{$url}\">
                <i class=\"fa fa-trash\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    /**
     * Parse show button to HTML.
     *
     * @param string      $url
     * @param int|null    $id
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    protected function getShowButton(string $url, int $id = null, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.show') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-show-{$id}\"
                class=\"btn btn-sm btn-primary btn-embossed m-0 m-b-5\" href=\"{$url}\" title=\"$title\">
                        <i class=\"fa fa-file-text\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    /**
     * Parse list image.
     *
     * @param string $url
     * @param string $alt
     * @param string|null $styles
     * @param string|null $classes
     * @return string
     */
    public function getListImage(string $url, string $alt = 'thumbnail', string $styles = null, string $classes = null): string
    {
        return "<img style=\"{$styles}\" class=\"{$classes}\" src=\"{$url}\" width=\"100%\" alt=\"{$alt}\">";
    }

    /**
     * Parse FontAwesome icon in boolean column list.
     *
     * @param bool $var
     * @return string
     */
    public function getListBooleanIcon(bool $var)
    {
        return $var
            ? "<i class='fa fa-check-circle-o c-green' aria-hidden='true'></i>"
            : "<i class='fa fa-times-circle-o c-red' aria-hidden='true'></i>";
    }

    /**
     * Generic action button.
     *
     * @param string      $url
     * @param string      $title
     * @param string|null $colour
     * @param int|null    $id
     * @param string|null $icon
     * @param bool        $showLabels
     * @param string|null $prefix
     * @param array       $additional
     * @return string
     */
    public function addGenericButton(string $url, string $title, string $colour = null, int $id = null, string $icon = null, bool $showLabels = false, string $prefix = null, array $additional = []): string
    {
        $colour = is_null($colour) ? 'btn-primary' : $colour;
        $icon   = is_null($icon) ? 'fa fa-file-text' : $icon;

        $additional = implode(" ", array_map(function ($key, $value) {
            return "{$key}=\"{$value}\"";
        }, array_keys($additional), $additional));

        return "<a role=\"button\" id=\"{$prefix}btn-action-show-{$id}\"
                class=\"btn btn-sm {$colour} btn-embossed m-0 m-b-5\" href=\"{$url}\" title=\"$title\" {$additional}>
                        <i class=\"{$icon}\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }


    /**
     * Parse activate button to HTML.
     *
     * @param string      $url
     * @param int         $statusType
     * @param int         $stateId
     * @param int|null    $id
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    public function getActivateButton(string $url, int $statusType, int $stateId, int $id = null, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.activate') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-enable-{$id}\"
                class=\"btn btn-sm btn-success btn-embossed m-0 m-b-5\" title=\"{$title}\"
                data-item-id=\"{$id}\" data-action=\"activate\" data-url=\"{$url}\"
                data-type=\"{$statusType}\" data-status=\"{$stateId}\"  " .
               (in_array($statusType, [BasicModel::TYPE_CHANGE_STATE, BasicModel::TYPE_CHANGE_USER_STATE])
                && !in_array($stateId, [State::ACTIVO, UserState::ACTIVO])
                   ? '' : " hide ") .
               " ><i class=\"fa fa-check-circle-o\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    /**
     * Parse deactivate button to HTML.
     *
     * @param string      $url
     * @param int         $statusType
     * @param int         $stateId
     * @param int|null    $id
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    public function getDeactivateButton(string $url, int $statusType, int $stateId, int $id = null, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.deactivate') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-deactivate-{$id}\"
                class=\"btn btn-sm btn-warning btn-embossed m-0 m-b-5\" title=\"{$title}\"
                data-item-id=\"{$id}\" data-action=\"deactivate\" data-url=\"{$url}\"
                data-type=\"{$statusType}\" data-status=\"{$stateId}\"  " .
               (in_array($statusType, [BasicModel::TYPE_CHANGE_STATE, BasicModel::TYPE_CHANGE_USER_STATE])
                && !in_array($stateId, [State::BAJA, UserState::BORRADO])
                   ? '' : 'hide') .
               " ><i class=\"fa fa-times-circle-o\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    /**
     * Parse disable button to HTML. (only for users)
     *
     * @param string      $url
     * @param int         $statusType
     * @param int         $stateId
     * @param int|null    $id
     * @param bool        $showLabels
     * @param string      $title
     * @param string|null $prefix
     * @return string
     */
    public function getDisableButton(string $url, int $statusType, int $stateId, int $id = null, bool $showLabels = false, string $title = null, string $prefix = null): string
    {
        $title = is_null($title) ? trans('canelatools::canelatools.buttons.disable') : $title;
        return "<a role=\"button\" id=\"{$prefix}btn-action-disable-{$id}\"
                class=\"btn btn-sm btn-warning btn-embossed m-0 m-b-5\" title=\"{$title}\"
                data-item-id=\"{$id}\" data-action=\"disable\" data-url=\"{$url}\"
                data-type=\"{$statusType}\" data-status=\"{$stateId}\"  " .
               ($statusType == BasicModel::TYPE_CHANGE_USER_STATE && $stateId != UserState::DESHABILITADO
                   ? '' : "hide") .
               " ><i class=\"fa fa-ban\"></i>  " . ($showLabels ? $title : "") . "</a>";
    }

    public function addGenericLink(string $url, string $text, string $class = null, string $title = null, string $id = null, string $prefix = null, array $additional = [], array $ariaData = [])
    {
        $additional = implode(" ", array_map(function ($key, $value) {
            return "{$key}=\"{$value}\"";
        }, array_keys($additional), $additional));

        $ariaData = implode(" ", array_map(function ($key, $value) {
            return "data-{$key}=\"{$value}\"";
        }, array_keys($ariaData), $ariaData));

        return "<a id=\"{$prefix}link-action-{$id}\" class=\"{$class}\" href=\"{$url}\" title=\"$title\"
                {$additional} {$ariaData}>{$text}</a>";
    }
}
