@section('body-modals')
    @parent

    <!-- Modal -->
    <div id="modal-select-input" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h4">Large Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!--end: Modal -->

@endsection

@section('body-scripts-extra')
    @parent

    <script type="text/javascript">
        jQuery(function () {
            jQuery('a.btn-select-input').on('click', function (e) {
                e.preventDefault();
                const btn = jQuery(e.currentTarget);
                switch(btn.data('type')) {
                    case 'select':
                        jQuery.selectItem(btn.data('name'));
                        break;
                    case 'clean':
                        jQuery.cleanInput(btn.data('name'));
                        break;
                    case 'add':
                        jQuery.addItem(btn.data('name'));
                        break;
                    case 'edit':
                        jQuery.editItem(btn.data('name'));
                }
            });

            jQuery.selectItem = function (name) {
                console.log(name);

                const modal = jQuery('#modal-select-input');

                $.get("{{route('admin.general.archive.ajax.list')}}",
                function (data) {
                    modal.find('.modal-content').html(data);
                    modal.modal('show')
                });
            };

            jQuery.cleanInput = function (name) {
                console.log(name);
            };

            jQuery.addItem = function (name) {
                console.log(name);
            };

            jQuery.editItem = function (name) {
                console.log(name);
            };
        });
    </script>
@endsection
