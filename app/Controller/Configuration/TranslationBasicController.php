<?php

namespace Canela\CanelaTools\Controller\Configuration;


use Canela\CanelaTools\Controller\BasicController;
use Canela\CanelaTools\Enums\AdminMenu;
use Canela\CanelaTools\Enums\TableColumnTypes;
use Canela\CanelaTools\Enums\TableFilterTypes;
use Canela\CanelaTools\HtmlLists;
use Canela\CanelaTools\Manager\TranslationManager;
use Canela\CanelaTools\Models\Entity;
use Canela\CanelaTools\Models\Language;
use Canela\CanelaTools\Models\Table\TableColumn;
use Canela\CanelaTools\Models\Table\TableForm;
use Canela\CanelaTools\Models\Table\TableRest;
use Canela\CanelaTools\Models\Translation;
use Canela\CanelaTools\Requests\Configuration\GetSetDeleteTranslationRequest;
use Canela\CanelaTools\Requests\Configuration\TranslationRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Log;

/**
 * Class TranslationBasicController
 * @package Canela\CanelaTools\Controller\Configuration
 */
class TranslationBasicController extends BasicController
{
    use HtmlLists;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->entidad     = Translation::class;
        $this->routeEntity = 'admin.configuration.translation';

        $this->vistaLista  = 'canelatools::rest.list';
        $this->vistaEdit   = 'canelatools::rest.addEdit';
        $this->vistaAdd    = 'canelatools::rest.addEdit';

        $this->menu       = AdminMenu::TRANSLATION;
        $this->menu_group = AdminMenu::GROUP_CONFIG;

        // Informacion de la tabla
        $this->tableRest = (new TableRest())
            ->setTitle(trans('canelatools.entity.translation.list'))
            ->setRouteEntity($this->routeEntity)
            ->setUrlIndex(route($this->routeEntity.'.index'))
            ->setUrlListAjax(route($this->routeEntity.'.listAjax'))
            ->setShowBtnNew(true)
            ->setTableColumn([  (new TableColumn())->setTable('translation')->setFieldOrder('id'),
                                (new TableColumn())->setTable('language')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.translation.field.language_id'))
                                    ->setFieldFilter('id')->setComboList(Language::comboTranslate())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                                (new TableColumn())->setTable('entity')->setFieldOrder('name')->setLabel(trans('canelatools::canelatools.entity.translation.field.entity_id'))
                                    ->setFieldFilter('id')->setComboList(Entity::comboTranslate())->setType(TableFilterTypes::FILTRO_TIPO_SELECT),
                                (new TableColumn())->setTable('translation')->setFieldOrder('register_id')->setLabel(trans('canelatools::canelatools.entity.translation.field.register_id')),
                                (new TableColumn())->setTable('translation')->setFieldOrder('code')->setLabel(trans('canelatools::canelatools.entity.translation.field.code')),
                                (new TableColumn())->setTable('translation')->setFieldOrder('value')->setLabel(trans('canelatools::canelatools.entity.translation.field.value')),
                                (new TableColumn())->setWithOrderBy(false)->setWithFilter(false)->setLabel(trans('canelatools::canelatools.general.acciones')),
            ])
            ->setTableForm([ (new TableForm())->setName('language_id')->setLabel(trans('canelatools::canelatools.entity.translation.field.language_id'))
                                ->setComboListJson(Language::comboTranslate())->setType(TableColumnTypes::SELECT)->setCol(6),
                             (new TableForm())->setName('entity_id')->setLabel(trans('canelatools::canelatools.entity.translation.field.entity_id'))
                                ->setComboListJson(Entity::comboTranslate())->setType(TableColumnTypes::SELECT)->setCol(6),
                            (new TableForm())->setName('register_id')->setLabel(trans('canelatools::canelatools.entity.translation.field.register_id'))->setReadOnly(true)->setCol(6),
                            (new TableForm())->setName('code')->setLabel(trans('canelatools::canelatools.entity.translation.field.code')),
                            (new TableForm())->setName('value')->setLabel(trans('canelatools::canelatools.entity.translation.field.value')),
            ]);

        parent::__construct();
    }


    /**
     * Index Query build
     * @param Request $request
     * @return Builder|Configuration
     */
    public function getConsultaPersonalizada($request)
    {
        $lang     = App::getLocale();
        return Translation::select(['translation.id',
                                    DB::Raw((Language::DEFAULT_CODE == $lang ? "language.name" : "trans('" . $lang . "', 'language', language.id, 'name', language.name)") . " as language_name"),
                                    DB::Raw((Language::DEFAULT_CODE == $lang ? "entity.name" : "trans('" . $lang . "', 'entity', entity.id, 'name', entity.name)") . " as entity_name"),
                                    'translation.register_id',
                                    'translation.code',
                                    'translation.value',
                                    'translation.id as action_translation_id',
                                   ])
                           ->join('language', 'translation.language_id', 'language.id')
                           ->join('entity', 'translation.entity_id', 'entity.id');
    }


    /**
     * Transform buttons.
     *
     * @param \stdClass $jsonItem
     * @return string[]
     */
    public function personalizaLista($jsonItem)
    {
        $fila = [];
        foreach ($jsonItem as $key => $value) {
            $txt = $value;
            if ($key == 'action_translation_id') {
                $txt = $this->addActionButton($value, $this->routeEntity, null,
                                              true, false, false, false,
                                              false, false, 0, 0, true,
                                              true);
            }
            $fila[] = $txt;
        }
        return $fila;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param TranslationRequest $request
     *
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function store(TranslationRequest $request)
    {
        return $this->storeBasic($request);
    }

    public function storePre($model, $request)
    {
        $model->entity = $model->entity->reference;
        return $model;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param TranslationRequest $request
     * @param int $id
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function update(TranslationRequest $request, int $id): JsonResponse|RedirectResponse
    {
        return $this->updateBasic($request, $id);
    }
    public function updatePre($model, $request)
    {
        $model->entity = $model->entity->reference;
        return $model;
    }


    /**
     * @param GetSetDeleteTranslationRequest $request
     * @return JsonResponse
     */
    public function getTranslation(GetSetDeleteTranslationRequest $request): JsonResponse
    {
        return response()->json([
            'result' => true,
            'msg' => trans('canelatools::canelatools.general.label.success'),
            'data' => [
                'translation' => TranslationManager::getTranslation($request->entity_id, $request->register_id, $request->attribute, $request->language),
            ]
        ]);
    }


    /**
     * @param GetSetDeleteTranslationRequest $request
     * @return JsonResponse
     */
    public function setTranslation(GetSetDeleteTranslationRequest $request): JsonResponse
    {
        try {
            if (TranslationManager::setTranslation($request)) {
                return response()->json([
                    'result' => true,
                    'msg' => trans('canelatools::canelatools.general.label.success'),
                ]);
            } else {
                throw(new Exception(trans('canelatools::canelatools.model.update.error')));
            }

        } catch (Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'result' => false,
                'msg' => trans('canelatools::canelatools.model.update.error'),
            ]);
        }
    }

    /**
     * @param GetSetDeleteTranslationRequest $request
     * @return JsonResponse
     */
    public function deleteTranslation(GetSetDeleteTranslationRequest $request): JsonResponse
    {
        if (TranslationManager::deleteTranslation($request)) {
            return response()->json([
                'result' => true,
                'msg' => trans('canelatools::canelatools.general.label.success'),
            ]);
        } else {
            return response()->json([
                'result' => false,
                'msg' => trans('canelatools::canelatools.model.delete.error'),
            ]);
        }
    }

}
