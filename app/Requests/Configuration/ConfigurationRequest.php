<?php

namespace Canela\CanelaTools\Requests\Configuration;

use Canela\CanelaTools\Requests\GenericRequest;


/**
 * Class ConfigurationRequest
 * @package Canela\CanelaTools\Requests\Configuration
 */
class ConfigurationRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                //'reference'   => 'required|string|max:45',
                'name'        => 'required|string|max:45',
                'description' => 'nullable|max:255',
                'value'       => 'nullable|string|max:255',
            ];

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'reference'   => trans('canelatools::canelatools.entity.configuration.field.reference'),
            'name'        => trans('canelatools::canelatools.entity.configuration.field.name'),
            'description' => trans('canelatools::canelatools.entity.configuration.field.description'),
            'value'       => trans('canelatools::canelatools.entity.configuration.field.value'),
        ];
    }

}
