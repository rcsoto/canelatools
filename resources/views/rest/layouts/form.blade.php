﻿<?php
/* @var Canela\CanelaTools\Models\Table\TableRest $tableRest */

    use Canela\CanelaTools\Models\WebBuilder\WebPage;
    use Canela\CanelaTools\Models\WebBuilder\WebPageNotice;

    $isWebPageNotice = !empty($register) && $register instanceof WebPageNotice;
    $withArchives = !empty($register) && method_exists($register, 'archives');
    $withSections = !empty($register) && $register instanceof WebPage && $register->type->with_section;

    /** @var String $nameSectionScript nombre de la seccion donde se añade el script*/
    $nameSectionScript = $nameSectionScript ?? config('canelatools.blade.section.bodyScriptsExtra');
?>

@includeWhen($isWebPageNotice, 'canelatools::rest.layouts.associated-horaries', ['webPageNoticeHoraryTableRest' => $webPageNoticeHoraryTableRest ?? null])
@includeWhen($withArchives, 'canelatools::rest.layouts.associated-archives')
@includeWhen($withSections, 'canelatools::rest.layouts.associated-sections')

@if ($isWebPageNotice || $withArchives || $withSections)
    <ul class="nav nav-tabs mb-3" id="editTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link text-uppercase has-ripple active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">@lang('canelatools::canelatools.canela-rest.form.label.general')<span class="ripple ripple-animate"></span></a>
        </li>
        @yield('associated-horaries-nav-item')
        @yield('associated-archives-nav-item')
        @yield('associated-sections-nav-item')
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane p-t-10 fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
            @include('canelatools::rest.layouts.form-items-include')
        </div>
        @yield('associated-horaries-tab-content')
        @yield('associated-archives-tab-content')
        @yield('associated-sections-tab-content')
    </div>
@else
    @include('canelatools::rest.layouts.form-items-include')
@endif

@section('global-scripts')
    @parent
    <script type="text/javascript">
        var classicEditors = {};
    </script>
@endsection


@section('body-scripts-extra')
    @parent

    <script type="text/javascript">

        jQuery(document).ready(function () {

            /******************************************************************************************
             * Control de numero de caracteres
             ******************************************************************************************/
            jQuery.updateCharacterText = function(object)
            {
                var str  = jQuery(object).val();
                var len  = str.length;
                var id = jQuery(object).attr('id');
                jQuery('span[data-id="'+id+'"].canela-input-text').html("&nbsp;("+len+")&nbsp;");
            }

            jQuery("input.canela-input-text, textarea.canela-input-text").keyup(function(){
                jQuery.updateCharacterText(this);
            })

            jQuery('input.canela-input-text, textarea.canela-input-text').each(function()
            {
                jQuery.updateCharacterText(this);
            });
            /******************************************************************************************
             * FIN - Control de numero de caracteres
             ******************************************************************************************/

        });
    </script>
@endsection

