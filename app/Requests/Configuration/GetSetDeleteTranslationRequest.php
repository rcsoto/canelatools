<?php

namespace Canela\CanelaTools\Requests\Configuration;

use Canela\CanelaTools\Enums\HttpMethod;
use Canela\CanelaTools\Enums\TableColumnMaxSize;
use App\Models\General\Archive;
use Canela\CanelaTools\Models\General\ArchiveType;
use Canela\CanelaTools\Requests\GenericRequest;

/**
 * Class GetSetDeleteTranslationRequest
 *
 * @property int $entity_id
 * @property int $register_id
 * @property string $attribute
 * @property string $language
 * @property string $translation
 *
 * @package Canela\CanelaTools\Requests\Configuration
 */
class GetSetDeleteTranslationRequest extends GenericRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'language'          => 'required|string|max:45|exists:language,code',
            'entity_id'         => 'required|integer|exists:entity,id',
            'register_id'       => 'required|integer|min:1',
            'attribute'         => 'required|string|max:45',
            'archive_type_id'   => 'nullable|integer|min:1|exists:archive_type,id',
        ];

        $archiveTypeId = $this->get('archive_type_id');
        if (!is_null($archiveTypeId)) {
            $archiveType = ArchiveType::findOrFail($archiveTypeId);
            if ($archiveType->isTextInputArchiveType($archiveTypeId)) {
                $translationRule = 'nullable';
            } else {
                $translationRule = 'required';
            }
            ArchiveType::addArchiveRuleDependingOnArchiveType($archiveTypeId, $translationRule);
        } else {
            $translationRule = 'nullable|string|max:'.TableColumnMaxSize::TEXT_MAX;
        }

        if (!empty($translationRule)) {
            $rules = array_merge([
                'translation'      => $translationRule,
            ], $rules);
        }

        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'language'          => trans('canelatools::canelatools.entity.translation.field.language_id'),
            'entity_id'         => trans('canelatools::canelatools.entity.translation.field.entity_id'),
            'register_id'       => trans('canelatools::canelatools.entity.translation.field.register_id'),
            'attribute'         => trans('canelatools::canelatools.entity.translation.field.code'),
            'translation'       => trans('canelatools::canelatools.entity.translation.field.translation'),
            'archive_type_id'   => trans('canelatools::canelatools.entity.translation.field.archive_type_id'),
        ];
    }

}
