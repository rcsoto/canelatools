<?php

namespace Canela\CanelaTools\Models;

/**
 * Canela\CanelaTools\Models\ModelCatalog
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property string                                 $description
 * @property-read string                            $value_identificator
 * @property-read string                            $created_at_short
 * @property-read string                            $updated_at_short
 * @property-read string                            $created_at_format
 * @property-read string                            $updated_at_format
 * @property-read string                            $combo_identificator
 * @property-read \Canela\CanelaTools\Models\Entity $table
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboList()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboListFilterId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel combolistFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\ModelCatalog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\ModelCatalog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\ModelCatalog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboBoolean()
 * @method static \Illuminate\Database\Eloquent\Builder|\Canela\CanelaTools\Models\BasicModel comboTranslate($filters = array())
 * @mixin \Eloquent
 */
class ModelCatalog extends BasicModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'int',
        'name'        => 'string',
        'description' => 'string',
    ];

}
