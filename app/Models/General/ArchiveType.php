<?php namespace Canela\CanelaTools\Models\General;

use Canela\CanelaTools\Models\BasicModel;
use DB;

/**
 * Class ArchiveType
 * @package Canela\CanelaTools\Models\General
 *
 * @property string $name
 * @property string $description
 * @property boolean $is_generic
 * @property boolean $is_video
 * @property boolean $is_image
 * @property boolean $is_pdf
 * @property boolean $is_url
 */
class ArchiveType extends BasicModel
{
    const VIDEO     = 1;
    const PDF       = 2;
    const IMAGE     = 3;
    const PODCASTS  = 4;
    const YOUTUBE   = 5;
    const OTHER     = 6;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'archive_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'is_generic',
        'is_video',
        'is_image',
        'is_pdf',
        'is_url',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'name'          => 'string',
        'description'   => 'string',
        'is_generic'    => 'boolean',
        'is_video'      => 'boolean',
        'is_image'      => 'boolean',
        'is_pdf'        => 'boolean',
        'is_url'        => 'boolean',
    ];

    /**
     * @return string
     */
    public static function isTextInputArchiveTypeListJson() {
        return self::select(['id', DB::raw("IF(is_url, 1, 0) as is_text_input")])->get()->toJson();
    }

    /**
     * archiveTypeListJson
     *
     * @return string
     */
    public static function archiveTypeListJson(): string {
        return self::all()->map(function ($archiveType) {
            return [
                'id' => $archiveType->id,
                'name' => $archiveType->name,
                'is_text_input' => $archiveType->is_url,
                'is_file_input' => $archiveType->is_generic || $archiveType->is_video || $archiveType->is_pdf,
                'is_image_cropper_input' => $archiveType->is_image,
            ];
        })->toJson();
    }

    /**
     * Returns if text input is used for editing the archive type.
     *
     * @return boolean
     */
    public function isTextInputArchiveType(): bool {
        return $this->is_url;
    }

    /**
     * Returns if text input is used for editing the archive type.
     *
     * @param integer $id
     * @return boolean
     */
    public static function isTextInputArchiveTypeById(int $id): bool {
        return self::findOrFail($id)->isTextInputArchiveType();
    }

    /**
     * addArchiveRuleDependingOnArchiveType
     *
     * @param integer $typeId
     * @param [type] $archiveRule
     * @return void
     */
    public static function addArchiveRuleDependingOnArchiveType(int $typeId, &$archiveRule): void {
        $archiveType = self::findOrFail($typeId);

        if ($archiveType->isTextInputArchiveType($typeId)) {
            $archiveRule .= '|max:255';
        } else {
            if ($archiveType->is_generic) {
                $archiveRule .= '|file';
            } elseif ($archiveType->is_pdf) {
                $archiveRule .= '|mimetypes:application/pdf';
            } elseif ($archiveType->is_video) {
                $archiveRule .= '|mimetypes:video/x-msvideo';   // .avi
                $archiveRule .= ',video/mp4';                   // .mp4
                $archiveRule .= ',video/mpeg';                  // .mpeg
                $archiveRule .= ',video/ogg';                   // .ogv
                $archiveRule .= ',video/webm';                  // .webm
                $archiveRule .= ',video/3gpp';                  // .3gp
                $archiveRule .= ',video/3gpp2';                 // .3g2
            } elseif ($archiveType->is_image) {
                $archiveRule .= '|image';
            }
        }
    }

}
