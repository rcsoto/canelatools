<?php


namespace Canela\CanelaTools\Enums;


class TableColumnMaxSize
{
    const SIGNED_INT_MIN            = -2147483648;
    const SIGNED_INT_MAX            = 2147483647;
    const UNSIGNED_INT_MAX          = 4294967295;

    const SIGNED_SMALLINT_MIN       = -32768;
    const SIGNED_SMALLINT_MAX       = 32767;
    const UNSIGNED_SMALLINT_MAX     = 65535;

    const TEXT_MAX                  = 65535;
    const MEDIUMTEXT_MAX            = 16777215;
    const LONGTEXT_MAX              = 4294967295;
}
