<div class="page-header-title">
    <h5 class="m-b-10">{{ $title }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="feather icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#">{{ $title }}</a></li>
</ul>

